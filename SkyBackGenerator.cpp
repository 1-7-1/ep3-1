/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "SkyBackGenerator.h"
#include "Room.h"
#include "Decoration.h"
#include "TileCalculator.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "AutoTransition.h"
#include "Wall.h"
#include "Bench.h"
#include "Schoolgirl.h"
#include "SkyFse.h"
#include "Player.h"

SkyBackGenerator::SkyBackGenerator()
{

}

SkyBackGenerator::~SkyBackGenerator()
{

}

void SkyBackGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/tilesets/18.png", sf::Color(63, 139, 255, 255));
    _db.loadTexture("graphics/tilesets/19.png", sf::Color(0, 255, 255, 255));
    _db.loadTexture("graphics/char/11.png", sf::Color(0, 255, 255, 255));
    _db.loadTexture("graphics/events/1.png", sf::Color(63, 139, 255, 255));
    _db.loadTexture("graphics/bg/5.png", sf::Color(0, 0, 0, 255));
    _isLoaded = true;
}

Bench* SkyBackGenerator::generateBench(sf::Vector2f position) const
{
    TileCalculator bgCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(48, 32));

    Animation* anim = new Animation(bgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(bench));

    return new Bench(position, sf::Vector2f(24.f, 16.f), sf::FloatRect(-21.f, 0.f, 42.f, 11.f), anim, 2, 16, 4);
}

Schoolgirl* SkyBackGenerator::generateSchoolgirl(sf::Vector2f position, Player* player) const
{
    TileCalculator girlCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(24, 32));

    std::vector<Animation*> anims;
    anims.push_back(new Animation(girlCalc(std::vector<sf::Vector2u>({sf::Vector2u(3, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(schoolgirl)));
    anims.push_back(new Animation(girlCalc(std::vector<sf::Vector2u>({sf::Vector2u(3, 1)}),
        std::vector<unsigned int>({0})), _db.getTexture(schoolgirl)));
    anims.push_back(new Animation(girlCalc(std::vector<sf::Vector2u>({sf::Vector2u(3, 2)}),
        std::vector<unsigned int>({0})), _db.getTexture(schoolgirl)));
    anims.push_back(new Animation(girlCalc(std::vector<sf::Vector2u>({sf::Vector2u(3, 3)}),
        std::vector<unsigned int>({0})), _db.getTexture(schoolgirl)));
    anims.push_back(new Animation(girlCalc(std::vector<sf::Vector2u>({sf::Vector2u(4, 0), sf::Vector2u(4, 1), sf::Vector2u(4, 2)}),
        std::vector<unsigned int>({12, 12, 12})), _db.getTexture(schoolgirl)));

    return new Schoolgirl(position, sf::Vector2f(12.f, 29.f), sf::FloatRect(-6.f, -2.f, 12.f, 4.f), anims, player);
}

SkyFse* SkyBackGenerator::generateFse() const
{
    TileCalculator calc = TileCalculator(sf::Vector2u(), sf::Vector2u(320, 240));
    std::vector<Decoration*> backs;

    for (unsigned int i = 0; i < 2; i++)
    {
        Animation* anim = new Animation(calc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(fsebg));

        backs.push_back(new Decoration(sf::Vector2f(0.f, 0.f), sf::Vector2f(), sf::Vector2f(-0.1f, 0.f), anim));
    }

    Animation* fseAnim = new Animation(calc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(fse));

    return new SkyFse(backs, fseAnim);
}

Decoration* SkyBackGenerator::generateBackground() const
{
    TileCalculator bgCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(320, 240));

    Animation* anim = new Animation(bgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(bg));

    return new Decoration(sf::Vector2f(0.f, 0.f), sf::Vector2f(), sf::Vector2f(), anim);
}

std::vector<AutoTransition*> SkyBackGenerator::generateTiles(sf::Vector2f position, Player* player) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    std::vector<AutoTransition*> tiles;

    {
        Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(invisible));

        tiles.push_back(new AutoTransition(position + sf::Vector2f(0.f, 160.f), sf::Vector2f(),
            sf::FloatRect(0.f, 0.f, 16.f, 48.f), anim, 17, sf::Vector2f(744.f, 136.f), true, player));
    }

    {
        Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(invisible));

        tiles.push_back(new AutoTransition(position + sf::Vector2f(304.f, 160.f), sf::Vector2f(),
            sf::FloatRect(0.f, 0.f, 16.f, 48.f), anim, 17, sf::Vector2f(536.f, 136.f), true, player));
    }

    return tiles;
}

Wall* SkyBackGenerator::generateWall(sf::Vector2f position, sf::FloatRect hitbox) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(invisible));

    return new Wall(position, sf::Vector2f(), anim, hitbox);
}

std::vector<Wall*> SkyBackGenerator::generateWalls(sf::Vector2f position) const
{
    std::vector<Wall*> walls;

    walls.push_back(generateWall(position + sf::Vector2f(0.f, 144.f),
        sf::FloatRect(0.f, 0.f, 320.f, 16.f)));
    walls.push_back(generateWall(position + sf::Vector2f(0.f, 208.f),
        sf::FloatRect(0.f, 0.f, 320.f, 16.f)));

    return walls;
}

Room* SkyBackGenerator::generateSkyBack(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;
    std::vector<GameObject*> foregrounds;

    backgrounds.push_back(generateBackground());

    std::vector<Wall*> walls = generateWalls(sf::Vector2f());
    collidables.insert(collidables.end(), walls.begin(), walls.end());

    Schoolgirl* girl = generateSchoolgirl(sf::Vector2f(87.f, 167.f), player);
    collidables.push_back(girl);
    interactibles.push_back(girl);

    Bench* newBench = generateBench(sf::Vector2f(96.f, 160.f));
    newBench->occupy(girl);
    collidables.push_back(newBench);
    interactibles.push_back(newBench);
    
    std::vector<AutoTransition*> tiles = generateTiles(sf::Vector2f(), player);
    floors.insert(floors.end(), tiles.begin(), tiles.end());

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, foregrounds, std::vector<GameObject*>() };

    return new Room(player, graph, generateFse(), objects, sf::Vector2f(_roomWidth, _roomHeight),
        sf::Vector2<bool>(false, false));
}
