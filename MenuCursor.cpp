/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "MenuCursor.h"
#include "Animation.h"

MenuCursor::MenuCursor(std::vector<Animation*> animations)
    : GameObject(sf::Vector2f(), sf::Vector2f(0.f, 0.f)), _animations(animations)
{
    _currentAnimation = _animations[0];
}

MenuCursor::~MenuCursor()
{
    for (Animation* anim: _animations)
        delete anim;
}

void MenuCursor::behave()
{
    _currentAnimation->setPosition(_position);
    changeAnimation();
}
