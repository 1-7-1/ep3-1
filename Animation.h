/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ANIMATION_H
#define ANIMATION_H

#include "SFML/Graphics.hpp"
#include <vector>

struct AnimationFrame;

class Animation: public sf::Drawable, public sf::Transformable
{
    private:
        std::vector<AnimationFrame> _frames;

        sf::Texture* _spriteSheet;
        sf::VertexArray _vertices;

        unsigned int _elapsedTicks;
        unsigned int _frameIndex;
        unsigned int _numberOfFrames;
        bool _looping;

        void setVertices();

    public:
        Animation();
        Animation(std::vector<AnimationFrame> frames, sf::Texture* spriteSheet, unsigned int frameIndex = 0);
        ~Animation();

        unsigned int getNumberOfFrames() const;
        unsigned int getFrameIndex() const;
        unsigned int getElapsedTicks() const;
        std::vector<AnimationFrame> getFrames() const;
        sf::Vector2u getSize() const;

        void setFrameIndex(unsigned int index);
        void setElapsedTicks(unsigned int ticks);
        void setLooping(bool looping);
        void setColor(sf::Color color);
        void setTransparency(sf::Uint8 alpha);

        bool isFinished();
        void animate();
        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif
