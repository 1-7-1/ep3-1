/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GRID_H
#define GRID_H

#include "SFML/Graphics.hpp"
#include <vector>

class CollidableObject;
class Entity;
class Room;
class GridNode;
class GameObject;

class Grid
{
    friend class Room;
    private:
        std::vector<std::vector<std::vector<CollidableObject*>>> _grid;
        Room* _room;
        int _gridSize;
        sf::Vector2f _gridDimensions;

        static constexpr float _marginOfError = 4.f;

    public:
        Grid(Room* room, int gridSize);
        ~Grid();

        std::vector<std::vector<std::vector<CollidableObject*>>> getGrid() const;

        void updateGrid(CollidableObject* obj);
        void removeObject(GameObject* obj);
};

#endif
