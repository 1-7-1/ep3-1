/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "JungleGenerator.h"

std::vector<Wall*> JungleGenerator::generateMazeALeftArea() const
{
    std::vector<Wall*> walls;
    walls.push_back(generateWall(sf::Vector2f(2464.f, 3904.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 3904.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 3904.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 3904.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 3904.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 3888.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 3872.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 3856.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 3840.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 3824.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 3808.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 3808.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 3792.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 3792.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 3792.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 3792.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 3776.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 3760.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 3744.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 3728.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 3712.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 3712.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 3696.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 3696.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 3680.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 3680.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2544.f, 3680.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2560.f, 3680.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3680.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3664.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3648.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3632.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3616.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3600.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 3600.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 3584.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 3568.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2608.f, 3568.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 3568.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 3568.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 3584.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 3584.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 3600.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 3600.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 3616.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 3632.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 3632.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 3648.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 3664.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 3664.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 3680.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 3696.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 3712.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 3728.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 3744.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 3760.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 3760.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 3776.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 3792.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 3808.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 3824.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 3840.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 3840.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 3856.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 3872.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2720.f, 3872.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2720.f, 3888.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2720.f, 3904.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2736.f, 3904.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2736.f, 3920.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2736.f, 3936.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2752.f, 3936.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2752.f, 3952.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2768.f, 3952.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2784.f, 3952.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 3952.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2816.f, 3952.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2816.f, 3936.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2832.f, 3936.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2848.f, 3936.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2848.f, 3920.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 3920.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 3920.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2896.f, 3920.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2896.f, 3904.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2912.f, 3904.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2912.f, 3888.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2928.f, 3888.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2928.f, 3872.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2928.f, 3856.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2928.f, 3840.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2928.f, 3824.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2928.f, 3808.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2944.f, 3808.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2944.f, 3792.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2960.f, 3792.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2976.f, 3792.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2992.f, 3792.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3008.f, 3792.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3008.f, 3808.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3024.f, 3808.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3024.f, 3824.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3024.f, 3840.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3024.f, 3856.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3024.f, 3872.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3024.f, 3888.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3040.f, 3888.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3040.f, 3904.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3056.f, 3904.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3072.f, 3904.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3088.f, 3904.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3104.f, 3904.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3120.f, 3904.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3136.f, 3904.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3152.f, 3904.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3168.f, 3904.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3168.f, 3888.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3184.f, 3888.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3200.f, 3888.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3200.f, 3872.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3216.f, 3872.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3216.f, 3856.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3216.f, 3840.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3216.f, 3824.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3216.f, 3808.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3200.f, 3808.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3200.f, 3792.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3200.f, 3776.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3200.f, 3760.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3216.f, 3760.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3216.f, 3744.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3232.f, 3744.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3248.f, 3744.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3264.f, 3744.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3280.f, 3744.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3280.f, 3760.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3296.f, 3760.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3312.f, 3760.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3328.f, 3760.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3344.f, 3760.f), down, false));
    
    walls.push_back(generateWall(sf::Vector2f(3344.f, 3808.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3328.f, 3808.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3312.f, 3808.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3296.f, 3808.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3280.f, 3808.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3280.f, 3824.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3264.f, 3824.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3264.f, 3840.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3264.f, 3856.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3264.f, 3872.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3264.f, 3888.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3280.f, 3888.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3280.f, 3904.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3280.f, 3920.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3280.f, 3936.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3264.f, 3936.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3264.f, 3952.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3248.f, 3952.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3232.f, 3952.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3216.f, 3952.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3200.f, 3952.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3200.f, 3936.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3184.f, 3936.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3168.f, 3936.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3152.f, 3936.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3136.f, 3936.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3120.f, 3936.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3104.f, 3936.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3088.f, 3936.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3072.f, 3936.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3056.f, 3936.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3040.f, 3936.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3040.f, 3952.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3024.f, 3952.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3008.f, 3952.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2992.f, 3952.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2992.f, 3968.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2976.f, 3968.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2960.f, 3968.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2960.f, 3984.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2944.f, 3984.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2944.f, 4000.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2944.f, 4000.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2928.f, 4000.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2912.f, 4000.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2896.f, 4000.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 4000.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 4000.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2848.f, 4000.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2832.f, 4000.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2816.f, 4000.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 4000.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2784.f, 4000.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2768.f, 4000.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2768.f, 4016.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2752.f, 4016.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2736.f, 4016.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2720.f, 4016.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 4016.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 4032.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 4032.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 4048.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 4048.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 4064.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 4064.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 4080.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 4080.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 4096.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 4096.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 4112.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2608.f, 4112.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2608.f, 4128.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 4128.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 4128.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 4144.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2560.f, 4144.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2560.f, 4160.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2544.f, 4160.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 4160.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 4160.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 4160.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 4176.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 4176.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 4192.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2464.f, 4192.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2448.f, 4192.f), up, false));

    walls.push_back(generateWall(sf::Vector2f(2448.f, 4128.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2464.f, 4128.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 4128.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 4128.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 4128.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 4128.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 4112.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2544.f, 4112.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2544.f, 4096.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2560.f, 4096.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2560.f, 4080.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 4080.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 4064.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 4064.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 4048.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2608.f, 4048.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2608.f, 4032.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 4032.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 4016.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 4016.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 4000.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 4000.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 3984.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 3968.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 3952.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 3936.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 3920.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 3904.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 3888.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 3888.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 3872.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 3856.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 3840.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 3824.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 3808.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 3792.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 3776.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 3760.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 3744.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 3744.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 3728.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 3712.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 3696.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 3680.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 3680.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 3664.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 3648.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 3648.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 3664.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 3680.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 3696.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 3712.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 3728.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2608.f, 3728.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 3728.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3728.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3744.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2560.f, 3744.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2544.f, 3744.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 3744.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 3760.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2544.f, 3760.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2560.f, 3760.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2560.f, 3776.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3776.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3792.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3808.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3824.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3840.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3856.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3872.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3888.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3904.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3920.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3936.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3952.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2560.f, 3952.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2544.f, 3952.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 3952.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 3952.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 3952.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 3952.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2464.f, 3952.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2448.f, 3952.f), up, false));

    walls.push_back(generateWall(sf::Vector2f(2992.f, 3904.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2992.f, 3888.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2992.f, 3872.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2992.f, 3856.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2992.f, 3840.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2976.f, 3840.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2960.f, 3840.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2960.f, 3840.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2960.f, 3856.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2960.f, 3872.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2960.f, 3888.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2960.f, 3904.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2960.f, 3920.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2976.f, 3920.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2992.f, 3920.f), downright, false));

    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 2; i++)
            walls.push_back(generateWall(sf::Vector2f(2464.f + i * 32.f, 3824.f + j * 32.f), turnupright, true));

    for (int j = 0; j < 4; j++)
        for (int i = 0; i < 6; i++)
            walls.push_back(generateWall(sf::Vector2f(2288.f + i * 32.f, 3584.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 4; j++)
        for (int i = 0; i < 3; i++)
            walls.push_back(generateWall(sf::Vector2f(2480.f + i * 32.f, 3552.f + j * 32.f), turnupright, true));
            
    for (int j = 0; j < 9; j++)
        for (int i = 0; i < 6; i++)
            walls.push_back(generateWall(sf::Vector2f(2720.f + i * 32.f, 3584.f + j * 32.f), turnupright, true));
    for (int i = 0; i < 5; i++)
        walls.push_back(generateWall(sf::Vector2f(2736.f + i * 32.f, 3872.f), turnupright, true));
    for (int i = 0; i < 3; i++)
        walls.push_back(generateWall(sf::Vector2f(2752.f + i * 32.f, 3904.f), turnupright, true));
    for (int j = 0; j < 4; j++)
        for (int i = 0; i < 4; i++)
            walls.push_back(generateWall(sf::Vector2f(2912.f + i * 32.f, 3664.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 7; j++)
        for (int i = 0; i < 5; i++)
            walls.push_back(generateWall(sf::Vector2f(3040.f + i * 32.f, 3664.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 6; i++)
            walls.push_back(generateWall(sf::Vector2f(3168.f + i * 32.f, 3664.f + j * 32.f), turnupright, true));
            
    for (int j = 0; j < 9; j++)
        for (int i = 0; i < 2; i++)
            walls.push_back(generateWall(sf::Vector2f(3296.f + i * 32.f, 3824.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 4; j++)
        for (int i = 0; i < 9; i++)
            walls.push_back(generateWall(sf::Vector2f(3008.f + i * 32.f, 3968.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 5; j++)
        walls.push_back(generateWall(sf::Vector2f(2976.f, 3984.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 8; j++)
        for (int i = 0; i < 8; i++)
            walls.push_back(generateWall(sf::Vector2f(2720.f + i * 32.f, 4032.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 8; j++)
        for (int i = 0; i < 2; i++)
            walls.push_back(generateWall(sf::Vector2f(2656.f + i * 32.f, 4096.f + j * 32.f), turnupright, true));
    for (int i = 0; i < 2; i++)
        walls.push_back(generateWall(sf::Vector2f(2592.f + i * 32.f, 4144.f), turnupright, true));
    for (int i = 0; i < 2; i++)
        walls.push_back(generateWall(sf::Vector2f(2512.f + i * 32.f, 4192.f), turnupright, true));
    for (int j = 0; j < 8; j++)
        for (int i = 0; i < 8; i++)
            walls.push_back(generateWall(sf::Vector2f(2464.f + i * 32.f, 4208.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 6; j++)
        walls.push_back(generateWall(sf::Vector2f(2592.f, 3744.f + j * 32.f), turnupright, true));

    for (int i = 0; i < 6; i++)
        walls.push_back(generateWall(sf::Vector2f(2464.f + i * 32.f, 3968.f), turnupright, true));
    for (int i = 0; i < 5; i++)
        walls.push_back(generateWall(sf::Vector2f(2464.f + i * 32.f, 4000.f), turnupright, true));
    for (int i = 0; i < 4; i++)
        walls.push_back(generateWall(sf::Vector2f(2464.f + i * 32.f, 4032.f), turnupright, true));
    for (int i = 0; i < 3; i++)
        walls.push_back(generateWall(sf::Vector2f(2464.f + i * 32.f, 4064.f), turnupright, true));
    for (int i = 0; i < 2; i++)
        walls.push_back(generateWall(sf::Vector2f(2464.f + i * 32.f, 4096.f), turnupright, true));

    std::vector<Wall*> decorations;
    
    decorations = generateBigTree(sf::Vector2f(2907.f, 3952.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateTallTree(sf::Vector2f(2469.f, 4179.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(2734.f, 3962.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(2669.f, 3885.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(2697.f, 3685.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(2696.f, 3570.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(2569.f, 3871.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3243.f, 3818.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateSmallTree(sf::Vector2f(2514.f, 4146.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3273.f, 3784.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(2617.f, 3725.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(2627.f, 3612.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(2692.f, 3981.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(2761.f, 3730.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(2854.f, 3967.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(2986.f, 3967.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3257.f, 3942.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateVerySmallTree(sf::Vector2f(2617.f, 4109.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3204.f, 3914.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2512.f, 3789.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2401.f, 3922.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2541.f, 3711.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2564.f, 3741.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2553.f, 3784.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2681.f, 3821.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2666.f, 4027.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2698.f, 3933.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateBigRock(sf::Vector2f(3234.f, 3794.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(2993.f, 3822.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(2563.f, 3706.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateSmallRock(sf::Vector2f(3256.f, 3867.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3271.f, 3916.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3149.f, 3922.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3033.f, 3914.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(2931.f, 3935.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(2811.f, 3985.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(2662.f, 3770.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(2682.f, 3719.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(2616.f, 3596.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(2550.f, 3822.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(2564.f, 3911.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(2640.f, 4054.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateWeirdPlant(sf::Vector2f(2488.f, 4167.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(2705.f, 3919.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(2680.f, 3841.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(2553.f, 3888.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(2969.f, 3817.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3049.f, 3922.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateBigFern(sf::Vector2f(2681.f, 4045.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2729.f, 4009.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2767.f, 3977.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2685.f, 3962.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2681.f, 3655.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2662.f, 3674.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2636.f, 3645.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2526.f, 3721.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2505.f, 3769.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2824.f, 3873.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2874.f, 3986.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2938.f, 3947.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3222.f, 3911.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3238.f, 3846.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3278.f, 3792.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3322.f, 3749.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    decorations = generateSmallFern(sf::Vector2f(3251.f, 3771.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2552.f, 4135.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2607.f, 4085.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2695.f, 4025.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2724.f, 3972.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2690.f, 3870.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2679.f, 3743.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2659.f, 3622.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2603.f, 3626.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2564.f, 3810.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2508.f, 3922.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2838.f, 3960.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2934.f, 3982.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3251.f, 3908.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3325.f, 3785.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    return walls;
}
