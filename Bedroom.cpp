/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Bedroom.h"
#include "Player.h"
#include "RoomGraph.h"

Bedroom::Bedroom(Player* player, RoomGraph* graph,
    std::vector<CollidableObject*> walls, std::vector<GameObject*> floors, std::vector<InteractibleObject*> interactibles,
    sf::Sound *sound)
    : Room(player, graph, nullptr,
    {interactibles, walls, std::vector<Entity*>({player}), floors, std::vector<GameObject*>(), std::vector<GameObject*>(),
    std::vector<GameObject*>()}, sf::Vector2f(320.f, 240.f), sf::Vector2<bool>(false, false)),
    _wakingUp(_graph->isWakingUp()), _waitTimer(180), _sound(sound)
{
    if (_wakingUp)
    {
        _graph->setWakingUp(false);
        _player->endScript(wakeup);
        _sound->play();
    }
}

Bedroom::~Bedroom()
{

}

void Bedroom::tick()
{
    if (_waitTimer > 0 && _wakingUp)
        _waitTimer--;
    else
        Room::tick();
}
