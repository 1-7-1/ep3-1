/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WAIT_ROOM_H
#define WAIT_ROOM_H

#include "Room.h"
#include "SFML/Audio.hpp"

class WaitingRoom: public Room
{
    protected:
        unsigned int _waitTimer;
        sf::Sound* _sound;
        
    public:
        WaitingRoom(Player* player, RoomGraph* graph, FullScreenEvent* fse, RoomObjects objects,
            sf::Vector2f size, sf::Vector2<bool> looping, sf::Sound* sound, unsigned int waitTimer);
        virtual ~WaitingRoom();

        virtual void tick();
};

#endif
