/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SPIKE_ROOM_H
#define SPIKE_ROOM_H

#include "Room.h"

class Waterfall;

class SpikeRoom: public Room
{
    private:
        unsigned int _waitTimer;
        sf::Sound* _sound;

        std::vector<Waterfall*> _waterfalls;
        int _waterfallCounter;

        int findWaterfall(std::vector<Waterfall*> vec, GameObject* object);
        //virtual void destroy();
            
    public:
        SpikeRoom(Player* player, RoomGraph* graph, RoomObjects objects, std::vector<Waterfall*> waterfalls);
        ~SpikeRoom();

        virtual void tick();
};

#endif
