/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "JungleGenerator.h"

std::vector<Wall*> JungleGenerator::generateStartingArea() const
{
    std::vector<Wall*> walls;
    walls.push_back(generateWall(sf::Vector2f(2496.f, 2560.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 2560.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2464.f, 2560.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2464.f, 2576.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2464.f, 2592.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2448.f, 2592.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2448.f, 2608.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(2432.f, 2608.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2432.f, 2624.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2432.f, 2640.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2432.f, 2656.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2416.f, 2656.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2416.f, 2672.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(2400.f, 2672.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2384.f, 2672.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2368.f, 2672.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2352.f, 2672.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2336.f, 2672.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2336.f, 2656.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2336.f, 2640.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2336.f, 2624.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2320.f, 2624.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(2320.f, 2608.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2304.f, 2608.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2288.f, 2608.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2272.f, 2608.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(2272.f, 2592.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2272.f, 2576.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2272.f, 2560.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2272.f, 2544.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2272.f, 2528.f), left, false));

    walls.push_back(generateWall(sf::Vector2f(2624.f, 2528.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 2544.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 2560.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 2576.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 2576.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 2576.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 2576.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 2592.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 2592.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 2608.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 2624.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 2640.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 2656.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 2672.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 2688.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 2704.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 2720.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 2736.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 2736.f), upleft, true));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 2752.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 2752.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 2768.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 2768.f), upleft, true));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 2784.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 2784.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2608.f, 2784.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 2784.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 2784.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2560.f, 2784.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2544.f, 2784.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 2784.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 2784.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 2768.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 2768.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 2752.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 2736.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 2720.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 2720.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 2704.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 2688.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2464.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2448.f, 2688.f), upleft, true));
    walls.push_back(generateWall(sf::Vector2f(2448.f, 2704.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2448.f, 2720.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2448.f, 2736.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2432.f, 2736.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2416.f, 2736.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2400.f, 2736.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2384.f, 2736.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2384.f, 2720.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2384.f, 2704.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(2368.f, 2704.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2352.f, 2704.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2336.f, 2704.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2320.f, 2704.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2304.f, 2704.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2288.f, 2704.f), upleft, true));
    walls.push_back(generateWall(sf::Vector2f(2288.f, 2720.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2272.f, 2720.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2256.f, 2720.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2240.f, 2720.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2224.f, 2720.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2224.f, 2704.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 2704.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 2688.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 2672.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2192.f, 2672.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2192.f, 2656.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2192.f, 2640.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2192.f, 2624.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2192.f, 2608.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 2608.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 2592.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2224.f, 2592.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2224.f, 2576.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2224.f, 2560.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2224.f, 2544.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2224.f, 2528.f), right, false));

    for (int i = 0; i < 6; i++)
        walls.push_back(generateWall(sf::Vector2f(2288.f + i * 32.f, 2528.f), turnupright, true));
    for (int i = 0; i < 5; i++)
        walls.push_back(generateWall(sf::Vector2f(2304.f + i * 32.f, 2560.f), turnupright, true));
    for (int i = 0; i < 2; i++)
        walls.push_back(generateWall(sf::Vector2f(2352.f + i * 32.f, 2592.f), turnupright, true));
    for (int i = 0; i < 2; i++)
        walls.push_back(generateWall(sf::Vector2f(2352.f + i * 32.f, 2640.f), turnupright, true));

    for (int i = 0; i < 3; i++)
        walls.push_back(generateWall(sf::Vector2f(2640.f + i * 32.f, 2544.f), turnupright, true));
    for (int j = 0; j < 6; j++)
        for (int i = 0; i < 1; i++)
            walls.push_back(generateWall(sf::Vector2f(2704.f + i * 32.f, 2576.f + j * 32.f), turnupright, true));
    for (int i = 0; i < 2; i++)
        walls.push_back(generateWall(sf::Vector2f(2672.f + i * 32.f, 2768.f), turnupright, true));
    for (int i = 0; i < 19; i++)
        walls.push_back(generateWall(sf::Vector2f(2144.f + i * 32.f, 2800.f), turnupright, true));
    for (int i = 0; i < 11; i++)
        walls.push_back(generateWall(sf::Vector2f(2144.f + i * 32.f, 2778.f), turnupright, true));
    for (int i = 0; i < 7; i++)
        walls.push_back(generateWall(sf::Vector2f(2144.f + i * 32.f, 2736.f), turnupright, true));
    for (int i = 0; i < 2; i++)
        walls.push_back(generateWall(sf::Vector2f(2144.f + i * 32.f, 2688.f), turnupright, true));
    for (int i = 0; i < 5; i++)
        walls.push_back(generateWall(sf::Vector2f(2144.f, 2528.f + i * 32.f), turnupright, true));
    for (int i = 0; i < 3; i++)
        walls.push_back(generateWall(sf::Vector2f(2176.f, 2528.f + i * 32.f), turnupright, true));

    std::vector<Wall*> decorations;

    decorations = generateBigFern(sf::Vector2f(2541.f, 2715.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2604.f, 2686.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2508.f, 2634.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2578.f, 2656.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2596.f, 2590.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2507.f, 2672.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2457.f, 2685.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2434.f, 2729.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2247.f, 2590.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2265.f, 2577.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateSmallFern(sf::Vector2f(2518.f, 2660.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2563.f, 2719.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2606.f, 2755.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2561.f, 2725.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2626.f, 2656.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2415.f, 2729.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2362.f, 2701.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    decorations = generateTallTree(sf::Vector2f(2423.f, 2722.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(2639.f, 2678.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(2534.f, 2753.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(2237.f, 2641.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateBigTree(sf::Vector2f(2582.f, 2623.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    decorations = generateSmallTree(sf::Vector2f(2617.f, 2760.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(2526.f, 2600.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    decorations = generateVerySmallTree(sf::Vector2f(2457.f, 2636.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2243.f, 2679.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2305.f, 2684.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2286.f, 2646.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2561.f, 2691.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    return walls;
}