/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "View.h"
#include "Room.h"
#include "GameObject.h"
#include "Animation.h"
#include <math.h>

View::View()
{
    _position = sf::Vector2f(0.f, 0.f);
    _size = sf::Vector2f(0.f, 0.f);
}

View::View(GameObject* player, Room* room): _player(player), _room(room)
{
    _position = _player->getCornerPosition() + _player->getSize() / 2.f - _size / 2.f;
}

View::~View()
{
    
}

bool View::isInView(const GameObject* object) const
{
    bool goodX = (object->getCornerPosition().x + object->getSize().x >= _position.x - 1.f) &&
        (object->getCornerPosition().x <= _position.x + _size.x + 1.f);
    bool goodY = (object->getCornerPosition().y + object->getSize().y >= _position.y - 1.f) &&
        (object->getCornerPosition().y <= _position.y + _size.y + 1.f);
    if (_room->_looping.x)
    {
        goodX |= (_room->_size.x + object->getCornerPosition().x + object->getSize().x >= _position.x - 1.f) &&
        (_room->_size.x + object->getCornerPosition().x <= _position.x + _size.x + 1.f);
        goodX |= (-_room->_size.x + object->getCornerPosition().x + object->getSize().x >= _position.x - 1.f) &&
        (-_room->_size.x + object->getCornerPosition().x <= _position.x + _size.x + 1.f);
    }
    if (_room->_looping.y)
    {
        goodY |= (_room->_size.y + object->getCornerPosition().y + object->getSize().y >= _position.y - 1.f) &&
        (_room->_size.y + object->getCornerPosition().y <= _position.y + _size.y + 1.f);
        goodY |= (-_room->_size.y + object->getCornerPosition().y + object->getSize().y >= _position.y - 1.f) &&
        (-_room->_size.y + object->getCornerPosition().y <= _position.y + _size.y + 1.f);
    }
    return goodX && goodY;
}

void View::setObjectPositions(const std::vector<GameObject*>& vec)
{
    for (GameObject* obj: vec)
    {
        sf::Vector2f animationPosition = obj->getPosition() - _position;
        if (_room->_looping.x)
        {
            sf::Vector2f objectPosition = obj->getCornerPosition();
            sf::Vector2f objectSize = obj->getSize();
            if (objectPosition.x + objectSize.x < _position.x)
                animationPosition.x = _room->_size.x - _position.x + obj->getPosition().x;
            else if (objectPosition.x > _position.x + _size.x)
                animationPosition.x = -_room->_size.x - _position.x + obj->getPosition().x;
        }
        if (_room->_looping.y)
        {
            sf::Vector2f objectPosition = obj->getCornerPosition();
            sf::Vector2f objectSize = obj->getSize();
            if (objectPosition.y + objectSize.y < _position.y)
                animationPosition.y = _room->_size.y - _position.y + obj->getPosition().y;
            else if (objectPosition.y > _position.y + _size.y)
                animationPosition.y = -_room->_size.y - _position.y + obj->getPosition().y;
        }

        obj->getAnimation()->setPosition(animationPosition);
    }
}

void View::tick()
{
    _position = _player->getCornerPosition() + _player->getSize() / 2.f - _size / 2.f;

    if (_room->_looping.x)
    {
        if (_position.x >= _room->_size.x)
            _position.x -= _room->_size.x;
        else if (_position.x < 0.f)
            _position.x += _room->_size.x;
    }
    else
    {
        if (_position.x < 0.f)
            _position.x = 0.f;
        else if (_position.x + _size.x >= _room->_size.x)
            _position.x = _room->_size.x - _size.x;
    }
    if (_room->_looping.y)
    {
        if (_position.y >= _room->_size.y)
            _position.y -= _room->_size.y;
        else if (_position.y < 0.f)
            _position.y += _room->_size.y;
    }
    else
    {
        if (_position.y < 0.f)
            _position.y = 0.f;
        else if (_position.y + _size.y >= _room->_size.y)
            _position.y = _room->_size.y - _size.y;
    }
    
    setObjectPositions(_objectList);
    setObjectPositions(_backgroundObjects);
    setObjectPositions(_foregroundObjects);
    setObjectPositions(_floorTiles);
}

void View::merge(unsigned int begin, unsigned int half, unsigned int end)
{
    std::vector<GameObject*> vec;
    unsigned int leftIndex = begin;
    unsigned int rightIndex = half + 1;
    for (unsigned int i = begin; i <= end; i++)
    {
        if (leftIndex <= half && (rightIndex > end || _objectList[leftIndex]->getAnimation()->getPosition().y < _objectList[rightIndex]->getAnimation()->getPosition().y))
        {
            vec.push_back(_objectList[leftIndex]);
            leftIndex++;
        }
        else
        {
            vec.push_back(_objectList[rightIndex]);
            rightIndex++;
        }
    }
    for (unsigned int i = 0; i < vec.size(); i++)
    {
        _objectList[begin + i] = vec[i];
    }
    return;
}

void View::mergeSort(unsigned int begin, unsigned int end)
{
    if (end - begin < 1)
        return;

    unsigned int half = (begin + end) / 2;
    mergeSort(begin, half);
    mergeSort(half + 1, end);
    merge(begin, half, end);
    return;
}

void View::sort()
{
    if (_objectList.size() >= 1)
        mergeSort(0, _objectList.size() - 1);
}

sf::Vector2f View::getPosition() const
{
    return _position;
}

sf::Vector2f View::getSize() const
{
    return _size;
}

std::vector<GameObject*> View::getFloorTiles() const
{
    return _floorTiles;
}

std::vector<GameObject*> View::getBackgrounds() const
{
    return _backgroundObjects;
}

std::vector<GameObject*> View::getForegrounds() const
{
    return _foregroundObjects;
}

std::vector<GameObject*> View::getGraphics() const
{
    return _objectList;
}

void View::clearGraphics()
{
    _floorTiles.clear();
    _backgroundObjects.clear();
    _foregroundObjects.clear();
    _objectList.clear();
}

void View::addFloorTile(GameObject* object)
{
    if (isInView(object))
        _floorTiles.push_back(object);
}

void View::addBackgroundObject(GameObject* object)
{
    if (isInView(object))
        _backgroundObjects.push_back(object);
}

void View::addForegroundObject(GameObject* object)
{
    if (isInView(object))
        _foregroundObjects.push_back(object);
}

void View::addEntity(GameObject* object)
{
    if (isInView(object))
        _objectList.push_back(object);
}
