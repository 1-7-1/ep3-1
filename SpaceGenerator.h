/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SPACE_GEN_H
#define SPACE_GEN_H

#include "GameObjectGenerator.h"

class Room;
class Player;
class RoomGraph;
class Decoration;
class Wall;
class AutoTransition;
class Post;

class SpaceGenerator: public GameObjectGenerator
{
    private:
        enum Graphics { invisible, tileset, bg };
        enum FloorType { vtopwall, vtop, vbottomwall, vbottom, vmiddle, hleftwall, hleft, hrightwall, hright, hmiddle,
            diagleft, diagright, inter, upleft, upright, downleft, downright, grid, gridcorner };
        virtual void load();
        
        const float _roomWidth = 2560.f;
        const float _roomHeight = 1920.f;

        std::vector<Decoration*> generateBackground() const;

        Post* generatePost(sf::Vector2f position) const;

        Decoration* generateFloorTile(sf::Vector2f position, FloorType type) const;
        std::vector<Wall*> generateFloorWalls(sf::Vector2f position, FloorType type) const;
        Wall* generateWall(sf::Vector2f position, sf::FloatRect hitbox) const;

        AutoTransition* generateTile(Player* player) const;

    public:
        SpaceGenerator();
        ~SpaceGenerator();

        Room* generateSpace(Player* player, RoomGraph* graph);
};

#endif
