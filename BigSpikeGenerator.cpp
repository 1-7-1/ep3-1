/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "BigSpikeGenerator.h"
#include "Room.h"
#include "Wall.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "TileCalculator.h"
#include "Decoration.h"
#include "AutoTransition.h"
#include "Stairs.h"
#include "FseTrigger.h"
#include "SpikeFse.h"
#include "Player.h"
#include "MathConstants.h"
#include <math.h>

BigSpikeGenerator::BigSpikeGenerator()
{

}

BigSpikeGenerator::~BigSpikeGenerator()
{

}

void BigSpikeGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/tilesets/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/events/3.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/events/4.png", sf::Color(0, 0, 0, 255));
    _isLoaded = true;
}

AutoTransition* BigSpikeGenerator::generateTile(const sf::Vector2f position, const unsigned int destination,
    const sf::Vector2f transitionVec, const bool absolute, Player* _player) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}), std::vector<unsigned int>({0})),
        _db.getTexture(spikeInvisible));

    return new AutoTransition(position, sf::Vector2f(0.f, 0.f), sf::FloatRect(0.f, 0.f, 16.f, 16.f),
        anim, destination, transitionVec, absolute, _player);
}

Animation* BigSpikeGenerator::generateBigSpikeFloorAnimation() const
{
    TileCalculator floorCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(576, 688));

    std::vector<sf::Vector2u> offsets = { sf::Vector2u(0, 0) };

    return new Animation(floorCalc(offsets, std::vector<unsigned int>( { 0 } )),
        _db.getTexture(bigSpike));
}

Decoration* BigSpikeGenerator::generateBigSpikeFloor(sf::Vector2f position) const
{
    return new Decoration(position, sf::Vector2f(272.f, 544.f), sf::Vector2f(), generateBigSpikeFloorAnimation());
}

std::vector<Animation*> BigSpikeGenerator::generateBigSpikeFloorAnimations() const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    std::vector<Animation*> anims;

    for (int i = 0 ; i < 60; i++)
    {
        anims.push_back(new Animation(wallCalc(std::vector<sf::Vector2u>( { sf::Vector2u(0, 0) } ),
            std::vector<unsigned int>( { 0 } )),
            _db.getTexture(spikeInvisible)));
    }

    return anims;
}

std::vector<Wall*> BigSpikeGenerator::generateBigSpikeWalls(sf::Vector2f position) const
{
    std::vector<Animation*> anims = generateBigSpikeFloorAnimations();
    std::vector<Wall*> walls;
    for (unsigned int i = 0; i < 30; i++)
        walls.push_back(new Wall(position + sf::Vector2f(-272.f + 16.f * i, -544.f + 16.f * i), sf::Vector2f(), anims[i], sf::FloatRect(0.f, 0.f, 16.f, 16.f)));
    for (unsigned int i = 0; i < 30; i++)
        walls.push_back(new Wall(position + sf::Vector2f(-272.f + 16.f * i, -480.f + 16.f * i), sf::Vector2f(), anims[i + 30], sf::FloatRect(0.f, 0.f, 16.f, 16.f)));

    return walls;
}

Stairs* BigSpikeGenerator::generateStairs(const sf::Vector2f position, const sf::Vector2f rightVec, Player* _player) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}), std::vector<unsigned int>({0})),
        _db.getTexture(spikeInvisible));

    return new Stairs(position, sf::Vector2f(0.f, 0.f), sf::FloatRect(0.f, 0.f, 16.f, 16.f),
        anim, _player, rightVec);
}

FseTrigger* BigSpikeGenerator::generateFseTrigger(const sf::Vector2f position, Player* _player) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}), std::vector<unsigned int>({0})),
        _db.getTexture(spikeInvisible));

    return new FseTrigger(position, sf::Vector2f(0.f, 0.f), sf::FloatRect(0.f, 0.f, 16.f, 16.f),
        anim, _player);
}

Animation* BigSpikeGenerator::generateSpikeFseSkyAnimation() const
{
    TileCalculator floorCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(320, 480));

    std::vector<sf::Vector2u> offsets = { sf::Vector2u(0, 0) };

    return new Animation(floorCalc(offsets, std::vector<unsigned int>( { 0 } )),
        _db.getTexture(spikeFseSky));
}

Animation* BigSpikeGenerator::generateSpikeFseAnimation() const
{
    TileCalculator floorCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(320, 480));

    std::vector<sf::Vector2u> offsets = { sf::Vector2u(0, 0) };

    return new Animation(floorCalc(offsets, std::vector<unsigned int>( { 0 } )),
        _db.getTexture(spikeFse));
}

SpikeFse* BigSpikeGenerator::generateSpikeFse() const
{
    Decoration* sky1 = new Decoration(sf::Vector2f(-160.f, 0.f), sf::Vector2f(), sf::Vector2f(-0.03f, 0.f),
        generateSpikeFseSkyAnimation());
    Decoration* sky2 = new Decoration(sf::Vector2f(160.f, 0.f), sf::Vector2f(), sf::Vector2f(-0.03f, 0.f),
        generateSpikeFseSkyAnimation());
    return new SpikeFse(std::vector<Decoration*>({ sky1, sky2 }), generateSpikeFseAnimation());
}

Room* BigSpikeGenerator::generateBigSpikeArea(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;

    floors.push_back(generateBigSpikeFloor(sf::Vector2f(500.f, 656.f)));

    std::vector<Wall*> bigSpikeWalls = generateBigSpikeWalls(sf::Vector2f(500.f, 656.f));
    collidables.insert(collidables.end(), bigSpikeWalls.begin(), bigSpikeWalls.end());

    for (unsigned int j = 0; j < 3; j++)
        for (unsigned int i = 0; i < 30; i++)
            floors.push_back(generateStairs(sf::Vector2f(228.f + 16.f * i, 128.f + 16.f * j + 16.f * i),
                sf::Vector2f(1.f, 1.f), player));

    floors.push_back(generateTile(sf::Vector2f(624.f, 528.f), 4,
        sf::Vector2f(1212.f, 888.f), true, player));
    floors.push_back(generateTile(sf::Vector2f(624.f, 544.f), 4,
        sf::Vector2f(1212.f, 888.f), true, player));

    floors.push_back(generateFseTrigger(sf::Vector2f(260, 160.f), player));
    floors.push_back(generateFseTrigger(sf::Vector2f(260, 176.f), player));
    floors.push_back(generateFseTrigger(sf::Vector2f(260, 192.f), player));

    SpikeFse* fse = generateSpikeFse();

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, std::vector<GameObject*>(), std::vector<GameObject*>() };

    return new Room(player, graph, fse, objects, sf::Vector2f(640.f, 640.f), sf::Vector2<bool>(false, false));
}
