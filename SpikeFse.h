/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SPIKE_FSE_H
#define SPIKE_FSE_H

#include "FullScreenEvent.h"

class SpikeFse: public FullScreenEvent
{
    private:
        sf::Vector2f _relativePosition;
        
    public:
        SpikeFse(std::vector<Decoration*> elems, Animation* anim);
        ~SpikeFse();

        virtual void behave();
        virtual void activate();
};

#endif
