/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "BedroomGenerator.h"
#include "TileCalculator.h"
#include "AnimationFrames.h"
#include "Animation.h"
#include "Wall.h"
#include "Desk.h"
#include "Bed.h"
#include "Decoration.h"
#include "Bedroom.h"
#include "AutoTransition.h"
#include "RoomGraph.h"

BedroomGenerator::BedroomGenerator()
{

}

BedroomGenerator::~BedroomGenerator()
{

}

void BedroomGenerator::load()
{
    _db.loadTexture("graphics/tilesets/16.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/object/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));

    _db.loadSound("sounds/sfx/3.wav");
    _isLoaded = true;
}

std::vector<Animation*> BedroomGenerator::generateDiagBedroomWallAnimations() const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    std::vector<sf::Vector2u> offsets;
    offsets.push_back(sf::Vector2u(3, 5));
    for (unsigned int i = 0; i < 4; i++)
        offsets.push_back(sf::Vector2u(4, 5));
    offsets.push_back(sf::Vector2u(3, 6));
    offsets.push_back(sf::Vector2u(4, 6));
    offsets.push_back(sf::Vector2u(5, 6));
    offsets.push_back(sf::Vector2u(4, 6));
    offsets.push_back(sf::Vector2u(5, 6));
    offsets.push_back(sf::Vector2u(3, 6));
    offsets.push_back(sf::Vector2u(17, 0));
    offsets.push_back(sf::Vector2u(5, 6));
    offsets.push_back(sf::Vector2u(3, 6));
    offsets.push_back(sf::Vector2u(3, 9));
    offsets.push_back(sf::Vector2u(3, 7));
    offsets.push_back(sf::Vector2u(17, 1));
    offsets.push_back(sf::Vector2u(5, 7));
    offsets.push_back(sf::Vector2u(2, 10));
    offsets.push_back(sf::Vector2u(3, 10));

    std::vector<Animation*> anims;
    for (unsigned int i = 0; i < offsets.size(); i++)
    {
        anims.push_back(new Animation(wallCalc(std::vector<sf::Vector2u>( { offsets[i] } ),
            std::vector<unsigned int>( { 0 } )),
            _db.getTexture(bedroom)));
    }
    return anims;
}

std::vector<Wall*> BedroomGenerator::generateBedroomDiagonalWall(sf::Vector2f position) const
{
    std::vector<Animation*> anims = generateDiagBedroomWallAnimations();
    std::vector<Wall*> walls;
    sf::FloatRect hitbox = sf::FloatRect(0.f, 0.f, 80.f, 16.f);
    int width = 5;
    int height = 4;
    for (int j = 0; j < height; j++)
        for (int i = 0; i < width; i++)
            walls.push_back(new Wall(position, sf::Vector2f(-i * 16.f, 48.f - j * 16.f), anims[j * width + i], hitbox));

    return walls;
}

std::vector<Animation*> BedroomGenerator::generateBackBedroomWallAnimations() const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    std::vector<sf::Vector2u> offsets;
    for (unsigned int i = 0; i < 6; i++)
        offsets.push_back(sf::Vector2u(4, 5));
    offsets.push_back(sf::Vector2u(5, 5));
    for (unsigned int i = 0; i < 6; i++)
        offsets.push_back(sf::Vector2u(4, 6));
    offsets.push_back(sf::Vector2u(5, 6));
    for (unsigned int i = 0; i < 6; i++)
        offsets.push_back(sf::Vector2u(4, 7));
    offsets.push_back(sf::Vector2u(5, 7));

    std::vector<Animation*> anims;
    for (unsigned int i = 0; i < offsets.size(); i++)
    {
        anims.push_back(new Animation(wallCalc(std::vector<sf::Vector2u>( { offsets[i] } ),
            std::vector<unsigned int>( { 0 } )),
            _db.getTexture(bedroom)));
    }
    return anims;
}

std::vector<Wall*> BedroomGenerator::generateBedroomBackWall(sf::Vector2f position) const
{
    std::vector<Animation*> anims = generateBackBedroomWallAnimations();
    std::vector<Wall*> walls;
    sf::FloatRect hitbox = sf::FloatRect(0.f, 0.f, 112.f, 16.f);
    int width = 7;
    int height = 3;
    for (int j = 0; j < height; j++)
        for (int i = 0; i < width; i++)
            walls.push_back(new Wall(position, sf::Vector2f(-i * 16.f, 32.f - j * 16.f), anims[j * width + i], hitbox));

    return walls;
}

std::vector<Animation*> BedroomGenerator::generateFrontBedroomWallAnimations() const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    std::vector<sf::Vector2u> offsets;
    offsets.push_back(sf::Vector2u(10, 0));
    for (unsigned int i = 0; i < 7; i++)
        offsets.push_back(sf::Vector2u(10, 1));
    offsets.push_back(sf::Vector2u(10, 2));
    offsets.push_back(sf::Vector2u(11, 2));
    offsets.push_back(sf::Vector2u(12, 2));
    offsets.push_back(sf::Vector2u(10, 2));
    for (unsigned int i = 0; i < 8; i++)
        offsets.push_back(sf::Vector2u(13, 3));
    offsets.push_back(sf::Vector2u(16, 2));
    offsets.push_back(sf::Vector2u(14, 2));
    offsets.push_back(sf::Vector2u(15, 2));
    offsets.push_back(sf::Vector2u(16, 2));
    for (unsigned int i = 0; i < 7; i++)
        offsets.push_back(sf::Vector2u(16, 1));
    offsets.push_back(sf::Vector2u(16, 0));

    std::vector<Animation*> anims;
    for (unsigned int i = 0; i < offsets.size(); i++)
    {
        anims.push_back(new Animation(wallCalc(std::vector<sf::Vector2u>( { offsets[i] } ),
            std::vector<unsigned int>( { 0 } )),
            _db.getTexture(bedroom)));
    }
    return anims;
}

std::vector<Wall*> BedroomGenerator::generateBedroomFrontWall(sf::Vector2f position) const
{
    std::vector<Animation*> anims = generateFrontBedroomWallAnimations();
    std::vector<Wall*> walls;
    sf::FloatRect bottomHitbox = sf::FloatRect(0.f, 0.f, 192.f, 16.f);
    sf::FloatRect leftHitbox = sf::FloatRect(-16.f, -128.f, 16.f, 144.f);
    sf::FloatRect rightHitbox = sf::FloatRect(192.f, -128.f, 16.f, 144.f);
    int sideHeight = 9;
    int bottomWidth = 10;
    for (int i = 0; i < sideHeight; i++)
        walls.push_back(new Wall(position, sf::Vector2f(16.f, 144.f - i * 16.f), anims[i], leftHitbox));
    walls.push_back(new Wall(position, sf::Vector2f(0.f, 16.f), anims[9], bottomHitbox));
    walls.push_back(new Wall(position, sf::Vector2f(-16.f, 16.f), anims[10], bottomHitbox));
    for (int i = 0; i < bottomWidth; i++)
        walls.push_back(new Wall(position, sf::Vector2f(-16.f - i * 16.f, 0.f), anims[11 + i], bottomHitbox));
    walls.push_back(new Wall(position, sf::Vector2f(-160.f, 16.f), anims[21], bottomHitbox));
    walls.push_back(new Wall(position, sf::Vector2f(-176.f, 16.f), anims[22], bottomHitbox));
    for (int i = 0; i < sideHeight; i++)
        walls.push_back(new Wall(position, sf::Vector2f(-192.f, 16.f + i * 16.f), anims[23 + i], rightHitbox));

    return walls;
}

Animation* BedroomGenerator::generateCupboardAnimation() const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(32, 48));
    return new Animation(wallCalc(std::vector<sf::Vector2u>( { sf::Vector2u(0, 2) } ),
            std::vector<unsigned int>( { 0 } )),
            _db.getTexture(bedroom));
}

Wall* BedroomGenerator::generateCupboard(sf::Vector2f position) const
{
    return new Wall(position, sf::Vector2f(0.f, 32.f), generateCupboardAnimation(), sf::FloatRect(0.f, 0.f, 32.f, 16.f));
}

Animation* BedroomGenerator::generateDeskAnimation() const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(48, 32));
    return new Animation(wallCalc(std::vector<sf::Vector2u>( { sf::Vector2u(4, 0) } ),
            std::vector<unsigned int>( { 0 } )),
            _db.getTexture(bedroom));
}

Desk* BedroomGenerator::generateDesk(sf::Vector2f position) const
{
    return new Desk(position, sf::Vector2f(0.f, 16.f), generateDeskAnimation(), sf::FloatRect(1.f, 0.f, 38.f, 14.f));
}

std::vector<Animation*> BedroomGenerator::generateBedAnimations() const
{
    TileCalculator bedCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(32, 32));
    std::vector<Animation*> anims;
    std::vector<sf::Vector2u> offsets;
    for (unsigned int i = 0; i < 8; i++)
        offsets.push_back(sf::Vector2u(i, 0));
    std::vector<unsigned int> times = { 30, 6, 6, 60, 6, 6, 60, 6 };

    anims.push_back(new Animation(bedCalc(std::vector<sf::Vector2u>( { sf::Vector2u(0, 0) } ),
        std::vector<unsigned int>( { 0 } )),
        _db.getTexture(bed)));

    anims.push_back(new Animation(bedCalc(std::vector<sf::Vector2u>( offsets ),
        std::vector<unsigned int>( times )),
        _db.getTexture(bed)));
    return anims;
}

Bed* BedroomGenerator::generateBed(sf::Vector2f position) const
{
    /*return new Bed(position, sf::Vector2f(0.f, 16.f), generateBedAnimations(), sf::FloatRect(0.f, 0.f, 32.f, 14.f),
        14, sf::Vector2f(1280.f, 960.f), true);*/
    return new Bed(position, sf::Vector2f(0.f, 16.f), generateBedAnimations(), sf::FloatRect(0.f, 0.f, 32.f, 14.f),
        4, sf::Vector2f(320.f, 320.f), true);
    /*return new Bed(position, sf::Vector2f(0.f, 16.f), generateBedAnimations(), sf::FloatRect(0.f, 0.f, 32.f, 14.f),
        10, sf::Vector2f(104.f, 136.f), true);*/
    /*return new Bed(position, sf::Vector2f(0.f, 16.f), generateBedAnimations(), sf::FloatRect(0.f, 0.f, 32.f, 14.f),
        21, sf::Vector2f(320.f, 280.f), true);*/
}

std::vector<Animation*> BedroomGenerator::generateBedroomFloorAnimations() const
{
    TileCalculator floorCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 32));

    TileCalculator carpetCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(128, 64));

    std::vector<Animation*> anims;
    for (unsigned int i = 0; i < 16; i++)
    {
        anims.push_back(new Animation(floorCalc(std::vector<sf::Vector2u>( { sf::Vector2u(8, 0) } ),
            std::vector<unsigned int>( { 0 } )),
            _db.getTexture(bedroom)));
    }
    anims.push_back(new Animation(floorCalc(std::vector<sf::Vector2u>( { sf::Vector2u(9, 0) } ),
            std::vector<unsigned int>( { 0 } )),
            _db.getTexture(bedroom)));
    anims.push_back(new Animation(carpetCalc(std::vector<sf::Vector2u>( { sf::Vector2u(0, 0) } ),
            std::vector<unsigned int>( { 0 } )),
            _db.getTexture(bedroom)));

    return anims;
}

std::vector<Decoration*> BedroomGenerator::generateBedroomFloor(sf::Vector2f position) const
{
    std::vector<Animation*> anims = generateBedroomFloorAnimations();
    std::vector<Decoration*> floor;
    int topLenght = 9;
    // top floor tiles
    for (int i = 0; i < topLenght; i++)
        floor.push_back(new Decoration(position + sf::Vector2f(48.f + i * 16.f, -32.f), sf::Vector2f(0.f, 0.f),
            sf::Vector2f(), anims[i]));

    // left corner
    floor.push_back(new Decoration(position + sf::Vector2f(0.f, 0.f), sf::Vector2f(0.f, 0.f),
        sf::Vector2f(), anims[9]));
    floor.push_back(new Decoration(position + sf::Vector2f(16.f, 0.f), sf::Vector2f(0.f, 0.f),
        sf::Vector2f(), anims[10]));
    floor.push_back(new Decoration(position + sf::Vector2f(0.f, 32.f), sf::Vector2f(0.f, 0.f),
        sf::Vector2f(), anims[11]));
    floor.push_back(new Decoration(position + sf::Vector2f(16.f, 32.f), sf::Vector2f(0.f, 0.f),
        sf::Vector2f(), anims[12]));

    // right corner
    floor.push_back(new Decoration(position + sf::Vector2f(160.f, 0.f), sf::Vector2f(0.f, 0.f),
        sf::Vector2f(), anims[13]));
    floor.push_back(new Decoration(position + sf::Vector2f(176.f, 0.f), sf::Vector2f(0.f, 0.f),
        sf::Vector2f(), anims[16]));
    floor.push_back(new Decoration(position + sf::Vector2f(160.f, 32.f), sf::Vector2f(0.f, 0.f),
        sf::Vector2f(), anims[14]));
    floor.push_back(new Decoration(position + sf::Vector2f(176.f, 32.f), sf::Vector2f(0.f, 0.f),
        sf::Vector2f(), anims[15]));

    // carpet
    floor.push_back(new Decoration(position + sf::Vector2f(32.f, 0.f), sf::Vector2f(0.f, 0.f),
        sf::Vector2f(), anims[17]));

    return floor;
}

AutoTransition* BedroomGenerator::generateTransTile(const sf::Vector2f position, const unsigned int destination,
    const sf::Vector2f transitionVec, const bool absolute, Player* _player) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}), std::vector<unsigned int>({0})),
        _db.getTexture(tile));

    return new AutoTransition(position, sf::Vector2f(0.f, 0.f), sf::FloatRect(0.f, 0.f, 16.f, 16.f),
        anim, destination, transitionVec, absolute, _player);
}

Room* BedroomGenerator::generateBedroom(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<GameObject*> floors;

    sf::Vector2f corner = sf::Vector2f(48.f, 32.f);

    std::vector<Wall*> diagWall = generateBedroomDiagonalWall(corner + sf::Vector2f(16.f, 64.f));
    std::vector<Wall*> backWall = generateBedroomBackWall(corner + sf::Vector2f(96.f, 48.f));
    std::vector<Wall*> frontWall = generateBedroomFrontWall(corner + sf::Vector2f(16.f, 144.f));
    collidables.insert(collidables.end(), diagWall.begin(), diagWall.end());
    collidables.insert(collidables.end(), backWall.begin(), backWall.end());
    collidables.insert(collidables.end(), frontWall.begin(), frontWall.end());
    collidables.push_back(generateCupboard(corner + sf::Vector2f(96.f, 64.f)));
    Desk* desk = generateDesk(corner + sf::Vector2f(128.f, 64.f));
    interactibles.push_back(desk);
    collidables.push_back(desk);
    Bed* newBed = generateBed(corner + sf::Vector2f(176.f, 64.f));
    interactibles.push_back(newBed);
    collidables.push_back(newBed);

    std::vector<Decoration *> roomFloor = generateBedroomFloor(corner + sf::Vector2f(16.f, 80.f));
    floors.insert(floors.end(), roomFloor.begin(), roomFloor.end());

    floors.push_back(generateTransTile(sf::Vector2f(255.f, 128.f), 3, sf::Vector2f(144.f, 224.f), true, player));
    floors.push_back(generateTransTile(sf::Vector2f(255.f, 112.f), 3, sf::Vector2f(144.f, 224.f), true, player));

    sf::Sound* bedSound = _db.getSound(bedSfx);

    return new Bedroom(player, graph, collidables, floors, interactibles, bedSound);
}
