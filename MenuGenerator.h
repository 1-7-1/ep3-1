/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MENU_GEN_H
#define MENU_GEN_H

#include "GameObjectGenerator.h"

class PauseMenu;
class MainMenu;
class Player;
class RoomGraph;
class Decoration;

class MenuGenerator: public GameObjectGenerator
{
    private:
        enum MenuGraphics { pauseMenu, effectMenu, pauseCursor, effectCursor, effectLabel, mainMenu, mainMenuCursor };
        enum Sounds { accept, cancel };

        virtual void load();

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateMainCursorOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateMainCursorTimeVectors() const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateMainMenuOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateMainMenuTimeVectors() const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generatePauseCursorOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generatePauseCursorTimeVectors() const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generatePauseMenuOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generatePauseMenuTimeVectors() const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateEffectCursorOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateEffectCursorTimeVectors() const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateEffectMenuOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateEffectMenuTimeVectors() const;

        std::vector<Decoration*> generateEffectLabels() const;

    public:
        MenuGenerator();
        virtual ~MenuGenerator();

        MainMenu* generateMainMenu(RoomGraph* graph);
        PauseMenu* generatePauseMenu(Player* player, RoomGraph* graph);
};

#endif
