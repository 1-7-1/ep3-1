/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "MachetePedestal.h"
#include "Animation.h"
#include "Player.h"
#include "Room.h"
#include "MathConstants.h"


MachetePedestal::MachetePedestal(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox,
    std::vector<Animation*> animations, Player* player)
    : CollidableObject(position, origin, hitbox), InteractibleObject(position, origin, hitbox), _animations(animations)
{
    if (player->getObtainedEffects()[machete])
        _currentAnimation = _animations[1];
    else
        _currentAnimation = _animations[0];

    for (Animation* anim: _animations)
        anim->setOrigin(_origin);
}

MachetePedestal::~MachetePedestal()
{
    for (Animation* anim: _animations)
        delete anim;
}

void MachetePedestal::interact(Player* player)
{
    float playerDir = player->getDirection() * 180.f / math::PI;
    if (player->getState() == idle && playerDir > 20.f && playerDir < 160.f && !player->getObtainedEffects()[machete])
    {
        std::vector<bool> obtEffects = player->getObtainedEffects();
        obtEffects[machete] = true;
        player->setObtainedEffects(obtEffects);
        _room->createSpecialEffect(player->getPosition() + sf::Vector2f(0.f, 0.5f), sf::Vector2f(0.f, 45.f), effectGet);
        _currentAnimation = _animations[1];
    }
}

void MachetePedestal::behave()
{
    changeAnimation();
}
