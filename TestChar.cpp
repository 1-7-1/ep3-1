/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "TestChar.h"
#include "MathConstants.h"
#include "Animation.h"
#include "Player.h"
#include "Room.h"
#include <cmath>

TestChar::TestChar(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox, float speedLength,
    std::vector<std::vector<Animation*>> animations, std::vector<sf::Sound*> sounds)
    : CollidableObject(position, origin, hitbox), InteractibleObject(position, origin, hitbox), Entity(position, origin, speedLength, hitbox), _animations(animations), _sounds(sounds)
{
    _currentDirection = right;
    _currentState = idle;

    for (std::vector<Animation*> animList : _animations)
    {
        for (Animation* anim : animList)
            anim->setOrigin(_origin);
    }

    _currentAnimation = _animations[_currentState][_currentDirection];
}

TestChar::~TestChar()
{
    for (std::vector<Animation*> animList : _animations)
    {
        for (Animation* anim : animList)
            delete anim;
    }
}

void TestChar::interact(Player* player)
{
    if (_currentState != dead)
    {
        if (player->getState() == PlayerState::idle)
        {
            _sounds[interaction]->play();
            float direction = atan2(_position.y - player->getPosition().y, player->getPosition().x - _position.x);
            float degrees = direction * 180.f / math::PI;
            if (degrees < 45.f && degrees >= -45.f)
                _currentDirection = right;
            else if (degrees < 135.f && degrees >= 45.f)
                _currentDirection = up;
            else if (degrees < -135.f || degrees >= 135.f)
                _currentDirection = left;
            else
                _currentDirection = down;
            _currentState = idle;
            _speed.x = 0.f;
            _speed.y = 0.f;
        }
        if (player->getEffect() == machete && player->getState() == action)
        {
            _room->createSpecialEffect(_position, sf::Vector2f(0.f, 16.f), blood);
            _timeToDie = 63;
            _currentState = dead;
        }
    }
}

void TestChar::behave()
{
    if (_currentState != dead)
    {
        if (_currentState == idle && rand() % 60 == 0)
        {
            int degrees = rand() % 360;
            if (degrees < 45 || degrees >= 315)
                _currentDirection = right;
            else if (degrees < 135 && degrees >= 45)
                _currentDirection = up;
            else if (degrees < 225 && degrees >= 135)
                _currentDirection = left;
            else
                _currentDirection = down;
            _direction = float(degrees) * math::PI / 180.f;
            _speed.x = _speedLength * cos(_direction);
            _speed.y = -_speedLength * sin(_direction);
            _currentState = moving;
        }
        else
        {
            if (rand() % 60 == 0)
            {
                _speed = sf::Vector2f(0.f, 0.f);
                _currentState = idle;
            }
            else if (_currentState == moving)
            {
                _speed.x = _speedLength * cos(_direction);
                _speed.y = -_speedLength * sin(_direction);
            }
        }

        if (_speed.x != 0.f || _speed.y != 0.f)
        {
            bool freeNext = true;
            bool freeX = (_speed.x != 0.f);
            bool freeY = (_speed.y != 0.f);

            for (unsigned int j = 0; j < _lists.size(); j++)
            {
                if (freeNext)
                    freeNext = !checkCollision(_speed, _lists[j]);
                if (freeX)
                    freeX = !checkCollision(sf::Vector2f(copysign(_speedLength, _speed.x), 0.f), _lists[j]);
                if (freeY)
                    freeY = !checkCollision(sf::Vector2f(0.f, copysign(_speedLength, _speed.y)), _lists[j]);
            }

            if (!freeNext)
            {
                if (freeX && fabs(_speed.x) > sin(math::PI / 12.0) * _speedLength)
                    _speed = sf::Vector2f(copysign(_speedLength, _speed.x), 0.f);
                else if (freeY && fabs(_speed.y) > sin(math::PI / 12.0) * _speedLength)
                    _speed = sf::Vector2f(0.f, copysign(_speedLength, _speed.y));
                else
                    _speed = sf::Vector2f(0.f, 0.f);
            }
        }
        move();
        changeAnimation();
    }
    else
    {
        _currentAnimation->setColor(sf::Color(255, 255, 255, _timeToDie * 4));
        _timeToDie--;
        if (_timeToDie == 0)
            _room->setToDestroy(this);
    }
}

void TestChar::move()
{
    Entity::move();
}

void TestChar::changeAnimation()
{
    if (_speed == sf::Vector2f(0.f, 0.f))
        _currentState = idle;
    else
    {
        _currentState = moving;
        float direction = atan2(-_speed.y, _speed.x);
        float degrees = direction * 180.f / math::PI;

        if (degrees < 45.f && degrees >= -45.f)
            _currentDirection = right;
        else if (degrees < 135.f && degrees >= 45.f)
            _currentDirection = up;
        else if (degrees < -135.f || degrees >= 135.f)
            _currentDirection = left;
        else
            _currentDirection = down;
    }
    
    _currentAnimation = _animations[_currentState][_currentDirection];
    Entity::changeAnimation();
}
