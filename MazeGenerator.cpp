/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "MazeGenerator.h"
#include "TileCalculator.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "Wall.h"
#include "MazeBackground.h"
#include "Room.h"
#include "Door.h"
#include "Chaser.h"
#include "Player.h"

MazeGenerator::MazeGenerator()
{

}

MazeGenerator::~MazeGenerator()
{

}

void MazeGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/bg/3.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/tilesets/14.png", sf::Color(0, 255, 255, 255));
    _db.loadTexture("graphics/char/2.png", sf::Color(0, 255, 255, 255));
    _isLoaded = true;
}

std::vector<MazeBackground*> MazeGenerator::generateBackground(Player* player) const
{
    TileCalculator bgCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(960, 720));
    std::vector<MazeBackground*> back;
    for (float i = 1.f; i > 0.001f; i *= 0.8f)
    {
        Animation* anim = new Animation(bgCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
            std::vector<unsigned int>({0})), _db.getTexture(bg));
        anim->setScale(i, i);
        back.push_back(new MazeBackground(sf::Vector2f(), sf::Vector2f(480.f, 360.f), anim, player));
    }

    return back;
}

std::vector<std::vector<std::vector<sf::Vector2u>>> MazeGenerator::generateDoorOffsetVectors() const
{
    std::vector<std::vector<sf::Vector2u>> closedVec;
    closedVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(2, 8) }));

    std::vector<std::vector<sf::Vector2u>> openVec;
    openVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(2, 8) }));

    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ closedVec, openVec });
}

std::vector<std::vector<unsigned int>> MazeGenerator::generateDoorTimeVectors() const
{
    std::vector<unsigned int> closedTimes = { 0 };
    std::vector<unsigned int> openTimes = { 0 };
    
    return std::vector<std::vector<unsigned int>>({ closedTimes, openTimes });
}

Door* MazeGenerator::generateDoor(const sf::Vector2f position, const unsigned int destination,
    const sf::Vector2f transitionVec, const bool absolute) const
{
    std::vector<std::vector<Animation*>> animationVector = generateAnimationVector(_db.getTexture(tileset),
    generateDoorOffsetVectors(), generateDoorTimeVectors(), sf::Vector2u(32, 32));

    return new Door(position, sf::Vector2f(16.f, 16.f), sf::FloatRect(-16.f, 14.f, 32.f, 2.f),
        animationVector, nullptr, destination, transitionVec, absolute, 1);
}

std::vector<std::vector<std::vector<sf::Vector2u>>> MazeGenerator::generateChaserOffsetVectors() const
{
    std::vector<std::vector<sf::Vector2u>> idleVec;
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(4, 1) }));
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(4, 0) }));
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(4, 3) }));
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(4, 2) }));

    std::vector<std::vector<sf::Vector2u>> walkVec;
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 1), sf::Vector2u(1, 1), sf::Vector2u(2, 1),
        sf::Vector2u(3, 1) }));
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 0), sf::Vector2u(1, 0), sf::Vector2u(2, 0),
        sf::Vector2u(3, 0) }));
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 3), sf::Vector2u(1, 3), sf::Vector2u(2, 3),
        sf::Vector2u(3, 3) }));
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 2), sf::Vector2u(1, 2), sf::Vector2u(2, 2),
        sf::Vector2u(3, 2) }));

    std::vector<std::vector<sf::Vector2u>> deadVec;
    deadVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(4, 2), sf::Vector2u(5, 6), sf::Vector2u(6, 6), sf::Vector2u(7, 6),
        sf::Vector2u(8, 6), sf::Vector2u(9, 6) }));
    deadVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(4, 2), sf::Vector2u(5, 6), sf::Vector2u(6, 6), sf::Vector2u(7, 6),
        sf::Vector2u(8, 6), sf::Vector2u(9, 6) }));
    deadVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(4, 2), sf::Vector2u(5, 6), sf::Vector2u(6, 6), sf::Vector2u(7, 6),
        sf::Vector2u(8, 6), sf::Vector2u(9, 6) }));
    deadVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(4, 2), sf::Vector2u(5, 6), sf::Vector2u(6, 6), sf::Vector2u(7, 6),
        sf::Vector2u(8, 6), sf::Vector2u(9, 6) }));

    std::vector<std::vector<sf::Vector2u>> chaseIdleVec;
    chaseIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(4, 5) }));
    chaseIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(4, 4) }));
    chaseIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(4, 7) }));
    chaseIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(4, 6) }));

    std::vector<std::vector<sf::Vector2u>> chaseWalkVec;
    chaseWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 5), sf::Vector2u(1, 5), sf::Vector2u(2, 5),
        sf::Vector2u(3, 5) }));
    chaseWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 4), sf::Vector2u(1, 4), sf::Vector2u(2, 4),
        sf::Vector2u(3, 4) }));
    chaseWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 7), sf::Vector2u(1, 7), sf::Vector2u(2, 7),
        sf::Vector2u(3, 7) }));
    chaseWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 6), sf::Vector2u(1, 6), sf::Vector2u(2, 6),
        sf::Vector2u(3, 6) }));

    std::vector<std::vector<sf::Vector2u>> chaseDeadVec;
    chaseDeadVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(4, 2), sf::Vector2u(5, 6), sf::Vector2u(6, 6), sf::Vector2u(7, 6),
        sf::Vector2u(8, 6), sf::Vector2u(9, 6) }));
    chaseDeadVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(4, 2), sf::Vector2u(5, 6), sf::Vector2u(6, 6), sf::Vector2u(7, 6),
        sf::Vector2u(8, 6), sf::Vector2u(9, 6) }));
    chaseDeadVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(4, 2), sf::Vector2u(5, 6), sf::Vector2u(6, 6), sf::Vector2u(7, 6),
        sf::Vector2u(8, 6), sf::Vector2u(9, 6) }));
    chaseDeadVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(4, 2), sf::Vector2u(5, 6), sf::Vector2u(6, 6), sf::Vector2u(7, 6),
        sf::Vector2u(8, 6), sf::Vector2u(9, 6) }));

    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ idleVec, walkVec, deadVec,
        chaseIdleVec, chaseWalkVec, chaseDeadVec });
}

std::vector<std::vector<unsigned int>> MazeGenerator::generateChaserTimeVectors() const
{
    std::vector<unsigned int> idleTimes = { 0 };
    std::vector<unsigned int> walkTimes = { 12, 12, 12, 12 };
    std::vector<unsigned int> deadTimes = { 24, 6, 6, 24, 6, 6 };
    std::vector<unsigned int> chaseIdleTimes = { 0 };
    std::vector<unsigned int> chaseWalkTimes = { 12, 12, 12, 12 };
    std::vector<unsigned int> chaseDeadTimes = { 24, 6, 6, 24, 6, 6 };
    
    return std::vector<std::vector<unsigned int>>({ idleTimes, walkTimes, deadTimes,
        chaseIdleTimes, chaseWalkTimes, chaseDeadTimes });
}

Chaser* MazeGenerator::generateChaser(const sf::Vector2f position, Player* player) const
{
    std::vector<std::vector<Animation*>> animationVector = generateAnimationVector(_db.getTexture(chaser),
    generateChaserOffsetVectors(), generateChaserTimeVectors(), sf::Vector2u(24, 64));

    std::vector<std::vector<Animation*>> normalVec = {animationVector[0], animationVector[1], animationVector[2]};
    std::vector<std::vector<Animation*>> chaseVec = {animationVector[3], animationVector[4], animationVector[5]};

    return new Chaser(position, 1.1f, sf::Vector2f(12.f, 61.f), normalVec, chaseVec, sf::FloatRect(-11.f, -2.f, 22.f, 5.f),
        player, true, 600, 11, sf::Vector2f(3380.f, 1933.f), true);
}

Decoration* MazeGenerator::generateFloor(sf::Vector2f position, FloorDirection dir) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(32, 32));
    Animation* anim;

    switch (dir)
    {
        case up:
            anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 2)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));
            return new Decoration(position + sf::Vector2f(0.f, 0.01f), sf::Vector2f(16.f, 16.f), sf::Vector2f(), anim);
            break;

        case down:
            anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 4)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));
            return new Decoration(position + sf::Vector2f(0.f, 0.01f), sf::Vector2f(16.f, 16.f), sf::Vector2f(), anim);
            break;

        case vmiddle:
            anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 3)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));
            return new Decoration(position + sf::Vector2f(0.f, 0.01f), sf::Vector2f(16.f, 16.f), sf::Vector2f(), anim);
            break;

        case left:
            anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 4)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));
            anim->setRotation(90.f);
            return new Decoration(position + sf::Vector2f(0.f, 0.01f), sf::Vector2f(16.f, 16.f), sf::Vector2f(), anim);
            break;

        case right:
            anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 2)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));
            anim->setRotation(90.f);
            return new Decoration(position + sf::Vector2f(0.f, 0.01f), sf::Vector2f(16.f, 16.f), sf::Vector2f(), anim);
            break;

        case hmiddle:
            anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 3)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));
            anim->setRotation(90.f);
            return new Decoration(position + sf::Vector2f(0.f, 0.01f), sf::Vector2f(16.f, 16.f), sf::Vector2f(), anim);
            break;

        case neutral:
            anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 6)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));
            return new Decoration(position + sf::Vector2f(0.f, 0.01f), sf::Vector2f(16.f, 16.f), sf::Vector2f(), anim);
            break;
        
        default:
            anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 2)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));
            return new Decoration(position + sf::Vector2f(0.f, 0.01f), sf::Vector2f(16.f, 16.f), sf::Vector2f(), anim);
            break;
    }
}

std::vector<Decoration*> MazeGenerator::generateGlow(sf::Vector2f position, FloorDirection dir, int length) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(32, 32));

    std::vector<Decoration*> floor;

    std::vector<sf::Vector2u> glowOffsets;
    std::vector<unsigned int> glowTimes;
    for (unsigned int i = 0; i < 8; i ++)
    {
        glowOffsets.push_back(sf::Vector2u(0, i));
        glowTimes.push_back(1);
    }

    switch (dir)
    {
        case up:
            for (int i = 0; i < length; i++)
            {
                Animation* glowAnim = new Animation(wallCalc(glowOffsets, glowTimes), _db.getTexture(tileset));
                glowAnim->setScale(1.f, -1.f);
                floor.push_back(new Decoration(position + sf::Vector2f(0.f, -i * 32.f),
                    sf::Vector2f(16.f, 16.f), sf::Vector2f(), glowAnim));
            }
            break;

        case down:
            for (int i = 0; i < length; i++)
            {
                Animation* glowAnim = new Animation(wallCalc(glowOffsets, glowTimes), _db.getTexture(tileset));
                floor.push_back(new Decoration(position + sf::Vector2f(0.f, i * 32.f),
                    sf::Vector2f(16.f, 16.f), sf::Vector2f(), glowAnim));
            }
            break;

        case left:
            for (int i = 0; i < length; i++)
            {
                Animation* glowAnim = new Animation(wallCalc(glowOffsets, glowTimes), _db.getTexture(tileset));
                glowAnim->setRotation(90.f);
                floor.push_back(new Decoration(position + sf::Vector2f(-i * 32.f, 0.f),
                    sf::Vector2f(16.f, 16.f), sf::Vector2f(), glowAnim));
            }
            break;

        case right:
            for (int i = 0; i < length; i++)
            {
                Animation* glowAnim = new Animation(wallCalc(glowOffsets, glowTimes), _db.getTexture(tileset));
                glowAnim->setRotation(-90.f);
                floor.push_back(new Decoration(position + sf::Vector2f(i * 32.f, 0.f),
                    sf::Vector2f(16.f, 16.f), sf::Vector2f(), glowAnim));
            }
            break;
        
        default:
            for (int i = 0; i < length; i++)
            {
                Animation* glowAnim = new Animation(wallCalc(glowOffsets, glowTimes), _db.getTexture(tileset));
                glowAnim->setScale(1.f, -1.f);
                floor.push_back(new Decoration(position + sf::Vector2f(0.f, -i * 32.f),
                    sf::Vector2f(16.f, 16.f), sf::Vector2f(), glowAnim));
            }
            break;
    }

    return floor;
}

std::vector<Wall*> MazeGenerator::generateWalls(sf::Vector2f position, FloorDirection dir, int length) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    std::vector<Wall*> walls;

    switch (dir)
    {
        case up:
            for (int i = 0; i < length; i++)
            {
                Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                std::vector<unsigned int>({0})), _db.getTexture(invisible));
                walls.push_back(new Wall(position + sf::Vector2f(0.f, -i * 32.f),
                    sf::Vector2f(16.f, 16.f), anim, sf::FloatRect(-16.f, -16.f, 32.f, 32.f)));
            }
            break;

        case down:
            for (int i = 0; i < length; i++)
            {
                Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                std::vector<unsigned int>({0})), _db.getTexture(invisible));
                walls.push_back(new Wall(position + sf::Vector2f(0.f, i * 32.f),
                    sf::Vector2f(16.f, 16.f), anim, sf::FloatRect(-16.f, -16.f, 32.f, 32.f)));
            }
            break;

        case left:
            for (int i = 0; i < length; i++)
            {
                Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                std::vector<unsigned int>({0})), _db.getTexture(invisible));
                walls.push_back(new Wall(position + sf::Vector2f(-i * 32.f, 0.f),
                    sf::Vector2f(16.f, 16.f), anim, sf::FloatRect(-16.f, -16.f, 32.f, 32.f)));
            }
            break;

        case right:
            for (int i = 0; i < length; i++)
            {
                Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                std::vector<unsigned int>({0})), _db.getTexture(invisible));
                walls.push_back(new Wall(position + sf::Vector2f(i * 32.f, 0.f),
                    sf::Vector2f(16.f, 16.f), anim, sf::FloatRect(-16.f, -16.f, 32.f, 32.f)));
            }
            break;
        
        default:
            for (int i = 0; i < length; i++)
            {
                Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                std::vector<unsigned int>({0})), _db.getTexture(invisible));
                walls.push_back(new Wall(position + sf::Vector2f(0.f, -i * 32.f),
                    sf::Vector2f(16.f, 16.f), anim, sf::FloatRect(-16.f, -16.f, 32.f, 32.f)));
            }
            break;
    }

    return walls;
}

Room* MazeGenerator::generateMaze(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;

    //path1
    std::vector<Decoration*> glows1 = generateGlow(sf::Vector2f(1280.f, 960.f), down, 20);
    floors.insert(floors.end(), glows1.begin(), glows1.end());
    std::vector<Wall*> walls1 = generateWalls(sf::Vector2f(1248.f, 960.f), down, 20);
    collidables.insert(collidables.end(), walls1.begin(), walls1.end());
    std::vector<Wall*> walls2 = generateWalls(sf::Vector2f(1312.f, 960.f), down, 20);
    collidables.insert(collidables.end(), walls2.begin(), walls2.end());

    //path2
    std::vector<Decoration*> glows2 = generateGlow(sf::Vector2f(1312.f, 1600.f), right, 14);
    floors.insert(floors.end(), glows2.begin(), glows2.end());
    std::vector<Wall*> walls3 = generateWalls(sf::Vector2f(1312.f, 1568.f), right, 14);
    collidables.insert(collidables.end(), walls3.begin(), walls3.end());
    
    std::vector<Wall*> walls4 = generateWalls(sf::Vector2f(832.f, 1632.f), right, 29);
    collidables.insert(collidables.end(), walls4.begin(), walls4.end());

    //path3
    std::vector<Decoration*> glows3 = generateGlow(sf::Vector2f(1248.f, 1600.f), left, 14);
    floors.insert(floors.end(), glows3.begin(), glows3.end());
    std::vector<Wall*> walls5 = generateWalls(sf::Vector2f(1248.f, 1568.f), left, 14);
    collidables.insert(collidables.end(), walls5.begin(), walls5.end());

    //path4
    std::vector<Decoration*> glows4 = generateGlow(sf::Vector2f(800.f, 1632.f), down, 14);
    floors.insert(floors.end(), glows4.begin(), glows4.end());
    std::vector<Wall*> walls6 = generateWalls(sf::Vector2f(768.f, 1632.f), down, 14);
    collidables.insert(collidables.end(), walls6.begin(), walls6.end());
    std::vector<Wall*> walls7 = generateWalls(sf::Vector2f(832.f, 1632.f), down, 15);
    collidables.insert(collidables.end(), walls7.begin(), walls7.end());

    //path5
    std::vector<Decoration*> glows5 = generateGlow(sf::Vector2f(768.f, 1600.f), left, 14);
    floors.insert(floors.end(), glows5.begin(), glows5.end());
    std::vector<Wall*> walls8 = generateWalls(sf::Vector2f(768.f, 1568.f), left, 15);
    collidables.insert(collidables.end(), walls8.begin(), walls8.end());
    std::vector<Wall*> walls9 = generateWalls(sf::Vector2f(768.f, 1632.f), left, 14);
    collidables.insert(collidables.end(), walls9.begin(), walls9.end());

    //path6
    std::vector<Decoration*> glows6 = generateGlow(sf::Vector2f(768.f, 2080.f), left, 14);
    floors.insert(floors.end(), glows6.begin(), glows6.end());
    std::vector<Wall*> walls10 = generateWalls(sf::Vector2f(768.f, 2048.f), left, 14);
    collidables.insert(collidables.end(), walls10.begin(), walls10.end());
    std::vector<Wall*> walls11 = generateWalls(sf::Vector2f(800.f, 2112.f), left, 16);
    collidables.insert(collidables.end(), walls11.begin(), walls11.end());

    //path7
    std::vector<Decoration*> glows7 = generateGlow(sf::Vector2f(320.f, 1632.f), down, 14);
    floors.insert(floors.end(), glows7.begin(), glows7.end());
    std::vector<Wall*> walls12 = generateWalls(sf::Vector2f(288.f, 1600.f), down, 16);
    collidables.insert(collidables.end(), walls12.begin(), walls12.end());
    std::vector<Wall*> walls13 = generateWalls(sf::Vector2f(352.f, 1632.f), down, 14);
    collidables.insert(collidables.end(), walls13.begin(), walls13.end());

    //path8
    std::vector<Decoration*> glows8 = generateGlow(sf::Vector2f(800.f, 1568.f), up, 40);
    floors.insert(floors.end(), glows8.begin(), glows8.end());
    std::vector<Wall*> walls14 = generateWalls(sf::Vector2f(768.f, 1568.f), up, 41);
    collidables.insert(collidables.end(), walls14.begin(), walls14.end());
    std::vector<Wall*> walls15 = generateWalls(sf::Vector2f(832.f, 1568.f), up, 40);
    collidables.insert(collidables.end(), walls15.begin(), walls15.end());

    //path9
    std::vector<Decoration*> glows9 = generateGlow(sf::Vector2f(832.f, 288.f), right, 12);
    floors.insert(floors.end(), glows9.begin(), glows9.end());
    std::vector<Wall*> walls17 = generateWalls(sf::Vector2f(832.f, 320.f), right, 12);
    collidables.insert(collidables.end(), walls17.begin(), walls17.end());
    
    std::vector<Wall*> walls16 = generateWalls(sf::Vector2f(800.f, 256.f), right, 39);
    collidables.insert(collidables.end(), walls16.begin(), walls16.end());

    //path10
    std::vector<Decoration*> glows10 = generateGlow(sf::Vector2f(1248.f, 288.f), right, 24);
    floors.insert(floors.end(), glows10.begin(), glows10.end());
    std::vector<Wall*> walls18 = generateWalls(sf::Vector2f(1248.f, 320.f), right, 24);
    collidables.insert(collidables.end(), walls18.begin(), walls18.end());

    //path11
    std::vector<Decoration*> glows11 = generateGlow(sf::Vector2f(2016.f, 320.f), down, 18);
    floors.insert(floors.end(), glows11.begin(), glows11.end());
    std::vector<Wall*> walls19 = generateWalls(sf::Vector2f(1984.f, 320.f), down, 18);
    collidables.insert(collidables.end(), walls19.begin(), walls19.end());
    std::vector<Wall*> walls20 = generateWalls(sf::Vector2f(2048.f, 288.f), down, 19);
    collidables.insert(collidables.end(), walls20.begin(), walls20.end());
    std::vector<Wall*> walls21 = generateWalls(sf::Vector2f(2016.f, 896.f), down, 1);
    collidables.insert(collidables.end(), walls21.begin(), walls21.end());

    //path12
    std::vector<Decoration*> glows12 = generateGlow(sf::Vector2f(1216.f, 320.f), down, 10);
    floors.insert(floors.end(), glows12.begin(), glows12.end());
    std::vector<Wall*> walls22 = generateWalls(sf::Vector2f(1184.f, 320.f), down, 11);
    collidables.insert(collidables.end(), walls22.begin(), walls22.end());
    std::vector<Wall*> walls23 = generateWalls(sf::Vector2f(1248.f, 320.f), down, 10);
    collidables.insert(collidables.end(), walls23.begin(), walls23.end());

    //path13
    std::vector<Decoration*> glows13 = generateGlow(sf::Vector2f(1248.f, 640.f), right, 6);
    floors.insert(floors.end(), glows13.begin(), glows13.end());
    std::vector<Wall*> walls24 = generateWalls(sf::Vector2f(1248.f, 608.f), right, 5);
    collidables.insert(collidables.end(), walls24.begin(), walls24.end());
    std::vector<Wall*> walls25 = generateWalls(sf::Vector2f(1216.f, 672.f), right, 7);
    collidables.insert(collidables.end(), walls25.begin(), walls25.end());
    std::vector<Wall*> walls26 = generateWalls(sf::Vector2f(1440.f, 640.f), right, 1);
    collidables.insert(collidables.end(), walls26.begin(), walls26.end());

    //path14
    std::vector<Decoration*> glows14 = generateGlow(sf::Vector2f(1792.f, 1600.f), right, 14);
    floors.insert(floors.end(), glows14.begin(), glows14.end());
    std::vector<Wall*> walls27 = generateWalls(sf::Vector2f(1792.f, 1568.f), right, 14);
    collidables.insert(collidables.end(), walls27.begin(), walls27.end());
    std::vector<Wall*> walls28 = generateWalls(sf::Vector2f(1792.f, 1632.f), right, 15);
    collidables.insert(collidables.end(), walls28.begin(), walls28.end());

    //path15
    std::vector<Decoration*> glows15 = generateGlow(sf::Vector2f(2240.f, 1568.f), up, 14);
    floors.insert(floors.end(), glows15.begin(), glows15.end());
    std::vector<Wall*> walls29 = generateWalls(sf::Vector2f(2208.f, 1568.f), up, 14);
    collidables.insert(collidables.end(), walls29.begin(), walls29.end());
    std::vector<Wall*> walls30 = generateWalls(sf::Vector2f(2272.f, 1600.f), up, 16);
    collidables.insert(collidables.end(), walls30.begin(), walls30.end());

    //path16
    std::vector<Decoration*> glows16 = generateGlow(sf::Vector2f(1760.f, 1568.f), up, 14);
    floors.insert(floors.end(), glows16.begin(), glows16.end());
    std::vector<Wall*> walls31 = generateWalls(sf::Vector2f(1728.f, 1568.f), up, 15);
    collidables.insert(collidables.end(), walls31.begin(), walls31.end());
    std::vector<Wall*> walls32 = generateWalls(sf::Vector2f(1792.f, 1568.f), up, 14);
    collidables.insert(collidables.end(), walls32.begin(), walls32.end());

    //path17
    std::vector<Decoration*> glows17 = generateGlow(sf::Vector2f(1792.f, 1120.f), right, 14);
    floors.insert(floors.end(), glows17.begin(), glows17.end());
    std::vector<Wall*> walls33 = generateWalls(sf::Vector2f(1760.f, 1088.f), right, 16);
    collidables.insert(collidables.end(), walls33.begin(), walls33.end());
    std::vector<Wall*> walls34 = generateWalls(sf::Vector2f(1792.f, 1152.f), right, 14);
    collidables.insert(collidables.end(), walls34.begin(), walls34.end());

    //path18
    std::vector<Decoration*> glows18 = generateGlow(sf::Vector2f(1760.f, 1632.f), down, 14);
    floors.insert(floors.end(), glows18.begin(), glows18.end());
    std::vector<Wall*> walls35 = generateWalls(sf::Vector2f(1728.f, 1632.f), down, 15);
    collidables.insert(collidables.end(), walls35.begin(), walls35.end());
    std::vector<Wall*> walls36 = generateWalls(sf::Vector2f(1792.f, 1632.f), down, 14);
    collidables.insert(collidables.end(), walls36.begin(), walls36.end());
    
    //path19
    std::vector<Decoration*> glows19 = generateGlow(sf::Vector2f(1792.f, 2080.f), right, 20);
    floors.insert(floors.end(), glows19.begin(), glows19.end());
    std::vector<Wall*> walls37 = generateWalls(sf::Vector2f(1792.f, 2048.f), right, 20);
    collidables.insert(collidables.end(), walls37.begin(), walls37.end());
    std::vector<Wall*> walls38 = generateWalls(sf::Vector2f(1760.f, 2112.f), right, 22);
    collidables.insert(collidables.end(), walls38.begin(), walls38.end());

    //path20
    std::vector<Decoration*> glows20 = generateGlow(sf::Vector2f(2432.f, 2048.f), up, 40);
    floors.insert(floors.end(), glows20.begin(), glows20.end());
    std::vector<Wall*> walls39 = generateWalls(sf::Vector2f(2400.f, 2048.f), up, 40);
    collidables.insert(collidables.end(), walls39.begin(), walls39.end());
    std::vector<Wall*> walls40 = generateWalls(sf::Vector2f(2464.f, 2080.f), up, 41);
    collidables.insert(collidables.end(), walls40.begin(), walls40.end());

    //path21
    std::vector<Decoration*> glows21 = generateGlow(sf::Vector2f(2432.f, 736.f), up, 12);
    floors.insert(floors.end(), glows21.begin(), glows21.end());
    std::vector<Wall*> walls41 = generateWalls(sf::Vector2f(2400.f, 736.f), up, 12);
    collidables.insert(collidables.end(), walls41.begin(), walls41.end());
    std::vector<Wall*> walls42 = generateWalls(sf::Vector2f(2464.f, 736.f), up, 12);
    collidables.insert(collidables.end(), walls42.begin(), walls42.end());
    std::vector<Wall*> walls43 = generateWalls(sf::Vector2f(2432.f, 352.f), up, 1);
    collidables.insert(collidables.end(), walls43.begin(), walls43.end());

    //path22
    std::vector<Decoration*> glows22 = generateGlow(sf::Vector2f(2464.f, 768.f), right, 3);
    floors.insert(floors.end(), glows22.begin(), glows22.end());
    std::vector<Wall*> walls44 = generateWalls(sf::Vector2f(2464.f, 736.f), right, 3);
    collidables.insert(collidables.end(), walls44.begin(), walls44.end());
    std::vector<Wall*> walls45 = generateWalls(sf::Vector2f(2464.f, 800.f), right, 3);
    collidables.insert(collidables.end(), walls45.begin(), walls45.end());
    std::vector<Wall*> walls46 = generateWalls(sf::Vector2f(2560.f, 768.f), right, 1);
    collidables.insert(collidables.end(), walls46.begin(), walls46.end());

    //path23
    std::vector<Decoration*> glows23 = generateGlow(sf::Vector2f(2400.f, 768.f), left, 10);
    floors.insert(floors.end(), glows23.begin(), glows23.end());
    std::vector<Wall*> walls47 = generateWalls(sf::Vector2f(2400.f, 736.f), left, 9);
    collidables.insert(collidables.end(), walls47.begin(), walls47.end());
    std::vector<Wall*> walls48 = generateWalls(sf::Vector2f(2400.f, 800.f), left, 10);
    collidables.insert(collidables.end(), walls48.begin(), walls48.end());
    std::vector<Wall*> walls49 = generateWalls(sf::Vector2f(2080.f, 768.f), left, 1);
    collidables.insert(collidables.end(), walls49.begin(), walls49.end());

    //path1
    floors.push_back(generateFloor(sf::Vector2f(1280.f, 960.f), up));
    for (int i = 1; i < 19; i++)
        floors.push_back(generateFloor(sf::Vector2f(1280.f, 960.f + i * 32.f), vmiddle));
    floors.push_back(generateFloor(sf::Vector2f(1280.f, 1568.f), down));

    floors.push_back(generateFloor(sf::Vector2f(1280.f, 1600.f), neutral));

    //path2
    floors.push_back(generateFloor(sf::Vector2f(1312.f, 1600.f), left));
    for (int i = 1; i < 13; i++)
        floors.push_back(generateFloor(sf::Vector2f(1312.f + i * 32.f, 1600.f), hmiddle));
    floors.push_back(generateFloor(sf::Vector2f(1728.f, 1600.f), right));

    floors.push_back(generateFloor(sf::Vector2f(1760.f, 1600.f), neutral));

    //path3
    floors.push_back(generateFloor(sf::Vector2f(1248.f, 1600.f), right));
    for (int i = 1; i < 13; i++)
        floors.push_back(generateFloor(sf::Vector2f(1248.f - i * 32.f, 1600.f), hmiddle));
    floors.push_back(generateFloor(sf::Vector2f(832.f, 1600.f), left));

    floors.push_back(generateFloor(sf::Vector2f(800.f, 1600.f), neutral));

    //path4
    floors.push_back(generateFloor(sf::Vector2f(800.f, 1632.f), up));
    for (int i = 1; i < 13; i++)
        floors.push_back(generateFloor(sf::Vector2f(800.f, 1632.f + i * 32.f), vmiddle));
    floors.push_back(generateFloor(sf::Vector2f(800.f, 2048.f), down));

    floors.push_back(generateFloor(sf::Vector2f(800.f, 2080.f), neutral));

    //path5
    floors.push_back(generateFloor(sf::Vector2f(768.f, 1600.f), right));
    for (int i = 1; i < 13; i++)
        floors.push_back(generateFloor(sf::Vector2f(768.f - i * 32.f, 1600.f), hmiddle));
    floors.push_back(generateFloor(sf::Vector2f(352.f, 1600.f), left));

    floors.push_back(generateFloor(sf::Vector2f(320.f, 1600.f), neutral));

    //path6
    floors.push_back(generateFloor(sf::Vector2f(768.f, 2080.f), right));
    for (int i = 1; i < 13; i++)
        floors.push_back(generateFloor(sf::Vector2f(768.f - i * 32.f, 2080.f), hmiddle));
    floors.push_back(generateFloor(sf::Vector2f(352.f, 2080.f), left));

    floors.push_back(generateFloor(sf::Vector2f(320.f, 2080.f), neutral));

    Chaser* chaser1 = generateChaser(sf::Vector2f(320.f, 2080.f), player);
    entities.push_back(chaser1);
    interactibles.push_back(chaser1);

    //path7
    floors.push_back(generateFloor(sf::Vector2f(320.f, 1632.f), up));
    for (int i = 1; i < 13; i++)
        floors.push_back(generateFloor(sf::Vector2f(320.f, 1632.f + i * 32.f), vmiddle));
    floors.push_back(generateFloor(sf::Vector2f(320.f, 2048.f), down));

    //path8
    floors.push_back(generateFloor(sf::Vector2f(800.f, 1568.f), down));
    for (int i = 1; i < 39; i++)
        floors.push_back(generateFloor(sf::Vector2f(800.f, 1568.f - i * 32.f), vmiddle));
    floors.push_back(generateFloor(sf::Vector2f(800.f, 320.f), up));
    floors.push_back(generateFloor(sf::Vector2f(800.f, 288.f), neutral));

    //path9
    floors.push_back(generateFloor(sf::Vector2f(832.f, 288.f), left));
    for (int i = 1; i < 11; i++)
        floors.push_back(generateFloor(sf::Vector2f(832.f + i * 32.f, 288.f), hmiddle));
    floors.push_back(generateFloor(sf::Vector2f(1184.f, 288.f), right));
    floors.push_back(generateFloor(sf::Vector2f(1216.f, 288.f), neutral));

    //path10
    floors.push_back(generateFloor(sf::Vector2f(1248.f, 288.f), left));
    for (int i = 1; i < 23; i++)
        floors.push_back(generateFloor(sf::Vector2f(1248.f + i * 32.f, 288.f), hmiddle));
    floors.push_back(generateFloor(sf::Vector2f(1984.f, 288.f), right));
    floors.push_back(generateFloor(sf::Vector2f(2016.f, 288.f), neutral));

    //path11
    floors.push_back(generateFloor(sf::Vector2f(2016.f, 320.f), up));
    for (int i = 1; i < 17; i++)
        floors.push_back(generateFloor(sf::Vector2f(2016.f, 320.f + i * 32.f), vmiddle));
    floors.push_back(generateFloor(sf::Vector2f(2016.f, 864.f), down));

    //path12
    floors.push_back(generateFloor(sf::Vector2f(1216.f, 320.f), up));
    for (int i = 1; i < 9; i++)
        floors.push_back(generateFloor(sf::Vector2f(1216.f, 320.f + i * 32.f), vmiddle));
    floors.push_back(generateFloor(sf::Vector2f(1216.f, 608.f), down));
    floors.push_back(generateFloor(sf::Vector2f(1216.f, 640.f), neutral));

    //path13
    floors.push_back(generateFloor(sf::Vector2f(1248.f, 640.f), left));
    for (int i = 1; i < 5; i++)
        floors.push_back(generateFloor(sf::Vector2f(1248.f + i * 32.f, 640.f), hmiddle));
    floors.push_back(generateFloor(sf::Vector2f(1408.f, 640.f), right));

    //path14
    floors.push_back(generateFloor(sf::Vector2f(1792.f, 1600.f), left));
    for (int i = 1; i < 13; i++)
        floors.push_back(generateFloor(sf::Vector2f(1792.f + i * 32.f, 1600.f), hmiddle));
    floors.push_back(generateFloor(sf::Vector2f(2208.f, 1600.f), right));

    floors.push_back(generateFloor(sf::Vector2f(2240.f, 1600.f), neutral));

    //path15
    floors.push_back(generateFloor(sf::Vector2f(2240.f, 1568.f), down));
    for (int i = 1; i < 13; i++)
        floors.push_back(generateFloor(sf::Vector2f(2240.f, 1568.f - i * 32.f), vmiddle));
    floors.push_back(generateFloor(sf::Vector2f(2240.f, 1152.f), up));

    floors.push_back(generateFloor(sf::Vector2f(2240.f, 1120.f), neutral));

    Chaser* chaser2 = generateChaser(sf::Vector2f(2240.f, 1120.f), player);
    entities.push_back(chaser2);
    interactibles.push_back(chaser2);

    //path16
    floors.push_back(generateFloor(sf::Vector2f(1760.f, 1568.f), down));
    for (int i = 1; i < 13; i++)
        floors.push_back(generateFloor(sf::Vector2f(1760.f, 1568.f - i * 32.f), vmiddle));
    floors.push_back(generateFloor(sf::Vector2f(1760.f, 1152.f), up));

    floors.push_back(generateFloor(sf::Vector2f(1760.f, 1120.f), neutral));

    //path17
    floors.push_back(generateFloor(sf::Vector2f(1792.f, 1120.f), left));
    for (int i = 1; i < 13; i++)
        floors.push_back(generateFloor(sf::Vector2f(1792.f + i * 32.f, 1120.f), hmiddle));
    floors.push_back(generateFloor(sf::Vector2f(2208.f, 1120.f), right));

    //path18
    floors.push_back(generateFloor(sf::Vector2f(1760.f, 1632.f), up));
    for (int i = 1; i < 13; i++)
        floors.push_back(generateFloor(sf::Vector2f(1760.f, 1632.f + i * 32.f), vmiddle));
    floors.push_back(generateFloor(sf::Vector2f(1760.f, 2048.f), down));

    floors.push_back(generateFloor(sf::Vector2f(1760.f, 2080.f), neutral));

    //path19
    floors.push_back(generateFloor(sf::Vector2f(1792.f, 2080.f), left));
    for (int i = 1; i < 19; i++)
        floors.push_back(generateFloor(sf::Vector2f(1792.f + i * 32.f, 2080.f), hmiddle));
    floors.push_back(generateFloor(sf::Vector2f(2400.f, 2080.f), right));

    floors.push_back(generateFloor(sf::Vector2f(2432.f, 2080.f), neutral));

    //path20
    floors.push_back(generateFloor(sf::Vector2f(2432.f, 2048.f), down));
    for (int i = 1; i < 39; i++)
        floors.push_back(generateFloor(sf::Vector2f(2432.f, 2048.f - i * 32.f), vmiddle));
    floors.push_back(generateFloor(sf::Vector2f(2432.f, 800.f), up));

    floors.push_back(generateFloor(sf::Vector2f(2432.f, 768.f), neutral));

    //path21
    floors.push_back(generateFloor(sf::Vector2f(2432.f, 736.f), down));
    for (int i = 1; i < 11; i++)
        floors.push_back(generateFloor(sf::Vector2f(2432.f, 736.f - i * 32.f), vmiddle));
    floors.push_back(generateFloor(sf::Vector2f(2432.f, 384.f), up));

    //path22
    floors.push_back(generateFloor(sf::Vector2f(2464.f, 768.f), left));
    for (int i = 1; i < 2; i++)
        floors.push_back(generateFloor(sf::Vector2f(2464.f + i * 32.f, 768.f), hmiddle));
    floors.push_back(generateFloor(sf::Vector2f(2528.f, 768.f), right));

    //path23
    floors.push_back(generateFloor(sf::Vector2f(2400.f, 768.f), right));
    for (int i = 1; i < 9; i++)
        floors.push_back(generateFloor(sf::Vector2f(2400.f - i * 32.f, 768.f), hmiddle));
    floors.push_back(generateFloor(sf::Vector2f(2112.f, 768.f), left));

    std::vector<MazeBackground*> back = generateBackground(player);
    backgrounds.insert(backgrounds.end(), back.begin(), back.end());

    Door* wastelandDoor = generateDoor(sf::Vector2f(2112.f, 736.f), 15, sf::Vector2f(960.f, 960.f), true);
    interactibles.push_back(wastelandDoor);
    collidables.push_back(wastelandDoor);

    Door* plantationDoor = generateDoor(sf::Vector2f(1408.f, 608.f), 20, sf::Vector2f(288.f, 696.f), true);
    interactibles.push_back(plantationDoor);
    collidables.push_back(plantationDoor);

    Door* cityDoor = generateDoor(sf::Vector2f(1280.f, 928.f), 12, sf::Vector2f(852.f, 1200.f), true);
    interactibles.push_back(cityDoor);
    collidables.push_back(cityDoor);
    
    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, std::vector<GameObject*>(), std::vector<GameObject*>() };

    return new Room(player, graph, nullptr, objects, sf::Vector2f(_roomWidth, _roomHeight), sf::Vector2<bool>(false, false));
}
