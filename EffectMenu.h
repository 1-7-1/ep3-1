/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef EFF_MENU_H
#define EFF_MENU_H

#include "Menu.h"

class Player;
class Decoration;

namespace sf
{
    class Sound;
}

class EffectMenu: public Menu
{
    private:
        Player* _player;
        unsigned int _nbOfEffects = 4;

        bool _leftPressed = true;
        bool _rightPressed = true;
        bool _zPressed = true;
        bool _xPressed = true;
        bool _enterPressed = true;

        std::vector<Decoration*> _labels;

        std::vector<sf::Sound*> _sounds;

        virtual void resetInputs();
        virtual void handleInput();
        virtual void behaveYourself();

    public:
        EffectMenu(Player* player, std::vector<Animation*> animations, MenuCursor* cursor,
            std::vector<Decoration*> labels, Menu* parent = nullptr,
            std::vector<sf::Sound*> sounds = std::vector<sf::Sound*>({nullptr, nullptr}));
        ~EffectMenu();

        virtual void placeCursors();
        virtual std::vector<GameObject*> getGraphics();
};

#endif
