/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GAME_CONST_H
#define GAME_CONST_H

#include <string>

namespace game
{
    const double frameLength = 1000.0 / 60.0;

    const unsigned int smallWindowWidth = 320;
    const unsigned int smallWindowHeight = 240;

    const unsigned int medWindowFactor = 2;
    const unsigned int medWindowWidth = smallWindowWidth * medWindowFactor;
    const unsigned int medWindowHeight = smallWindowHeight * medWindowFactor;

    const unsigned int largeWindowFactor = 3;
    const unsigned int largeWindowWidth = smallWindowWidth * largeWindowFactor;
    const unsigned int largeWindowHeight = smallWindowHeight * largeWindowFactor;

    const std::string title = "Ep3";
}

#endif
