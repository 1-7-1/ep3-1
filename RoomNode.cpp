/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "RoomNode.h"
#include "RoomGraph.h"
#include "Room.h"

RoomNode::RoomNode(RoomGraph* graph): _graph(graph)
{
    _room = nullptr;
}

RoomNode::~RoomNode()
{
    if (_room != nullptr)
        delete _room;
}

void RoomNode::deleteRoom()
{
    if (_room != nullptr)
    {
        delete _room;
        _room = nullptr;
    }
}
