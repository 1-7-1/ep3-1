/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "CityNpc2.h"
#include "Room.h"
#include "ColorController.h"
#include "Animation.h"
#include "MathConstants.h"
#include <math.h>

CityNpc2::CityNpc2(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox, std::vector<Animation*> animations,
    ColorController* controller, std::vector<std::vector<Animation*>> fakerAnimations): CollidableObject(position, origin, hitbox),
    CityNpc(position, origin, 0.5f, hitbox, controller, fakerAnimations),
    _animations(animations), _currentDirection(horizontal)
{
    _currentAnimation = _animations[_currentDirection];
    _currentState = moving;
    _speed = sf::Vector2f(_speedLength, 0.f);
    for (Animation* anim: _animations)
        anim->setOrigin(_origin);
}

CityNpc2::~CityNpc2()
{
    for (Animation* anim: _animations)
        delete anim;
}

void CityNpc2::behave()
{
    CityNpc::behave();
    if (!_faker)
    {
        if (_currentState != dead)
        {
            if (rand() % 180 == 0 || (_speed.x == 0 && _speed.y == 0))
            {
                int dir = rand() % 4;
                switch (dir)
                {
                    case 0:
                        _speed.x = _speedLength;
                        _speed.y = 0;
                        _currentDirection = horizontal;
                        break;

                    case 1:
                        _speed.x = -_speedLength;
                        _speed.y = 0;
                        _currentDirection = horizontal;
                        break;

                    case 2:
                        _speed.x = 0;
                        _speed.y = _speedLength;
                        _currentDirection = vertical;
                        break;

                    default:
                        _speed.x = 0;
                        _speed.y = -_speedLength;
                        _currentDirection = vertical;
                        break;
                }
            }

            if (_speed.x != 0.f || _speed.y != 0.f)
            {
                bool freeNext = true;
                bool freeX = (_speed.x != 0.f);
                bool freeY = (_speed.y != 0.f);

                for (unsigned int j = 0; j < _lists.size(); j++)
                {
                    if (freeNext)
                        freeNext = !checkCollision(_speed, _lists[j]);
                    if (freeX)
                        freeX = !checkCollision(sf::Vector2f(copysign(_speedLength, _speed.x), 0.f), _lists[j]);
                    if (freeY)
                        freeY = !checkCollision(sf::Vector2f(0.f, copysign(_speedLength, _speed.y)), _lists[j]);
                }

                if (!freeNext)
                {
                    if (freeX && fabs(_speed.x) > sin(math::PI / 12.0) * _speedLength)
                        _speed = sf::Vector2f(copysign(_speedLength, _speed.x), 0.f);
                    else if (freeY && fabs(_speed.y) > sin(math::PI / 12.0) * _speedLength)
                        _speed = sf::Vector2f(0.f, copysign(_speedLength, _speed.y));
                    else
                        _speed = sf::Vector2f(0.f, 0.f);
                }
            }
            move();
            changeAnimation();
        }
        else
        {
            _currentAnimation->setTransparency(_timeToDie * 4);
            _timeToDie--;
            if (_timeToDie == 0)
                _room->setToDestroy(this);
        }
    }
}

void CityNpc2::changeAnimation()
{
    _currentAnimation = _animations[_currentDirection];
    Entity::changeAnimation();
}
