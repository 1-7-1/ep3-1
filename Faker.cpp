/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Faker.h"
#include "Animation.h"
#include "Room.h"
#include "RoomGraph.h"
#include "Player.h"
#include "MathConstants.h"
#include <math.h>

Faker::Faker(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox,
    std::vector<std::vector<Animation*>> animations, Player* player, RoomGraph* graph)
    : CollidableObject(position, origin, hitbox), Npc(position, origin, 1.f, hitbox),
    _player(player), _animations(animations), _currentDirection(down), _graph(graph)
{
    _currentState = moving;
    _currentAnimation = _animations[_currentState][_currentDirection];
    for (std::vector<Animation*> animVec: _animations)
        for (Animation* anim: animVec)
            anim->setOrigin(_origin);
}

Faker::~Faker()
{
    for (std::vector<Animation*> animVec: _animations)
        for (Animation* anim: animVec)
            delete anim;
}

void Faker::interact(Player* player)
{

}

void Faker::behave()
{
    sf::Vector2f roomSize = _room->getSize();
    sf::Vector2f targetPos = _player->getPosition();
    sf::Vector2f tempPos = _position;

    if (_position.x - (targetPos.x - roomSize.x) < targetPos.x - _position.x)
        tempPos.x += roomSize.x;
    else if (targetPos.x - (_position.x - roomSize.x) < _position.x - targetPos.x)
        tempPos.x -= roomSize.x;

    if (_position.y - (targetPos.y - roomSize.y) < targetPos.y - _position.y)
        tempPos.y += roomSize.y;
    else if (targetPos.y - (_position.y - roomSize.y) < _position.y - targetPos.y)
        tempPos.y -= roomSize.y;
    
    sf::Vector2f positionDiff = targetPos - tempPos;
    float diffLength = sqrt(positionDiff.x * positionDiff.x + positionDiff.y * positionDiff.y);
    _speed = _speedLength * positionDiff / diffLength;

    if (diffLength < 50)
    {
        _speedLength = 2.f;
        _graph->augmentVolume(117.5 - diffLength * 7 / 4);
    }

    sf::Vector2f selfPos = _position + _speed;
    
    sf::FloatRect adjHitbox = sf::FloatRect(selfPos.x + _hitbox.left,
                                                selfPos.y + _hitbox.top,
                                                _hitbox.width, _hitbox.height);

    if (_room->checkCollision(adjHitbox, _player))
        _room->changeRoom(2, sf::Vector2f(244.f, 120.f), true, 1);

    if (_speed.x != 0.f || _speed.y != 0.f)
    {
        bool freeNext = true;
        bool freeX = (_speed.x != 0.f);
        bool freeY = (_speed.y != 0.f);

        for (unsigned int j = 0; j < _lists.size(); j++)
        {
            if (freeNext)
                freeNext = !checkCollision(_speed, _lists[j]);
            if (freeX)
                freeX = !checkCollision(sf::Vector2f(copysign(_speedLength, _speed.x), 0.f), _lists[j]);
            if (freeY)
                freeY = !checkCollision(sf::Vector2f(0.f, copysign(_speedLength, _speed.y)), _lists[j]);
        }

        if (!freeNext)
        {
            if (freeX && fabs(_speed.x) > sin(math::PI / 12.0) * _speedLength)
                _speed = sf::Vector2f(copysign(_speedLength, _speed.x), 0.f);
            else if (freeY && fabs(_speed.y) > sin(math::PI / 12.0) * _speedLength)
                _speed = sf::Vector2f(0.f, copysign(_speedLength, _speed.y));
            else
                _speed = sf::Vector2f(0.f, 0.f);
        }
    }

    move();
    changeAnimation();
}

void Faker::changeAnimation()
{
    if (_speed == sf::Vector2f(0.f, 0.f))
        _currentState = idle;
    else
    {
        _currentState = moving;
        float direction = atan2(-_speed.y, _speed.x);
        float degrees = direction * 180.f / math::PI;

        if (degrees < 45.f && degrees >= -45.f)
            _currentDirection = right;
        else if (degrees < 135.f && degrees >= 45.f)
            _currentDirection = up;
        else if (degrees < -135.f || degrees >= 135.f)
            _currentDirection = left;
        else
            _currentDirection = down;
    }
    
    _currentAnimation = _animations[_currentState][_currentDirection];
    GameObject::changeAnimation();
}
