/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "CityNpc.h"
#include "ColorController.h"
#include "Animation.h"
#include "Player.h"
#include "Room.h"
#include "MathConstants.h"
#include <math.h>

unsigned int CityNpc::_killCount = 0;
bool CityNpc::_transforming = false;

CityNpc::CityNpc(sf::Vector2f position, sf::Vector2f origin, float speedLength, sf::FloatRect hitbox,
    ColorController* controller, std::vector<std::vector<Animation*>> fakerAnimations)
    : Npc(position, origin, speedLength, hitbox),
    _fakerAnimationDirection(down), _fakerAnimations(fakerAnimations), _faker(false), _colorController(controller)
{

}

CityNpc::~CityNpc()
{
    for (std::vector<Animation*> animVec: _fakerAnimations)
        for (Animation* anim: animVec)
            delete anim;
}

void CityNpc::interact(Player* player)
{
    if (_currentState != dead)
    {
        if (player->getEffect() == machete && player->getState() == action)
        {
            if (!_faker)
            {
                _room->createSpecialEffect(_position + sf::Vector2f(0.f, 0.01f), sf::Vector2f(0.f, getSize().y / 4.f), blood);
                _timeToDie = 63;
                _currentState = dead;
                _killCount++;
                _colorController->remove(this);
                _transforming = true;
            }
            else
            {
                player->startScript(shortWait);
                _room->setToDestroy(this);
                _room->changeRoom(13, sf::Vector2f(), false, 1);
            }
        }
    }
}

void CityNpc::behave()
{
    if (_currentState != dead)
    {
        if (_transforming && !_faker && !_room->isInView(this))
        {
            _transforming = false;
            if ((unsigned)rand() % 100 < _killCount)
            {
                _faker = true;
                _colorController->remove(this);
                _hitbox = sf::FloatRect(-6.f, -2.f, 12.f, 4.f);
                _origin = sf::Vector2f(12.f, 29.f);
                _speedLength = 1.f;
                for (std::vector<Animation*> animVec: _fakerAnimations)
                    for (Animation* anim: animVec)
                        anim->setOrigin(_origin);
            }
        }
    }
    if (_faker)
    {
        if (_currentState == idle && rand() % 60 == 0)
        {
            int degrees = rand() % 360;
            if (degrees < 45 || degrees >= 315)
                _fakerAnimationDirection = right;
            else if (degrees < 135 && degrees >= 45)
                _fakerAnimationDirection = up;
            else if (degrees < 225 && degrees >= 135)
                _fakerAnimationDirection = left;
            else
                _fakerAnimationDirection = down;
            _fakerDirection = float(degrees) * math::PI / 180.f;
            _speed.x = _speedLength * cos(_fakerDirection);
            _speed.y = -_speedLength * sin(_fakerDirection);
            _currentState = moving;
        }
        else
        {
            if (rand() % 60 == 0)
            {
                _speed = sf::Vector2f(0.f, 0.f);
                _currentState = idle;
            }
            else if (_currentState == moving)
            {
                _speed.x = _speedLength * cos(_fakerDirection);
                _speed.y = -_speedLength * sin(_fakerDirection);
            }
        }

        if (_speed.x != 0.f || _speed.y != 0.f)
        {
            bool freeNext = true;
            bool freeX = (_speed.x != 0.f);
            bool freeY = (_speed.y != 0.f);

            for (unsigned int j = 0; j < _lists.size(); j++)
            {
                if (freeNext)
                    freeNext = !checkCollision(_speed, _lists[j]);
                if (freeX)
                    freeX = !checkCollision(sf::Vector2f(copysign(_speedLength, _speed.x), 0.f), _lists[j]);
                if (freeY)
                    freeY = !checkCollision(sf::Vector2f(0.f, copysign(_speedLength, _speed.y)), _lists[j]);
            }

            if (!freeNext)
            {
                if (freeX && fabs(_speed.x) > sin(math::PI / 12.0) * _speedLength)
                    _speed = sf::Vector2f(copysign(_speedLength, _speed.x), 0.f);
                else if (freeY && fabs(_speed.y) > sin(math::PI / 12.0) * _speedLength)
                    _speed = sf::Vector2f(0.f, copysign(_speedLength, _speed.y));
                else
                    _speed = sf::Vector2f(0.f, 0.f);
            }
        }
        move();
        if (_speed == sf::Vector2f(0.f, 0.f))
            _currentState = idle;
        else
        {
            _currentState = moving;
            float direction = atan2(-_speed.y, _speed.x);
            float degrees = direction * 180.f / math::PI;

            if (degrees < 45.f && degrees >= -45.f)
                _fakerAnimationDirection = right;
            else if (degrees < 135.f && degrees >= 45.f)
                _fakerAnimationDirection = up;
            else if (degrees < -135.f || degrees >= 135.f)
                _fakerAnimationDirection = left;
            else
                _fakerAnimationDirection = down;
        }

        _currentAnimation = _fakerAnimations[_currentState][_fakerAnimationDirection];
        _currentAnimation->animate();
    }
}
