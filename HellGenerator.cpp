/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "HellGenerator.h"
#include "Room.h"
#include "TileCalculator.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "Decoration.h"
#include "Wall.h"
#include "Player.h"
#include "HellRoom.h"
#include "AutoTransition.h"

HellGenerator::HellGenerator()
{
    
}

HellGenerator::~HellGenerator()
{

}

void HellGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/tilesets/5.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/tilesets/6.png", sf::Color(255, 0, 255, 255));
    _isLoaded = true;
}

Decoration* HellGenerator::generateBackground() const
{
    TileCalculator bgCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(320, 240));

    Animation* anim = new Animation(bgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(bg));

    return new Decoration(sf::Vector2f(), sf::Vector2f(), sf::Vector2f(), anim);
}

Decoration* HellGenerator::generateForeground() const
{
    TileCalculator fgCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(320, 240));

    Animation* anim = new Animation(fgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(fg));

    return new Decoration(sf::Vector2f(), sf::Vector2f(), sf::Vector2f(), anim);
}

std::vector<Wall*> HellGenerator::generateWalls() const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    std::vector<Wall*> walls;

    std::vector<std::pair<sf::Vector2f, sf::FloatRect>> wallParams = {
        //left side
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(127.f, 20.f), sf::FloatRect(0.f, 0.f, 4.f, 14.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(129.f, 0.f), sf::FloatRect(0.f, 0.f, 4.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(129.f, 4.f), sf::FloatRect(0.f, 0.f, 3.f, 18.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(128.f, 9.f), sf::FloatRect(0.f, 0.f, 4.f, 15.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(126.f, 33.f), sf::FloatRect(0.f, 0.f, 4.f, 13.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(124.f, 45.f), sf::FloatRect(0.f, 0.f, 4.f, 12.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(122.f, 53.f), sf::FloatRect(0.f, 0.f, 6.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(121.f, 57.f), sf::FloatRect(0.f, 0.f, 6.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(120.f, 60.f), sf::FloatRect(0.f, 0.f, 6.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(119.f, 64.f), sf::FloatRect(0.f, 0.f, 6.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(118.f, 67.f), sf::FloatRect(0.f, 0.f, 6.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(117.f, 70.f), sf::FloatRect(0.f, 0.f, 6.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(116.f, 73.f), sf::FloatRect(0.f, 0.f, 6.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(115.f, 76.f), sf::FloatRect(0.f, 0.f, 6.f, 10.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(114.f, 79.f), sf::FloatRect(0.f, 0.f, 6.f, 10.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(113.f, 83.f), sf::FloatRect(0.f, 0.f, 6.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(110.f, 93.f), sf::FloatRect(0.f, 0.f, 8.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(112.f, 97.f), sf::FloatRect(0.f, 0.f, 5.f, 3.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(106.f, 98.f), sf::FloatRect(0.f, 0.f, 10.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(111.f, 103.f), sf::FloatRect(0.f, 0.f, 4.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(106.f, 107.f), sf::FloatRect(0.f, 0.f, 8.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(107.f, 109.f), sf::FloatRect(0.f, 0.f, 6.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(103.f, 112.f), sf::FloatRect(0.f, 0.f, 9.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(101.f, 116.f), sf::FloatRect(0.f, 0.f, 10.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(98.f, 120.f), sf::FloatRect(0.f, 0.f, 12.f, 3.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(101.f, 118.f), sf::FloatRect(0.f, 0.f, 8.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(103.f, 121.f), sf::FloatRect(0.f, 0.f, 7.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(102.f, 124.f), sf::FloatRect(0.f, 0.f, 7.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(100.f, 130.f), sf::FloatRect(0.f, 0.f, 7.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(98.f, 135.f), sf::FloatRect(0.f, 0.f, 8.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(97.f, 140.f), sf::FloatRect(0.f, 0.f, 7.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(97.f, 146.f), sf::FloatRect(0.f, 0.f, 5.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(95.f, 149.f), sf::FloatRect(0.f, 0.f, 6.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(94.f, 153.f), sf::FloatRect(0.f, 0.f, 6.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(94.f, 155.f), sf::FloatRect(0.f, 0.f, 5.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(95.f, 160.f), sf::FloatRect(0.f, 0.f, 3.f, 3.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(92.f, 161.f), sf::FloatRect(0.f, 0.f, 5.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(92.f, 164.f), sf::FloatRect(0.f, 0.f, 4.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(86.f, 165.f), sf::FloatRect(0.f, 0.f, 9.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(85.f, 168.f), sf::FloatRect(0.f, 0.f, 7.f, 12.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(83.f, 177.f), sf::FloatRect(0.f, 0.f, 6.f, 10.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(86.f, 186.f), sf::FloatRect(0.f, 0.f, 2.f, 3.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(78.f, 186.f), sf::FloatRect(0.f, 0.f, 9.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(71.f, 190.f), sf::FloatRect(0.f, 0.f, 14.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(76.f, 195.f), sf::FloatRect(0.f, 0.f, 8.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(75.f, 200.f), sf::FloatRect(0.f, 0.f, 6.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(73.f, 205.f), sf::FloatRect(0.f, 0.f, 4.f, 11.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(72.f, 215.f), sf::FloatRect(0.f, 0.f, 3.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(70.f, 216.f), sf::FloatRect(0.f, 0.f, 4.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(65.f, 221.f), sf::FloatRect(0.f, 0.f, 6.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(59.f, 223.f), sf::FloatRect(0.f, 0.f, 11.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(64.f, 230.f), sf::FloatRect(0.f, 0.f, 3.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(54.f, 231.f), sf::FloatRect(0.f, 0.f, 12.f, 9.f)),

        //right side
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(187.f, 0.f), sf::FloatRect(0.f, 0.f, 4.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(188.f, 4.f), sf::FloatRect(0.f, 0.f, 7.f, 18.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(189.f, 20.f), sf::FloatRect(0.f, 0.f, 6.f, 14.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(190.f, 33.f), sf::FloatRect(0.f, 0.f, 7.f, 13.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(191.f, 45.f), sf::FloatRect(0.f, 0.f, 9.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(192.f, 51.f), sf::FloatRect(0.f, 0.f, 7.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(193.f, 58.f), sf::FloatRect(0.f, 0.f, 7.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(194.f, 60.f), sf::FloatRect(0.f, 0.f, 8.f, 10.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(195.f, 68.f), sf::FloatRect(0.f, 0.f, 10.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(196.f, 72.f), sf::FloatRect(0.f, 0.f, 8.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(197.f, 73.f), sf::FloatRect(0.f, 0.f, 9.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(198.f, 79.f), sf::FloatRect(0.f, 0.f, 8.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(199.f, 86.f), sf::FloatRect(0.f, 0.f, 5.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(200.f, 89.f), sf::FloatRect(0.f, 0.f, 7.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(201.f, 91.f), sf::FloatRect(0.f, 0.f, 14.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(202.f, 95.f), sf::FloatRect(0.f, 0.f, 11.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(204.f, 100.f), sf::FloatRect(0.f, 0.f, 9.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(205.f, 104.f), sf::FloatRect(0.f, 0.f, 11.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(207.f, 110.f), sf::FloatRect(0.f, 0.f, 14.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(208.f, 116.f), sf::FloatRect(0.f, 0.f, 17.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(210.f, 121.f), sf::FloatRect(0.f, 0.f, 7.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(211.f, 124.f), sf::FloatRect(0.f, 0.f, 11.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(213.f, 130.f), sf::FloatRect(0.f, 0.f, 10.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(214.f, 136.f), sf::FloatRect(0.f, 0.f, 17.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(216.f, 142.f), sf::FloatRect(0.f, 0.f, 10.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(219.f, 146.f), sf::FloatRect(0.f, 0.f, 10.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(221.f, 151.f), sf::FloatRect(0.f, 0.f, 10.f, 10.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(223.f, 157.f), sf::FloatRect(0.f, 0.f, 21.f, 10.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(225.f, 166.f), sf::FloatRect(0.f, 0.f, 10.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(228.f, 171.f), sf::FloatRect(0.f, 0.f, 18.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(231.f, 179.f), sf::FloatRect(0.f, 0.f, 10.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(233.f, 186.f), sf::FloatRect(0.f, 0.f, 11.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(236.f, 193.f), sf::FloatRect(0.f, 0.f, 9.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(239.f, 199.f), sf::FloatRect(0.f, 0.f, 14.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(243.f, 205.f), sf::FloatRect(0.f, 0.f, 14.f, 11.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(245.f, 213.f), sf::FloatRect(0.f, 0.f, 5.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(246.f, 216.f), sf::FloatRect(0.f, 0.f, 10.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(250.f, 223.f), sf::FloatRect(0.f, 0.f, 11.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(254.f, 230.f), sf::FloatRect(0.f, 0.f, 13.f, 10.f)) };

    for (unsigned int i = 0; i < wallParams.size(); i++)
    {
        Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));

        walls.push_back(new Wall(wallParams[i].first, sf::Vector2f(), anim, wallParams[i].second));
    }

    return walls;
}

std::vector<AutoTransition*> HellGenerator::generateTiles(Player* player) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    std::vector<AutoTransition*> tiles;

    {
        Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));

        tiles.push_back(new AutoTransition(sf::Vector2f(133.f, -7.f), sf::Vector2f(), sf::FloatRect(0.f, 0.f, 54.f, 8.f),
            anim, 24, sf::Vector2f(168.f, 152.f), true, player));
    }

    {
        Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));

        tiles.push_back(new AutoTransition(sf::Vector2f(66.f, 239.f), sf::Vector2f(), sf::FloatRect(0.f, 0.f, 188.f, 8.f),
            anim, 23, sf::Vector2f(160.f, 232.f), true, player));
    }

    return tiles;
}

Room* HellGenerator::generateHell(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;
    std::vector<GameObject*> foregrounds;

    backgrounds.push_back(generateBackground());
    foregrounds.push_back(generateForeground());

    std::vector<Wall*> walls = generateWalls();
    collidables.insert(collidables.end(), walls.begin(), walls.end());
    
    std::vector<AutoTransition*> tiles = generateTiles(player);
    floors.insert(floors.end(), tiles.begin(), tiles.end());

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, foregrounds, std::vector<GameObject*>() };

    return new HellRoom(player, graph, objects);
}
