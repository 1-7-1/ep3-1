/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Spaceship.h"
#include "Animation.h"
#include <math.h>

Spaceship::Spaceship(sf::Vector2f position, sf::Vector2f origin, std::vector<Animation*> animations)
    : GameObject(position, origin), _animations(animations), _timer(0)
{
    _currentAnimation = _animations[0];
    for (Animation* anim: _animations)
        anim->setOrigin(_origin);
}

Spaceship::~Spaceship()
{
    for (Animation* anim: _animations)
        delete anim;
}

void Spaceship::behave()
{
    if (_timer < 3600)
    {
        if (_timer > 1800)
        {
            if (_timer > 2040)
                _speed.y = (float)(-_timer + 2040) / 12.f;
            else
                _speed.y = 0.1f;

            if (_timer > 2080)
                _currentAnimation = _animations[3];
            else if (_timer > 2046)
                _currentAnimation = _animations[2];
            else
                _currentAnimation = _animations[1];
            
            float scale = pow((float)(_timer - 1800) / 100.f, 2.f) / 9.f;
            _currentAnimation->setScale(scale, scale);
        }
        else
            _currentAnimation->setScale(0.f, 0.f);
        _timer++;
        _position += _speed;
        changeAnimation();
    }
}
