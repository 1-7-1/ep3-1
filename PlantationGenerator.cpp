/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "PlantationGenerator.h"
#include "Room.h"
#include "TileCalculator.h"
#include "AnimationFrames.h"
#include "Animation.h"
#include "Wall.h"
#include "AutoTransition.h"
#include "Decoration.h"
#include "Plant.h"
#include "Player.h"
#include "Bubble.h"

PlantationGenerator::PlantationGenerator()
{

}

PlantationGenerator::~PlantationGenerator()
{

}

void PlantationGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/tilesets/15.png", sf::Color(63, 0, 63, 255));
    _db.loadTexture("graphics/char/10.png", sf::Color(63, 0, 63, 255));
    _db.loadTexture("graphics/object/2.png", sf::Color(255, 0, 255, 255));
    _isLoaded = true;
}

std::vector<Decoration*> PlantationGenerator::generateClouds() const
{
    TileCalculator bgCalc = TileCalculator(sf::Vector2u(0, 80), sf::Vector2u(322, 64));
    
    std::vector<Decoration*> backs;

    for (int i = 0; i < 4; i++)
    {
        Animation* anim = new Animation(bgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})),
            _db.getTexture(tileset));
        backs.push_back(new Decoration(sf::Vector2f(-160.f + i * 320.f, 96.f), sf::Vector2f(0.f, 0.f),
            sf::Vector2f(-0.1f, 0.f), anim));
    }

    return backs;
}

std::vector<Decoration*> PlantationGenerator::generateCity() const
{
    TileCalculator bgCalc = TileCalculator(sf::Vector2u(0, 144), sf::Vector2u(320, 48));
    TileCalculator fillCalc = TileCalculator(sf::Vector2u(0, 224), sf::Vector2u(320, 16));
    
    std::vector<Decoration*> backs;

    for (int i = 0; i < 4; i++)
    {
        Animation* anim = new Animation(bgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})),
            _db.getTexture(tileset));
        backs.push_back(new Decoration(sf::Vector2f(i * 320.f, 160.f), sf::Vector2f(0.f, 0.f),
            sf::Vector2f(0.f, 0.f), anim));
    }

    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 4; i++)
        {
            Animation* anim = new Animation(fillCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                std::vector<unsigned int>({0})),
                _db.getTexture(tileset));
            backs.push_back(new Decoration(sf::Vector2f(i * 320.f, 192.f + j * 16.f), sf::Vector2f(0.f, 0.f),
                sf::Vector2f(0.f, 0.f), anim));
        }

    return backs;
}

std::vector<Wall*> PlantationGenerator::generateBarrier() const
{
    TileCalculator bgCalc = TileCalculator(sf::Vector2u(144, 0), sf::Vector2u(32, 32));
    
    std::vector<Wall*> barriers;

    for (int i = 0; i < 40; i++)
    {
        Animation* anim = new Animation(bgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})),
            _db.getTexture(tileset));
        barriers.push_back(new Wall(sf::Vector2f(i * 32.f, 226.f), sf::Vector2f(0.f, 18.f),
            anim, sf::FloatRect(-16.f, 0.f, 32.f, 4.f)));
    }

    return barriers;
}

std::vector<Decoration*> PlantationGenerator::generateFloor() const
{
    TileCalculator floorCalc = TileCalculator(sf::Vector2u(0, 0), sf::Vector2u(16, 16));
    
    std::vector<Decoration*> backs;

    //top sidewalk
    for (int i = 0; i < 80; i++)
    {
        Animation* anim = new Animation(floorCalc(std::vector<sf::Vector2u>({sf::Vector2u(7, 0)}),
            std::vector<unsigned int>({0})),
            _db.getTexture(tileset));
        backs.push_back(new Decoration(sf::Vector2f(i * 16.f, 224.f), sf::Vector2f(0.f, 0.f),
            sf::Vector2f(0.f, 0.f), anim));
    }

    //bottom 2 screens
    for (int j = 1; j < 3; j++)
        for (int i = 0; i < 4; i++)
        {
            //dirt
            for (int k = 0; k < 16; k++)
                for (int l = 0; l < 15; l++)
                {
                    Animation* anim = new Animation(floorCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 1)}),
                        std::vector<unsigned int>({0})),
                        _db.getTexture(tileset));
                    backs.push_back(new Decoration(sf::Vector2f(i * 320.f + k * 16.f, j * 240.f + l * 16.f), sf::Vector2f(0.f, 0.f),
                        sf::Vector2f(0.f, 0.f), anim));
                }

            //left sidewalk
            for (int l = 0; l < 15; l++)
            {
                Animation* anim = new Animation(floorCalc(std::vector<sf::Vector2u>({sf::Vector2u(5, 0)}),
                    std::vector<unsigned int>({0})),
                    _db.getTexture(tileset));
                backs.push_back(new Decoration(sf::Vector2f(i * 320.f + 256.f, j * 240.f + l * 16.f), sf::Vector2f(0.f, 0.f),
                    sf::Vector2f(0.f, 0.f), anim));
            }

            //roght sidewalk
            for (int l = 0; l < 15; l++)
            {
                Animation* anim = new Animation(floorCalc(std::vector<sf::Vector2u>({sf::Vector2u(5, 0)}),
                    std::vector<unsigned int>({0})),
                    _db.getTexture(tileset));
                backs.push_back(new Decoration(sf::Vector2f(i * 320.f + 304.f, j * 240.f + l * 16.f), sf::Vector2f(0.f, 0.f),
                    sf::Vector2f(0.f, 0.f), anim));
            }

            //road
            for (int k = 17; k < 19; k++)
                for (int l = 0; l < 15; l++)
                {
                    Animation* anim = new Animation(floorCalc(std::vector<sf::Vector2u>({sf::Vector2u(6, 0)}),
                        std::vector<unsigned int>({0})),
                        _db.getTexture(tileset));
                    backs.push_back(new Decoration(sf::Vector2f(i * 320.f + k * 16.f, j * 240.f + l * 16.f), sf::Vector2f(0.f, 0.f),
                        sf::Vector2f(0.f, 0.f), anim));
                }
        }

    return backs;
}

std::vector<Plant*> PlantationGenerator::generatePlants() const
{
    TileCalculator plantCalc = TileCalculator(sf::Vector2u(0, 0), sf::Vector2u(16, 64));
    
    std::vector<Plant*> plants;

    //bottom 2 screens
    for (int j = 1; j < 3; j++)
        for (int i = 0; i < 4; i++)
        {
            if (j == 0)
                if (i == 0 || i == 1 || i == 3)
                    for (int k = 0; k < 16; k++)
                        for (int l = 0; l < 15; l++)
                        {
                            Animation* anim = new Animation(plantCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                                std::vector<unsigned int>({0})),
                                _db.getTexture(plant));
                            plants.push_back(new Plant(sf::Vector2f(i * 320.f + k * 16.f + 8.f, j * 240.f + l * 16.f + 8.f)
                                + sf::Vector2f((rand() % 9) - 4, (rand() % 9) - 4),
                                sf::Vector2f(8.f, 60.f), sf::FloatRect(-4.f, -1.f, 8.f, 4.f), anim));
                        }
                else
                    for (int k = 0; k < 16; k++)
                        for (int l = 0; l < 15; l++)
                        {
                            Animation* anim = new Animation(plantCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 0)}),
                                std::vector<unsigned int>({0})),
                                _db.getTexture(plant));
                            plants.push_back(new Plant(sf::Vector2f(i * 320.f + k * 16.f + 8.f, j * 240.f + l * 16.f + 8.f)
                                + sf::Vector2f((rand() % 9) - 4, (rand() % 9) - 4),
                                sf::Vector2f(8.f, 60.f), sf::FloatRect(-4.f, -1.f, 8.f, 4.f), anim));
                        }
            else if (j == 1)
                if (i == 0)
                    for (int k = 0; k < 16; k++)
                        for (int l = 0; l < 15; l++)
                        {
                            Animation* anim = new Animation(plantCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                                std::vector<unsigned int>({0})),
                                _db.getTexture(plant));
                            plants.push_back(new Plant(sf::Vector2f(i * 320.f + k * 16.f + 8.f, j * 240.f + l * 16.f + 8.f)
                                + sf::Vector2f((rand() % 9) - 4, (rand() % 9) - 4),
                                sf::Vector2f(8.f, 60.f), sf::FloatRect(-4.f, -1.f, 8.f, 4.f), anim));
                        }
                else if (i == 1)
                    for (int k = 0; k < 16; k++)
                        for (int l = 0; l < 15; l++)
                        {
                            Animation* anim = new Animation(plantCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 0)}),
                                std::vector<unsigned int>({0})),
                                _db.getTexture(plant));
                            plants.push_back(new Plant(sf::Vector2f(i * 320.f + k * 16.f + 8.f, j * 240.f + l * 16.f + 8.f)
                                + sf::Vector2f((rand() % 9) - 4, (rand() % 9) - 4),
                                sf::Vector2f(8.f, 60.f), sf::FloatRect(-4.f, -1.f, 8.f, 4.f), anim));
                        }
                else if (i == 2)
                    for (int k = 0; k < 16; k++)
                        for (int l = 0; l < 15; l++)
                        {
                            Animation* anim = new Animation(plantCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 0)}),
                                std::vector<unsigned int>({0})),
                                _db.getTexture(plant));
                            plants.push_back(new Plant(sf::Vector2f(i * 320.f + k * 16.f + 8.f, j * 240.f + l * 16.f + 8.f)
                                + sf::Vector2f((rand() % 9) - 4, (rand() % 9) - 4),
                                sf::Vector2f(8.f, 60.f), sf::FloatRect(-4.f, -1.f, 8.f, 4.f), anim));
                        }
                else
                    for (int k = 0; k < 16; k++)
                        for (int l = 0; l < 15; l++)
                        {
                            Animation* anim = new Animation(plantCalc(std::vector<sf::Vector2u>({sf::Vector2u(l % 3, 1)}),
                                std::vector<unsigned int>({0})),
                                _db.getTexture(plant));
                            plants.push_back(new Plant(sf::Vector2f(i * 320.f + k * 16.f + 8.f, j * 240.f + l * 16.f + 8.f)
                                + sf::Vector2f((rand() % 9) - 4, (rand() % 9) - 4),
                                sf::Vector2f(8.f, 60.f), sf::FloatRect(-4.f, -1.f, 8.f, 4.f), anim));
                        }
            else
                if (i == 0)
                    for (int k = 0; k < 16; k++)
                        for (int l = 0; l < 15; l++)
                        {
                            Animation* anim = new Animation(plantCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 0)}),
                                std::vector<unsigned int>({0})),
                                _db.getTexture(plant));
                            plants.push_back(new Plant(sf::Vector2f(i * 320.f + k * 16.f + 8.f, j * 240.f + l * 16.f + 8.f)
                                + sf::Vector2f((rand() % 9) - 4, (rand() % 9) - 4),
                                sf::Vector2f(8.f, 60.f), sf::FloatRect(-4.f, -1.f, 8.f, 4.f), anim));
                        }
                else if (i == 1)
                    for (int k = 0; k < 16; k++)
                        for (int l = 0; l < 15; l++)
                        {
                            Animation* anim = new Animation(plantCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                                std::vector<unsigned int>({0})),
                                _db.getTexture(plant));
                            plants.push_back(new Plant(sf::Vector2f(i * 320.f + k * 16.f + 8.f, j * 240.f + l * 16.f + 8.f)
                                + sf::Vector2f((rand() % 9) - 4, (rand() % 9) - 4),
                                sf::Vector2f(8.f, 60.f), sf::FloatRect(-4.f, -1.f, 8.f, 4.f), anim));
                        }
                else if (i == 2)
                    for (int k = 0; k < 16; k++)
                        for (int l = 0; l < 15; l++)
                        {
                            Animation* anim = new Animation(plantCalc(std::vector<sf::Vector2u>({sf::Vector2u(l % 3, 0)}),
                                std::vector<unsigned int>({0})),
                                _db.getTexture(plant));
                            plants.push_back(new Plant(sf::Vector2f(i * 320.f + k * 16.f + 8.f, j * 240.f + l * 16.f + 8.f)
                                + sf::Vector2f((rand() % 9) - 4, (rand() % 9) - 4),
                                sf::Vector2f(8.f, 60.f), sf::FloatRect(-4.f, -1.f, 8.f, 4.f), anim));
                        }
                else
                    for (int k = 0; k < 16; k++)
                        for (int l = 0; l < 15; l++)
                        {
                            Animation* anim = new Animation(plantCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 0)}),
                                std::vector<unsigned int>({0})),
                                _db.getTexture(plant));
                            plants.push_back(new Plant(sf::Vector2f(i * 320.f + k * 16.f + 8.f, j * 240.f + l * 16.f + 8.f)
                                + sf::Vector2f((rand() % 9) - 4, (rand() % 9) - 4),
                                sf::Vector2f(8.f, 60.f), sf::FloatRect(-4.f, -1.f, 8.f, 4.f), anim));
                        }
        }

    return plants;
}

std::vector<AutoTransition*> PlantationGenerator::generateTiles(Player* player) const
{
    TileCalculator calc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    std::vector<AutoTransition*> tiles;

    for (int i = 0; i < 80; i++)
    {
        Animation* anim = new Animation(calc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                                std::vector<unsigned int>({0})), _db.getTexture(invisible));
        tiles.push_back(new AutoTransition(sf::Vector2f(i * 16.f, 720.f), sf::Vector2f(),
            sf::FloatRect(0.f, 0.f, 16.f, 16.f), anim, 14, sf::Vector2f(1408.f, 632.f), true, player));
    }

    return tiles;
}

Bubble* PlantationGenerator::generateBubble() const
{
    TileCalculator bubCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    std::vector<sf::Vector2u> offsets;
    for (unsigned int j = 0; j < 2; j++)
        for (unsigned int i = 0; i < 4; i++)
            offsets.push_back(sf::Vector2u(i, j));

    std::vector<unsigned int> times;
    for (unsigned int i = 0; i < 8; i++)
        times.push_back(8);

    Animation* anim = new Animation(bubCalc(offsets, times), _db.getTexture(bubble));

    return new Bubble(sf::Vector2f(928.f, 240.f), sf::Vector2f(8.f, 13.f), sf::FloatRect(-8.f, -3.f, 16.f, 6.f), anim);
}

Room* PlantationGenerator::generatePlantation(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;
    std::vector<GameObject*> foregrounds;

    std::vector<Decoration*> clouds = generateClouds();
    backgrounds.insert(backgrounds.end(), clouds.begin(), clouds.end());

    std::vector<Decoration*> city = generateCity();
    backgrounds.insert(backgrounds.end(), city.begin(), city.end());
    
    std::vector<Wall*> walls = generateBarrier();
    collidables.insert(collidables.end(), walls.begin(), walls.end());
    
    std::vector<Decoration*> floor = generateFloor();
    floors.insert(floors.end(), floor.begin(), floor.end());
    
    std::vector<Plant*> plants = generatePlants();
    collidables.insert(collidables.end(), plants.begin(), plants.end());
    interactibles.insert(interactibles.end(), plants.begin(), plants.end());
    
    std::vector<AutoTransition*> tiles = generateTiles(player);
    floors.insert(floors.end(), tiles.begin(), tiles.end());

    Bubble* bub = generateBubble();
    interactibles.push_back(bub);
    collidables.push_back(bub);

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, foregrounds, std::vector<GameObject*>() };

    return new Room(player, graph, nullptr, objects, sf::Vector2f(_roomWidth, _roomHeight),
        sf::Vector2<bool>(true, false));
}
