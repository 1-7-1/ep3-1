/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "MainMenu.h"
#include "RoomGraph.h"
#include "MenuCursor.h"

MainMenu::MainMenu(std::vector<Animation*> animations, MenuCursor* cursor, RoomGraph* graph)
    : Menu(animations, cursor), _nbOfItems(3), _graph(graph)
{
    _currentAnimation = _animations[0];
}

MainMenu::~MainMenu()
{

}

void MainMenu::resetInputs()
{
    _upPressed = true;
    _downPressed = true;
    _zPressed = true;
}

void MainMenu::handleInput()
{
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        if (!_upPressed)
        {
            if (_cursorPosition == 0)
                _cursorPosition = _nbOfItems - 1;
            else
                _cursorPosition--;
            _upPressed = true;
        }
    }
    else
        _upPressed = false;
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        if (!_downPressed)
        {
            _cursorPosition++;
            _cursorPosition %= _nbOfItems;
            _downPressed = true;
        }
    }
    else
        _downPressed = false;
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
    {
        if (!_zPressed)
        {
            switch (_cursorPosition)
            {
            case 0:
                _graph->setState(playing);
                break;

            case 1:
                _graph->load();
                _graph->setState(playing);
                break;

            case 2:
                _graph->setState(quitting);
                break;
            
            default:
                break;
            }
        }
    }
    else
        _zPressed = false;
}

void MainMenu::placeCursors()
{
    _cursor->setPosition(_position + sf::Vector2f(128.f, 176.f + _cursorPosition * 16.f));
}

void MainMenu::behaveYourself()
{
    if (_active)
        placeCursors();
}

void MainMenu::leave()
{
    resetInputs();
    _graph->setState(quitting);
}
