/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BED_H
#define BED_H

#include "InteractibleObject.h"

class Bed: public InteractibleObject
{
    private:
        enum BedState { free, occupied };
        BedState _state;
        std::vector<Animation*> _animations;
        unsigned int _destination;
        sf::Vector2f _transitionVector;
        bool _transitionAbsolute;

    public:
        Bed(sf::Vector2f position, sf::Vector2f origin, std::vector<Animation*> animations,
            sf::FloatRect hitbox, unsigned int destination, sf::Vector2f transitionVec, bool absolute);
        ~Bed();

        virtual void interact(Player* player);
        virtual void behave();
};

#endif
