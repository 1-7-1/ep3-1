/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STAIRS_4_H
#define STAIRS_4_H

#include "GameObjectGenerator.h"

class Room;
class RoomGraph;
class Player;
class Decoration;
class AutoTransition;
class Stairs;

class Stairs4Generator: public GameObjectGenerator
{
    private:
        enum Graphics { invisible, bg };
        AutoTransition* generateTile(const sf::Vector2f position, const unsigned int destination,
            const sf::Vector2f transitionVec, const bool absolute, Player* _player) const;
        Decoration* generateBackground(sf::Vector2f position) const;

        std::vector<Stairs*> generateStairs(sf::Vector2f position, Player* player) const;

        virtual void load();

    public:
        Stairs4Generator();
        ~Stairs4Generator();

        Room* generateStairs4(Player* player, RoomGraph* graph);
};

#endif
