/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "RoomGenerator.h"

RoomGenerator::RoomGenerator(RoomGraph* graph): _graph(graph)
{
    
}

RoomGenerator::~RoomGenerator()
{
    
}

Player* RoomGenerator::generatePlayer()
{
    return _playerGen.generatePlayer(sf::Vector2f(160.f, 130.f));
}

MainMenu* RoomGenerator::generateMainMenu()
{
    return _menuGen.generateMainMenu(_graph);
}

PauseMenu* RoomGenerator::generatePauseMenu(Player* player)
{
    return _menuGen.generatePauseMenu(player, _graph);
}

Room* RoomGenerator::generateRoom(unsigned int id, Player* player)
{
    switch (id)
    {
        case 2:
            return _bedroomGen.generateBedroom(player, _graph);
            break;

        case 3:
            return _balcGen.generateBalcony(player, _graph);
            break;

        case 4:
            return _spikeGen.generateSpikeArea(player, _graph);
            break;

        case 5:
            return _bigSpikeGen.generateBigSpikeArea(player, _graph);
            break;

        case 6:
            return _stairs1Gen.generateStairs1(player, _graph);
            break;

        case 7:
            return _stairs2Gen.generateStairs2(player, _graph);
            break;

        case 8:
            return _stairs3Gen.generateStairs3(player, _graph);
            break;

        case 9:
            return _stairs4Gen.generateStairs4(player, _graph);
            break;

        case 10:
            return _stairs5Gen.generateStairs5(player, _graph);
            break;

        case 11:
            return _jungleGen.generateJungle(player, _graph);
            break;

        case 12:
            return _cityGen.generateCity(player, _graph);
            break;

        case 13:
            return _desolateCityGen.generateDesolateCity(player, _graph);
            break;

        case 14:
            return _mazeGen.generateMaze(player, _graph);
            break;

        case 15:
            return _wlGen.generateWasteland(player, _graph);
            break;

        case 16:
            return _waterGen.generateUnderwater(player, _graph);
            break;
        
        case 17:
            return _skyGen.generateSky(player, _graph);
            break;

        case 18:
            return _houseGen.generateHouse(player, _graph);
            break;

        case 19:
            return _skyBackGen.generateSkyBack(player, _graph);
            break;
        
        case 20:
            return _plantGen.generatePlantation(player, _graph);
            break;

        case 21:
            return _darkGen.generateDarkWorld(player, _graph);
            break;

        case 22:
            return _hellGen.generateHell(player, _graph);
            break;

        case 23:
            return _hellPedGen.generateHell(player, _graph);
            break;

        case 24:
            return _hellEntryGen.generateHell(player, _graph);
            break;

        case 25:
            return _spaceGen.generateSpace(player, _graph);
            break;

        case 26:
            return _spaceHallGen.generateHall(player, _graph);
            break;

        case 27:
            return _spaceViewGen.generateView(player, _graph);
            break;

        case 28:
            return _end1Gen.generateEnd1(player, _graph);
            break;

        case 29:
            return _end2Gen.generateEnd2(player, _graph);
            break;
        
        default:
            return _bedroomGen.generateBedroom(player, _graph);
            break;
    }
}

void RoomGenerator::unloadRoom(unsigned int id)
{
    switch (id)
    {
        case 2:
            _bedroomGen.unload();
            break;

        case 3:
            _balcGen.unload();
            break;

        case 4:
            _spikeGen.unload();
            break;

        case 5:
            _bigSpikeGen.unload();
            break;

        case 6:
            _stairs1Gen.unload();
            break;

        case 7:
            _stairs2Gen.unload();
            break;

        case 8:
            _stairs3Gen.unload();
            break;

        case 9:
            _stairs4Gen.unload();
            break;

        case 10:
            _stairs5Gen.unload();
            break;

        case 11:
            _jungleGen.unload();
            break;

        case 12:
            _cityGen.unload();
            break;

        case 13:
            _desolateCityGen.unload();
            break;

        case 14:
            _mazeGen.unload();
            break;

        case 15:
            _wlGen.unload();
            break;

        case 16:
            _waterGen.unload();
            break;

        case 17:
            _skyGen.unload();
            break;

        case 18:
            _houseGen.unload();
            break;

        case 19:
            _skyBackGen.unload();
            break;
        
        case 20:
            _plantGen.unload();
            break;

        case 21:
            _darkGen.unload();
            break;

        case 22:
            _hellGen.unload();
            break;

        case 23:
            _hellPedGen.unload();
            break;

        case 24:
            _hellEntryGen.unload();
            break;

        case 25:
            _spaceGen.unload();
            break;

        case 26:
            _spaceHallGen.unload();
            break;

        case 27:
            _spaceViewGen.unload();
            break;

        case 28:
            _end1Gen.unload();
            break;

        case 29:
            _end2Gen.unload();
            break;

        default:
            break;
    }
}
