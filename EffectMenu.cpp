/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "EffectMenu.h"
#include "MenuCursor.h"
#include "Player.h"
#include "Animation.h"
#include "Decoration.h"
#include "SFML/Audio.hpp"

EffectMenu::EffectMenu(Player* player, std::vector<Animation*> animations, MenuCursor* cursor,
    std::vector<Decoration*> labels, Menu* parent, std::vector<sf::Sound*> sounds)
    : Menu(animations, cursor, parent, std::vector<Menu*>({nullptr})), _player(player), _labels(labels), _sounds(sounds)
{
    _currentAnimation = _animations[0];
    _cursor->getAnimation()->setPosition(_position + sf::Vector2f(0.f, 208.f));
    for (unsigned int i = 0; i < _labels.size(); i++)
        _labels[i]->setPosition(sf::Vector2f(i * 80.f, 208.f));
}

EffectMenu::~EffectMenu()
{
    for (Decoration* dec: _labels)
        delete dec;
}

void EffectMenu::resetInputs()
{
    _leftPressed = true;
    _rightPressed = true;
    _zPressed = true;
    _xPressed = true;
    _enterPressed = true;
}

void EffectMenu::handleInput()
{
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        if (!_leftPressed)
        {
            if (_cursorPosition == 0)
                _cursorPosition = _nbOfEffects - 1;
            else
                _cursorPosition--;
            _leftPressed = true;
        }
    }
    else
        _leftPressed = false;
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        if (!_rightPressed)
        {
            _cursorPosition++;
            _cursorPosition %= _nbOfEffects;
            _rightPressed = true;
        }
    }
    else
        _rightPressed = false;
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
    {
        if (!_zPressed)
        {
            if (_player->getState() != action)
            {
                switch (_cursorPosition)
                {
                    case 0:
                        if (_player->getObtainedEffects()[machete])
                        {
                            if (_sounds[0])
                                _sounds[0]->play();
                            _player->changeEffect(machete);
                            leaveAll();
                            return;
                        }
                        break;

                    case 1:
                        if (_player->getObtainedEffects()[scorpion])
                        {
                            if (_sounds[0])
                                _sounds[0]->play();
                            _player->changeEffect(scorpion);
                            leaveAll();
                            return;
                        }
                        break;

                    case 2:
                        if (_player->getObtainedEffects()[liquid])
                        {
                            if (_sounds[0])
                                _sounds[0]->play();
                            _player->changeEffect(liquid);
                            leaveAll();
                            return;
                        }
                        break;

                    case 3:
                        if (_player->getObtainedEffects()[mask])
                        {
                            if (_sounds[0])
                                _sounds[0]->play();
                            _player->changeEffect(mask);
                            leaveAll();
                            return;
                        }
                        break;
                    
                    default:
                        break;
                }
            }
            _zPressed = true;
        }
    }
    else
        _zPressed = false;
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::X))
    {
        if (!_xPressed)
        {
            if (_sounds[1])
                _sounds[1]->play();
            leave();
            return;
        }
    }
    else
        _xPressed = false;
}

void EffectMenu::placeCursors()
{
    _cursor->setPosition(_position + sf::Vector2f(_cursorPosition * 80.f, 208.f));
}

void EffectMenu::behaveYourself()
{
    if (_active)
        placeCursors();
}

std::vector<GameObject*> EffectMenu::getGraphics()
{
    if (!_active && _subMenus[_cursorPosition] != nullptr)
    {
        return _subMenus[_cursorPosition]->getGraphics();
    }
    else
    {
        std::vector<GameObject*> objects;
        objects.push_back(this);
        for (unsigned int i = 0; i < _labels.size(); i++)
        {
            if (_player->getObtainedEffects()[i + 1])
                objects.push_back(_labels[i]);
        }
        if (_cursor != nullptr)
            objects.push_back(_cursor);
        return objects;
    }
}
