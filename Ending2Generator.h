/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ENDING_2_GEN_H
#define ENDING_2_GEN_H

#include "GameObjectGenerator.h"

class Room;
class Player;
class RoomGraph;
class EndingCutscene;
class EndingText;

class Ending2Generator: public GameObjectGenerator
{
    private:
        enum Graphics { invisible, bg1, bg2 };
        enum Sounds { open, close };
        virtual void load();

        EndingCutscene* generateScene() const;

    public:
        Ending2Generator();
        ~Ending2Generator();

        Room* generateEnd2(Player* player, RoomGraph* graph);
};

#endif
