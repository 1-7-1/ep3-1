/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SKY_GEN_H
#define SKY_GEN_H

#include "GameObjectGenerator.h"

class Room;
class Player;
class RoomGraph;
class Decoration;
class Wall;
class Door;
class AutoTransition;

class SkyGenerator: public GameObjectGenerator
{
    private:
        enum Graphics { invisible, floor, bg, door };
        enum Sounds { doorSfx };
        virtual void load();

        std::vector<Decoration*> generateBackground() const;
        Decoration* generateFloor() const;

        Wall* generateWall(sf::Vector2f position, sf::FloatRect hitbox) const;
        std::vector<Wall*> generateWalls(sf::Vector2f position) const;
        
        std::vector<Wall*> generateHouse(sf::Vector2f position) const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateDoorOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateDoorTimeVectors() const;

        Door* generateDoor(sf::Vector2f position) const;

        std::vector<AutoTransition*> generateTiles(sf::Vector2f position, Player* player) const;

    public:
        SkyGenerator();
        ~SkyGenerator();

        Room* generateSky(Player* player, RoomGraph* graph);
};

#endif
