/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "SpaceGenerator.h"
#include "Room.h"
#include "TileCalculator.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "Decoration.h"
#include "Post.h"
#include "Player.h"
#include "Wall.h"
#include "AutoTransition.h"

SpaceGenerator::SpaceGenerator()
{

}

SpaceGenerator::~SpaceGenerator()
{

}

void SpaceGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/tilesets/21.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/bg/6.png", sf::Color(0, 0, 0, 255));
    _isLoaded = true;
}

std::vector<Decoration*> SpaceGenerator::generateBackground() const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(320, 240));

    std::vector<Decoration*> backs;

    for (int j = 0; j < 1920; j += 240)
        for (int i = 0; i < 2560; i += 320)
        {
            Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                std::vector<unsigned int>({0})),
                _db.getTexture(bg));
            backs.push_back(new Decoration(sf::Vector2f(i, j), sf::Vector2f(0.f, 0.f),
                sf::Vector2f(-0.2f, 0.1f), anim));
        }

    return backs;
}

Post* SpaceGenerator::generatePost(sf::Vector2f position) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 48));

    std::vector<sf::Vector2u> offsets = {sf::Vector2u(0, 0), sf::Vector2u(0, 1), sf::Vector2u(0, 4), sf::Vector2u(0, 5),
        sf::Vector2u(0, 6), sf::Vector2u(0, 7)};

    std::vector<unsigned int> times = {12, 12, 12, 12, 12, 12};

    Animation* anim = new Animation(tileCalc(offsets, times), _db.getTexture(tileset));

    return new Post(position, sf::Vector2f(9.f, 39.f), sf::FloatRect(-4.f, -2.f, 9.f, 5.f), anim,
        21, sf::Vector2f(960.f, 968.f), true);
}

Decoration* SpaceGenerator::generateFloorTile(sf::Vector2f position, FloorType type) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(32, 32));
    TileCalculator diagCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(64, 32));

    Animation* anim;

    switch (type)
    {
        case vtopwall:
        case vtop:
            anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 2), sf::Vector2u(3, 2),
                sf::Vector2u(5, 2), sf::Vector2u(7, 2), sf::Vector2u(5, 2), sf::Vector2u(3, 2)}),
                std::vector<unsigned int>({12, 12, 12, 12, 12, 12})), _db.getTexture(tileset));

                return new Decoration(position, sf::Vector2f(), sf::Vector2f(), anim);
            break;

        case vbottomwall:
        case vbottom:
            anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 6), sf::Vector2u(3, 6),
                sf::Vector2u(5, 6)}), std::vector<unsigned int>({9, 9, 9})), _db.getTexture(tileset));

                return new Decoration(position, sf::Vector2f(), sf::Vector2f(), anim);
            break;

        case vmiddle:
            anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 3), sf::Vector2u(3, 3),
                sf::Vector2u(5, 3), sf::Vector2u(7, 3), sf::Vector2u(9, 3), sf::Vector2u(11, 3)}),
                std::vector<unsigned int>({6, 6, 6, 6, 6, 6})), _db.getTexture(tileset));

                return new Decoration(position, sf::Vector2f(), sf::Vector2f(), anim);
            break;

        case hleftwall:
        case hleft:
            anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 8), sf::Vector2u(3, 8),
                sf::Vector2u(5, 8), sf::Vector2u(7, 8), sf::Vector2u(9, 8), sf::Vector2u(11, 8), sf::Vector2u(13, 8)}),
                std::vector<unsigned int>({30, 6, 6, 6, 6, 6, 6})), _db.getTexture(tileset));

                return new Decoration(position, sf::Vector2f(), sf::Vector2f(), anim);
            break;

        case hrightwall:
        case hright:
            anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 10)}),
                std::vector<unsigned int>({6, 6, 6, 6, 6, 6, 6, 6})), _db.getTexture(tileset));

                return new Decoration(position, sf::Vector2f(), sf::Vector2f(), anim);
            break;

        case hmiddle:
            anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 9), sf::Vector2u(3, 9),
                sf::Vector2u(5, 9), sf::Vector2u(7, 9), sf::Vector2u(9, 9), sf::Vector2u(11, 9),
                sf::Vector2u(13, 9), sf::Vector2u(15, 9), sf::Vector2u(17, 9)}),
                std::vector<unsigned int>({6, 6, 6, 6, 6, 6, 6, 6, 30})), _db.getTexture(tileset));

                return new Decoration(position, sf::Vector2f(), sf::Vector2f(), anim);
            break;

        case diagleft:
            anim = new Animation(diagCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 4), sf::Vector2u(1, 4),
                sf::Vector2u(2, 4), sf::Vector2u(3, 4), sf::Vector2u(4, 4), sf::Vector2u(5, 4)}),
                std::vector<unsigned int>({9, 9, 9, 9, 9, 30})), _db.getTexture(tileset));

                return new Decoration(position, sf::Vector2f(16.f, 0.f), sf::Vector2f(), anim);
            break;

        case diagright:
            anim = new Animation(diagCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 5), sf::Vector2u(1, 5),
                sf::Vector2u(2, 5), sf::Vector2u(3, 5), sf::Vector2u(4, 5), sf::Vector2u(5, 5)}),
                std::vector<unsigned int>({6, 6, 6, 6, 6, 6})), _db.getTexture(tileset));

                return new Decoration(position, sf::Vector2f(32.f, 0.f), sf::Vector2f(), anim);
            break;

        case inter:
            anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 7), sf::Vector2u(3, 7),
                sf::Vector2u(5, 7), sf::Vector2u(7, 7)}),
                std::vector<unsigned int>({12, 12, 12, 12})), _db.getTexture(tileset));

                return new Decoration(position, sf::Vector2f(), sf::Vector2f(), anim);
            break;

        case upleft:
            anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 11)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));

                return new Decoration(position, sf::Vector2f(), sf::Vector2f(), anim);
            break;

        case upright:
            anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 13)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));

                return new Decoration(position, sf::Vector2f(), sf::Vector2f(), anim);
            break;

        case downleft:
            anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 12)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));

                return new Decoration(position, sf::Vector2f(), sf::Vector2f(), anim);
            break;

        case downright:
            anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 14)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));

                return new Decoration(position, sf::Vector2f(), sf::Vector2f(), anim);
            break;

        case grid:
            anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 1)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));

                return new Decoration(position, sf::Vector2f(), sf::Vector2f(), anim);
            break;

        default:
            anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 1)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));

                return new Decoration(position, sf::Vector2f(), sf::Vector2f(), anim);
            break;
    }
}

std::vector<Wall*> SpaceGenerator::generateFloorWalls(sf::Vector2f position, FloorType type) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    std::vector<Wall*> walls;

    switch (type)
    {
        case vtopwall:
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(0.f, -4.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 32.f, 4.f)));
            }
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(-4.f, 0.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 4.f, 32.f)));
            }
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(32.f, 0.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 4.f, 32.f)));
            }
            break;

        case vbottomwall:
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(0.f, 32.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 32.f, 4.f)));
            }
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(-4.f, 0.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 4.f, 32.f)));
            }
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(32.f, 0.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 4.f, 32.f)));
            }
            break;

        case vtop:
        case vbottom:
        case vmiddle:
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(-4.f, 0.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 4.f, 32.f)));
            }
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(32.f, 0.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 4.f, 32.f)));
            }
            break;

        case hleftwall:
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(-4.f, 0.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 4.f, 32.f)));
            }
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(0.f, -4.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 32.f, 4.f)));
            }
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(0.f, 32.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 32.f, 4.f)));
            }
            break;

        case hrightwall:
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(32.f, 0.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 4.f, 32.f)));
            }
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(0.f, -4.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 32.f, 4.f)));
            }
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(0.f, 32.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 32.f, 4.f)));
            }
            break;

        case hleft:
        case hright:
        case hmiddle:
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(0.f, -4.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 32.f, 4.f)));
            }
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(0.f, 32.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 32.f, 4.f)));
            }
            break;

        case diagleft:
            for (int i = 0; i < 16; i++)
            {
                {
                    Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                        std::vector<unsigned int>({0})), _db.getTexture(invisible));

                    walls.push_back(new Wall(position + sf::Vector2f(14.f - i, i * 2.f), sf::Vector2f(),
                    anim, sf::FloatRect(0.f, 0.f, 2.f, 2.f)));
                }
                {
                    Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                        std::vector<unsigned int>({0})), _db.getTexture(invisible));

                    walls.push_back(new Wall(position + sf::Vector2f(47.f - i, i * 2.f), sf::Vector2f(),
                    anim, sf::FloatRect(0.f, 0.f, 2.f, 2.f)));
                }
            }
            break;

        case diagright:
            for (int i = 0; i < 16; i++)
            {
                {
                    Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                        std::vector<unsigned int>({0})), _db.getTexture(invisible));

                    walls.push_back(new Wall(position + sf::Vector2f(-17.f + i, i * 2.f), sf::Vector2f(),
                    anim, sf::FloatRect(0.f, 0.f, 2.f, 2.f)));
                }
                {
                    Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                        std::vector<unsigned int>({0})), _db.getTexture(invisible));

                    walls.push_back(new Wall(position + sf::Vector2f(16.f + i, i * 2.f), sf::Vector2f(),
                    anim, sf::FloatRect(0.f, 0.f, 2.f, 2.f)));
                }
            }
            break;

        case upleft:
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(0.f, -4.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 32.f, 4.f)));
            }
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(-4.f, 0.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 4.f, 32.f)));
            }
            break;

        case upright:
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(0.f, -4.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 32.f, 4.f)));
            }
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(32.f, 0.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 4.f, 32.f)));
            }
            break;

        case downleft:
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(0.f, 32.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 32.f, 4.f)));
            }
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(-4.f, 0.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 4.f, 32.f)));
            }
            break;

        case downright:
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(0.f, 32.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 32.f, 4.f)));
            }
            {
                Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                    std::vector<unsigned int>({0})), _db.getTexture(invisible));

                walls.push_back(new Wall(position + sf::Vector2f(32.f, 0.f), sf::Vector2f(),
                anim, sf::FloatRect(0.f, 0.f, 4.f, 32.f)));
            }
            break;
        
        default:
            break;
    }

    return walls;
}

Wall* SpaceGenerator::generateWall(sf::Vector2f position, sf::FloatRect hitbox) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(invisible));

    return new Wall(position, sf::Vector2f(), anim, hitbox);
}

AutoTransition* SpaceGenerator::generateTile(Player* player) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(invisible));

    return new AutoTransition(sf::Vector2f(2384.f, 576.f), sf::Vector2f(), sf::FloatRect(0.f, 0.f, 16.f, 32.f),
        anim, 26, sf::Vector2f(24.f, 208.f), true, player);
}

Room* SpaceGenerator::generateSpace(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;
    std::vector<GameObject*> foregrounds;

    std::vector<Decoration*> backs = generateBackground();
    backgrounds.insert(backgrounds.end(), backs.begin(), backs.end());

    Post* post = generatePost(sf::Vector2f(336.f, 256.f));
    interactibles.push_back(post);
    collidables.push_back(post);

    {
        sf::Vector2f pos = sf::Vector2f(320.f, 240.f);
        FloorType type = vtopwall;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(320.f, 272.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(320.f, 304.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(304.f, 336.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(288.f, 368.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(304.f, 400.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(320.f, 432.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(320.f, 464.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(320.f, 496.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(320.f, 528.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(320.f, 560.f);
        FloorType type = downleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(352.f, 560.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(384.f, 560.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(416.f, 560.f);
        FloorType type = hright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    for (int j = 464; j < 688; j += 32)
        for (int i = 448; i < 672; i += 32)
        {
            sf::Vector2f pos = sf::Vector2f(i, j);
            FloorType type = grid;
            Decoration* floor = generateFloorTile(pos, type);
            std::vector<Wall*> walls = generateFloorWalls(pos, type);
            floors.push_back(floor);
            collidables.insert(collidables.end(), walls.begin(), walls.end());
        }
    {
        collidables.push_back(generateWall(sf::Vector2f(448.f, 432.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(480.f, 432.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(512.f, 432.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(576.f, 432.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(608.f, 432.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(640.f, 432.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        
        collidables.push_back(generateWall(sf::Vector2f(448.f, 688.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(480.f, 688.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(512.f, 688.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(544.f, 688.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(576.f, 688.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(608.f, 688.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(640.f, 688.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        
        collidables.push_back(generateWall(sf::Vector2f(416.f, 464.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(416.f, 496.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(416.f, 528.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(416.f, 592.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(416.f, 624.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(416.f, 656.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        
        collidables.push_back(generateWall(sf::Vector2f(672.f, 464.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(672.f, 496.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(672.f, 512.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(672.f, 576.f), sf::FloatRect(0.f, 0.f, 32.f, 48.f)));
        collidables.push_back(generateWall(sf::Vector2f(672.f, 624.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(672.f, 656.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
    }
    {
        sf::Vector2f pos = sf::Vector2f(544.f, 432.f);
        FloorType type = vbottom;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(544.f, 400.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(544.f, 368.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(544.f, 336.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(544.f, 304.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(544.f, 272.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(544.f, 240.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(528.f, 208.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(512.f, 176.f);
        FloorType type = upright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(480.f, 176.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(448.f, 176.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(416.f, 176.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(384.f, 176.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(352.f, 176.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(320.f, 176.f);
        FloorType type = hleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(288.f, 176.f);
        FloorType type = inter;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        collidables.push_back(generateWall(sf::Vector2f(256.f, 176.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
    }
    {
        sf::Vector2f pos = sf::Vector2f(288.f, 144.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(272.f, 112.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(256.f, 80.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(240.f, 48.f);
        FloorType type = vtopwall;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(272.f, 208.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(256.f, 240.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(240.f, 272.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(240.f, 304.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(240.f, 336.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(240.f, 368.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(240.f, 400.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(240.f, 432.f);
        FloorType type = downright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(208.f, 432.f);
        FloorType type = hleftwall;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(672.f, 544.f);
        FloorType type = hleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(704.f, 544.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(736.f, 544.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(768.f, 544.f);
        FloorType type = upright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(768.f, 576.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(768.f, 608.f);
        FloorType type = vbottom;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(768.f, 640.f);
        FloorType type = inter;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        collidables.push_back(generateWall(sf::Vector2f(736.f, 640.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
    }
    {
        sf::Vector2f pos = sf::Vector2f(768.f, 672.f);
        FloorType type = vtop;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(768.f, 704.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(768.f, 736.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(768.f, 768.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(768.f, 800.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(768.f, 832.f);
        FloorType type = downright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(736.f, 832.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(704.f, 832.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(672.f, 832.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(640.f, 832.f);
        FloorType type = upleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(640.f, 864.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(640.f, 896.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(640.f, 928.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(624.f, 960.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(608.f, 992.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(592.f, 1024.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(592.f, 1056.f);
        FloorType type = downright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(560.f, 1056.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(528.f, 1056.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(496.f, 1056.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(464.f, 1056.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(432.f, 1056.f);
        FloorType type = hleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(400.f, 1056.f);
        FloorType type = inter;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        collidables.push_back(generateWall(sf::Vector2f(368.f, 1056.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
    }
    {
        sf::Vector2f pos = sf::Vector2f(400.f, 1024.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(400.f, 992.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(400.f, 960.f);
        FloorType type = upright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(368.f, 960.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(336.f, 960.f);
        FloorType type = hleftwall;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(400.f, 1088.f);
        FloorType type = vtop;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(400.f, 1120.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(400.f, 1152.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(400.f, 1184.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(400.f, 1216.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(400.f, 1248.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(400.f, 1280.f);
        FloorType type = downleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(432.f, 1280.f);
        FloorType type = hright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(464.f, 1280.f);
        FloorType type = inter;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        collidables.push_back(generateWall(sf::Vector2f(464.f, 1248.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
    }
    {
        sf::Vector2f pos = sf::Vector2f(464.f, 1312.f);
        FloorType type = vtop;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(464.f, 1344.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(464.f, 1376.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(464.f, 1408.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(464.f, 1440.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(464.f, 1472.f);
        FloorType type = downleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(496.f, 1472.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(528.f, 1472.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(560.f, 1472.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(592.f, 1472.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(624.f, 1472.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(656.f, 1472.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(688.f, 1472.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(720.f, 1472.f);
        FloorType type = hrightwall;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(496.f, 1280.f);
        FloorType type = hleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(528.f, 1280.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(560.f, 1280.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(592.f, 1280.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(624.f, 1280.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(656.f, 1280.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(688.f, 1280.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(720.f, 1280.f);
        FloorType type = downright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(720.f, 1248.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(736.f, 1216.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(752.f, 1184.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(768.f, 1152.f);
        FloorType type = upleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(800.f, 1152.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(832.f, 1152.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(864.f, 1152.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(896.f, 1152.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(928.f, 1152.f);
        FloorType type = hright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(960.f, 1152.f);
        FloorType type = inter;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        collidables.push_back(generateWall(sf::Vector2f(992.f, 1152.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
    }
    {
        sf::Vector2f pos = sf::Vector2f(960.f, 1184.f);
        FloorType type = vtop;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(960.f, 1216.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(960.f, 1248.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(960.f, 1280.f);
        FloorType type = vbottom;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(960.f, 1312.f);
        FloorType type = inter;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        collidables.push_back(generateWall(sf::Vector2f(960.f, 1344.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
    }
    {
        sf::Vector2f pos = sf::Vector2f(928.f, 1312.f);
        FloorType type = hright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(896.f, 1312.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(864.f, 1312.f);
        FloorType type = hleftwall;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(992.f, 1312.f);
        FloorType type = hleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1024.f, 1312.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1056.f, 1312.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1088.f, 1312.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1120.f, 1312.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1152.f, 1312.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1184.f, 1312.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1216.f, 1312.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1248.f, 1312.f);
        FloorType type = upright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1248.f, 1344.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1248.f, 1376.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1248.f, 1408.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1248.f, 1440.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1248.f, 1472.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1248.f, 1504.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1248.f, 1536.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1248.f, 1568.f);
        FloorType type = vbottom;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1248.f, 1600.f);
        FloorType type = inter;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        collidables.push_back(generateWall(sf::Vector2f(1248.f, 1632.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
    }
    {
        sf::Vector2f pos = sf::Vector2f(1216.f, 1600.f);
        FloorType type = hright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1184.f, 1600.f);
        FloorType type = hleftwall;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1280.f, 1600.f);
        FloorType type = hleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1312.f, 1600.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1344.f, 1600.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1376.f, 1600.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1408.f, 1600.f);
        FloorType type = hrightwall;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(960.f, 1120.f);
        FloorType type = vbottom;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(960.f, 1088.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(960.f, 1056.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(960.f, 1024.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(960.f, 992.f);
        FloorType type = vtop;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(960.f, 960.f);
        FloorType type = inter;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        collidables.push_back(generateWall(sf::Vector2f(928.f, 960.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
    }
    {
        sf::Vector2f pos = sf::Vector2f(960.f, 928.f);
        FloorType type = vbottom;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(960.f, 896.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(944.f, 864.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(928.f, 832.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(912.f, 800.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(896.f, 768.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(880.f, 736.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(864.f, 704.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(848.f, 672.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(832.f, 640.f);
        FloorType type = upright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(800.f, 640.f);
        FloorType type = hleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(992.f, 960.f);
        FloorType type = hleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1024.f, 960.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1056.f, 960.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1088.f, 960.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1120.f, 960.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1152.f, 960.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1184.f, 960.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1216.f, 960.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1248.f, 960.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1280.f, 960.f);
        FloorType type = downright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1280.f, 928.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1280.f, 896.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1280.f, 864.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1280.f, 832.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1280.f, 800.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1280.f, 768.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1280.f, 736.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1280.f, 704.f);
        FloorType type = vtop;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    for (int j = 544; j < 704; j += 32)
        for (int i = 1216; i < 1376; i += 32)
        {
            sf::Vector2f pos = sf::Vector2f(i, j);
            FloorType type = grid;
            Decoration* floor = generateFloorTile(pos, type);
            std::vector<Wall*> walls = generateFloorWalls(pos, type);
            floors.push_back(floor);
            collidables.insert(collidables.end(), walls.begin(), walls.end());
        }
    {
        collidables.push_back(generateWall(sf::Vector2f(1216.f, 512.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(1248.f, 512.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(1312.f, 512.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(1344.f, 512.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));

        collidables.push_back(generateWall(sf::Vector2f(1216.f, 704.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(1248.f, 704.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(1312.f, 704.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(1344.f, 704.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        
        collidables.push_back(generateWall(sf::Vector2f(1184.f, 544.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(1184.f, 576.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(1184.f, 608.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(1184.f, 640.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(1184.f, 672.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        
        collidables.push_back(generateWall(sf::Vector2f(1376.f, 544.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(1376.f, 576.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(1376.f, 640.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
        collidables.push_back(generateWall(sf::Vector2f(1376.f, 672.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
    }
    {
        sf::Vector2f pos = sf::Vector2f(1280.f, 512.f);
        FloorType type = vbottom;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1280.f, 480.f);
        FloorType type = upright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1248.f, 480.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1216.f, 480.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1184.f, 480.f);
        FloorType type = downleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1184.f, 448.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1184.f, 416.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1168.f, 384.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1168.f, 352.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1168.f, 320.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1168.f, 288.f);
        FloorType type = vtop;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1168.f, 256.f);
        FloorType type = inter;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1136.f, 256.f);
        FloorType type = hright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1104.f, 256.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1072.f, 256.f);
        FloorType type = hleftwall;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1168.f, 224.f);
        FloorType type = vbottom;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1168.f, 192.f);
        FloorType type = vtopwall;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1200.f, 256.f);
        FloorType type = hleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    for (int i = 1200; i < 1978; i += 32)
    {
        sf::Vector2f pos = sf::Vector2f(i, 256.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1978, 256.f);
        FloorType type = hright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2000.f, 256.f);
        FloorType type = inter;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        collidables.push_back(generateWall(sf::Vector2f(2000.f, 224.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
    }
    {
        sf::Vector2f pos = sf::Vector2f(2032.f, 256.f);
        FloorType type = hleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2064.f, 256.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2096.f, 256.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2128.f, 256.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2160.f, 256.f);
        FloorType type = hrightwall;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2000.f, 288.f);
        FloorType type = vtop;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2000.f, 320.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2000.f, 352.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2000.f, 384.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2000.f, 416.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2000.f, 448.f);
        FloorType type = vbottom;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2000.f, 480.f);
        FloorType type = inter;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        collidables.push_back(generateWall(sf::Vector2f(2000.f, 512.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
    }
    {
        sf::Vector2f pos = sf::Vector2f(2032.f, 480.f);
        FloorType type = hleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2064.f, 480.f);
        FloorType type = hrightwall;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1968.f, 480.f);
        FloorType type = hright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1936.f, 480.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1904.f, 480.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1872.f, 480.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1840.f, 480.f);
        FloorType type = hleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1808.f, 480.f);
        FloorType type = inter;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        collidables.push_back(generateWall(sf::Vector2f(1808.f, 448.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
    }
    {
        sf::Vector2f pos = sf::Vector2f(1776.f, 480.f);
        FloorType type = hright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1744.f, 480.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1712.f, 480.f);
        FloorType type = hleftwall;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1808.f, 512.f);
        FloorType type = vtop;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1808.f, 544.f);
        FloorType type = vbottom;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1808.f, 576.f);
        FloorType type = inter;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        collidables.push_back(generateWall(sf::Vector2f(1808.f, 608.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
    }
    {
        sf::Vector2f pos = sf::Vector2f(1776.f, 576.f);
        FloorType type = hright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1744.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1712.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1680.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1648.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1616.f, 576.f);
        FloorType type = downleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1616.f, 544.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1616.f, 512.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1616.f, 480.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1616.f, 448.f);
        FloorType type = upright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1584.f, 448.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1552.f, 448.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1520.f, 448.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1488.f, 448.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1456.f, 448.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1424.f, 448.f);
        FloorType type = upleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1424.f, 480.f);
        FloorType type = vbottom;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1424.f, 512.f);
        FloorType type = inter;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        collidables.push_back(generateWall(sf::Vector2f(1392.f, 512.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
    }
    {
        sf::Vector2f pos = sf::Vector2f(1424.f, 544.f);
        FloorType type = vtop;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1408.f, 576.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1408.f, 608.f);
        FloorType type = downright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1376.f, 608.f);
        FloorType type = hleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1456.f, 512.f);
        FloorType type = hleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1488.f, 512.f);
        FloorType type = upright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1488.f, 544.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1488.f, 576.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1488.f, 608.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1488.f, 640.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1488.f, 672.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1488.f, 704.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1488.f, 736.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1488.f, 768.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1488.f, 800.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1504.f, 832.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1520.f, 864.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1536.f, 896.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1552.f, 928.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1568.f, 960.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1584.f, 992.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1600.f, 1024.f);
        FloorType type = diagright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1600.f, 1056.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1600.f, 1088.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1600.f, 1120.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1600.f, 1152.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1600.f, 1184.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1600.f, 1216.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1600.f, 1248.f);
        FloorType type = vbottom;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1600.f, 1280.f);
        FloorType type = inter;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        collidables.push_back(generateWall(sf::Vector2f(1600.f, 1312.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
    }
    {
        sf::Vector2f pos = sf::Vector2f(1568.f, 1280.f);
        FloorType type = hright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1536.f, 1280.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1504.f, 1280.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1472.f, 1280.f);
        FloorType type = hleftwall;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1632.f, 1280.f);
        FloorType type = hleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1664.f, 1280.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1696.f, 1280.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1728.f, 1280.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1760.f, 1280.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1792.f, 1280.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1824.f, 1280.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1856.f, 1280.f);
        FloorType type = downright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1856.f, 1248.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1856.f, 1216.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1856.f, 1184.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1856.f, 1152.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1856.f, 1120.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1856.f, 1088.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1856.f, 1056.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1872.f, 1024.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1888.f, 992.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1904.f, 960.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1920.f, 928.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1936.f, 896.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1952.f, 864.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1968.f, 832.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1984.f, 800.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2000.f, 768.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2016.f, 736.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2032.f, 704.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2048.f, 672.f);
        FloorType type = diagleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2064.f, 640.f);
        FloorType type = vmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2064.f, 608.f);
        FloorType type = vtop;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2064.f, 576.f);
        FloorType type = inter;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        collidables.push_back(generateWall(sf::Vector2f(2064.f, 544.f), sf::FloatRect(0.f, 0.f, 32.f, 32.f)));
    }
    {
        sf::Vector2f pos = sf::Vector2f(2032.f, 576.f);
        FloorType type = hright;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2000.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1968.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1936.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1904.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1872.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(1840.f, 576.f);
        FloorType type = hleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2096.f, 576.f);
        FloorType type = hleft;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2128.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2160.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2192.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2224.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2256.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2288.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2320.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2352.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2384.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2416.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2448.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2480.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2512.f, 576.f);
        FloorType type = hmiddle;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }
    {
        sf::Vector2f pos = sf::Vector2f(2544.f, 576.f);
        FloorType type = hrightwall;
        Decoration* floor = generateFloorTile(pos, type);
        std::vector<Wall*> walls = generateFloorWalls(pos, type);
        floors.push_back(floor);
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }

    floors.push_back(generateTile(player));

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, foregrounds, std::vector<GameObject*>() };

    return new Room(player, graph, nullptr, objects, sf::Vector2f(_roomWidth, _roomHeight),
        sf::Vector2<bool>(true, true));
}
