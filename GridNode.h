/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GRID_NODE_H
#define GRID_NODE_H

#include "SFML/Graphics.hpp"
#include <vector>

class CollidableObject;

class GridNode
{
    private:
        CollidableObject* _object;
        std::vector<sf::Vector2i> _gridPositions;

    public:
        GridNode(CollidableObject* obj);
        ~GridNode();

        void setPositions(std::vector<sf::Vector2i> pos);

        std::vector<sf::Vector2i> getPositions() const;
        CollidableObject* getObject() const;
};

#endif
