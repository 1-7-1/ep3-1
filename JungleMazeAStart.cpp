/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "JungleGenerator.h"

std::vector<Wall*> JungleGenerator::generateMazeAStart() const
{
    std::vector<Wall*> walls;
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3680.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3664.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3648.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3632.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3616.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3472.f, 3616.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3488.f, 3616.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(3488.f, 3600.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 3600.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 3600.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3536.f, 3600.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3552.f, 3600.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3600.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3584.f, 3600.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3600.f, 3600.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 3600.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 3600.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 3600.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 3584.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 3584.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 3568.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3680.f, 3568.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 3568.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 3552.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 3552.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 3552.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 3536.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3744.f, 3536.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(3744.f, 3520.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3744.f, 3504.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 3504.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 3504.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 3504.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3680.f, 3504.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 3504.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 3504.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 3504.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 3504.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 3488.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3600.f, 3488.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3584.f, 3488.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3584.f, 3472.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3584.f, 3456.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3584.f, 3440.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3584.f, 3424.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3424.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3408.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3392.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(3552.f, 3392.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3536.f, 3392.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 3392.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 3392.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 3408.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 3424.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 3424.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 3440.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 3456.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 3472.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 3488.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3536.f, 3488.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3536.f, 3504.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3536.f, 3520.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 3520.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 3520.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 3504.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 3488.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 3472.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3488.f, 3472.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3488.f, 3456.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(3472.f, 3456.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3472.f, 3440.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3472.f, 3424.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3472.f, 3408.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3472.f, 3392.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3392.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3376.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3360.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3344.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3328.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3312.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3296.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3280.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3472.f, 3280.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3472.f, 3264.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3472.f, 3248.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3488.f, 3248.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3488.f, 3232.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3488.f, 3216.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3488.f, 3200.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3488.f, 3184.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3472.f, 3184.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3472.f, 3168.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3168.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3152.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3440.f, 3152.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3440.f, 3136.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(3424.f, 3136.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3408.f, 3136.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3408.f, 3152.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3392.f, 3152.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3376.f, 3152.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3360.f, 3152.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3344.f, 3152.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3328.f, 3152.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3328.f, 3168.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3328.f, 3184.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3328.f, 3200.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3312.f, 3200.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3296.f, 3200.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3280.f, 3200.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3264.f, 3200.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3264.f, 3184.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3264.f, 3168.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3264.f, 3152.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3264.f, 3136.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3264.f, 3120.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3264.f, 3104.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3280.f, 3104.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3280.f, 3088.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3296.f, 3088.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3312.f, 3088.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3328.f, 3088.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3344.f, 3088.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3360.f, 3088.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3376.f, 3088.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3392.f, 3088.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3408.f, 3088.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3424.f, 3088.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3440.f, 3088.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3440.f, 3072.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3072.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3472.f, 3072.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3488.f, 3072.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 3072.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 3056.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 3040.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 3040.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3536.f, 3040.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3552.f, 3040.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3040.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3584.f, 3040.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3600.f, 3040.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 3040.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 3024.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 3024.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 3008.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2992.f), right, false));

    walls.push_back(generateWall(sf::Vector2f(3600.f, 3680.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3600.f, 3664.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 3664.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 3664.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 3664.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 3664.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 3648.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3680.f, 3648.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 3648.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 3648.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 3632.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 3632.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 3632.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 3616.f), upleft, true));
    walls.push_back(generateWall(sf::Vector2f(3744.f, 3616.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 3616.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 3600.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3776.f, 3600.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3776.f, 3584.f), upleft, true));
    walls.push_back(generateWall(sf::Vector2f(3792.f, 3584.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3792.f, 3568.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3792.f, 3552.f), upleft, true));
    walls.push_back(generateWall(sf::Vector2f(3808.f, 3552.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3808.f, 3536.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3808.f, 3520.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3808.f, 3504.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3808.f, 3488.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3792.f, 3488.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3792.f, 3472.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3776.f, 3472.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(3776.f, 3456.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 3456.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3744.f, 3456.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 3456.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 3456.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 3456.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3680.f, 3456.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(3680.f, 3440.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 3440.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 3424.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 3408.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 3392.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 3376.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 3360.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 3344.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 3328.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 3312.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 3296.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 3280.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 3280.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 3264.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 3248.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 3248.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 3248.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3600.f, 3248.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3584.f, 3248.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3248.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3232.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3216.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3200.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3184.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3168.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3152.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3136.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3120.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3104.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3088.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3584.f, 3088.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3600.f, 3088.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 3088.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 3088.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 3088.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 3088.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3680.f, 3088.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3680.f, 3072.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 3072.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 3056.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 3040.f), upleft, true));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 3040.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 3024.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 3008.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 2992.f), left, false));

    for (int j = 0; j < 4; j++)
        for (int i = 0; i < 3; i++)
            walls.push_back(generateWall(sf::Vector2f(3584.f + i * 32.f, 3104.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 11; j++)
        walls.push_back(generateWall(sf::Vector2f(3696.f, 3088.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 14; j++)
        for (int i = 0; i < 20; i++)
            walls.push_back(generateWall(sf::Vector2f(3728.f + i * 32.f, 2992.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 7; j++)
        for (int i = 0; i < 20; i++)
            walls.push_back(generateWall(sf::Vector2f(3824.f + i * 32.f, 3456.f + j * 32.f), turnupright, true));
    
    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 16; i++)
            walls.push_back(generateWall(sf::Vector2f(3104.f + i * 32.f, 2976.f + j * 32.f), turnupright, true));
    for (int i = 0; i < 12; i++)
        walls.push_back(generateWall(sf::Vector2f(3104.f + i * 32.f, 3024.f), turnupright, true));
    for (int i = 0; i < 10; i++)
        walls.push_back(generateWall(sf::Vector2f(3104.f + i * 32.f, 3040.f), turnupright, true));
    for (int j = 0; j < 19; j++)
        for (int i = 0; i < 5; i++)
            walls.push_back(generateWall(sf::Vector2f(3104.f + i * 32.f, 3072.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 14; j++)
        for (int i = 0; i < 6; i++)
            walls.push_back(generateWall(sf::Vector2f(3264.f + i * 32.f, 3216.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 6; i++)
            walls.push_back(generateWall(sf::Vector2f(3456.f + i * 32.f, 3536.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 3; j++)
            walls.push_back(generateWall(sf::Vector2f(3552.f, 3440.f + j * 32.f), turnupright, true));

    std::vector<Wall*> decorations;
    
    decorations = generateBigTree(sf::Vector2f(3364.f, 3125.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigTree(sf::Vector2f(3553.f, 3666.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    decorations = generateTallTree(sf::Vector2f(3687.f, 3064.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3655.f, 3469.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3557.f, 3101.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3539.f, 3174.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3298.f, 3190.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3571.f, 3282.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3544.f, 3340.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3796.f, 3532.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3724.f, 3586.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateSmallTree(sf::Vector2f(3663.f, 3006.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3677.f, 3639.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3481.f, 3337.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3512.f, 3450.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3303.f, 3138.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3610.f, 3485.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3556.f, 3242.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    decorations = generateVerySmallTree(sf::Vector2f(3516.f, 3136.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3572.f, 3629.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3712.f, 3498.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3612.f, 3319.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3505.f, 3283.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3468.f, 3144.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateWeirdPlant(sf::Vector2f(3471.f, 3098.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3437.f, 3113.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3634.f, 3290.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3645.f, 3322.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3495.f, 3667.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateBigFern(sf::Vector2f(3696.f, 3026.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3592.f, 3343.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3318.f, 3120.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3370.f, 3145.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3572.f, 3063.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3528.f, 3079.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3551.f, 3124.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3555.f, 3149.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3495.f, 3144.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3422.f, 3131.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3555.f, 3156.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3515.f, 3197.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3554.f, 3262.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3511.f, 3301.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3505.f, 3378.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3603.f, 3379.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3630.f, 3390.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3644.f, 3442.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3652.f, 3498.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3692.f, 3483.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3740.f, 3573.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3745.f, 3609.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3693.f, 3603.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3646.f, 3645.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3589.f, 3659.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3543.f, 3633.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateSmallFern(sf::Vector2f(3641.f, 3058.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3665.f, 3618.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3642.f, 3622.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3609.f, 3627.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3740.f, 3494.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3767.f, 3479.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3789.f, 3515.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3779.f, 3556.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3612.f, 3471.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3495.f, 3435.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3540.f, 3357.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3549.f, 3325.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3588.f, 3301.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3540.f, 3220.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3521.f, 3228.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3289.f, 3165.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3307.f, 3153.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3318.f, 3168.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3510.f, 3159.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateBigRock(sf::Vector2f(3645.f, 3353.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(3560.f, 3379.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateSmallRock(sf::Vector2f(3599.f, 3635.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3703.f, 3633.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3782.f, 3569.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3757.f, 3496.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3613.f, 3452.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3492.f, 3354.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3559.f, 3299.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3502.f, 3173.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3498.f, 3107.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3330.f, 3145.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3540.f, 3066.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3703.f, 3008.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    return walls;
}