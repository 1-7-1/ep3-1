/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ROOM_H
#define ROOM_H

#include "View.h"
#include "Grid.h"
#include "SpecialEffectGenerator.h"

class InteractibleObject;
class RoomGraph;
class Player;
class FullScreenEvent;

enum RoomState { entering, active, exiting };
enum Layer { bgLayer, floorLayer, specialLayer };

struct RoomObjects
{
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> walls;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floorTiles;
    std::vector<GameObject*> backgroundObjects;
    std::vector<GameObject*> foregroundObjects;
    std::vector<GameObject*> specialEffects;
};

class Room
{
    friend class Grid;
    friend class View;
    protected:
        sf::Vector2f _size = sf::Vector2f(640.f, 480.f);
        sf::Vector2<bool> _looping;
        
        Player* _player;
        RoomGraph* _graph;

        bool _fseActive = false;
        FullScreenEvent* _fullScreenEvent;

        RoomObjects _objects;
        std::vector<GameObject*> _toBeDestroyed;

        SpecialEffectGenerator _specialEffectGenerator;
        
        RoomState _currentState;
        unsigned int _transitionTimer;
        unsigned int _destination;
        sf::Vector2f _transitionVector;
        bool _transitionAbsolute;

        Grid _grid;
        View _view;

        virtual void destroy();

        template <class T>
        void setRooms(std::vector<T*> vec);
        template <class T>
        void deleteObjects(std::vector<T*> vec);
        template <class T1, class T2>
        int findObject(std::vector<T1*> vec, T2* object);
        template <class T>
        void vectorTick(std::vector<T*> vec);
        template <class T>
        void backgroundTick(std::vector<T*> vec);
        template <class T>
        void foregroundTick(std::vector<T*> vec);
        template <class T>
        void floorTick(std::vector<T*> vec);
        void recenter(GameObject* object);
        
    public:
        Room(Player* player, RoomGraph* graph, FullScreenEvent* fse, RoomObjects objects,
        sf::Vector2f size, sf::Vector2<bool> looping);
        virtual ~Room();
        
        unsigned int _fadeLength;

        sf::Vector2f getSize() const;
        std::vector<InteractibleObject*> getInteractibles() const;
        unsigned int getTransitionTime() const;
        sf::Vector2<bool> getLooping() const;
        RoomState getState() const;
        std::vector<GameObject*> getGraphics();
        sf::Vector2f getViewPosition() const;
        bool getFseActive() const;

        void setFseActive(bool active);

        bool isInView(GameObject* obj) const;
        
        bool checkCollision(sf::FloatRect hitbox, CollidableObject* other);
        void updateGrid(CollidableObject* obj);

        void createSpecialEffect(sf::Vector2f position, sf::Vector2f origin, EffectType type, Layer layer = specialLayer,
            GameObject* target = nullptr);
        void setToDestroy(GameObject* obj);
        
        virtual void changeRoom(unsigned int destination, sf::Vector2f transitionVec, bool absolute, unsigned int fadeLength);

        virtual void tick();
};

#endif
