/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Plant.h"
#include "Animation.h"
#include "Room.h"

Plant::Plant(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox, Animation* anim)
    : CollidableObject(position, origin, hitbox), Npc(position, origin, 0.f, hitbox)
{
    _currentAnimation = anim;
    _currentAnimation->setOrigin(_origin);
}

Plant::~Plant()
{
    delete _currentAnimation;
}

void Plant::behave()
{
    if (_currentState != dead)
        changeAnimation();
    else
    {
        _currentAnimation->setColor(sf::Color(255, 255, 255, _timeToDie * 4));
        _timeToDie--;
        if (_timeToDie == 0)
            _room->setToDestroy(this);
    }
}
