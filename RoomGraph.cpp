/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "RoomGraph.h"
#include "RoomNode.h"
#include "Room.h"
#include "Player.h"
#include "PauseMenu.h"
#include "MainMenu.h"
#include <fstream>

RoomGraph::RoomGraph(): _generator(this), _idToDestroy(-1), _state(title), _musicFade(true), _wakingUp(false)
{
    _player = _generator.generatePlayer();
    _pauseMenu = _generator.generatePauseMenu(_player);
    _mainMenu = _generator.generateMainMenu();

    _player->setGraph(this);

    _titleMusic = new sf::Music();
    _titleMusic->openFromFile("sounds/music/EP3.ogg");
    _titleMusic->setLoop(true);
    _titleMusic->play();

    for (unsigned int i = 0; i < _nbOfRooms; i++)
    {
        _nodes.push_back(new RoomNode(this));
        _roomMusics.push_back(nullptr);
        _roomMusicVolumes.push_back(100.f);
    }

    sf::Music* spikeMus = new sf::Music();
    spikeMus->openFromFile("sounds/music/DIRT.ogg");
    spikeMus->setLoop(true);
    _musics.push_back(spikeMus);

    sf::Music* stairsMus1 = new sf::Music();
    stairsMus1->openFromFile("sounds/music/RAIN1.ogg");
    stairsMus1->setLoop(true);
    _musics.push_back(stairsMus1);

    sf::Music* stairsMus2 = new sf::Music();
    stairsMus2->openFromFile("sounds/music/RAIN2.ogg");
    stairsMus2->setLoop(true);
    _musics.push_back(stairsMus2);

    sf::Music* jungleMus = new sf::Music();
    jungleMus->openFromFile("sounds/music/HUNTED.ogg");
    jungleMus->setLoop(true);
    _musics.push_back(jungleMus);

    sf::Music* cityMus = new sf::Music();
    cityMus->openFromFile("sounds/music/WASTE.ogg");
    cityMus->setLoop(true);
    _musics.push_back(cityMus);

    sf::Music* desoMus = new sf::Music();
    desoMus->openFromFile("sounds/music/OLYMPOS.ogg");
    desoMus->setLoop(true);
    desoMus->setVolume(30.f);
    _musics.push_back(desoMus);

    sf::Music* mazeMus = new sf::Music();
    mazeMus->openFromFile("sounds/music/CROSS.ogg");
    mazeMus->setLoop(true);
    _musics.push_back(mazeMus);

    sf::Music* wasteMus = new sf::Music();
    wasteMus->openFromFile("sounds/music/DRY.ogg");
    wasteMus->setLoop(true);
    _musics.push_back(wasteMus);

    sf::Music* underwMus = new sf::Music();
    underwMus->openFromFile("sounds/music/EP3.ogg");
    underwMus->setLoop(true);
    underwMus->setPitch(0.5f);
    _musics.push_back(underwMus);

    sf::Music* skyhouseMus = new sf::Music();
    skyhouseMus->openFromFile("sounds/music/POD.ogg");
    skyhouseMus->setLoop(true);
    _musics.push_back(skyhouseMus);

    sf::Music* darkMus = new sf::Music();
    darkMus->openFromFile("sounds/music/WARM.ogg");
    darkMus->setLoop(true);
    _musics.push_back(darkMus);

    sf::Music* hellMus = new sf::Music();
    hellMus->openFromFile("sounds/music/CYBELE.ogg");
    hellMus->setLoop(true);
    _musics.push_back(hellMus);

    sf::Music* spaceMus = new sf::Music();
    spaceMus->openFromFile("sounds/music/SLEEP.ogg");
    spaceMus->setLoop(true);
    _musics.push_back(spaceMus);

    sf::Music* plantMus = new sf::Music();
    plantMus->openFromFile("sounds/music/BREATHING.ogg");
    plantMus->setLoop(true);
    _musics.push_back(plantMus);

    sf::Music* endingMus = new sf::Music();
    endingMus->openFromFile("sounds/music/TERMINUS.ogg");
    endingMus->setLoop(true);
    _musics.push_back(endingMus);

    for (int i = 0; i < 2; i++)
        _roomMusics[i + 4] = _musics[0];

    _roomMusics[7] = stairsMus1;
    _roomMusics[8] = stairsMus2;
    _roomMusics[9] = stairsMus1;
    _roomMusics[10] = stairsMus1;
    _roomMusics[11] = jungleMus;
    _roomMusics[12] = cityMus;
    _roomMusics[13] = desoMus;
    _roomMusicVolumes[13] = 30.f;
    _roomMusics[14] = mazeMus;
    _roomMusics[15] = wasteMus;
    _roomMusics[16] = underwMus;
    _roomMusics[17] = skyhouseMus;
    _roomMusics[19] = skyhouseMus;
    _roomMusics[20] = plantMus;
    _roomMusics[21] = darkMus;
    _roomMusics[22] = hellMus;
    _roomMusics[23] = hellMus;
    _roomMusics[25] = spaceMus;
    _roomMusics[26] = spaceMus;
    _roomMusics[27] = spaceMus;
    _roomMusics[28] = endingMus;

    loadRoom(2);

    _roomId = 2;
}

RoomGraph::~RoomGraph()
{
    delete _player;
    delete _pauseMenu;
    delete _mainMenu;
    
    _titleMusic->stop();
    delete _titleMusic;

    for (unsigned int i = 0; i < _nodes.size(); i++)
        delete _nodes[i];
    for (unsigned int i = 0; i < _musics.size(); i++)
    {
        if (_musics[i] != nullptr)
        {
            _musics[i]->stop();
            delete _musics[i];
        }
    }

    _nodes.clear();
}

void RoomGraph::prepareRoomChange(unsigned int index)
{
    _musicFade = (_roomMusics[_roomId] != _roomMusics[index]);
    
    if (index == 2 && _roomId != 3 && _roomId < 28)
        _wakingUp = true;
    else
        _wakingUp = false;
}

void RoomGraph::changeRoom(unsigned int index)
{
    if (index == 3)
    {
        std::vector<bool> effs = _player->getObtainedEffects();
        int effCount = 0;
        for (unsigned int i = 1; i < effs.size(); i++)
            if (effs[i])
                effCount++;
        
        if (effCount >= 3)
            index = 28;
    }
    _idToDestroy = _roomId;
    _roomId = index;
    loadRoom(_roomId);

    if (_roomId == 2)
        _player->setEffect(normal);
    
    if (_roomMusics[_idToDestroy] != _roomMusics[_roomId])
    {
        if (_roomMusics[_idToDestroy] != nullptr)
            _roomMusics[_idToDestroy]->stop();
        if (_roomMusics[_roomId] != nullptr)
            _roomMusics[_roomId]->play();
    }
}

void RoomGraph::setState(GameState state)
{
    if (state != title)
        _titleMusic->stop();
    else
    {
        _player->endScript();
        _player->setPosition(sf::Vector2f(160.f, 130.f));
        _player->setSchoolgirlKilled(false);
        _player->setWaterfallActivated(false);
        std::vector<bool> effs = _player->getObtainedEffects();
        for (unsigned int i = 0; i < effs.size(); i++)
            effs[i] = false;
        _player->setObtainedEffects(effs);
        if (_roomId == 2)
            changeRoom(3);
        changeRoom(2);
        _titleMusic->play();
        _mainMenu->start();
        _pauseMenu->start();
    }

    _state = state;
}

void RoomGraph::setWakingUp(bool wakeup)
{
    _wakingUp = wakeup;
}

std::vector<GameObject*> RoomGraph::getGraphics()
{
    if (_state == playing)
        return _nodes[_roomId]->_room->getGraphics();
    else if (_state == pausing)
        return _pauseMenu->getGraphics();
    else
        return _mainMenu->getGraphics();
}

unsigned int RoomGraph::getFade()
{
    if (_state == playing)
    {
        if (_roomId != 11)
            return _nodes[_roomId]->_room->getTransitionTime() * _fadeFactor;
        else
            return std::max(_nodes[_roomId]->_room->getTransitionTime() * _fadeFactor, 127u);
    }
    else
    {
        if (_roomId != 11)
            return 0;
        else
            return 127;
    }
}

int RoomGraph::getCurrentRoom() const
{
    return _roomId;
}

GameState RoomGraph::getState() const
{
    return _state;
}

bool RoomGraph::isWakingUp() const
{
    return _wakingUp;
}

void RoomGraph::pause()
{
    if (_state == playing)
    {
        _player->resetInputs();
        _state = pausing;
        _pauseMenu->start();
    }
    else
    {
        _pauseMenu->leaveAll();
        _state = playing;
    }
}

void RoomGraph::leavePauseMenu()
{
    _state = playing;
}

void RoomGraph::load()
{
    std::ifstream stream;
    char* c = new char;
    stream.open("save.bin");
    if (stream.fail() || stream.peek() == EOF)
        return;
        
    stream.read(c, 1);
    std::vector<bool> effVec;
    effVec = _player->getObtainedEffects();

    effVec[machete] = c[0] & 0b00000010;
    effVec[scorpion] = c[0] & 0b00001000;
    effVec[liquid] = c[0] & 0b00010000;
    effVec[mask] = c[0] & 0b01000000;

    _player->setObtainedEffects(effVec);

    delete c;
}

void RoomGraph::augmentVolume(float volume)
{
    if (_roomMusics[_roomId]->getVolume() < volume)
        _roomMusics[_roomId]->setVolume(volume);
}

void RoomGraph::loadRoom(unsigned int id)
{
    _nodes[id]->_room = _generator.generateRoom(id, _player);
    _fadeFactor = 255 / _nodes[id]->_room->_fadeLength;
}

void RoomGraph::deleteRoom(RoomNode* node)
{
    node->deleteRoom();
}

void RoomGraph::tick()
{
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return) &&
        !sf::Keyboard::isKeyPressed(sf::Keyboard::LAlt) && !sf::Keyboard::isKeyPressed(sf::Keyboard::RAlt))
    {
        if (!_enterPressed && _nodes[_roomId]->_room->getState() == active && !_nodes[_roomId]->_room->getFseActive()
            && _roomId < 28)
        {
            _enterPressed = true;
            pause();
        }
    }
    else
        _enterPressed = false;

    if (_state == playing)
    {
        _nodes[_roomId]->_room->tick();
        if (_musicFade && _roomMusics[_roomId] != nullptr && _nodes[_roomId]->_room->getState() != active)
            _roomMusics[_roomId]->setVolume(_roomMusicVolumes[_roomId] * (1 - ((float)_nodes[_roomId]->_room->getTransitionTime() / (float)_nodes[_roomId]->_room->_fadeLength)));
    }
    else if (_state == title)
        _mainMenu->behave();
    else if (_state == pausing)
        _pauseMenu->behave();
    
    if (_idToDestroy != -1)
    {
        _generator.unloadRoom(_idToDestroy);
        _nodes[_idToDestroy]->deleteRoom();
        _idToDestroy = -1;
    }
}
