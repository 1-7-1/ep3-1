/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SCHOOLGIRL_H
#define SCHOOLGIRL_H

#include "Npc.h"

class Schoolgirl: public Npc
{
    private:
        std::vector<Animation*> _animations;
        Player* _player;

    public:
        Schoolgirl(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox,
            std::vector<Animation*> animations, Player* player);
        ~Schoolgirl();

        virtual void interact(Player* player);
        virtual void behave();
};

#endif
