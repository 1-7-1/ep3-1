/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "CollidableObject.h"

CollidableObject::CollidableObject(const sf::Vector2f position, const sf::Vector2f origin, const sf::FloatRect hitbox)
    : GameObject(position, origin), _hitbox(hitbox)
{

}

CollidableObject::~CollidableObject()
{

}

void CollidableObject::setHitbox(const sf::FloatRect hitbox)
{
    _hitbox = hitbox;
}

sf::FloatRect CollidableObject::getHitbox() const
{
    return _hitbox;
}

std::vector<std::vector<CollidableObject*>*> CollidableObject::getLists()
{
    return _lists;
}

std::vector<sf::Vector2i> CollidableObject::getGridPositions() const
{
    return _gridPositions;
}

void CollidableObject::setGridPositions(const std::vector<sf::Vector2i> pos)
{
    _gridPositions = pos;
}

void CollidableObject::addList(std::vector<CollidableObject*>* list)
{
    _lists.push_back(list);
}

void CollidableObject::clearList()
{
    _lists.clear();
    _gridPositions.clear();
}
