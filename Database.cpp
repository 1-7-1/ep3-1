/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Database.h"

Database::Database()
{
    
}

Database::~Database()
{
    unload();
}

unsigned int Database::loadTexture(std::string fileName, sf::Color transColor)
{
    sf::Image image = sf::Image();
    image.loadFromFile(fileName);
    image.createMaskFromColor(transColor);
	sf::Texture* texture = new sf::Texture();
	texture->loadFromImage(image);
    _textures.push_back(texture);

    return _textures.size();
}

unsigned int Database::loadSound(std::string fileName)
{
    sf::SoundBuffer* buffer = new sf::SoundBuffer();
    buffer->loadFromFile(fileName);
    sf::Sound* sound = new sf::Sound();
    sound->setBuffer(*buffer);
    _soundBuffers.push_back(buffer);
    _sounds.push_back(sound);

    return _sounds.size();
}

sf::Texture* Database::getTexture(unsigned int index) const
{
    if (index < _textures.size())
        return _textures[index];
    else
        return nullptr;
}

sf::Sound* Database::getSound(unsigned int index) const
{
    if (index < _sounds.size())
        return _sounds[index];
    else
        return nullptr;
}

void Database::unload()
{
    for(sf::Texture* tex: _textures)
        delete tex;
    _textures.clear();

    for(unsigned int i = 0; i < _sounds.size(); i++)
    {
        _sounds[i]->stop();
        delete _sounds[i];
    }
    _sounds.clear();

    for(unsigned int i = 0; i < _soundBuffers.size(); i++)
        delete _soundBuffers[i];
    _soundBuffers.clear();
}