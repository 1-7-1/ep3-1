/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "EndingText.h"
#include "Animation.h"
#include "Room.h"

EndingText::EndingText(sf::Vector2f position, sf::Vector2f origin, sf::Vector2f speed, Animation* animation)
    : Decoration(position, origin, speed, animation), _timer(0), _state(invisible)
{
    _currentAnimation->setTransparency(0);
}

EndingText::~EndingText()
{

}

void EndingText::behave()
{
    switch (_state)
    {
        case invisible:
            if (_timer > 680)
                _state = appearing;
            else
                _timer++;
            break;

        case appearing:
            if (_timer >= 935)
            {
                _state = waiting;
                _currentAnimation->setTransparency(255);
            }
            else
            {
                _currentAnimation->setTransparency(_timer - 680);
                _timer++;
            }
            break;

        case waiting:
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
                _state = disappearing;
            break;

        case disappearing:
            if (_timer <= 680)
                _room->changeRoom(29, sf::Vector2f(), false, 255);
            else
            {
                _currentAnimation->setTransparency(_timer - 680);
                _timer--;
            }
            break;
        
        default:
            break;
    }

    changeAnimation();
}
