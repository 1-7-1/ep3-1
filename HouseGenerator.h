/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef HOUSE_GEN_H
#define HOUSE_GEN_H

#include "GameObjectGenerator.h"

class Room;
class Player;
class RoomGraph;
class Decoration;
class Wall;
class Door;

class HouseGenerator: public GameObjectGenerator
{
    private:
        enum Graphics { invisible, floor, door, pipe };
        enum Sounds { doorSfx };
        virtual void load();

        const float _roomWidth = 320.f;
        const float _roomHeight = 240.f;

        Decoration* generateFloor() const;

        Wall* generateWall(sf::Vector2f position, sf::FloatRect hitbox) const;
        std::vector<Wall*> generateWalls(sf::Vector2f position) const;

        std::vector<Wall*> generatePipes(sf::Vector2f position) const;
        
        std::vector<std::vector<std::vector<sf::Vector2u>>> generateDoorOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateDoorTimeVectors() const;

        Door* generateDoor(sf::Vector2f position) const;

    public:
        HouseGenerator();
        ~HouseGenerator();

        Room* generateHouse(Player* player, RoomGraph* graph);
};

#endif
