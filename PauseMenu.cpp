/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "PauseMenu.h"
#include "MenuCursor.h"
#include "RoomGraph.h"
#include "SFML/Audio.hpp"

PauseMenu::PauseMenu(std::vector<Animation*> animations, MenuCursor* cursor, RoomGraph* graph,
    std::vector<Menu*> subMenus, std::vector<sf::Sound*> sounds)
    : Menu(animations, cursor, nullptr, subMenus), _graph(graph), _sounds(sounds)
{
    _currentAnimation = _animations[0];
    _cursor->setPosition(sf::Vector2f(0.f, 208.f));
}

PauseMenu::~PauseMenu()
{
    
}

void PauseMenu::resetInputs()
{
    _leftPressed = true;
    _rightPressed = true;
    _zPressed = true;
    _xPressed = true;
}

void PauseMenu::handleInput()
{
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        if (!_leftPressed)
        {
            if (_cursorPosition == 0)
                _cursorPosition = _subMenus.size() - 1;
            else
                _cursorPosition--;
            _leftPressed = true;
        }
    }
    else
        _leftPressed = false;
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        if (!_rightPressed)
        {
            _cursorPosition++;
            _cursorPosition %= _subMenus.size();
            _rightPressed = true;
        }
    }
    else
        _rightPressed = false;
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
    {
        if (!_zPressed)
        {
            if (_cursorPosition == 1)
            {
                _graph->setState(title);
                return;
            }
            if (_subMenus[_cursorPosition] != nullptr)
            {
                if (_sounds[0])
                    _sounds[0]->play();
                enter();
                return;
            }
        }
    }
    else
        _zPressed = false;
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::X))
    {
        if (!_xPressed)
        {
            if (_sounds[1])
                _sounds[1]->play();
            leave();
        }
    }
    else
        _xPressed = false;
}

void PauseMenu::placeCursors()
{
    _cursor->setPosition(_position + sf::Vector2f(_cursorPosition * 160.f, 208.f));
}

void PauseMenu::behaveYourself()
{
    if (_active)
        placeCursors();
}

void PauseMenu::leave()
{
    resetInputs();
    _graph->leavePauseMenu();
}
