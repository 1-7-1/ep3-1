/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Scorpion.h"
#include "Animation.h"
#include "Player.h"
#include "Room.h"
#include "MathConstants.h"
#include <math.h>

Scorpion::Scorpion(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox,
    std::vector<std::vector<Animation*>> animations)
    : CollidableObject(position, origin, hitbox), Npc(position, origin, 0.6f, hitbox),
    _currentDirection(down), _animations(animations)
{
    _currentState = idle;

    for (std::vector<Animation*> animList : _animations)
    {
        for (Animation* anim : animList)
            anim->setOrigin(_origin);
    }

    _currentAnimation = _animations[_currentState][_currentDirection];
}

Scorpion::~Scorpion()
{
    for (std::vector<Animation*> animList : _animations)
        for (Animation* anim : animList)
            delete anim;
}

void Scorpion::interact(Player* player)
{
    Npc::interact(player);
    if (player->getState() != action && !player->getObtainedEffects()[scorpion])
    {
        _room->createSpecialEffect(player->getPosition() + sf::Vector2f(0.f, 0.5f), sf::Vector2f(0.f, 45.f), effectGet);
        std::vector<bool> effs = player->getObtainedEffects();
        effs[scorpion] = true;
        player->setObtainedEffects(effs);
    }
}

void Scorpion::behave()
{
    if (_currentState != dead)
    {
        if (_currentState == idle && rand() % 60 == 0)
        {
            int degrees = rand() % 360;
            if (degrees < 45 || degrees >= 315)
                _currentDirection = right;
            else if (degrees < 135 && degrees >= 45)
                _currentDirection = up;
            else if (degrees < 225 && degrees >= 135)
                _currentDirection = left;
            else
                _currentDirection = down;
            _direction = float(degrees) * math::PI / 180.f;
            _speed.x = _speedLength * cos(_direction);
            _speed.y = -_speedLength * sin(_direction);
            _currentState = moving;
        }
        else
        {
            if (rand() % 300 == 0)
            {
                _speed = sf::Vector2f(0.f, 0.f);
                _currentState = idle;
            }
            else if (_currentState == moving)
            {
                _speed.x = _speedLength * cos(_direction);
                _speed.y = -_speedLength * sin(_direction);
            }
        }

        if (_speed.x != 0.f || _speed.y != 0.f)
        {
            bool freeNext = true;
            bool freeX = (_speed.x != 0.f);
            bool freeY = (_speed.y != 0.f);

            for (unsigned int j = 0; j < _lists.size(); j++)
            {
                if (freeNext)
                    freeNext = !checkCollision(_speed, _lists[j]);
                if (freeX)
                    freeX = !checkCollision(sf::Vector2f(copysign(_speedLength, _speed.x), 0.f), _lists[j]);
                if (freeY)
                    freeY = !checkCollision(sf::Vector2f(0.f, copysign(_speedLength, _speed.y)), _lists[j]);
            }

            if (!freeNext)
            {
                if (freeX && fabs(_speed.x) > sin(math::PI / 12.0) * _speedLength)
                    _speed = sf::Vector2f(copysign(_speedLength, _speed.x), 0.f);
                else if (freeY && fabs(_speed.y) > sin(math::PI / 12.0) * _speedLength)
                    _speed = sf::Vector2f(0.f, copysign(_speedLength, _speed.y));
                else
                    _speed = sf::Vector2f(0.f, 0.f);
            }
        }
        move();
        changeAnimation();
    }
    else
    {
        _currentAnimation->setColor(sf::Color(255, 255, 255, _timeToDie * 4));
        _timeToDie--;
        if (_timeToDie == 0)
            _room->setToDestroy(this);
    }
}

void Scorpion::changeAnimation()
{
    if (_speed == sf::Vector2f(0.f, 0.f))
        _currentState = idle;
    else
    {
        _currentState = moving;
        float direction = atan2(-_speed.y, _speed.x);
        float degrees = direction * 180.f / math::PI;

        if (degrees < 45.f && degrees >= -45.f)
            _currentDirection = right;
        else if (degrees < 135.f && degrees >= 45.f)
            _currentDirection = up;
        else if (degrees < -135.f || degrees >= 135.f)
            _currentDirection = left;
        else
            _currentDirection = down;
    }
    
    _currentAnimation = _animations[_currentState][_currentDirection];
    Entity::changeAnimation();
}
