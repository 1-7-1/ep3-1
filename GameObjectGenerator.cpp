/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "GameObjectGenerator.h"
#include "TileCalculator.h"
#include "Animation.h"
#include "AnimationFrames.h"

GameObjectGenerator::GameObjectGenerator(): _db()
{
    
}

GameObjectGenerator::~GameObjectGenerator()
{

}

std::vector<std::vector<Animation*>> GameObjectGenerator::generateAnimationVector(sf::Texture* texture,
    const std::vector<std::vector<std::vector<sf::Vector2u>>> offsets, const std::vector<std::vector<unsigned int>> times,
    const sf::Vector2u frameSize) const
{
    sf::Vector2u initOff = sf::Vector2u(0, 0);

    TileCalculator frameCalc = TileCalculator(initOff, frameSize);

    std::vector<std::vector<Animation*>> animationVector = std::vector<std::vector<Animation*>>(offsets.size());

    for (unsigned int i = 0; i < offsets.size(); i++)
        for (unsigned int j = 0; j < offsets[i].size(); j++)
            animationVector[i].push_back(new Animation(frameCalc(offsets[i][j], times[i]), texture));

    return animationVector;
}

void GameObjectGenerator::unload()
{
    _db.unload();
    _isLoaded = false;
}
