/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BIG_SPIKE_GEN_H
#define BIG_SPIKE_GEN_H

#include "GameObjectGenerator.h"

class Room;
class Player;
class RoomGraph;
class Wall;
class Decoration;
class AutoTransition;
class Stairs;
class FseTrigger;
class SpikeFse;

class BigSpikeGenerator: public GameObjectGenerator
{
    private:
        enum SpikeGraphics { spikeInvisible, bigSpike, spikeFse, spikeFseSky };
        virtual void load();

        AutoTransition* generateTile(const sf::Vector2f position, const unsigned int destination,
            const sf::Vector2f transitionVec, const bool absolute, Player* _player) const;

        Animation* generateBigSpikeFloorAnimation() const;
        Decoration* generateBigSpikeFloor(sf::Vector2f position) const;

        std::vector<Animation*> generateBigSpikeFloorAnimations() const;
        std::vector<Wall*> generateBigSpikeWalls(sf::Vector2f position) const;

        Stairs* generateStairs(const sf::Vector2f position, const sf::Vector2f rightVec,
            Player* _player) const;

        Animation* generateSpikeFseSkyAnimation() const;
        Animation* generateSpikeFseAnimation() const;
        FseTrigger* generateFseTrigger(const sf::Vector2f position,
            Player* _player) const;

        SpikeFse* generateSpikeFse() const;

    public:
        BigSpikeGenerator();
        ~BigSpikeGenerator();

        Room* generateBigSpikeArea(Player* player, RoomGraph* graph);
};

#endif
