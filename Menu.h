/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MENU_H
#define MENU_H

#include "GameObject.h"

class MenuCursor;

class Menu: public GameObject
{
    protected:
        std::vector<Animation*> _animations;

        MenuCursor* _cursor;
        unsigned int _cursorPosition;

        Menu* _parentMenu;
        std::vector<Menu*> _subMenus;

        bool _active;

        virtual void resetInputs() = 0;
        virtual void handleInput() = 0;
        virtual void behaveYourself() = 0;

    public:
        Menu(std::vector<Animation*> animations, MenuCursor* cursor,
            Menu* parent = nullptr, std::vector<Menu*> subMenus = std::vector<Menu*>({nullptr}));
        ~Menu();

        virtual std::vector<GameObject*> getGraphics();

        void setActive(bool active);
        void setParent(Menu* parent);

        bool isActive() const;

        virtual void start();

        virtual void enter();
        virtual void leave();
        virtual void leaveAll();

        virtual void placeCursors() = 0;

        virtual void behave();
};

#endif
