/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "TileCalculator.h"
#include "AnimationFrames.h"

TileCalculator::TileCalculator(sf::Vector2u initOff, sf::Vector2u frameSize)
                              : _initialOffset(initOff), _frameSize(frameSize)
{

}

TileCalculator::~TileCalculator()
{

}

std::vector<AnimationFrame> TileCalculator::operator()(std::vector<sf::Vector2u> coordList, std::vector<unsigned int> times)
{
    std::vector<AnimationFrame> frames;
    for (unsigned int i = 0; i < coordList.size(); i++)
	{
        sf::IntRect rect = sf::IntRect(_initialOffset.x + coordList[i].x * _frameSize.x,
                                       _initialOffset.y + coordList[i].y * _frameSize.y,
                                       _frameSize.x, _frameSize.y);
		frames.push_back({ rect, times[i] });
	}
    return frames;
}
