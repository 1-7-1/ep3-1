/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DB_H
#define DB_H

#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"
#include <vector>

class Database
{
    private:
        std::vector<sf::Texture*> _textures;
        std::vector<sf::Sound*> _sounds;
        std::vector<sf::SoundBuffer*> _soundBuffers;
        
    public:
        Database();
        ~Database();

        unsigned int loadTexture(std::string fileName, sf::Color transColor = sf::Color(0, 0, 0, 0));
        unsigned int loadSound(std::string fileName);

        sf::Texture* getTexture(unsigned int index) const;
        sf::Sound* getSound(unsigned int index) const;

        void unload();
};

#endif
