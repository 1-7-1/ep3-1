/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "JungleGenerator.h"
#include "TileCalculator.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "Decoration.h"
#include "Room.h"
#include "Wall.h"
#include "SeekingFloor.h"
#include "AutoTransition.h"
#include "ScreenEffect.h"
#include "Player.h"
#include "MachetePedestal.h"
#include "Spider.h"

JungleGenerator::JungleGenerator()
{

}

JungleGenerator::~JungleGenerator()
{

}

void JungleGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/tilesets/12.png", sf::Color(81, 87, 95, 255));
    _db.loadTexture("graphics/tilesets/13.png", sf::Color(81, 87, 95, 255));
    _db.loadTexture("graphics/effect/4.png", sf::Color(81, 87, 95, 255));
    _db.loadTexture("graphics/object/4.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/char/13.png", sf::Color(255, 0, 255, 255));
    _isLoaded = true;
}

std::vector<ScreenEffect*> JungleGenerator::generateRain() const
{
    TileCalculator rainCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 64));
    std::vector<ScreenEffect*> raindrops;

    sf::Vector2f origin = sf::Vector2f(8.f, 32.f);
    sf::Vector2f speed = sf::Vector2f(-6.f, 24.f);

    std::vector<sf::Vector2f> positions = {sf::Vector2f(112.f, 15.f), sf::Vector2f(213.f, 124.f), sf::Vector2f(255.f, 188.f),
    sf::Vector2f(282.f, 21.f), sf::Vector2f(148.f, 186.f), sf::Vector2f(80.f, 11.f), sf::Vector2f(309.f, 234.f),
    sf::Vector2f(143.f, 253.f), sf::Vector2f(5.f, 0.f), sf::Vector2f(251.f, 8.f), sf::Vector2f(85.f, 161.f),
    sf::Vector2f(85.f, 141.f), sf::Vector2f(118.f, 87.f), sf::Vector2f(234.f, 130.f), sf::Vector2f(257.f, 209.f)
    ,
    sf::Vector2f(204.f, 62.f), sf::Vector2f(85.f, 263.f), sf::Vector2f(196.f, 52.f), sf::Vector2f(104.f, 263.f),
    sf::Vector2f(302.f, 137.f), sf::Vector2f(182.f, 103.f), sf::Vector2f(202.f, 122.f), sf::Vector2f(53.f, 297.f),
    sf::Vector2f(67.f, 203.f), sf::Vector2f(76.f, 302.f), sf::Vector2f(167.f, 258.f), sf::Vector2f(30.f, 247.f),
    sf::Vector2f(70.f, 70.f), sf::Vector2f(252.f, 258.f), sf::Vector2f(296.f, 111.f), sf::Vector2f(55.f, 87.f),
    sf::Vector2f(184.f, 25.f), sf::Vector2f(335.f, 159.f), sf::Vector2f(335.f, 8.f), sf::Vector2f(113.f, 171.f),
    sf::Vector2f(4.f, 187.f), sf::Vector2f(302.f, 191.f), sf::Vector2f(218.f, 253.f), sf::Vector2f(224.f, 66.f)};

    for (unsigned int i = 0; i < positions.size(); i ++)
    {
        Animation* anim = new Animation(rainCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(rain));

        raindrops.push_back(new ScreenEffect(positions[i], origin, speed, anim));
    }

    return raindrops;
}

std::vector<SeekingFloor*> JungleGenerator::generateFloorTiles(Player* player) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    std::vector<SeekingFloor*> floors;

    for (int i = -32; i < 352; i += 16)
        for (int j = -16; j < 256; j += 16)
        {
            Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));

            floors.push_back(new SeekingFloor(sf::Vector2f(0.f, 0.f), anim,
                sf::Vector2f(i, j), sf::Vector2f(16.f, 16.f), player));
        }

    return floors;
}

Wall* JungleGenerator::generateWall(sf::Vector2f position, WallDir dir, bool tree) const
{
    Animation* anim;

    if (!tree)
    {
        TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
        switch (dir)
        {
            case right:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(5, 2)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                break;
            
            case upright:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(5, 1)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                break;
            
            case up:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(3, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                break;
            
            case upleft:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                break;
            
            case left:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 1)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                break;
            
            case downleft:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 2)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                break;
            
            case down:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(4, 3)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                break;
            
            case downright:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(5, 3)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                break;
            
            case turnupright:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(4, 1)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                break;
            
            case turnupleft:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(7, 1)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                break;
            
            case turndownleft:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(3, 2)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                break;
            
            case turndownright:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(8, 2)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                break;
        }

        return new Wall(position, sf::Vector2f(0.f, 0.f), anim, sf::FloatRect(0.f, 0.f, 16.f, 16.f));
    }
    else
    {
        TileCalculator wallCalc = TileCalculator(sf::Vector2u(272, 0), sf::Vector2u(96, 112));
        switch (dir)
        {
            case right:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(3, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                return new Wall(position + sf::Vector2f(8.f, 12.f), sf::Vector2f(24.f, 108.f), anim, sf::FloatRect(-8.f, -12.f, 16.f, 16.f));
                break;

            case left:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                return new Wall(position + sf::Vector2f(8.f, 12.f), sf::Vector2f(72.f, 108.f), anim, sf::FloatRect(-8.f, -12.f, 16.f, 16.f));
                break;

            case downright:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(3, 1)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                return new Wall(position + sf::Vector2f(8.f, 8.f), sf::Vector2f(56.f, 104.f), anim, sf::FloatRect(-8.f, -8.f, 16.f, 16.f));
                break;

            case downleft:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 1)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                return new Wall(position + sf::Vector2f(8.f, 8.f), sf::Vector2f(40.f, 104.f), anim, sf::FloatRect(-8.f, -8.f, 16.f, 16.f));
                break;

            case upright:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 1)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                return new Wall(position, sf::Vector2f(48.f, 96.f), anim, sf::FloatRect(0.f, 0.f, 16.f, 16.f));
                break;

            case upleft:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 1)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                return new Wall(position + sf::Vector2f(16.f, 16.f), sf::Vector2f(48.f, 112.f), anim, sf::FloatRect(-16.f, -16.f, 32.f, 32.f));
                break;
            
            case up:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(5, 1)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                return new Wall(position + sf::Vector2f(8.f, 8.f), sf::Vector2f(40.f, 104.f), anim, sf::FloatRect(-8.f, -8.f, 16.f, 16.f));
                break;
                
            case down:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(4, 1)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                return new Wall(position + sf::Vector2f(8.f, 8.f), sf::Vector2f(40.f, 104.f), anim, sf::FloatRect(-8.f, -8.f, 16.f, 16.f));
                break;
            
            default:
                anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(5, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                return new Wall(position + sf::Vector2f(16.f, 30.f), sf::Vector2f(48.f, 110.f), anim, sf::FloatRect(-16.f, -30.f, 32.f, 32.f));
                break;
        }
    }
}

std::vector<Wall*> JungleGenerator::generateSpike(sf::Vector2f position) const
{
    TileCalculator invisibleCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(448, 128));

    std::vector<Wall*> walls;

    Animation* spikeAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(spike));

    Wall* bigSpike = new Wall(position, sf::Vector2f(256.f, 64.f), spikeAnim, sf::FloatRect(0.f, 0.f, 16.f, 1.f));

    walls.push_back(bigSpike);
    
    Animation* wallAnim = new Animation(invisibleCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(invisible));

    Wall* edgeWall = new Wall(position + sf::Vector2f(32.f, 62.f), sf::Vector2f(), wallAnim, sf::FloatRect(0.f, -3.f, 3.f, 4.f));
    walls.push_back(edgeWall);

    for (int i = 0; i < 8; i++)
    {
        Animation* anim = new Animation(invisibleCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(invisible));

        Wall* wall = new Wall(position + sf::Vector2f(32.f + i * 9.f, 61.f - i), sf::Vector2f(), anim, sf::FloatRect(0.f, -3.f, 9.f, 4.f));
        walls.push_back(wall);
    }

    Animation* leftAnim = new Animation(invisibleCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(invisible));

    Wall* leftWall = new Wall(position + sf::Vector2f(-32.f, 48.f), sf::Vector2f(), leftAnim, sf::FloatRect(0.f, 0.f, 32.f, 16.f));
    walls.push_back(leftWall);

    return walls;
}

AutoTransition* JungleGenerator::generateSpikeTile(sf::Vector2f position, Player* player) const
{
    TileCalculator invisibleCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    
    Animation* anim = new Animation(invisibleCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(invisible));

    return new AutoTransition(position + sf::Vector2f(0.f, 48.f), sf::Vector2f(),
        sf::FloatRect(0.f, 0.f, 32.f, 14.f), anim, 10, sf::Vector2f(104.f, 136.f), true, player);
}

std::vector<Wall*> JungleGenerator::generateSmallTree(sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(0, 64), sf::Vector2u(112, 144));

    Animation* treeAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(tileset));

    Wall* treeWall = new Wall(position, sf::Vector2f(40.f, 94.f), treeAnim, sf::FloatRect(-8.f, -4.f, 16.f, 6.f));

    std::vector<Wall*> walls;
    walls.push_back(treeWall);

    return walls;
}

std::vector<Wall*> JungleGenerator::generateVerySmallTree(sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(112, 144), sf::Vector2u(96, 96));

    Animation* treeAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(tileset));

    Wall* treeWall = new Wall(position, sf::Vector2f(24.f, 62.f), treeAnim, sf::FloatRect(-6.f, -2.f, 12.f, 4.f));

    std::vector<Wall*> walls;
    walls.push_back(treeWall);

    return walls;
}

std::vector<Wall*> JungleGenerator::generateTallTree(sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(432, 0), sf::Vector2u(32, 96));

    Animation* treeAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(tileset));

    Wall* treeWall = new Wall(position, sf::Vector2f(16.f, 95.f), treeAnim, sf::FloatRect(-8.f, -7.f, 15.f, 7.f));

    std::vector<Wall*> walls;
    walls.push_back(treeWall);

    return walls;
}

std::vector<Wall*> JungleGenerator::generateBigTree(sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(320, 0), sf::Vector2u(64, 112));
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(0, 0), sf::Vector2u(16, 16));

    std::vector<Wall*> walls;
    std::vector<Animation*> anims;

    Animation* treeAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(tileset));

    Wall* treeWall = new Wall(position, sf::Vector2f(32.f, 88.f), treeAnim, sf::FloatRect(-16.f, -8.f, 32.f, 10.f));

    for (int i = 0; i < 18; i++)
        anims.push_back(new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible)));
    
    // root1
    walls.push_back(new Wall(position + sf::Vector2f(-21.f, -6.f), sf::Vector2f(),
        anims[0], sf::FloatRect(0.f, 0.f, 4.f, 8.f)));
    walls.push_back(new Wall(position + sf::Vector2f(-26.f, -4.f), sf::Vector2f(),
        anims[1], sf::FloatRect(0.f, 0.f, 5.f, 7.f)));
    walls.push_back(new Wall(position + sf::Vector2f(-29.f, 0.f), sf::Vector2f(),
        anims[2], sf::FloatRect(0.f, 0.f, 5.f, 4.f)));
    walls.push_back(new Wall(position + sf::Vector2f(-31.f, 3.f), sf::Vector2f(),
        anims[3], sf::FloatRect(0.f, 0.f, 7.f, 4.f)));
    walls.push_back(new Wall(position + sf::Vector2f(-32.f, 6.f), sf::Vector2f(),
        anims[4], sf::FloatRect(0.f, 0.f, 2.f, 2.f)));

    // root2
    walls.push_back(new Wall(position + sf::Vector2f(-10.f, 2.f), sf::Vector2f(),
        anims[5], sf::FloatRect(0.f, 0.f, 8.f, 2.f)));
    walls.push_back(new Wall(position + sf::Vector2f(-12.f, 4.f), sf::Vector2f(),
        anims[6], sf::FloatRect(0.f, 0.f, 6.f, 3.f)));
    walls.push_back(new Wall(position + sf::Vector2f(-13.f, 8.f), sf::Vector2f(),
        anims[7], sf::FloatRect(0.f, 0.f, 3.f, 2.f)));
    walls.push_back(new Wall(position + sf::Vector2f(-13.f, 11.f), sf::Vector2f(),
        anims[8], sf::FloatRect(0.f, 0.f, 2.f, 3.f)));

    // root3
    walls.push_back(new Wall(position + sf::Vector2f(6.f, 1.f), sf::Vector2f(),
        anims[9], sf::FloatRect(0.f, 0.f, 8.f, 2.f)));
    walls.push_back(new Wall(position + sf::Vector2f(11.f, 4.f), sf::Vector2f(),
        anims[10], sf::FloatRect(0.f, 0.f, 4.f, 1.f)));
    walls.push_back(new Wall(position + sf::Vector2f(14.f, 6.f), sf::Vector2f(),
        anims[11], sf::FloatRect(0.f, 0.f, 3.f, 2.f)));
    walls.push_back(new Wall(position + sf::Vector2f(16.f, 8.f), sf::Vector2f(),
        anims[12], sf::FloatRect(0.f, 0.f, 2.f, 2.f)));

    // root4
    walls.push_back(new Wall(position + sf::Vector2f(16.f, -5.f), sf::Vector2f(),
        anims[13], sf::FloatRect(0.f, 0.f, 8.f, 6.f)));
    walls.push_back(new Wall(position + sf::Vector2f(24.f, -2.f), sf::Vector2f(),
        anims[14], sf::FloatRect(0.f, 0.f, 3.f, 5.f)));
    walls.push_back(new Wall(position + sf::Vector2f(27.f, 0.f), sf::Vector2f(),
        anims[15], sf::FloatRect(0.f, 0.f, 3.f, 4.f)));
    walls.push_back(new Wall(position + sf::Vector2f(29.f, 3.f), sf::Vector2f(),
        anims[16], sf::FloatRect(0.f, 0.f, 2.f, 2.f)));
    walls.push_back(new Wall(position + sf::Vector2f(30.f, 5.f), sf::Vector2f(),
        anims[17], sf::FloatRect(0.f, 0.f, 2.f, 3.f)));

    walls.push_back(treeWall);

    return walls;
}

std::vector<Wall*> JungleGenerator::generateSmallRock(sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(240, 48), sf::Vector2u(64, 32));

    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(tileset));

    Wall* rockWall = new Wall(position, sf::Vector2f(8.f, 14.f), anim, sf::FloatRect(-6.f, -1.f, 12.f, 3.f));

    std::vector<Wall*> walls;
    walls.push_back(rockWall);

    return walls;
}

std::vector<Wall*> JungleGenerator::generateBigRock(sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(240, 0), sf::Vector2u(80, 48));

    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(tileset));

    Wall* rockWall = new Wall(position, sf::Vector2f(16.f, 30.f), anim, sf::FloatRect(-14.f, -3.f, 28.f, 5.f));

    std::vector<Wall*> walls;
    walls.push_back(rockWall);

    return walls;
}

std::vector<Wall*> JungleGenerator::generateSmallFern(sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(160, 48), sf::Vector2u(32, 16));

    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(tileset));

    Wall* plantWall = new Wall(position, sf::Vector2f(16.f, 14.f), anim, sf::FloatRect(-1.f, -1.f, 2.f, 3.f));

    std::vector<Wall*> walls;
    walls.push_back(plantWall);

    return walls;
}

std::vector<Wall*> JungleGenerator::generateBigFern(sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(160, 0), sf::Vector2u(80, 48));

    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(tileset));

    Wall* plantWall = new Wall(position, sf::Vector2f(24.f, 30.f), anim, sf::FloatRect(-1.f, -1.f, 3.f, 3.f));

    std::vector<Wall*> walls;
    walls.push_back(plantWall);

    return walls;
}

std::vector<Wall*> JungleGenerator::generateWeirdPlant(sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(208, 144), sf::Vector2u(48, 32));

    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(tileset));

    Wall* plantWall = new Wall(position, sf::Vector2f(26.f, 30.f), anim, sf::FloatRect(0.f, -1.f, 1.f, 3.f));

    std::vector<Wall*> walls;
    walls.push_back(plantWall);

    return walls;
}

std::vector<Wall*> JungleGenerator::generateTrunk(sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(112, 80), sf::Vector2u(144, 64));

    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(tileset));

    Wall* trunkWall = new Wall(position, sf::Vector2f(48.f, 46.f), anim, sf::FloatRect(-32.f, -1.f, 80.f, 3.f));

    std::vector<Wall*> walls;
    walls.push_back(trunkWall);

    return walls;
}

MachetePedestal* JungleGenerator::generatePedestal(Player* player) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(32, 32));

    std::vector<Animation*> anims;

    anims.push_back(new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(pedestal)));
    anims.push_back(new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(pedestal)));

    return new MachetePedestal(sf::Vector2f(1877.f, 3662.f), sf::Vector2f(16.f, 22.f), sf::FloatRect(-7.f, 0.f, 14.f, 9.f),
        anims, player);
}

std::vector<Spider*> JungleGenerator::generateSpiders() const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(64, 48));

    std::vector<Spider*> spiders;

    std::vector<sf::Vector2f> positions = {sf::Vector2f(1494.f, 2738.f), sf::Vector2f(1651.f, 3024.f),
        sf::Vector2f(1385.f, 3590.f), sf::Vector2f(1347.f, 4199.f), sf::Vector2f(1788.f, 4182.f),
        sf::Vector2f(3727.f, 4102.f), sf::Vector2f(3776.f, 2796.f)};

    for (unsigned int i = 0; i < positions.size(); i++)
    {
        std::vector<Animation*> idleAnims;

        idleAnims.push_back(new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 2), sf::Vector2u(0, 0),
            sf::Vector2u(1, 0), sf::Vector2u(2, 0), sf::Vector2u(3, 0), sf::Vector2u(4, 0), sf::Vector2u(5, 0)}),
            std::vector<unsigned int>({240, 6, 6, 6, 6, 6, 6})), _db.getTexture(spider)));
        idleAnims.push_back(new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 4), sf::Vector2u(3, 2),
            sf::Vector2u(4, 2), sf::Vector2u(5, 2), sf::Vector2u(3, 4), sf::Vector2u(4, 4), sf::Vector2u(5, 4)}),
            std::vector<unsigned int>({240, 6, 6, 6, 6, 6, 6})), _db.getTexture(spider)));

        std::vector<Animation*> walkAnims;

        walkAnims.push_back(new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 2), sf::Vector2u(1, 2),
            sf::Vector2u(2, 2)}),
            std::vector<unsigned int>({9, 9, 9})), _db.getTexture(spider)));
        walkAnims.push_back(new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 4), sf::Vector2u(1, 4),
            sf::Vector2u(2, 4)}),
            std::vector<unsigned int>({9, 9, 9})), _db.getTexture(spider)));

        spiders.push_back(new Spider(positions[i], sf::Vector2f(32.f, 42.f),
            sf::FloatRect(-32.f, -2.f, 64.f, 4.f), std::vector<std::vector<Animation*>>({idleAnims, walkAnims})));
    }

    return spiders;
}

Room* JungleGenerator::generateJungle(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;
    std::vector<GameObject*> foregrounds;
    
    std::vector<ScreenEffect*> rain = generateRain();
    foregrounds.insert(foregrounds.end(), rain.begin(), rain.end());

    std::vector<SeekingFloor*> terrain = generateFloorTiles(player);
    floors.insert(floors.end(), terrain.begin(), terrain.end());

    std::vector<Wall*> bigSpike = generateSpike(sf::Vector2f(2544.f, 2496.f));
    collidables.insert(collidables.end(), bigSpike.begin(), bigSpike.end());
    floors.push_back(generateSpikeTile(sf::Vector2f(2544.f, 2496.f), player));

    std::vector<Wall*> patch = generateStartingArea();
    collidables.insert(collidables.end(), patch.begin(), patch.end());

    patch = generateHubArea();
    collidables.insert(collidables.end(), patch.begin(), patch.end());

    patch = generateUnderPassage();
    collidables.insert(collidables.end(), patch.begin(), patch.end());

    patch = generateOpenSpace();
    collidables.insert(collidables.end(), patch.begin(), patch.end());

    patch = generateMacheteArea();
    collidables.insert(collidables.end(), patch.begin(), patch.end());

    patch = generateMazeALeftArea();
    collidables.insert(collidables.end(), patch.begin(), patch.end());

    patch = generateMazeAHand();
    collidables.insert(collidables.end(), patch.begin(), patch.end());
    
    patch = generateMazeAStart();
    collidables.insert(collidables.end(), patch.begin(), patch.end());
    
    patch = generateMazeAEntrance();
    collidables.insert(collidables.end(), patch.begin(), patch.end());
    
    patch = generateSecludedArea();
    collidables.insert(collidables.end(), patch.begin(), patch.end());

    MachetePedestal* ped = generatePedestal(player);
    collidables.push_back(ped);
    interactibles.push_back(ped);

    std::vector<Spider*> spiders = generateSpiders();
    entities.insert(entities.end(), spiders.begin(), spiders.end());
    interactibles.insert(interactibles.end(), spiders.begin(), spiders.end());

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, foregrounds, std::vector<GameObject*>() };

    return new Room(player, graph, nullptr, objects, sf::Vector2f(_roomWidth, _roomHeight), sf::Vector2<bool>(false, false));
}
