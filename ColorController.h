/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COL_CTRL_H
#define COL_CTRL_H

#include "GameObject.h"

class ColorController: public GameObject
{
    private:
        std::vector<GameObject*> _objects;
        sf::Color _currentColor;

    public:
        ColorController(Animation* anim, std::vector<GameObject*> objects);
        ~ColorController();

        virtual void behave();

        void remove(GameObject* obj);
        void add(std::vector<GameObject*> objects);
};

#endif
