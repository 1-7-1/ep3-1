/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SPECIAL_EFF_GEN_H
#define SPECIAL_EFF_GEN_H

#include "GameObjectGenerator.h"
#include "GameObject.h"

class SpecialEffect;

enum EffectType { blood, effectSwitch, gameSaved, effectGet, ripple, swim1, swim2, swim3, swim4 };

class SpecialEffectGenerator: public GameObjectGenerator
{
    private:
        enum Sound { rippleSfx, bloodSfx, effectGetSfx };
        virtual void load();

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateBloodOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateBloodTimeVectors() const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateSwitchOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateSwitchTimeVectors() const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateSavedOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateSavedTimeVectors() const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateSwimOffsetVectors(EffectType type) const;
        std::vector<std::vector<unsigned int>> generateSwimTimeVectors() const;

        Animation* generateRippleAnimation() const;

    public:
        SpecialEffectGenerator();
        virtual ~SpecialEffectGenerator();

        SpecialEffect* generateSpecialEffect(sf::Vector2f position, sf::Vector2f origin, EffectType type, Room* room);
        SpecialEffect* generateSeekingSpecialEffect(GameObject* target, sf::Vector2f position, sf::Vector2f origin,
            EffectType type, Room* room);
};

#endif
