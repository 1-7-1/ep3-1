/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Stairs2Generator.h"
#include "TileCalculator.h"
#include "AnimationFrames.h"
#include "Animation.h"
#include "AutoTransition.h"
#include "Decoration.h"
#include "Room.h"
#include "Stairs.h"
#include "Player.h"

Stairs2Generator::Stairs2Generator()
{

}

Stairs2Generator::~Stairs2Generator()
{

}

void Stairs2Generator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/bg/13.png", sf::Color(0, 0, 0, 255));
    _isLoaded = true;
}

AutoTransition* Stairs2Generator::generateTile(const sf::Vector2f position, const unsigned int destination,
    const sf::Vector2f transitionVec, const bool absolute, Player* _player) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}), std::vector<unsigned int>({0})),
        _db.getTexture(invisible));

    return new AutoTransition(position, sf::Vector2f(0.f, 0.f), sf::FloatRect(0.f, 0.f, 16.f, 16.f),
        anim, destination, transitionVec, absolute, _player);
}

Decoration* Stairs2Generator::generateBackground(sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(320, 240));
    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}), std::vector<unsigned int>({0})),
        _db.getTexture(bg));
    
    return new Decoration(position, sf::Vector2f(), sf::Vector2f(), anim);
}

std::vector<Stairs*> Stairs2Generator::generateStairs(sf::Vector2f position, Player* player) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    std::vector<Animation*> anims;
    for (int i = 0; i < 45; i++)
    {
        anims.push_back(new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}), std::vector<unsigned int>({0})),
            _db.getTexture(invisible)));
    }

    std::vector<Stairs*> stairs;
    
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 15; j++)
            stairs.push_back(new Stairs(position + sf::Vector2f(256.f - j * 16.f + i * 16.f, j * 16.f), sf::Vector2f(),
                sf::FloatRect(0.f, 0.f, 16.f, 16.f), anims[i * 15 + j], player, sf::Vector2f(1.f, -1.f)));

    return stairs;
}

Room* Stairs2Generator::generateStairs2(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;

    backgrounds.push_back(generateBackground(sf::Vector2f()));
    floors.push_back(generateTile(sf::Vector2f(272.f, 0.f), 6, sf::Vector2f(136.f, 152.f), true, player));
    floors.push_back(generateTile(sf::Vector2f(48.f, 224.f), 8, sf::Vector2f(264.f, 24.f), true, player));

    std::vector<Stairs*> stairs = generateStairs(sf::Vector2f(), player);
    floors.insert(floors.end(), stairs.begin(), stairs.end());

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, std::vector<GameObject*>(), std::vector<GameObject*>() };

    return new Room(player, graph, nullptr, objects, sf::Vector2f(320.f, 240.f), sf::Vector2<bool>(false, false));
}
