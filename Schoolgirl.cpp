/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Schoolgirl.h"
#include "Animation.h"
#include "Player.h"
#include "Room.h"

Schoolgirl::Schoolgirl(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox,
    std::vector<Animation*> animations, Player* player): CollidableObject(position, origin, hitbox), Npc(position, origin, 1.f, hitbox),
    _animations(animations), _player(player)
{
    if (player->isSchoolgirlKilled())
    {
        _currentState = dead;
        _currentAnimation = _animations[4];
    }
    else
    {
        _currentState = idle;
        _currentAnimation = _animations[0];
    }
    
    for (Animation* anim : _animations)
        anim->setOrigin(_origin);

}

Schoolgirl::~Schoolgirl()
{
    for (Animation* anim : _animations)
        delete anim;
}

void Schoolgirl::interact(Player* player)
{
    if (_currentState != dead)
    {
        if (player->getEffect() == machete && player->getState() == action)
        {
            _room->createSpecialEffect(_position + sf::Vector2f(0.f, 0.01f), sf::Vector2f(0.f, getSize().y / 4.f), blood);
            _currentState = dead;
            player->setSchoolgirlKilled(true);
            _currentAnimation = _animations[4];
        }
    }
}

void Schoolgirl::behave()
{
    if (_currentState != dead)
    {
        if (_player->getPosition().y > _position.y)
        {
            if (_player->getPosition().x < _position.x - 16.f && _player->getPosition().x > _position.x - 64.f)
                _currentAnimation = _animations[1];
            else if (_player->getPosition().x > _position.x + 16.f && _player->getPosition().x < _position.x + 64.f)
                _currentAnimation = _animations[2];
            else
                _currentAnimation = _animations[0];
        }
        else if (_player->getPosition().x > _position.x + 12.f && _player->getPosition().x < _position.x + 36.f)
            _currentAnimation = _animations[3];
        else
            _currentAnimation = _animations[0];
    }
    else
        _currentAnimation = _animations[4];
    changeAnimation();
}
