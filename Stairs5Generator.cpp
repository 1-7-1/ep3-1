/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Stairs5Generator.h"
#include "TileCalculator.h"
#include "AnimationFrames.h"
#include "Animation.h"
#include "AutoTransition.h"
#include "Decoration.h"
#include "Room.h"
#include "Wall.h"
#include "Player.h"

Stairs5Generator::Stairs5Generator()
{

}

Stairs5Generator::~Stairs5Generator()
{

}

void Stairs5Generator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/bg/16.png", sf::Color(0, 0, 0, 255));
    _isLoaded = true;
}

AutoTransition* Stairs5Generator::generateTile(const sf::Vector2f position, const unsigned int destination,
    const sf::Vector2f transitionVec, const bool absolute, Player* _player) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}), std::vector<unsigned int>({0})),
        _db.getTexture(invisible));

    return new AutoTransition(position, sf::Vector2f(0.f, 0.f), sf::FloatRect(0.f, 0.f, 16.f, 16.f),
        anim, destination, transitionVec, absolute, _player);
}

std::vector<AutoTransition*> Stairs5Generator::generateTiles(const sf::Vector2f position, const unsigned int destination,
    const sf::Vector2f transitionVec, const bool absolute, Player* _player) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    std::vector<Animation*> anims;
    for (int i = 0; i < 16; i++)
    {
        anims.push_back(new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}), std::vector<unsigned int>({0})),
            _db.getTexture(invisible)));
    }

    std::vector<AutoTransition*> tiles;

    for (int i = 0; i < 16; i++)
        tiles.push_back(new AutoTransition(position + sf::Vector2f(-i, -i), sf::Vector2f(0.f, 0.f), sf::FloatRect(0.f, 0.f, 16.f, 16.f),
            anims[i], destination, transitionVec, absolute, _player));

    return tiles;
}

Decoration* Stairs5Generator::generateBackground(sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(320, 240));
    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}), std::vector<unsigned int>({0})),
        _db.getTexture(bg));
    
    return new Decoration(position, sf::Vector2f(), sf::Vector2f(), anim);
}

std::vector<Wall*> Stairs5Generator::generateWalls(sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    std::vector<Animation*> anims;
    for (int i = 0; i < 20; i++)
    {
        anims.push_back(new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}), std::vector<unsigned int>({0})),
            _db.getTexture(invisible)));
    }

    std::vector<Wall*> walls;
    
    for (int i = 0; i < 16; i++)
        walls.push_back(new Wall(position + sf::Vector2f(0.f + i, 16.f + i), sf::Vector2f(), anims[i],
            sf::FloatRect(0.f, 0.f, 16.f, 16.f)));

    for (int i = 0; i < 3; i++)
        walls.push_back(new Wall(position + sf::Vector2f(32.f + i * 16.f, 32.f), sf::Vector2f(), anims[i + 16],
            sf::FloatRect(0.f, 0.f, 16.f, 16.f)));

    walls.push_back(new Wall(position + sf::Vector2f(48.f, 0.f), sf::Vector2f(), anims[19],
            sf::FloatRect(0.f, 0.f, 16.f, 16.f)));

    return walls;
}

Room* Stairs5Generator::generateStairs5(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;

    backgrounds.push_back(generateBackground(sf::Vector2f()));
    std::vector<AutoTransition*> tiles = generateTiles(sf::Vector2f(144.f, 128.f),
        9, sf::Vector2f(72.f, 216.f), true, player);

    tiles.push_back(generateTile(sf::Vector2f(80.f, 112.f), 11, sf::Vector2f(2560.f, 2568.f), true, player));
    tiles.push_back(generateTile(sf::Vector2f(96.f, 112.f), 11, sf::Vector2f(2560.f, 2568.f), true, player));
    
    floors.insert(floors.end(), tiles.begin(), tiles.end());

    std::vector<Wall*> walls = generateWalls(sf::Vector2f(64.f, 112.f));
    collidables.insert(collidables.end(), walls.begin(), walls.end());

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, std::vector<GameObject*>(), std::vector<GameObject*>() };

    return new Room(player, graph, nullptr, objects, sf::Vector2f(320.f, 240.f), sf::Vector2<bool>(false, false));
}
