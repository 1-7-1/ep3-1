/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ENTITY_H
#define ENTITY_H

#include "CollidableObject.h"
#include <vector>

class Room;

enum EntityDirection { right, up, left, down };

class Entity: virtual public CollidableObject
{
    protected:
        sf::Vector2f _speed;
        float _speedLength;

    public:
        Entity(sf::Vector2f position, sf::Vector2f origin, float speedLength, sf::FloatRect hitbox);
        virtual ~Entity() = 0;

        void setSpeed(sf::Vector2f speed);
        void setSpeedLength(float speed);

        sf::Vector2f getSpeed() const;
        float getSpeedLength() const;

        virtual void move();

        bool checkCollision(sf::Vector2f speed, std::vector<CollidableObject*>* list) const;
};

#endif
