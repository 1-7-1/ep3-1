/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ROOM_GEN_H
#define ROOM_GEN_H

#include "PlayerGenerator.h"
#include "MenuGenerator.h"
#include "BedroomGenerator.h"
#include "BalconyGenerator.h"
#include "SpikeGenerator.h"
#include "BigSpikeGenerator.h"
#include "Stairs1Generator.h"
#include "Stairs2Generator.h"
#include "Stairs3Generator.h"
#include "Stairs4Generator.h"
#include "Stairs5Generator.h"
#include "JungleGenerator.h"
#include "CityGenerator.h"
#include "DesolateCityGenerator.h"
#include "MazeGenerator.h"
#include "WastelandGenerator.h"
#include "UnderwaterGenerator.h"
#include "SkyGenerator.h"
#include "HouseGenerator.h"
#include "SkyBackGenerator.h"
#include "PlantationGenerator.h"
#include "DarkGenerator.h"
#include "HellGenerator.h"
#include "HellPedestalGenerator.h"
#include "HellEntryGenerator.h"
#include "SpaceGenerator.h"
#include "SpaceHallGenerator.h"
#include "SpaceViewGenerator.h"
#include "Ending1Generator.h"
#include "Ending2Generator.h"

class Room;

class Player;
class PauseMenu;
class MainMenu;
class RoomGraph;

class RoomGenerator
{
    private:
        static constexpr float _roomWidth = 1280.f;
        static constexpr float _roomHeight = 480.f;

        RoomGraph* _graph;
        
        PlayerGenerator _playerGen;
        MenuGenerator _menuGen;

        BedroomGenerator _bedroomGen;
        BalconyGenerator _balcGen;

        SpikeGenerator _spikeGen;
        BigSpikeGenerator _bigSpikeGen;

        Stairs1Generator _stairs1Gen;
        Stairs2Generator _stairs2Gen;
        Stairs3Generator _stairs3Gen;
        Stairs4Generator _stairs4Gen;
        Stairs5Generator _stairs5Gen;

        JungleGenerator _jungleGen;

        CityGenerator _cityGen;
        DesolateCityGenerator _desolateCityGen;

        MazeGenerator _mazeGen;

        WastelandGenerator _wlGen;

        UnderwaterGenerator _waterGen;
        SkyGenerator _skyGen;
        HouseGenerator _houseGen;
        SkyBackGenerator _skyBackGen;

        PlantationGenerator _plantGen;

        DarkGenerator _darkGen;
        HellGenerator _hellGen;
        HellPedestalGenerator _hellPedGen;
        HellEntryGenerator _hellEntryGen;

        SpaceGenerator _spaceGen;
        SpaceHallGenerator _spaceHallGen;
        SpaceViewGenerator _spaceViewGen;

        Ending1Generator _end1Gen;
        Ending2Generator _end2Gen;

    public:
        RoomGenerator(RoomGraph* graph);
        ~RoomGenerator();

        Player* generatePlayer();
        MainMenu* generateMainMenu();
        PauseMenu* generatePauseMenu(Player* player);

        Room* generateRoom(unsigned int id, Player* player);
        void unloadRoom(unsigned int id);
};

#endif
