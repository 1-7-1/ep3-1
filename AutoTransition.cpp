#include "AutoTransition.h"
#include "Animation.h"
#include "Room.h"
#include "Player.h"

AutoTransition::AutoTransition(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox, Animation* animation,
    unsigned int destination, sf::Vector2f transitionVec, bool absolute, Player* player)
    : CollidableObject(position, origin, hitbox), _destination(destination), _transitionVector(transitionVec), _transitionAbsolute(absolute),
    _player(player)
{
    _currentAnimation = animation;
    _currentAnimation->setOrigin(_origin);
}

AutoTransition::~AutoTransition()
{
    delete _currentAnimation;
}

void AutoTransition::behave()
{
    sf::FloatRect adjHitbox = sf::FloatRect(_hitbox.left + _position.x, _hitbox.top + _position.y,
        _hitbox.width, _hitbox.height);
    if (_room->checkCollision(adjHitbox, _player))
        _room->changeRoom(_destination, _transitionVector, _transitionAbsolute, 63);
    changeAnimation();
}
