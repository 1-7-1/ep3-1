#ifndef AUTO_TRANS_H
#define AUTO_TRANS_H

#include "CollidableObject.h"

class Player;

class AutoTransition: public CollidableObject
{
    protected:
        unsigned int _destination;
        sf::Vector2f _transitionVector;
        bool _transitionAbsolute;

        Player* _player;

    public:
        AutoTransition(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox, Animation* animation,
                unsigned int destination, sf::Vector2f transitionVec, bool absolute, Player* player);
        ~AutoTransition();

        virtual void behave();
};

#endif
