/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "SkyFse.h"
#include "Decoration.h"

SkyFse::SkyFse(std::vector<Decoration*> elems, Animation* anim)
    : FullScreenEvent(elems, anim)
{

}

SkyFse::~SkyFse()
{

}

void SkyFse::activate()
{
    _position = sf::Vector2f(0.f, 0.f);
    _elements[0]->setPosition(sf::Vector2f(-160.f, 0.f) + _position);
    _elements[1]->setPosition(sf::Vector2f(160.f, 0.f) + _position);
}
