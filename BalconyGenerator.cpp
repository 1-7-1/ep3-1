/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "BalconyGenerator.h"
#include "Animation.h"
#include "TileCalculator.h"
#include "AnimationFrames.h"
#include "Wall.h"
#include "Decoration.h"
#include "AutoTransition.h"
#include "Balcony.h"

BalconyGenerator::BalconyGenerator()
{

}

BalconyGenerator::~BalconyGenerator()
{

}

void BalconyGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/bg/1.png", sf::Color(255, 0, 255, 255));
    _isLoaded = true;
}

std::vector<Animation*> BalconyGenerator::generateBalconyWallAnimations() const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    std::vector<Animation*> anims;

    for (int i = 0 ; i < 3; i++)
    {
        anims.push_back(new Animation(wallCalc(std::vector<sf::Vector2u>( { sf::Vector2u(0, 0) } ),
            std::vector<unsigned int>( { 0 } )),
            _db.getTexture(balcInvisible)));
    }

    return anims;
}

std::vector<Wall*> BalconyGenerator::generateBalconyWall(sf::Vector2f position) const
{
    std::vector<Animation*> anims = generateBalconyWallAnimations();
    std::vector<Wall*> walls;
    walls.push_back(new Wall(position + sf::Vector2f(0.f, 16.f), sf::Vector2f(), anims[0], sf::FloatRect(0.f, 0.f, 16.f, 48.f)));
    walls.push_back(new Wall(position + sf::Vector2f(16.f, 0.f), sf::Vector2f(), anims[1], sf::FloatRect(0.f, 0.f, 128.f, 16.f)));
    walls.push_back(new Wall(position + sf::Vector2f(144.f, 16.f), sf::Vector2f(), anims[2], sf::FloatRect(0.f, 0.f, 16.f, 48.f)));

    return walls;
}

Decoration* BalconyGenerator::generateBackground(const sf::Vector2f position, const sf::Vector2f speed) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(320, 240));

    Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}), std::vector<unsigned int>({0})),
        _db.getTexture(balcony));

    return new Decoration(position, sf::Vector2f(0.f, 0.f), speed, anim);
}

AutoTransition* BalconyGenerator::generateTile(const sf::Vector2f position, const unsigned int destination,
    const sf::Vector2f transitionVec, const bool absolute, Player* _player) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}), std::vector<unsigned int>({0})),
        _db.getTexture(balcInvisible));

    return new AutoTransition(position, sf::Vector2f(0.f, 0.f), sf::FloatRect(0.f, 0.f, 16.f, 16.f),
        anim, destination, transitionVec, absolute, _player);
}

Room* BalconyGenerator::generateBalcony(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<CollidableObject*> collidables;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;

    std::vector<Wall*> walls = generateBalconyWall(sf::Vector2f(80.f, 176.f));
    collidables.insert(collidables.end(), walls.begin(), walls.end());

    for (int i = 0; i < 8; i++)
        floors.push_back(generateTile(sf::Vector2f(i * 16.f + 96.f, 239.f), 2, sf::Vector2f(232.f, 128.f), true, player));

    return new Balcony(player, graph, collidables, floors, generateBackground(sf::Vector2f(), sf::Vector2f()));
}
