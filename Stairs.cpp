/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Stairs.h"
#include "Animation.h"
#include "Room.h"
#include "Player.h"

Stairs::Stairs(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox, Animation* anim,
    Player* player, sf::Vector2f rightVec)
    : CollidableObject(position, origin, hitbox), _player(player), _movementModifier(rightVec)
{
    _currentAnimation = anim;
    _currentAnimation->setOrigin(_origin);
}

Stairs::~Stairs()
{
    delete _currentAnimation;
}

void Stairs::behave()
{
    sf::FloatRect adjHitbox = sf::FloatRect(_hitbox.left + _position.x, _hitbox.top + _position.y,
        _hitbox.width, _hitbox.height);
    if (_room->checkCollision(adjHitbox, _player))
    {
        _player->setStairs(_movementModifier);
    }
    changeAnimation();
}