/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "JungleGenerator.h"

std::vector<Wall*> JungleGenerator::generateMazeAHand() const
{
    std::vector<Wall*> walls;
    walls.push_back(generateWall(sf::Vector2f(3360.f, 3760.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3376.f, 3760.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3392.f, 3760.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3408.f, 3760.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3408.f, 3744.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3424.f, 3744.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3424.f, 3728.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3440.f, 3728.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3440.f, 3712.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3712.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3696.f), right, true));

    walls.push_back(generateWall(sf::Vector2f(3360.f, 3808.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3376.f, 3808.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3376.f, 3824.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3392.f, 3824.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3392.f, 3840.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3408.f, 3840.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3424.f, 3840.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3440.f, 3840.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3440.f, 3824.f), upleft, true));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3824.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 3808.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3472.f, 3808.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3488.f, 3808.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(3488.f, 3824.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 3824.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 3840.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 3856.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 3872.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 3888.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 3888.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 3904.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3536.f, 3904.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3536.f, 3920.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3552.f, 3920.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3552.f, 3936.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3936.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3952.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3584.f, 3952.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(3584.f, 3968.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3968.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 3984.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 4000.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 4016.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 4032.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 4048.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 4064.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 4080.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3552.f, 4080.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3536.f, 4080.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 4080.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 4080.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3488.f, 4080.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3472.f, 4080.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 4080.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3440.f, 4080.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3424.f, 4080.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3408.f, 4080.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3408.f, 4096.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3408.f, 4112.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3408.f, 4128.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3408.f, 4144.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3424.f, 4144.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3440.f, 4144.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3440.f, 4128.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 4128.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3472.f, 4128.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3488.f, 4128.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 4128.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 4128.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3536.f, 4128.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3552.f, 4128.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3552.f, 4144.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3552.f, 4160.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 4160.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 4176.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3584.f, 4176.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3584.f, 4192.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3584.f, 4208.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 4208.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3552.f, 4208.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3552.f, 4224.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3536.f, 4224.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3536.f, 4240.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 4240.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 4256.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 4272.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 4288.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 4304.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 4320.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 4336.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 4352.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3536.f, 4352.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3552.f, 4352.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 4352.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 4336.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 4320.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 4304.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 4288.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 4272.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3584.f, 4272.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3600.f, 4272.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 4272.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 4288.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 4304.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 4320.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 4336.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 4352.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 4368.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 4368.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 4384.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 4400.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 4416.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 4416.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 4432.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 4448.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 4448.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 4464.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 4480.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3680.f, 4480.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(3680.f, 4496.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 4496.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 4512.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 4512.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 4512.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3744.f, 4512.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 4512.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3776.f, 4512.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3776.f, 4496.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3776.f, 4480.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3776.f, 4464.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 4464.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3744.f, 4464.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3744.f, 4448.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3744.f, 4432.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 4432.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 4416.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 4416.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 4400.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 4400.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 4384.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 4368.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 4352.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 4336.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 4320.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 4304.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 4288.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 4272.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 4272.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 4272.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3744.f, 4272.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3744.f, 4288.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 4288.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 4304.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3776.f, 4304.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3776.f, 4320.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3792.f, 4320.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(3792.f, 4336.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3808.f, 4336.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3808.f, 4352.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3824.f, 4352.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3840.f, 4352.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3856.f, 4352.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3872.f, 4352.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3888.f, 4352.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3904.f, 4352.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3920.f, 4352.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3936.f, 4352.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3936.f, 4336.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3936.f, 4320.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3936.f, 4304.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3936.f, 4288.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3920.f, 4288.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3904.f, 4288.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3888.f, 4288.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3872.f, 4288.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3856.f, 4288.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3840.f, 4288.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3840.f, 4272.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3824.f, 4272.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3824.f, 4256.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3808.f, 4256.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(3808.f, 4240.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3792.f, 4240.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3792.f, 4224.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3792.f, 4208.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3792.f, 4192.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3792.f, 4176.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3808.f, 4176.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3824.f, 4176.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3840.f, 4176.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3856.f, 4176.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3872.f, 4176.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3888.f, 4176.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3904.f, 4176.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3920.f, 4176.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3936.f, 4176.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3952.f, 4176.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3968.f, 4176.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3968.f, 4192.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3984.f, 4192.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(3984.f, 4208.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(4000.f, 4208.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(4000.f, 4224.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(4016.f, 4224.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(4016.f, 4240.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(4032.f, 4240.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(4032.f, 4256.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(4048.f, 4256.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(4048.f, 4272.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(4064.f, 4272.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(4064.f, 4288.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(4080.f, 4288.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(4096.f, 4288.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(4096.f, 4272.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(4096.f, 4256.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(4096.f, 4240.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(4096.f, 4224.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(4096.f, 4208.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(4096.f, 4192.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(4096.f, 4176.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(4080.f, 4176.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(4080.f, 4160.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(4064.f, 4160.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(4064.f, 4144.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(4048.f, 4144.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(4048.f, 4128.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(4032.f, 4128.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(4032.f, 4112.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(4016.f, 4112.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(4000.f, 4112.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3984.f, 4112.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3968.f, 4112.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3952.f, 4112.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3936.f, 4112.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3920.f, 4112.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3904.f, 4112.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3888.f, 4112.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3872.f, 4112.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3872.f, 4096.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3856.f, 4096.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3840.f, 4096.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3840.f, 4080.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3824.f, 4080.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3808.f, 4080.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3808.f, 4064.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3792.f, 4064.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3792.f, 4048.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3776.f, 4048.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(3776.f, 4048.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 4048.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 4032.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 4016.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 4000.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 3984.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 3968.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 3952.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 3936.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 3920.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 3904.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 3888.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 3872.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3744.f, 3872.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 3872.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 3872.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 3872.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 3856.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3680.f, 3856.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 3856.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 3856.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 3840.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 3840.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 3824.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 3824.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 3808.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 3792.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3600.f, 3792.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3600.f, 3792.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3600.f, 3776.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3600.f, 3760.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3600.f, 3744.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3600.f, 3728.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3600.f, 3712.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3600.f, 3696.f), left, false));

    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 2; i++)
            walls.push_back(generateWall(sf::Vector2f(3360.f + i * 32.f, 3664.f + j * 32.f), turnupright, true));

    for (int j = 0; j < 3; j++)
        for (int i = 0; i < 20; i++)
            walls.push_back(generateWall(sf::Vector2f(3616.f + i * 32.f, 3696.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 17; i++)
            walls.push_back(generateWall(sf::Vector2f(3712.f + i * 32.f, 3792.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 5; j++)
        for (int i = 0; i < 20; i++)
            walls.push_back(generateWall(sf::Vector2f(3776.f + i * 32.f, 3872.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 20; i++)
            walls.push_back(generateWall(sf::Vector2f(3872.f + i * 32.f, 4032.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 20; j++)
        for (int i = 0; i < 20; i++)
            walls.push_back(generateWall(sf::Vector2f(4112.f + i * 32.f, 4096.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 20; j++)
        for (int i = 0; i < 20; i++)
            walls.push_back(generateWall(sf::Vector2f(4112.f + i * 32.f, 4096.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 20; j++)
        for (int i = 0; i < 5; i++)
            walls.push_back(generateWall(sf::Vector2f(3952.f + i * 32.f, 4304.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 3; j++)
        for (int i = 0; i < 3; i++)
            walls.push_back(generateWall(sf::Vector2f(3856.f + i * 32.f, 4192.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 15; j++)
        for (int i = 0; i < 5; i++)
            walls.push_back(generateWall(sf::Vector2f(3792.f + i * 32.f, 4368.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 15; j++)
        for (int i = 0; i < 5; i++)
            walls.push_back(generateWall(sf::Vector2f(3792.f + i * 32.f, 4368.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 10; j++)
        for (int i = 0; i < 10; i++)
            walls.push_back(generateWall(sf::Vector2f(3472.f + i * 32.f, 4528.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 4; j++)
        for (int i = 0; i < 4; i++)
            walls.push_back(generateWall(sf::Vector2f(3472.f + i * 32.f, 4368.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 6; j++)
        for (int i = 0; i < 4; i++)
            walls.push_back(generateWall(sf::Vector2f(3360.f + i * 32.f, 4176.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 10; j++)
        walls.push_back(generateWall(sf::Vector2f(3360.f, 3856.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 7; j++)
        for (int i = 0; i < 3; i++)
            walls.push_back(generateWall(sf::Vector2f(3392.f + i * 32.f, 3856.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 4; j++)
        for (int i = 0; i < 2; i++)
            walls.push_back(generateWall(sf::Vector2f(3488.f + i * 32.f, 3952.f + j * 32.f), turnupright, true));
            
    for (int j = 0; j < 5; j++)
        for (int i = 0; i < 3; i++)
            walls.push_back(generateWall(sf::Vector2f(3264.f + i * 32.f, 4112.f + j * 32.f), turnupright, true));

    std::vector<Wall*> decorations;

    decorations = generateBigTree(sf::Vector2f(3513.f, 3711.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigTree(sf::Vector2f(3680.f, 4066.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigTree(sf::Vector2f(3686.f, 4216.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateTallTree(sf::Vector2f(3428.f, 3819.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3748.f, 4205.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3561.f, 3894.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3658.f, 3920.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3624.f, 4036.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3746.f, 4013.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3872.f, 4141.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(4024.f, 4203.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(4059.f, 4189.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3822.f, 4199.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3438.f, 4106.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3906.f, 4322.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3558.f, 4307.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3543.f, 4346.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3689.f, 4422.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3734.f, 4391.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3540.f, 4173.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateSmallTree(sf::Vector2f(3413.f, 3786.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3630.f, 4444.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3360.f, 3736.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3681.f, 4333.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3586.f, 4248.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3770.f, 4244.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3713.f, 4154.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3635.f, 4134.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3918.f, 4166.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3722.f, 4041.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3378.f, 3754.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3467.f, 3765.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3539.f, 3820.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3636.f, 3890.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3686.f, 3905.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3675.f, 3986.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3772.f, 4088.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3810.f, 4146.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(4057.f, 4216.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3627.f, 4194.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3828.f, 4303.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3724.f, 4467.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateVerySmallTree(sf::Vector2f(3562.f, 3751.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3746.f, 4500.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3693.f, 4476.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3694.f, 4451.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3786.f, 4296.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3430.f, 4141.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(4081.f, 4258.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3990.f, 4143.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3583.f, 3793.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3741.f, 3910.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3729.f, 3960.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3604.f, 4016.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3814.f, 4107.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3541.f, 4104.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3661.f, 4381.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3713.f, 4326.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    decorations = generateBigRock(sf::Vector2f(3384.f, 3803.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(3488.f, 3750.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(3520.f, 3732.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(3700.f, 3960.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(3650.f, 4014.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(3658.f, 4172.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(3773.f, 4162.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(3951.f, 4150.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(3740.f, 4255.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(3648.f, 4298.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(3754.f, 4490.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(3604.f, 4234.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateSmallRock(sf::Vector2f(3495.f, 3796.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3550.f, 4272.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3646.f, 4353.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3677.f, 4404.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3811.f, 4281.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3864.f, 4326.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3922.f, 4311.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(4014.f, 4143.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(4064.f, 4242.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3604.f, 4073.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3551.f, 4115.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3472.f, 4107.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3676.f, 4132.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3740.f, 3944.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3543.f, 3861.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3675.f, 3893.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3587.f, 3727.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3445.f, 3778.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(3568.f, 3803.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateWeirdPlant(sf::Vector2f(3647.f, 4040.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3707.f, 4007.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3598.f, 4100.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3723.f, 4198.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3882.f, 4310.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3517.f, 3749.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3549.f, 3843.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3712.f, 3904.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3680.f, 4391.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateBigFern(sf::Vector2f(3607.f, 4104.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3637.f, 3914.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3640.f, 4276.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3782.f, 4216.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3553.f, 3787.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3601.f, 3820.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3589.f, 3912.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3658.f, 4037.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3590.f, 4144.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3661.f, 4199.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3731.f, 4223.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3822.f, 4329.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3682.f, 4362.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3462.f, 3799.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3410.f, 3808.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3580.f, 3860.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3538.f, 3877.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3633.f, 3994.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3656.f, 4108.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3690.f, 4124.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3488.f, 4100.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3610.f, 4254.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    decorations = generateSmallFern(sf::Vector2f(3586.f, 4136.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3646.f, 4315.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3708.f, 4260.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3750.f, 3903.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3547.f, 3735.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3606.f, 3860.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3561.f, 3852.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3723.f, 4479.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3564.f, 4245.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3771.f, 4269.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3664.f, 4234.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3519.f, 4122.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3454.f, 4116.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3593.f, 4037.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3617.f, 3952.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3592.f, 3718.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    return walls;
}
