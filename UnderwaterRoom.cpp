/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "UnderwaterRoom.h"
#include "Player.h"

UnderwaterRoom::UnderwaterRoom(Player* player, RoomGraph* graph, FullScreenEvent* fse, RoomObjects objects,
    sf::Vector2f size, sf::Vector2<bool> looping)
    : Room(player, graph, fse, objects, size, looping)
{
    _player->endScript(sink);
    _player->startScript(swim);
}

UnderwaterRoom::~UnderwaterRoom()
{

}

void UnderwaterRoom::tick()
{
    if (_player->getPosition().y > _size.y)
        changeRoom(17, sf::Vector2f(600.f, 28.f), true, 63);
        
    Room::tick();
}
