/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef JUNGLE_GEN_H
#define JUNGLE_GEN_H

#include "GameObjectGenerator.h"

class Room;
class Player;
class RoomGraph;
class SeekingFloor;
class Wall;
class AutoTransition;
class ScreenEffect;
class MachetePedestal;
class Spider;

class JungleGenerator: public GameObjectGenerator
{
    private:
        enum Graphics { invisible, tileset, spike, rain, pedestal, spider };
        enum WallDir { right, upright, up, upleft, left, downleft, down, downright,
            turnupright, turnupleft, turndownleft, turndownright};
        virtual void load();

        const float _roomWidth = 5120.f;
        const float _roomHeight = 5120.f;

        std::vector<ScreenEffect*> generateRain() const;

        std::vector<SeekingFloor*> generateFloorTiles(Player* player) const;
        Wall* generateWall(sf::Vector2f position, WallDir dir, bool tree) const;

        std::vector<Wall*> generateSpike(sf::Vector2f position) const;
        AutoTransition* generateSpikeTile(sf::Vector2f position, Player* player) const;

        std::vector<Wall*> generateSmallTree(sf::Vector2f position) const;
        std::vector<Wall*> generateVerySmallTree(sf::Vector2f position) const;
        std::vector<Wall*> generateTallTree(sf::Vector2f position) const;
        std::vector<Wall*> generateBigTree(sf::Vector2f position) const;

        std::vector<Wall*> generateSmallRock(sf::Vector2f position) const;
        std::vector<Wall*> generateBigRock(sf::Vector2f position) const;

        std::vector<Wall*> generateSmallFern(sf::Vector2f position) const;
        std::vector<Wall*> generateBigFern(sf::Vector2f position) const;
        std::vector<Wall*> generateWeirdPlant(sf::Vector2f position) const;

        std::vector<Wall*> generateTrunk(sf::Vector2f position) const;

        std::vector<Wall*> generateStartingArea() const;
        std::vector<Wall*> generateHubArea() const;
        std::vector<Wall*> generateUnderPassage() const;
        std::vector<Wall*> generateOpenSpace() const;
        std::vector<Wall*> generateMacheteArea() const;
        std::vector<Wall*> generateMazeALeftArea() const;
        std::vector<Wall*> generateMazeAHand() const;
        std::vector<Wall*> generateMazeAStart() const;
        std::vector<Wall*> generateMazeAEntrance() const;
        std::vector<Wall*> generateSecludedArea() const;

        MachetePedestal* generatePedestal(Player* player) const;
        std::vector<Spider*> generateSpiders() const;

    public:
        JungleGenerator();
        ~JungleGenerator();

        Room* generateJungle(Player* player, RoomGraph* graph);
};

#endif
