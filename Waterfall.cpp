/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Waterfall.h"
#include "Player.h"
#include "Animation.h"

Waterfall::Waterfall(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox, Animation* animation,
    unsigned int destination, Player* player)
    : AutoTransition(position, origin, hitbox, animation, destination, sf::Vector2f(), false, player), _state(hidden)
{
    _currentAnimation->setColor(sf::Color(0, 0, 0, 0));
    if (_position.y > 640.f)
        _transitionVector.y = -8.f;
    else
        _transitionVector.y = 8.f;

    if (_player->areWaterfallActivated())
        activate();
}

Waterfall::~Waterfall()
{
    
}

void Waterfall::uncover()
{
    if (_state == hidden)
        _state = uncovered;
}

void Waterfall::activate()
{
    _state = active;
    _player->setWaterfallActivated(true);
    _currentAnimation->setColor(sf::Color(255, 255, 255, 255));
}

void Waterfall::behave()
{
    switch (_state)
    {
        case uncovered:
            if (_player->getPosition().y < 1040.f && _player->getPosition().y > 240.f)
                activate();
            break;

        case active:
            AutoTransition::behave();
            break;
        
        default:
            break;
    }
}
