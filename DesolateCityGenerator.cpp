/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "DesolateCityGenerator.h"
#include "TileCalculator.h"
#include "AnimationFrames.h"
#include "Animation.h"
#include "Wall.h"
#include "Decoration.h"
#include "WaitingRoom.h"
#include "Faker.h"
#include "Player.h"

DesolateCityGenerator::DesolateCityGenerator()
{

}

DesolateCityGenerator::~DesolateCityGenerator()
{
    
}

void DesolateCityGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/tilesets/4.png", sf::Color(0, 68, 255, 255));
    _db.loadTexture("graphics/char/9.png", sf::Color(0, 255, 255, 255));

    _db.loadSound("sounds/sfx/9.wav");
    _isLoaded = true;
}

std::vector<Wall*> DesolateCityGenerator::generateBuilding1(const sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(0, 0), sf::Vector2u(128, 128));
    TileCalculator invisibleCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    Animation* wallAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 0)}), std::vector<unsigned int>({0})),
        _db.getTexture(tileset));

    std::vector<Wall*> walls;

    walls.push_back(new Wall(position, sf::Vector2f(48.f, 112.f),
        wallAnim, sf::FloatRect(-16.f, 15.f, 33.f, 1.f)));
    
    for (int i = 0; i < 16; i++)
    {
        Animation* anim = new Animation(invisibleCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));

        walls.push_back(new Wall(position, sf::Vector2f(),
            anim, sf::FloatRect(-16.f, 15.f - i, 33.f + i, 1.f)));
    }

    return walls;
}

std::vector<Wall*> DesolateCityGenerator::generateBuilding2(const sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(0, 0), sf::Vector2u(128, 128));
    TileCalculator invisibleCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    Animation* wallAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 0)}), std::vector<unsigned int>({0})),
        _db.getTexture(tileset));

    std::vector<Wall*> walls;

    walls.push_back(new Wall(position, sf::Vector2f(48.f, 112.f),
        wallAnim, sf::FloatRect(-32.f, 15.f, 17.f, 1.f)));
    
    for (int i = 0; i < 16; i++)
    {
        Animation* anim = new Animation(invisibleCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));

        walls.push_back(new Wall(position, sf::Vector2f(),
            anim, sf::FloatRect(-32.f, 15.f - i, 17.f + i, 1.f)));
    }

    for (int i = 0; i < 16; i++)
    {
        Animation* anim = new Animation(invisibleCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));

        walls.push_back(new Wall(position, sf::Vector2f(),
            anim, sf::FloatRect(32.f, 15.f - i, 17.f + i, 1.f)));
    }

    return walls;
}

std::vector<Wall*> DesolateCityGenerator::generateBuilding3(const sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(0, 0), sf::Vector2u(128, 128));
    TileCalculator invisibleCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    Animation* wallAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(3, 0)}), std::vector<unsigned int>({0})),
        _db.getTexture(tileset));

    std::vector<Wall*> walls;

    walls.push_back(new Wall(position, sf::Vector2f(56.f, 112.f),
        wallAnim, sf::FloatRect(-48.f, 15.f, 97.f, 1.f)));
    
    for (int i = 0; i < 16; i++)
    {
        Animation* anim = new Animation(invisibleCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));

        walls.push_back(new Wall(position, sf::Vector2f(),
            anim, sf::FloatRect(-48.f, 15.f - i, 97.f + i, 1.f)));
    }

    return walls;
}

std::vector<Wall*> DesolateCityGenerator::generateBuilding(const sf::Vector2f position, const unsigned int type) const
{
    switch (type % 3)
    {
    case 0:
        return generateBuilding1(position);
        break;

    case 1:
        return generateBuilding2(position);
        break;
    
    default:
        return generateBuilding3(position);
        break;
    }
}

std::vector<Wall*> DesolateCityGenerator::generatePost(const sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(256, 144), sf::Vector2u(128, 128));
    TileCalculator invisibleCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    Animation* wallAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(tileset));

    std::vector<Wall*> walls;

    walls.push_back(new Wall(position, sf::Vector2f(64.f, 112.f),
        wallAnim, sf::FloatRect(-4.f, 0.f, 8.f, 16.f)));

    return walls;
}

std::vector<Wall*> DesolateCityGenerator::generateBarrier(const sf::Vector2f position, const unsigned int length) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(0, 144), sf::Vector2u(128, 128));
    TileCalculator invisibleCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    std::vector<Wall*> walls;

    for (unsigned int i = 0; i < length; i++)
    {
        Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        walls.push_back(new Wall(position + sf::Vector2f(128.f * i, 0.f), sf::Vector2f(0.f, 124.f),
        anim, sf::FloatRect(0.f, 0.f, 128.f, 4.f)));
    }
    
    Animation* borderAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(tileset));

    walls.push_back(new Wall(position + sf::Vector2f(128.f * length, 0.f), sf::Vector2f(0.f, 124.f),
        borderAnim, sf::FloatRect(0.f, 0.f, 5.f, 4.f)));

    return walls;
}

std::vector<Decoration*> DesolateCityGenerator::generateFloor() const
{
    TileCalculator floorCalc = TileCalculator(sf::Vector2u(0, 0), sf::Vector2u(32, 32));
    std::vector<Decoration*> floors;

    for (int i = 0; i < _roomWidth / 32; i++)
        for (int j = 0; j < _roomHeight / 32; j++)
        {
            Animation* anim;
            
            anim = new Animation(floorCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));

            floors.push_back(new Decoration(sf::Vector2f(i * 32.f, j * 32.f), sf::Vector2f(),
                sf::Vector2f(), anim));
        }

    return floors;
}

std::vector<std::vector<std::vector<sf::Vector2u>>> DesolateCityGenerator::generateFakerOffsetVectors() const
{
    std::vector<std::vector<sf::Vector2u>> idleVec;
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 1) }));
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 0) }));
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 3) }));
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 2) }));

    std::vector<std::vector<sf::Vector2u>> walkVec;
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 1), sf::Vector2u(1, 1), sf::Vector2u(2, 1),
        sf::Vector2u(1, 1) }));
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 0), sf::Vector2u(1, 0), sf::Vector2u(2, 0),
        sf::Vector2u(1, 0) }));
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 3), sf::Vector2u(1, 3), sf::Vector2u(2, 3),
        sf::Vector2u(1, 3) }));
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 2), sf::Vector2u(1, 2), sf::Vector2u(2, 2),
        sf::Vector2u(1, 2) }));

    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ idleVec, walkVec });
}

std::vector<std::vector<unsigned int>> DesolateCityGenerator::generateFakerTimeVectors() const
{
    std::vector<unsigned int> idleTimes = { 0 };
    std::vector<unsigned int> walkTimes = { 10, 10, 10, 10 };
    
    return std::vector<std::vector<unsigned int>>({ idleTimes, walkTimes });
}

Faker* DesolateCityGenerator::generateFaker(const sf::Vector2f position, Player* player, RoomGraph* graph)
{
    std::vector<std::vector<Animation*>> animationVector = generateAnimationVector(_db.getTexture(faker),
    generateFakerOffsetVectors(), generateFakerTimeVectors(), sf::Vector2u(24, 32));

    return new Faker(position, sf::Vector2f(12.f, 29.f), sf::FloatRect(-6.f, -2.f, 12.f, 4.f),
        animationVector, player, graph);
}

Room* DesolateCityGenerator::generateDesolateCity(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;

    std::vector<Decoration*> cityfloor = generateFloor();
    floors.insert(floors.end(), cityfloor.begin(), cityfloor.end());

    std::vector<sf::Vector2f> buildingPos = { sf::Vector2f(759.f, 960.f), sf::Vector2f(503.f, 866.f),
        sf::Vector2f(631.f, 1631.f), sf::Vector2f(592.f, 1850.f), sf::Vector2f(981.f, 1084.f), sf::Vector2f(529.f, 336.f),
        sf::Vector2f(379.f, 1756.f), sf::Vector2f(886.f, 1052.f), sf::Vector2f(922.f, 1864.f), sf::Vector2f(110.f, 881.f),
        sf::Vector2f(860.f, 664.f), sf::Vector2f(828.f, 1174.f), sf::Vector2f(582.f, 1712.f), sf::Vector2f(514.f, 398.f),
        sf::Vector2f(623.f, 1780.f), sf::Vector2f(255.f, 1221.f), sf::Vector2f(599.f, 1040.f), sf::Vector2f(303.f, 783.f),
        sf::Vector2f(166.f, 1059.f), sf::Vector2f(853.f, 1714.f), sf::Vector2f(441.f, 711.f), sf::Vector2f(987.f, 889.f),
        sf::Vector2f(449.f, 1825.f), sf::Vector2f(111.f, 1467.f), sf::Vector2f(181.f, 1673.f), sf::Vector2f(276.f, 1805.f),
        sf::Vector2f(277.f, 1739.f), sf::Vector2f(405.f, 261.f), sf::Vector2f(306.f, 26.f), sf::Vector2f(870.f, 417.f),
        sf::Vector2f(719.f, 496.f), sf::Vector2f(826.f, 1827.f), sf::Vector2f(741.f, 1876.f), sf::Vector2f(531.f, 308.f),
        sf::Vector2f(715.f, 315.f), sf::Vector2f(487.f, 1575.f), sf::Vector2f(626.f, 776.f), sf::Vector2f(981.f, 1466.f),
        sf::Vector2f(775.f, 1090.f), sf::Vector2f(275.f, 1706.f), sf::Vector2f(762.f, 1017.f)};
    
    for (unsigned int i = 0; i < buildingPos.size(); i++)
    {
        std::vector<Wall*> building = generateBuilding(buildingPos[i], i % 3);
        collidables.insert(collidables.end(), building.begin(), building.end());
    }

    std::vector<sf::Vector2f> postPos = {sf::Vector2f(2235.f, 1096.f), sf::Vector2f(1277.f, 49.f),
        sf::Vector2f(2035.f, 1116.f), sf::Vector2f(2018.f, 381.f), sf::Vector2f(2214.f, 1404.f),
        sf::Vector2f(1978.f, 837.f), sf::Vector2f(2363.f, 863.f), sf::Vector2f(2162.f, 645.f),
        sf::Vector2f(1676.f, 1298.f), sf::Vector2f(1889.f, 1848.f), sf::Vector2f(1548.f, 781.f),
        sf::Vector2f(1633.f, 71.f), sf::Vector2f(2366.f, 256.f), sf::Vector2f(2131.f, 464.f),
        sf::Vector2f(1500.f, 937.f), sf::Vector2f(1636.f, 112.f), sf::Vector2f(2039.f, 16.f),
        sf::Vector2f(15.f, 220.f), sf::Vector2f(1569.f, 53.f), sf::Vector2f(1038.f, 999.f),
        sf::Vector2f(1240.f, 815.f), sf::Vector2f(2274.f, 705.f), sf::Vector2f(2025.f, 238.f),
        sf::Vector2f(1213.f, 1826.f), sf::Vector2f(2528.f, 1738.f), sf::Vector2f(2060.f, 889.f),
        sf::Vector2f(1880.f, 684.f), sf::Vector2f(1187.f, 1603.f), sf::Vector2f(1962.f, 1427.f),
        sf::Vector2f(1356.f, 218.f), sf::Vector2f(2353.f, 1084.f), sf::Vector2f(1625.f, 1138.f),
        sf::Vector2f(2394.f, 0.f), sf::Vector2f(1582.f, 709.f), sf::Vector2f(1092.f, 1323.f),
        sf::Vector2f(1520.f, 1614.f), sf::Vector2f(2387.f, 297.f), sf::Vector2f(1908.f, 1159.f)};

    for (unsigned int i = 0; i < postPos.size(); i++)
    {
        std::vector<Wall*> post = generatePost(postPos[i]);
        collidables.insert(collidables.end(), post.begin(), post.end());
    }

    std::vector<sf::Vector2f> barrierPos = {sf::Vector2f(1610.f, 43.f), sf::Vector2f(1117.f, 573.f),
        sf::Vector2f(1987.f, 142.f), sf::Vector2f(1403.f, 381.f), sf::Vector2f(1843.f, 923.f),
        sf::Vector2f(1844.f, 97.f), sf::Vector2f(1695.f, 458.f), sf::Vector2f(1236.f, 605.f)};

    std::vector<unsigned int> barrierLength = { 2, 4, 1, 3, 2, 3, 6, 1};

    for (unsigned int i = 0; i < barrierLength.size(); i++)
    {
        std::vector<Wall*> barrier = generateBarrier(barrierPos[i], barrierLength[i]);
        collidables.insert(collidables.end(), barrier.begin(), barrier.end());
    }

    for (int i = 0; i < _roomWidth / 1280; i++)
        for (int j = 0; j < _roomHeight / 960; j++)
        {
            sf::FloatRect adjHitbox = player->getHitbox();
            adjHitbox.left += player->getPosition().x;
            adjHitbox.top += player->getPosition().y;
            sf::Vector2f position = sf::Vector2f(640.f + i * 1280, 480.f + j * 960);
            if (!sf::FloatRect(position.x - 128.f, position.y - 128.f, 256.f, 256.f).intersects(adjHitbox))
            {
                Faker* fak = generateFaker(position, player, graph);
                entities.push_back(fak);
            }
        }

    sf::Sound* screamSfx = _db.getSound(scream);

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, std::vector<GameObject*>(), std::vector<GameObject*>() };

    return new WaitingRoom(player, graph, nullptr, objects, sf::Vector2f(_roomWidth, _roomHeight),
        sf::Vector2<bool>(true, true), screamSfx, 180);
}
