/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "SpaceHallGenerator.h"
#include "Room.h"
#include "TileCalculator.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "Decoration.h"
#include "Player.h"
#include "Stairs.h"
#include "AutoTransition.h"

SpaceHallGenerator::SpaceHallGenerator()
{

}

SpaceHallGenerator::~SpaceHallGenerator()
{

}

void SpaceHallGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/bg/7.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/bg/8.png", sf::Color(255, 0, 255, 255));
    _isLoaded = true;
}

Decoration* SpaceHallGenerator::generateBackground() const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(320, 240));

    Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(bg));
    return new Decoration(sf::Vector2f(), sf::Vector2f(), sf::Vector2f(), anim);
}

std::vector<Decoration*> SpaceHallGenerator::generateForeground() const
{
    std::vector<Decoration*> decs;

    TileCalculator bottomCalc = TileCalculator(sf::Vector2u(0, 200), sf::Vector2u(320, 40));
    {
        Animation* anim = new Animation(bottomCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
            std::vector<unsigned int>({0})), _db.getTexture(fg));
        decs.push_back(new Decoration(sf::Vector2f(0.f, 200.f), sf::Vector2f(), sf::Vector2f(), anim));
    }

    TileCalculator smallLightCalc = TileCalculator(sf::Vector2u(64, 0), sf::Vector2u(8, 8));
    {
        Animation* anim = new Animation(smallLightCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0), sf::Vector2u(1, 0),
            sf::Vector2u(2, 0), sf::Vector2u(3, 0), sf::Vector2u(4, 0), sf::Vector2u(5, 0), sf::Vector2u(6, 0),
            sf::Vector2u(5, 0), sf::Vector2u(4, 0), sf::Vector2u(3, 0), sf::Vector2u(2, 0), sf::Vector2u(1, 0)}),
            std::vector<unsigned int>({45, 6, 6, 6, 6, 6, 45, 6, 6, 6, 6, 6})), _db.getTexture(fg));
        decs.push_back(new Decoration(sf::Vector2f(83.f, 71.f), sf::Vector2f(4.f, 4.f), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(smallLightCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 1), sf::Vector2u(1, 1),
            sf::Vector2u(2, 1), sf::Vector2u(3, 1), sf::Vector2u(4, 1), sf::Vector2u(5, 1), sf::Vector2u(4, 1),
            sf::Vector2u(3, 1), sf::Vector2u(2, 1), sf::Vector2u(1, 1)}),
            std::vector<unsigned int>({45, 8, 7, 8, 7, 45, 8, 7, 8, 7})), _db.getTexture(fg));
        decs.push_back(new Decoration(sf::Vector2f(192.f, 125.f), sf::Vector2f(4.f, 4.f), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(smallLightCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 2), sf::Vector2u(1, 2),
            sf::Vector2u(2, 2), sf::Vector2u(3, 2), sf::Vector2u(2, 2), sf::Vector2u(1, 2)}),
            std::vector<unsigned int>({45, 15, 15, 45, 15, 15})), _db.getTexture(fg));
        decs.push_back(new Decoration(sf::Vector2f(140.f, 170.f), sf::Vector2f(4.f, 4.f), sf::Vector2f(), anim));
    }

    TileCalculator bigLightCalc = TileCalculator(sf::Vector2u(0, 0), sf::Vector2u(8, 32));
    {
        Animation* anim = new Animation(bigLightCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0), sf::Vector2u(1, 0),
            sf::Vector2u(2, 0), sf::Vector2u(3, 0), sf::Vector2u(4, 0), sf::Vector2u(5, 0), sf::Vector2u(6, 0),
            sf::Vector2u(7, 0), sf::Vector2u(6, 0), sf::Vector2u(5, 0), sf::Vector2u(4, 0), sf::Vector2u(3, 0), sf::Vector2u(2, 0), sf::Vector2u(1, 0)}),
            std::vector<unsigned int>({32, 15, 15, 15, 15, 15, 15, 32, 15, 15, 15, 15, 15, 15})), _db.getTexture(fg));
        decs.push_back(new Decoration(sf::Vector2f(11.f, 203.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(bigLightCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0), sf::Vector2u(1, 0),
            sf::Vector2u(2, 0), sf::Vector2u(3, 0), sf::Vector2u(4, 0), sf::Vector2u(5, 0), sf::Vector2u(6, 0),
            sf::Vector2u(7, 0), sf::Vector2u(6, 0), sf::Vector2u(5, 0), sf::Vector2u(4, 0), sf::Vector2u(3, 0), sf::Vector2u(2, 0), sf::Vector2u(1, 0)}),
            std::vector<unsigned int>({32, 15, 15, 15, 15, 15, 15, 32, 15, 15, 15, 15, 15, 15})), _db.getTexture(fg));
        decs.push_back(new Decoration(sf::Vector2f(52.f, 202.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(bigLightCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0), sf::Vector2u(1, 0),
            sf::Vector2u(2, 0), sf::Vector2u(3, 0), sf::Vector2u(4, 0), sf::Vector2u(5, 0), sf::Vector2u(6, 0),
            sf::Vector2u(7, 0), sf::Vector2u(6, 0), sf::Vector2u(5, 0), sf::Vector2u(4, 0), sf::Vector2u(3, 0), sf::Vector2u(2, 0), sf::Vector2u(1, 0)}),
            std::vector<unsigned int>({32, 15, 15, 15, 15, 15, 15, 32, 15, 15, 15, 15, 15, 15})), _db.getTexture(fg));
        decs.push_back(new Decoration(sf::Vector2f(91.f, 201.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(bigLightCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0), sf::Vector2u(1, 0),
            sf::Vector2u(2, 0), sf::Vector2u(3, 0), sf::Vector2u(4, 0), sf::Vector2u(5, 0), sf::Vector2u(6, 0),
            sf::Vector2u(7, 0), sf::Vector2u(6, 0), sf::Vector2u(5, 0), sf::Vector2u(4, 0), sf::Vector2u(3, 0), sf::Vector2u(2, 0), sf::Vector2u(1, 0)}),
            std::vector<unsigned int>({32, 15, 15, 15, 15, 15, 15, 32, 15, 15, 15, 15, 15, 15})), _db.getTexture(fg));
        decs.push_back(new Decoration(sf::Vector2f(131.f, 201.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(bigLightCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0), sf::Vector2u(1, 0),
            sf::Vector2u(2, 0), sf::Vector2u(3, 0), sf::Vector2u(4, 0), sf::Vector2u(5, 0), sf::Vector2u(6, 0),
            sf::Vector2u(7, 0), sf::Vector2u(6, 0), sf::Vector2u(5, 0), sf::Vector2u(4, 0), sf::Vector2u(3, 0), sf::Vector2u(2, 0), sf::Vector2u(1, 0)}),
            std::vector<unsigned int>({32, 15, 15, 15, 15, 15, 15, 32, 15, 15, 15, 15, 15, 15})), _db.getTexture(fg));
        decs.push_back(new Decoration(sf::Vector2f(171.f, 201.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(bigLightCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0), sf::Vector2u(1, 0),
            sf::Vector2u(2, 0), sf::Vector2u(3, 0), sf::Vector2u(4, 0), sf::Vector2u(5, 0), sf::Vector2u(6, 0),
            sf::Vector2u(7, 0), sf::Vector2u(6, 0), sf::Vector2u(5, 0), sf::Vector2u(4, 0), sf::Vector2u(3, 0), sf::Vector2u(2, 0), sf::Vector2u(1, 0)}),
            std::vector<unsigned int>({32, 15, 15, 15, 15, 15, 15, 32, 15, 15, 15, 15, 15, 15})), _db.getTexture(fg));
        decs.push_back(new Decoration(sf::Vector2f(211.f, 201.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(bigLightCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0), sf::Vector2u(1, 0),
            sf::Vector2u(2, 0), sf::Vector2u(3, 0), sf::Vector2u(4, 0), sf::Vector2u(5, 0), sf::Vector2u(6, 0),
            sf::Vector2u(7, 0), sf::Vector2u(6, 0), sf::Vector2u(5, 0), sf::Vector2u(4, 0), sf::Vector2u(3, 0), sf::Vector2u(2, 0), sf::Vector2u(1, 0)}),
            std::vector<unsigned int>({32, 15, 15, 15, 15, 15, 15, 32, 15, 15, 15, 15, 15, 15})), _db.getTexture(fg));
        decs.push_back(new Decoration(sf::Vector2f(251.f, 202.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(bigLightCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0), sf::Vector2u(1, 0),
            sf::Vector2u(2, 0), sf::Vector2u(3, 0), sf::Vector2u(4, 0), sf::Vector2u(5, 0), sf::Vector2u(6, 0),
            sf::Vector2u(7, 0), sf::Vector2u(6, 0), sf::Vector2u(5, 0), sf::Vector2u(4, 0), sf::Vector2u(3, 0), sf::Vector2u(2, 0), sf::Vector2u(1, 0)}),
            std::vector<unsigned int>({32, 15, 15, 15, 15, 15, 15, 32, 15, 15, 15, 15, 15, 15})), _db.getTexture(fg));
        decs.push_back(new Decoration(sf::Vector2f(291.f, 202.f), sf::Vector2f(), sf::Vector2f(), anim));
    }

    TileCalculator poleCalc = TileCalculator(sf::Vector2u(0, 32), sf::Vector2u(56, 144));
    {
        Animation* anim = new Animation(poleCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0), sf::Vector2u(1, 0),
            sf::Vector2u(2, 0), sf::Vector2u(3, 0), sf::Vector2u(2, 0), sf::Vector2u(1, 0)}),
            std::vector<unsigned int>({30, 10, 10, 30, 10, 10})), _db.getTexture(fg));
        decs.push_back(new Decoration(sf::Vector2f(41.f, 145.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(poleCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0), sf::Vector2u(1, 0),
            sf::Vector2u(2, 0), sf::Vector2u(3, 0), sf::Vector2u(2, 0), sf::Vector2u(1, 0)}),
            std::vector<unsigned int>({30, 10, 10, 30, 10, 10})), _db.getTexture(fg));
        decs.push_back(new Decoration(sf::Vector2f(251.f, 128.f), sf::Vector2f(), sf::Vector2f(), anim));
    }

    return decs;
}

std::vector<Stairs*> SpaceHallGenerator::generateStairs(Player* player) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    std::vector<Stairs*> stairs;

    for (int i = 0; i < 320; i += 16)
    {
        Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));
        stairs.push_back(new Stairs(sf::Vector2f(i, 200.f), sf::Vector2f(), sf::FloatRect(0.f, 0.f, 16.f, 16.f),
            anim, player, sf::Vector2f(1.f, 0.f)));
    }

    return stairs;
}

std::vector<AutoTransition*> SpaceHallGenerator::generateTiles(Player* player) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    std::vector<AutoTransition*> tiles;
    {
        Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));
        tiles.push_back(new AutoTransition(sf::Vector2f(0.f, 200.f), sf::Vector2f(), sf::FloatRect(0.f, 0.f, 16.f, 32.f),
            anim, 25, sf::Vector2f(2376.f, 592.f), true, player));
    }
    {
        Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));
        tiles.push_back(new AutoTransition(sf::Vector2f(304.f, 200.f), sf::Vector2f(), sf::FloatRect(0.f, 0.f, 16.f, 32.f),
            anim, 27, sf::Vector2f(160.f, 232.f), true, player));
    }

    return tiles;
}

Room* SpaceHallGenerator::generateHall(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;
    std::vector<GameObject*> foregrounds;

    std::vector<Stairs*> stairs = generateStairs(player);
    floors.insert(floors.end(), stairs.begin(), stairs.end());
    
    std::vector<AutoTransition*> tiles = generateTiles(player);
    floors.insert(floors.end(), tiles.begin(), tiles.end());
    
    std::vector<Decoration*> foreGr = generateForeground();
    foregrounds.insert(foregrounds.end(), foreGr.begin(), foreGr.end());

    backgrounds.push_back(generateBackground());

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, foregrounds, std::vector<GameObject*>() };

    return new Room(player, graph, nullptr, objects, sf::Vector2f(_roomWidth, _roomHeight),
        sf::Vector2<bool>(false, false));
}
