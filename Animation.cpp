/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Animation.h"
#include "AnimationFrames.h"

Animation::Animation()
{

}

Animation::Animation(std::vector<AnimationFrame> frames, sf::Texture* spriteSheet, unsigned int frameIndex)
                    : _frames(frames), _spriteSheet(spriteSheet), _frameIndex(frameIndex)
{
    _numberOfFrames = frames.size();
    _elapsedTicks = 0;
    _looping = true;

    _vertices.setPrimitiveType(sf::Quads);
    _vertices.resize(4);

    _vertices[0].position = sf::Vector2f(0.f, 0.f);
    _vertices[1].position = sf::Vector2f(_frames[_frameIndex].offset.width, 0.f);
    _vertices[2].position = sf::Vector2f(_frames[_frameIndex].offset.width, _frames[_frameIndex].offset.height);
    _vertices[3].position = sf::Vector2f(0.f, _frames[_frameIndex].offset.height);

    setVertices();
}

Animation::~Animation()
{
    _spriteSheet = nullptr;
    _vertices.clear();
	_frames.clear();
}

unsigned int Animation::getNumberOfFrames() const
{
	return _numberOfFrames;
}

unsigned int Animation::getFrameIndex() const
{
	return _frameIndex;
}

unsigned int Animation::getElapsedTicks() const
{
	return _elapsedTicks;
}

std::vector<AnimationFrame> Animation::getFrames() const
{
	return _frames;
}

sf::Vector2u Animation::getSize() const
{
    return sf::Vector2u(_frames[_frameIndex].offset.width, _frames[_frameIndex].offset.height);
}

void Animation::setFrameIndex(unsigned int index)
{
    _frameIndex = index;
    _elapsedTicks = 0;
    setVertices();
}

void Animation::setElapsedTicks(unsigned int ticks)
{
    _elapsedTicks = ticks;
}

void Animation::setLooping(bool looping)
{
    _looping = looping;
}

void Animation::setVertices()
{
    _vertices[0].texCoords = sf::Vector2f(_frames[_frameIndex].offset.left, _frames[_frameIndex].offset.top);
    _vertices[1].texCoords = sf::Vector2f(_frames[_frameIndex].offset.left + _frames[_frameIndex].offset.width, _frames[_frameIndex].offset.top);
    _vertices[2].texCoords = sf::Vector2f(_frames[_frameIndex].offset.left + _frames[_frameIndex].offset.width, _frames[_frameIndex].offset.top + _frames[_frameIndex].offset.height);
    _vertices[3].texCoords = sf::Vector2f(_frames[_frameIndex].offset.left, _frames[_frameIndex].offset.top + _frames[_frameIndex].offset.height);
}

void Animation::setColor(sf::Color color)
{
    for (unsigned int i = 0; i < _vertices.getVertexCount(); i++)
        _vertices[i].color = color;
}

void Animation::setTransparency(sf::Uint8 alpha)
{
    for (unsigned int i = 0; i < _vertices.getVertexCount(); i++)
        _vertices[i].color.a = alpha;
}

bool Animation::isFinished()
{
    return (_elapsedTicks >= _frames[_frameIndex].time && _frameIndex >= _numberOfFrames - 1);
}

void Animation::animate()
{
	if (_elapsedTicks >= _frames[_frameIndex].time)
	{
		_elapsedTicks = 0;
        if (_looping)
        {
            if (_frameIndex >= _numberOfFrames - 1)
                _frameIndex = 0;
            else
                _frameIndex++;
        }
        else if (_frameIndex < _numberOfFrames - 1)
            _frameIndex++;
	}
    _elapsedTicks++;
    setVertices();
}

void Animation::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    states.texture = _spriteSheet;

    target.draw(_vertices, states);
}
