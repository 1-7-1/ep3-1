/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "FseTrigger.h"
#include "Animation.h"
#include "Room.h"
#include "Player.h"

FseTrigger::FseTrigger(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox, Animation* animation,
    Player* player): CollidableObject(position, origin, hitbox), _player(player)
{
    _currentAnimation = animation;
    _currentAnimation->setOrigin(_origin);
}

FseTrigger::~FseTrigger()
{
    delete _currentAnimation;
}

void FseTrigger::behave()
{
    sf::FloatRect adjHitbox = sf::FloatRect(_hitbox.left + _position.x, _hitbox.top + _position.y,
        _hitbox.width, _hitbox.height);
    if (_room->checkCollision(adjHitbox, _player))
    {
        _room->setFseActive(true);
        _player->setPosition(_player->getPosition() + sf::Vector2f(1.f, 1.f));
    }
    changeAnimation();
}