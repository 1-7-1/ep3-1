/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "UnderwaterGenerator.h"
#include "UnderwaterRoom.h"
#include "Decoration.h"
#include "TileCalculator.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "Player.h"

UnderwaterGenerator::UnderwaterGenerator()
{

}

UnderwaterGenerator::~UnderwaterGenerator()
{

}

void UnderwaterGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/bg/17.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/char/1.png", sf::Color(0, 255, 255, 255));
    _isLoaded = true;
}

std::vector<Decoration*> UnderwaterGenerator::generateBackground() const
{
    TileCalculator bgCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(322, 240));
    
    std::vector<Decoration*> backs;

    for (int i = 0; i < 2; i++)
    {
        Animation* anim = new Animation(bgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0), sf::Vector2u(0, 1), sf::Vector2u(0, 2)}),
            std::vector<unsigned int>({12, 12, 12})),
            _db.getTexture(bg));
        backs.push_back(new Decoration(sf::Vector2f(-160.f + i * 320.f, 0.f), sf::Vector2f(1.f, 0.f),
            sf::Vector2f(-0.1f, 0.f), anim));
    }

    return backs;
}

Room* UnderwaterGenerator::generateUnderwater(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;
    std::vector<GameObject*> foregrounds;

    std::vector<Decoration*> back = generateBackground();
    backgrounds.insert(backgrounds.end(), back.begin(), back.end());

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, foregrounds, std::vector<GameObject*>() };

    return new UnderwaterRoom(player, graph, nullptr, objects, sf::Vector2f(_roomWidth, _roomHeight),
        sf::Vector2<bool>(true, false));
}
