/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Menu.h"
#include "Animation.h"
#include "MenuCursor.h"

Menu::Menu(std::vector<Animation*> animations, MenuCursor* cursor,
    Menu* parent, std::vector<Menu*> subMenus)
    : GameObject(sf::Vector2f(), sf::Vector2f()), _animations(animations), _cursor(cursor), _cursorPosition(0), _parentMenu(parent), _subMenus(subMenus),
    _active(false)
{
    for (Menu* menu: _subMenus)
    {
        if (menu != nullptr)
            menu->setParent(this);
    }
}

Menu::~Menu()
{
    delete _cursor;
    for (Animation* anim: _animations)
        delete anim;
    for (Menu* menu: _subMenus)
        delete menu;
}

void Menu::setActive(bool active)
{
    _active = active;
}

void Menu::setParent(Menu* parent)
{
    _parentMenu = parent;
}

bool Menu::isActive() const
{
    return _active;
}

void Menu::start()
{
    resetInputs();
    _cursorPosition = 0;
    _currentAnimation->setFrameIndex(0);
    _currentAnimation->animate();
}

void Menu::enter()
{
    resetInputs();
    if (_subMenus[_cursorPosition] != nullptr)
    {
        _active = false;
        _subMenus[_cursorPosition]->setActive(true);
        _subMenus[_cursorPosition]->placeCursors();
        _subMenus[_cursorPosition]->setPosition(_position);
        return;
    }
}

void Menu::leave()
{
    resetInputs();
    if (_parentMenu != nullptr)
    {
        _active = false;
        _parentMenu->setActive(true);
        _parentMenu->placeCursors();
        _parentMenu->setPosition(_position);
    }
}

void Menu::leaveAll()
{
    if (_active)
    {
        leave();
        if (_parentMenu != nullptr)
            _parentMenu->leaveAll();
    }
    else
        _subMenus[_cursorPosition]->leaveAll();
}

std::vector<GameObject*> Menu::getGraphics()
{
    if (!_active && _subMenus[_cursorPosition] != nullptr)
    {
        return _subMenus[_cursorPosition]->getGraphics();
    }
    else
    {
        std::vector<GameObject*> objects;
        objects.push_back(this);
        if (_cursor != nullptr)
            objects.push_back(_cursor);
        return objects;
    }
}

void Menu::behave()
{
    if (_active)
    {
        handleInput();
        behaveYourself();
        _cursor->behave();
    }
    else if (_subMenus[_cursorPosition] != nullptr)
    {
        _subMenus[_cursorPosition]->behave();
    }

    changeAnimation();
}
