/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Npc.h"
#include "Player.h"
#include "Room.h"

Npc::Npc(sf::Vector2f position, sf::Vector2f origin, float speedLength, sf::FloatRect hitbox)
    : CollidableObject(position, origin, hitbox), InteractibleObject(position, origin, hitbox),
    Entity(position, origin, speedLength, hitbox), _currentState(idle)
{

}

Npc::~Npc()
{

}

void Npc::interact(Player* player)
{
    if (_currentState != dead)
    {
        if (player->getEffect() == machete && player->getState() == action)
        {
            _room->createSpecialEffect(_position + sf::Vector2f(0.f, 0.01f), sf::Vector2f(0.f, getSize().y / 4.f), blood);
            _timeToDie = 63;
            _currentState = dead;
        }
    }
}
