/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Post.h"
#include "Animation.h"
#include "Player.h"
#include "Room.h"

Post::Post(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox, Animation* animation,
    unsigned int destination, sf::Vector2f transitionVec, bool absolute)
    : CollidableObject(position, origin, hitbox), InteractibleObject(position, origin, hitbox),
    _destination(destination), _transitionVector(transitionVec), _transitionAbsolute(absolute)
{
    _currentAnimation = animation;
    _currentAnimation->setOrigin(_origin);
}

Post::~Post()
{
    delete _currentAnimation;
}

void Post::interact(Player* player)
{
    if (player->getState() == idle)
        _room->changeRoom(_destination, _transitionVector, _transitionAbsolute, 63);
}

void Post::behave()
{
    changeAnimation();
}
