/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Player.h"
#include "MathConstants.h"
#include "Animation.h"
#include "Room.h"
#include "InteractibleObject.h"
#include "RoomGraph.h"
#include "Bench.h"
#include <cmath>

Player::Player(const sf::Vector2f position, const sf::Vector2f origin,
    const std::vector<std::vector<Animation*>> animations, std::vector<Animation*> extraAnimations,
    const sf::FloatRect hitbox)
    : CollidableObject(position, origin, hitbox), Entity(position, origin, 1.f, hitbox), _animationDirection(right),
    _effect(normal), _state(idle), _currentScript(none), _animations(animations), _extraAnimations(extraAnimations),
    _bench(nullptr), _schoolgirlKilled(false), _waterfallActivated(false), _direction(0.f)
{
    _obtainedEffects = { false, false, false, false, false };

    for (std::vector<Animation*> animList : _animations)
    {
        for (Animation* anim : animList)
            anim->setOrigin(_origin);
    }
    for (Animation* anim : _extraAnimations)
            anim->setOrigin(_origin);

    _currentAnimation = _animations[_state][_animationDirection];
}

Player::~Player()
{
    for (std::vector<Animation*> animList : _animations)
    {
        for (Animation* anim : animList)
            delete anim;
    }

    for (Animation* anim : _extraAnimations)
            delete anim;
}

float Player::getDirection() const
{
    return _direction;
}

EntityDirection Player::getAnimationDirection() const
{
    return _animationDirection;
}

Effect Player::getEffect() const
{
    return _effect;
}

PlayerState Player::getState() const
{
    return _state;
}

Script Player::getScript() const
{
    return _currentScript;
}

std::vector<bool> Player::getObtainedEffects() const
{
    return _obtainedEffects;
}

void Player::setObtainedEffects(const std::vector<bool> effects)
{
    _obtainedEffects = effects;
}

void Player::setGraph(RoomGraph* graph)
{
    _graph = graph;
}

void Player::setStairs(sf::Vector2f rightVec)
{
    _movementMode = stairMovement;
    _stairVector = rightVec;
}

void Player::setEffect(Effect effect)
{
    _effect = effect;
}

void Player::setScale(float scalex, float scaley)
{
    for (std::vector<Animation*> animList : _animations)
    {
        for (Animation* anim : animList)
            anim->setScale(scalex, scaley);
    }
    for (Animation* anim : _extraAnimations)
            anim->setScale(scalex, scaley);
}

void Player::changeEffect(Effect effect)
{
    if (_graph->getCurrentRoom() > 3 && _currentScript == none)
    {
        _room->createSpecialEffect(_position + sf::Vector2f(0.f, -12.f), sf::Vector2f(), effectSwitch, specialLayer, this);
        if (_effect != effect)
            _effect = effect;
        else
            _effect = normal;
    }
}

void Player::startScript(Script script)
{
    if (_currentScript == none)
    {
        switch (script)
        {
            case transitionWait:
                _state = idle;
                _currentAnimation = _animations[_state + _effect * LAST][_animationDirection];
                _currentScript = transitionWait;
                break;

            case sleep:
                _currentAnimation = _extraAnimations[0];
                _currentAnimation->setFrameIndex(0);
                _currentAnimation->setLooping(false);
                _currentScript = sleep;
                break;

            case shortWait:
                _currentScript = shortWait;
                _state = idle;
                _waitTimer = 60;
                break;

            case sink:
                _currentScript = sink;
                _currentAnimation = _extraAnimations[1];
                _currentAnimation->setFrameIndex(0);
                _currentAnimation->setLooping(false);
                break;

            case swim:
                _currentScript = swim;
                _currentAnimation = _extraAnimations[2];
                _speedLength = 0.1f;
                break;

            case drip:
                _currentScript = drip;
                _currentAnimation = _extraAnimations[3];
                _currentAnimation->setFrameIndex(0);
                _currentAnimation->setLooping(false);
                break;
            
            default:
                break;
        }
    }
    else if (_currentScript == transitionWait && script == invisibleWait)
    {
        _currentScript = invisibleWait;
        _currentAnimation = _extraAnimations[11];
    }
}

void Player::endScript(Script script)
{
    if (script == _currentScript)
    {
        _currentScript = none;
        if (script == swim)
            _speedLength = 1.f;
    }
    else if (script == transitionWait && _currentScript == invisibleWait)
        _currentScript = none;
}

void Player::endScript()
{
    if (_currentScript == swim)
        _speedLength = 1.f;
    _currentScript = none;
}

void Player::sit(Bench* bench)
{
    if (_effect != normal)
        changeEffect(_effect);
    _bench = bench;
    _currentScript = geton;
    _currentAnimation = _extraAnimations[10];
    _currentAnimation->setFrameIndex(0);
    _currentAnimation->setLooping(false);
}

void Player::setSchoolgirlKilled(bool killed)
{
    _schoolgirlKilled = killed;
}

bool Player::isSchoolgirlKilled() const
{
    return _schoolgirlKilled;
}

void Player::setWaterfallActivated(bool active)
{
    _waterfallActivated = active;
}

bool Player::areWaterfallActivated() const
{
    return _waterfallActivated;
}

void Player::resetInputs()
{
    _zPressed = true;
    _xPressed = true;
}

void Player::requestInteraction()
{
    float hitboxXDistance = _interactDistance * cos(_direction);
    float hitboxYDistance = -_interactDistance * sin(_direction);
    sf::FloatRect interactHitbox = _hitbox;
    interactHitbox.left += _position.x + hitboxXDistance;
    interactHitbox.top += _position.y + hitboxYDistance;
    
    for (InteractibleObject* obj: _room->getInteractibles())
    {
        if (_room->checkCollision(interactHitbox, obj))
        {
            obj->interact(this);
            break;
        }
    }
}

void Player::behave()
{
    //printf("POS: %f, %f\n", _position.x, _position.y);
    _speed.x = 0.f;
    _speed.y = 0.f;

    switch (_currentScript)
    {
        case none:
            if (_state != action)
                handleInput();

            calculateSpeed();

            if (_state == action)
            {
                if (_effect == machete && _currentAnimation->getFrameIndex() == 1)
                    requestInteraction();
                if (_currentAnimation->isFinished())
                    _state = idle;
            }
            break;

        case shortWait:
            if (_waitTimer > 0)
                _waitTimer--;
            else
                _currentScript = none;
            break;

        case sink:
            if (_currentAnimation->getFrameIndex() % 2 && _currentAnimation->getElapsedTicks() == 1)
                _room->createSpecialEffect(_position, sf::Vector2f(), ripple, floorLayer);
            if (_currentAnimation->isFinished())
            {
                _room->changeRoom(16, sf::Vector2f(160.f, 16.f), true, 63);
            }
            break;

        case swim:
            handleInput();
            _speed.y += 0.2f;
            calculateSpeed();
            _state = moving;
            if (_currentAnimation->getElapsedTicks() == 1)
            {
                EffectType type;
                switch (_currentAnimation->getFrameIndex())
                {
                    case 0:
                        type = swim1;
                        break;
                    
                    case 1:
                        type = swim2;
                        break;

                    case 2:
                        type = swim3;
                        break;

                    default:
                        type = swim4;
                        break;
                }
                _room->createSpecialEffect(_position, sf::Vector2f(), type, floorLayer);
            }
            break;

        case drip:
            if (_currentAnimation->isFinished())
            {
                _currentScript = drop;
                _currentAnimation = _extraAnimations[4];
                _currentAnimation->setFrameIndex(0);
                _currentAnimation->setLooping(false);
                _speedLength = 1.f;
            }
            break;

        case drop:
            _speed = sf::Vector2f(0.f, _speedLength);
            _speedLength += 0.2f;
            if (_position.y >= 176.f)
            {
                _currentScript = splash;
                _currentAnimation = _extraAnimations[5];
                _currentAnimation->setFrameIndex(0);
                _currentAnimation->setLooping(false);
                _speedLength = 1.f;
            }
            break;

        case splash:
            if (_currentAnimation->isFinished())
            {
                _currentScript = none;
                _currentAnimation = _animations[_state + _effect * LAST][_animationDirection];
            }
            break;

        case wakeup:
            if (_currentAnimation->isFinished())
            {
                _schoolgirlKilled = false;
                _waterfallActivated = false;
                if (_graph->getCurrentRoom() > 3)
                    _room->changeRoom(2, sf::Vector2f(244.f, 120.f), true, 1);
                else
                    _currentScript = none;
            }
            break;

        case sitting:
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::X))
            {
                if (!_xPressed)
                {
                    _xPressed = true;
                    _bench->leave(this);
                    _bench = nullptr;
                    _currentScript = getoff;
                    _currentAnimation = _extraAnimations[9];
                    _currentAnimation->setLooping(false);
                    _currentAnimation->setFrameIndex(0);
                }
            }
            else
                _xPressed = false;
            break;

        case sliding:
            if ((_currentAnimation->getFrameIndex() == 1 || _currentAnimation->getFrameIndex() == 5)
                && _currentAnimation->getElapsedTicks() == 9)
                _position.x -= 2.f;
            if (_currentAnimation->isFinished())
                _room->setFseActive(true);
                
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::X))
            {
                if (!_xPressed)
                {
                    _xPressed = true;
                    _bench->leave(this);
                    _bench = nullptr;
                    _currentScript = getoff;
                    _currentAnimation = _extraAnimations[9];
                    _currentAnimation->setLooping(false);
                    _currentAnimation->setFrameIndex(0);
                }
            }
            else
                _xPressed = false;
            break;

        case getoff:
            _position.y += 0.5f;
            if (_currentAnimation->isFinished())
            {
                _currentScript = none;
                _animationDirection = down;
            }
            break;

        case geton:
            if (_currentAnimation->getFrameIndex() == 0 && _currentAnimation->getElapsedTicks() == 40)
                _position.y -= 2.f;
            if (_currentAnimation->getFrameIndex() == 2 && _currentAnimation->getElapsedTicks() == 8)
                _position.y -= 2.f;
            if (_currentAnimation->isFinished())
            {
                _position.y -= 2.f;
                if (_schoolgirlKilled)
                {
                    _currentScript = sliding;
                    _currentAnimation = _extraAnimations[8];
                    _currentAnimation->setFrameIndex(0);
                    _currentAnimation->setLooping(false);
                }
                else
                {
                    _currentScript = sitting;
                    _currentAnimation = _extraAnimations[7];
                }
            }
            break;
        
        default:
            break;
    }

    move();
    changeAnimation();
}

void Player::handleInput()
{
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        _animationDirection = left;
        if (_movementMode == stairMovement)
            _speed = -_stairVector;
        else
            _speed.x = -_speedLength;
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        if (_movementMode != stairMovement)
        {
            _animationDirection = up;
            _speed.y = -_speedLength;
        }
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        _animationDirection = right;
        if (_movementMode == stairMovement)
            _speed = _stairVector;
        else
            _speed.x = _speedLength;
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        if (_movementMode != stairMovement)
        {
            _animationDirection = down;
            _speed.y = _speedLength;
        }
    }
    if (_currentScript == none)
    {
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
        {
            if (!_zPressed)
            {
                _zPressed = true;
                requestInteraction();
            }
        }
        else
            _zPressed = false;
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::X))
        {
            if (!_xPressed)
            {
                _xPressed = true;
                if (_state != action)
                {
                    _state = action;
                    _currentAnimation = _animations[_state + _effect * LAST][_animationDirection];
                    _currentAnimation->setFrameIndex(0);
                }
            }
        }
        else
            _xPressed = false;
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num9) || sf::Keyboard::isKeyPressed(sf::Keyboard::W))
        {
            if (_state != action)
            {
                if (_effect != normal)
                    changeEffect(_effect);
                _currentScript = wakeup;
                _currentAnimation = _extraAnimations[6];
                _currentAnimation->setFrameIndex(0);
                _currentAnimation->setLooping(false);
            }
        }
    }

    _movementMode = normalMovement;
}

void Player::calculateSpeed()
{
    if (_speed.x != 0 && _speed.y != 0)
    {
        _speed.x *= cos(math::PI / 4.0);
        _speed.y *= sin(math::PI / 4.0);
    }

    if (_speed.x != 0.f || _speed.y != 0.f)
        _direction = atan2(-_speed.y, _speed.x);

    if (_speed.x != 0.f || _speed.y != 0.f)
    {
        bool freeNext = true;
        bool freeX = (_speed.x != 0.f);
        bool freeY = (_speed.y != 0.f);

        for (unsigned int j = 0; j < _lists.size(); j++)
        {
            if (freeNext)
                freeNext = !checkCollision(_speed, _lists[j]);
            if (freeX)
                freeX = !checkCollision(sf::Vector2f(copysign(_speedLength, _speed.x), 0.f), _lists[j]);
            if (freeY)
                freeY = !checkCollision(sf::Vector2f(0.f, copysign(_speedLength, _speed.y)), _lists[j]);
        }

        if (!freeNext)
        {
            if (freeX)
                _speed = sf::Vector2f(copysign(_speedLength, _speed.x), 0.f);
            else if (freeY)
                _speed = sf::Vector2f(0.f, copysign(_speedLength, _speed.y));
            else
                _speed = sf::Vector2f(0.f, 0.f);
        }
    }
}

void Player::move()
{
    Entity::move();
}

void Player::changeAnimation()
{
    if (_speed.x == 0.f)
    {
        if (_speed.y < 0.f)
            _animationDirection = up;
        else if (_speed.y > 0.f)
            _animationDirection = down;
    }

    if (_speed.y == 0.f)
    {
        if (_speed.x < 0.f)
            _animationDirection = left;
        else if (_speed.x > 0.f)
            _animationDirection = right;
    }
    if (_state != action)
    {
        if (_speed.x == 0.f && _speed.y == 0.f)
            _state = idle;
        else
            _state = moving;
    }

    if (_graph->getCurrentRoom() == 4 && _state == moving && _currentAnimation->getFrameIndex() % 2 == 0 &&
        _currentAnimation->getElapsedTicks() == 1)
            _room->createSpecialEffect(_position, sf::Vector2f(), ripple, floorLayer);

    if (_currentScript == none || _currentScript == transitionWait || _currentScript == shortWait)
        _currentAnimation = _animations[_state + _effect * LAST][_animationDirection];
    GameObject::changeAnimation();
}
