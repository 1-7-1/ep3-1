/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UNDERW_GEN_H
#define UNDERW_GEN_H

#include "GameObjectGenerator.h"

class Room;
class Player;
class RoomGraph;
class Decoration;

class UnderwaterGenerator: public GameObjectGenerator
{
    private:
        enum Graphics { invisible, bg, beta };
        virtual void load();
        
        const float _roomWidth = 640.f;
        const float _roomHeight = 240.f;

        std::vector<Decoration*> generateBackground() const;

    public:
        UnderwaterGenerator();
        ~UnderwaterGenerator();

        Room* generateUnderwater(Player* player, RoomGraph* graph);
};

#endif
