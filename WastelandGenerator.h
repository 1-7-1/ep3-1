/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WASTEL_GEN_H
#define WASTEL_GEN_H

#include "GameObjectGenerator.h"

class Wall;
class Room;
class Player;
class RoomGraph;
class Decoration;
class WastelandEntrance;
class ScreenEffect;
class Scorpion;

class WastelandGenerator: public GameObjectGenerator
{
    private:
        enum Graphics { invisible, tileset, scorpion };
        enum WallType { small, arm, plus, leg, circle, big };
        virtual void load();

        const float _roomWidth = 1920.f;
        const float _roomHeight = 1920.f;
        
        std::vector<ScreenEffect*> generateSandEffect() const;
        std::vector<Decoration*> generateFloor() const;

        WastelandEntrance* generateEntrance(sf::Vector2f position, Player* player) const;
        Wall* generateWall(sf::Vector2f position, WallType type) const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateScorpionOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateScorpionTimeVectors() const;

        Scorpion* generateScorpion(sf::Vector2f position) const;

    public:
        WastelandGenerator();
        ~WastelandGenerator();

        Room* generateWasteland(Player* player, RoomGraph* graph);
};

#endif
