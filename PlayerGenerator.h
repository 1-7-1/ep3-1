/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PLAYER_GEN_H
#define PLAYER_GEN_H

#include "GameObjectGenerator.h"

class Player;

class PlayerGenerator: public GameObjectGenerator
{
    private:
        virtual void load();

        std::vector<std::vector<std::vector<sf::Vector2u>>> generatePlayerOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generatePlayerTimeVectors() const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateExtraOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateExtraTimeVectors() const;

    public:
        PlayerGenerator();
        ~PlayerGenerator();

        Player* generatePlayer(sf::Vector2f position);
};

#endif
