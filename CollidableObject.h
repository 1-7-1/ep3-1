/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COLL_OBJ_H
#define COLL_OBJ_H

#include "GameObject.h"

class CollidableObject: public GameObject
{
    protected:
        sf::FloatRect _hitbox;

        std::vector<std::vector<CollidableObject*>*> _lists;
        std::vector<sf::Vector2i> _gridPositions;

    public:
        CollidableObject(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox);
        virtual ~CollidableObject() = 0;

        void setHitbox(sf::FloatRect hitbox);
        void setGridPositions(std::vector<sf::Vector2i> pos);

        sf::FloatRect getHitbox() const;
        std::vector<sf::Vector2i> getGridPositions() const;
        std::vector<std::vector<CollidableObject*>*> getLists();

        void addList(std::vector<CollidableObject*>* list);
        void clearList();
};

#endif
