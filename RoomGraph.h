/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ROOM_GRAPH_H
#define ROOM_GRAPH_H

#include "RoomGenerator.h"
#include "SFML/Audio.hpp"
#include <vector>

class RoomNode;
class GameObject;
class MainMenu;

enum GameState { title, playing, pausing, quitting };

class RoomGraph
{
    private:
        std::vector<RoomNode*> _nodes;
        RoomGenerator _generator;

        int _idToDestroy;
        int _roomId;

        bool _enterPressed = true;

        Player* _player;
        PauseMenu* _pauseMenu;
        MainMenu* _mainMenu;
        unsigned int _fadeFactor = 4;

        const unsigned int _nbOfRooms = 30;

        GameState _state;

        sf::Music* _titleMusic;
        std::vector<sf::Music*> _musics;
        std::vector<sf::Music*> _roomMusics;
        std::vector<float> _roomMusicVolumes;

        bool _musicFade;
        bool _wakingUp;

    public:
        RoomGraph();
        ~RoomGraph();

        void prepareRoomChange(unsigned int index);
        void changeRoom(unsigned int index);

        void setState(GameState state);
        void setWakingUp(bool wakeup);

        std::vector<GameObject*> getGraphics();
        unsigned int getFade();
        int getCurrentRoom() const;
        GameState getState() const;
        bool isWakingUp() const;

        void pause();
        void leavePauseMenu();
        void load();

        void augmentVolume(float volume);

        void loadRoom(unsigned int id);
        void deleteRoom(RoomNode* node);

        void tick();
};

#endif
