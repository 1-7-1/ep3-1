/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BEDROOM_GEN_H
#define BEDROOM_GEN_H

#include "GameObjectGenerator.h"

class Room;
class Player;
class RoomGraph;
class Wall;
class Desk;
class Bed;
class AutoTransition;
class Decoration;

class BedroomGenerator: public GameObjectGenerator
{
    private:
        enum BedroomGraphicalRessources { bedroom, bed, tile };
        enum Sounds { bedSfx };
        virtual void load();

        std::vector<Animation*> generateDiagBedroomWallAnimations() const;
        std::vector<Wall*> generateBedroomDiagonalWall(sf::Vector2f position) const;

        std::vector<Animation*> generateBackBedroomWallAnimations() const;
        std::vector<Wall*> generateBedroomBackWall(sf::Vector2f position) const;

        std::vector<Animation*> generateFrontBedroomWallAnimations() const;
        std::vector<Wall*> generateBedroomFrontWall(sf::Vector2f position) const;

        Animation* generateCupboardAnimation() const;
        Wall* generateCupboard(sf::Vector2f position) const;

        Animation* generateDeskAnimation() const;
        Desk* generateDesk(sf::Vector2f position) const;

        std::vector<Animation*> generateBedAnimations() const;
        Bed* generateBed(sf::Vector2f position) const;

        std::vector<Animation*> generateBedroomFloorAnimations() const;
        std::vector<Decoration*> generateBedroomFloor(sf::Vector2f position) const;

        AutoTransition* generateTransTile(const sf::Vector2f position, const unsigned int destination,
            const sf::Vector2f transitionVec, const bool absolute, Player* _player) const;

    public:
        BedroomGenerator();
        ~BedroomGenerator();

        Room* generateBedroom(Player* player, RoomGraph* graph);
};

#endif
