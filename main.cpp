/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "RoomGraph.h"
#include "GameConstants.h"
#include "GameObject.h"
#include "Animation.h"

int main(int argc, char** argv)
{
    srand(time(NULL));

    bool showFrameLength = false;

    bool F1Pressed = false;
    bool F2Pressed = false;
    bool F3Pressed = false;
    bool F4Pressed = false;
    bool enterPressed = false;

    if (argc == 2)
    {
        std::string option = argv[1];
        showFrameLength = !option.compare("--count");
    }

    RoomGraph graph = RoomGraph();

    sf::RectangleShape fadeRect = sf::RectangleShape(sf::Vector2f(game::smallWindowWidth, game::smallWindowHeight));
    fadeRect.setFillColor(sf::Color(0, 0, 0, 0));

	sf::RenderWindow renderWindow(sf::VideoMode(game::smallWindowWidth, game::smallWindowHeight),
        game::title, sf::Style::None);

    if (sf::VideoMode::getDesktopMode().width > game::largeWindowWidth &&
        sf::VideoMode::getDesktopMode().height > game::largeWindowHeight)
        renderWindow.setSize(sf::Vector2u(game::largeWindowWidth, game::largeWindowHeight));
    else if (sf::VideoMode::getDesktopMode().width > game::medWindowWidth &&
        sf::VideoMode::getDesktopMode().height > game::medWindowHeight)
        renderWindow.setSize(sf::Vector2u(game::medWindowWidth, game::medWindowHeight));
    else
        renderWindow.setSize(sf::Vector2u(game::smallWindowWidth, game::smallWindowHeight));

    renderWindow.setPosition(sf::Vector2i(sf::VideoMode::getDesktopMode().width / 2 - renderWindow.getSize().x / 2, 0));

	sf::Event event;
	sf::Clock clock;
    bool borders = false;
    bool fullscreen = false;

    renderWindow.setVerticalSyncEnabled(true);

	while (renderWindow.isOpen())
	{
        while (renderWindow.pollEvent(event))
        {
            if (event.type == sf::Event::KeyPressed)
            {
                switch (event.key.code)
                {
                case sf::Keyboard::F1:
                    {
                        if (!F1Pressed && !fullscreen)
                        {
                            renderWindow.setPosition(sf::Vector2i(sf::VideoMode::getDesktopMode().width / 2 - renderWindow.getSize().x / 2,
                                sf::VideoMode::getDesktopMode().height / 2 - renderWindow.getSize().y / 2));
                            F1Pressed = true;
                        }
                    }
                    break;

                case sf::Keyboard::F2:
                    {
                        if (!F2Pressed && !fullscreen)
                        {
                            sf::Vector2u size = renderWindow.getSize();
                            sf::Vector2i position = renderWindow.getPosition();
                            if (borders)
                                renderWindow.create(sf::VideoMode(game::smallWindowWidth, game::smallWindowHeight),
                                    game::title, sf::Style::None);
                            else
                                renderWindow.create(sf::VideoMode(game::smallWindowWidth, game::smallWindowHeight),
                                    game::title, sf::Style::Default);
                            renderWindow.setSize(size);
                            renderWindow.setPosition(position);
                            borders = !borders;
                            F2Pressed = true;
                        }
                    }
                    break;

                case sf::Keyboard::F3:
                    {
                        if (!F3Pressed && !fullscreen)
                        {
                            if (renderWindow.getSize().x >= game::largeWindowWidth &&
                                renderWindow.getSize().y >= game::largeWindowHeight)
                                renderWindow.setSize(sf::Vector2u(game::smallWindowWidth, game::smallWindowHeight));
                            else if (renderWindow.getSize().x >= game::medWindowWidth &&
                                renderWindow.getSize().y >= game::medWindowHeight)
                                renderWindow.setSize(sf::Vector2u(game::largeWindowWidth, game::largeWindowHeight));
                            else
                                renderWindow.setSize(sf::Vector2u(game::medWindowWidth, game::medWindowHeight));

                            F3Pressed = true;
                        }
                    }
                    break;

                case sf::Keyboard::F4:
                    {
                        if (!F4Pressed && !event.key.alt)
                        {
                            if (fullscreen)
                            {
                                renderWindow.create(sf::VideoMode(game::smallWindowWidth, game::smallWindowHeight), game::title,
                                    sf::Style::Default);
                                renderWindow.setSize(sf::Vector2u(game::largeWindowWidth, game::largeWindowHeight));
                                renderWindow.setPosition(sf::Vector2i(sf::VideoMode::getDesktopMode().width / 2 - renderWindow.getSize().x / 2, 0));
                            }
                            else
                            {
                                renderWindow.create(sf::VideoMode(game::smallWindowWidth, game::smallWindowHeight), game::title,
                                    sf::Style::Fullscreen);
                            }
                            borders = true;
                            fullscreen = !fullscreen;
                            F4Pressed = true;
                        }
                    }
                    break;

                case sf::Keyboard::Return:
                    {
                        if (!enterPressed && event.key.alt)
                        {
                            if (fullscreen)
                            {
                                renderWindow.create(sf::VideoMode(game::smallWindowWidth, game::smallWindowHeight), game::title,
                                    sf::Style::Default);
                                renderWindow.setPosition(sf::Vector2i(sf::VideoMode::getDesktopMode().width / 2 - renderWindow.getSize().x / 2,
                                    sf::VideoMode::getDesktopMode().height / 2 - renderWindow.getSize().y / 2));
                            }
                            else
                            {
                                renderWindow.create(sf::VideoMode(game::smallWindowWidth, game::smallWindowHeight), game::title,
                                    sf::Style::Fullscreen);
                            }
                            borders = true;
                            fullscreen = !fullscreen;
                        }
                        enterPressed = true;
                    }
                    break;

                case sf::Keyboard::Escape:
                    renderWindow.create(sf::VideoMode(game::smallWindowWidth, game::smallWindowHeight), game::title,
                        sf::Style::Default);
                    renderWindow.close();
                    break;

                default:
                    break;
                }
            }
            if (event.type == sf::Event::KeyReleased)
            {
                switch (event.key.code)
                {
                    case sf::Keyboard::F1:
                        F1Pressed = false;
                        break;

                    case sf::Keyboard::F2:
                        F2Pressed = false;
                        break;

                    case sf::Keyboard::F3:
                        F3Pressed = false;
                        break;

                    case sf::Keyboard::F4:
                        F4Pressed = false;
                        break;

                    case sf::Keyboard::Return:
                        enterPressed = false;
                        break;

                    default:
                        break;
                }
            }
            if (event.type == sf::Event::EventType::Closed)
                renderWindow.close();
        }
        if (renderWindow.hasFocus())
        {
            while (clock.getElapsedTime().asMilliseconds() < game::frameLength)
            {

            }
            clock.restart();

            renderWindow.clear(sf::Color(0, 0, 0, 255));

            graph.tick();
            std::vector<GameObject*> objList = graph.getGraphics();

            for (GameObject* obj: objList)
                renderWindow.draw(*obj->getAnimation());

            fadeRect.setFillColor(sf::Color(0, 0, 0, graph.getFade()));

            if (graph.getState() == quitting)
                renderWindow.close();

            renderWindow.draw(fadeRect);

            if (showFrameLength)
                printf("%lli\n", clock.getElapsedTime().asMicroseconds());

            renderWindow.display();
        }
	}

    return 0;
}
