/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef HELL_PED_GEN_H
#define HELL_PED_GEN_H

#include "GameObjectGenerator.h"

class Room;
class Player;
class RoomGraph;
class Decoration;
class Wall;
class AutoTransition;
class MaskPedestal;

class HellPedestalGenerator: public GameObjectGenerator
{
    private:
        enum Graphics { invisible, bg, fg, pedestal };
        virtual void load();

        const float _roomWidth = 320.f;
        const float _roomHeight = 240.f;
        
        Decoration* generateBackground() const;
        Decoration* generateForeground() const;

        MaskPedestal* generatePedestal(Player* player) const;

        std::vector<Wall*> generateWalls() const;
        std::vector<AutoTransition*> generateTiles(Player* player) const;

    public:
        HellPedestalGenerator();
        ~HellPedestalGenerator();

        Room* generateHell(Player* player, RoomGraph* graph);
};

#endif
