/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "CityNpc5.h"
#include "Animation.h"
#include "Room.h"
#include "ColorController.h"

CityNpc5::CityNpc5(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox, Animation* animation,
    ColorController* controller, std::vector<std::vector<Animation*>> fakerAnimations)
    : CollidableObject(position, origin, hitbox), CityNpc(position, origin, 0.5f, hitbox, controller, fakerAnimations),
    _normalAnimation(animation)
{
    _currentAnimation = _normalAnimation;
    _currentAnimation->setOrigin(_origin);

    _currentState = moving;
    _speed.x = _speedLength;
    _speed.y = 0.f;
}

CityNpc5::~CityNpc5()
{
    delete _normalAnimation;
}

void CityNpc5::behave()
{
    CityNpc::behave();
    if (!_faker)
    {
        if (_currentState != dead)
        {
            _speed.x = _speedLength;
            if (_speed.x != 0.f || _speed.y != 0.f)
            {
                bool freeNext = true;

                for (unsigned int j = 0; j < _lists.size(); j++)
                {
                    if (freeNext)
                        freeNext = !checkCollision(_speed, _lists[j]);
                }

                if (!freeNext)
                    _speed.x = 0.f;
            }
            move();
            if (_speed.x != 0.f)
                changeAnimation();
        }
        else
        {
            _currentAnimation->setTransparency(_timeToDie * 4);
            _timeToDie--;
            if (_timeToDie == 0)
                _room->setToDestroy(this);
        }
    }
}
