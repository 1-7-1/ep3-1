/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef VIEW_H
#define VIEW_H

#include "SFML/Graphics.hpp"
#include <vector>

class GameObject;
class Room;

class View
{
    protected:
        static constexpr float _sizeX = 320.f;
        static constexpr float _sizeY = 240.f;

        sf::Vector2f _position;
        sf::Vector2f _size = sf::Vector2f(_sizeX, _sizeY);
        std::vector<GameObject*> _backgroundObjects;
        std::vector<GameObject*> _foregroundObjects;
        std::vector<GameObject*> _floorTiles;
        std::vector<GameObject*> _objectList;
        GameObject* _player;
        Room* _room;

        void setObjectPositions(const std::vector<GameObject*>& vec);

        void merge(unsigned int begin, unsigned int half, unsigned int end);
        void mergeSort(unsigned int begin, unsigned int end);

    public:
        View();
        View(GameObject* player, Room* room);
        ~View();

        void tick();
        void sort();
        
        bool isInView(const GameObject* obj) const;

        sf::Vector2f getPosition() const;
        sf::Vector2f getSize() const;
        std::vector<GameObject*> getFloorTiles() const;
        std::vector<GameObject*> getBackgrounds() const;
        std::vector<GameObject*> getForegrounds() const;
        std::vector<GameObject*> getGraphics() const;

        void clearGraphics();
        void addFloorTile(GameObject* object);
        void addBackgroundObject(GameObject* object);
        void addForegroundObject(GameObject* object);
        void addEntity(GameObject* object);
};

#endif
