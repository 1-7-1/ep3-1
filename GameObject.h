/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GAME_OBJ_H
#define GAME_OBJ_H

#include "SFML/Graphics.hpp"

class Animation;
class Room;

class GameObject
{
    protected:
        sf::Vector2f _position;
        sf::Vector2f _origin;
        Animation* _currentAnimation;
        Room* _room;

    public:
        GameObject(sf::Vector2f position, sf::Vector2f origin);
        virtual ~GameObject() = 0;

        void setPosition(sf::Vector2f position);
        void setOrigin(sf::Vector2f origin);
        void setRoom(Room* room);

        sf::Vector2f getPosition() const;
        sf::Vector2f getOrigin() const;
        virtual Animation* getAnimation() const;
        sf::Vector2f getCornerPosition() const;
        sf::Vector2f getSize() const;

        virtual void behave() = 0;
        virtual void changeAnimation() const;
};

#endif
