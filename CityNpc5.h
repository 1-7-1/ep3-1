/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CITY_NPC_5_H
#define CITY_NPC_5_H

#include "CityNpc.h"

class ColorController;

class CityNpc5: public CityNpc
{
    private:
        Animation* _normalAnimation;

    public:
        CityNpc5(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox,
            Animation* animation, ColorController* controller, std::vector<std::vector<Animation*>> fakerAnimations);
        ~CityNpc5();

        virtual void behave();
};

#endif
