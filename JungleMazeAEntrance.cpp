/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "JungleGenerator.h"

std::vector<Wall*> JungleGenerator::generateMazeAEntrance() const
{
    std::vector<Wall*> walls;
    walls.push_back(generateWall(sf::Vector2f(3712.f, 2976.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 2960.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 2944.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 2928.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 2912.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 2896.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 2896.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3744.f, 2896.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 2896.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3776.f, 2896.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3792.f, 2896.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3808.f, 2896.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3808.f, 2880.f), upleft, true));
    walls.push_back(generateWall(sf::Vector2f(3824.f, 2880.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3840.f, 2880.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3856.f, 2880.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3872.f, 2880.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3872.f, 2864.f), upleft, true));
    walls.push_back(generateWall(sf::Vector2f(3888.f, 2864.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3888.f, 2848.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3904.f, 2848.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3904.f, 2832.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3904.f, 2816.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3904.f, 2800.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3904.f, 2784.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3904.f, 2768.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3904.f, 2752.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3904.f, 2736.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3904.f, 2720.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3888.f, 2720.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3888.f, 2704.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3872.f, 2704.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3872.f, 2688.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3872.f, 2672.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3872.f, 2656.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3872.f, 2640.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3872.f, 2624.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3872.f, 2608.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3872.f, 2592.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3872.f, 2576.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3856.f, 2576.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3840.f, 2576.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(3840.f, 2560.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3840.f, 2544.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3840.f, 2528.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3840.f, 2512.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3840.f, 2496.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(3840.f, 2480.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3824.f, 2480.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3808.f, 2480.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3792.f, 2480.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3776.f, 2480.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3776.f, 2464.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3760.f, 2464.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3744.f, 2464.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 2464.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 2464.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 2464.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3680.f, 2464.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(3680.f, 2448.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 2448.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 2432.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 2416.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 2400.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 2400.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2400.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 2400.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3600.f, 2400.f), down, false));

    walls.push_back(generateWall(sf::Vector2f(3600.f, 2464.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 2464.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 2480.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2480.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2496.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 2496.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 2512.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 2512.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3680.f, 2512.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 2512.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 2512.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 2512.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 2528.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3744.f, 2528.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3744.f, 2544.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3744.f, 2560.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 2560.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 2560.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 2560.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 2576.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(3680.f, 2576.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 2576.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 2576.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2576.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 2576.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 2592.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(3600.f, 2592.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3584.f, 2592.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 2592.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3552.f, 2592.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3536.f, 2592.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 2592.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 2592.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 2608.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(3488.f, 2608.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3472.f, 2608.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 2608.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3440.f, 2608.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3424.f, 2608.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3408.f, 2608.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3408.f, 2624.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(3392.f, 2624.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3376.f, 2624.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3360.f, 2624.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3344.f, 2624.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3328.f, 2624.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3312.f, 2624.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3296.f, 2624.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3280.f, 2624.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3280.f, 2640.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(3264.f, 2640.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3248.f, 2640.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3232.f, 2640.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3216.f, 2640.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3200.f, 2640.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3184.f, 2640.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3168.f, 2640.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3152.f, 2640.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3136.f, 2640.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3120.f, 2640.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3104.f, 2640.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3088.f, 2640.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3072.f, 2640.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3072.f, 2640.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3072.f, 2656.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3056.f, 2656.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3040.f, 2656.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(3040.f, 2640.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(3024.f, 2640.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3008.f, 2640.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2992.f, 2640.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2976.f, 2640.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2960.f, 2640.f), down, false));

    walls.push_back(generateWall(sf::Vector2f(2960.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2976.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2992.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3008.f, 2688.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3024.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3040.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3056.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3072.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3088.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3104.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3120.f, 2688.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3120.f, 2672.f), upleft, true));
    walls.push_back(generateWall(sf::Vector2f(3136.f, 2672.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3152.f, 2672.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3168.f, 2672.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3184.f, 2672.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3200.f, 2672.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3200.f, 2688.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(3216.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3232.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3248.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3264.f, 2688.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3280.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3296.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3312.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3328.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3344.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3360.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3376.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3392.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3408.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3424.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3440.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3456.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3472.f, 2688.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3488.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3504.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3520.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3536.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3552.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3568.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3584.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3600.f, 2688.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 2688.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3616.f, 2672.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2672.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2656.f), upleft, true));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 2656.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 2640.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 2640.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 2624.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(3680.f, 2624.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(3680.f, 2608.f), upleft, true));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 2608.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 2608.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 2608.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 2624.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 2640.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 2656.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 2672.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3728.f, 2688.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 2688.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 2704.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 2720.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3712.f, 2736.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3696.f, 2736.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3680.f, 2736.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(3664.f, 2736.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(3648.f, 2736.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2736.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2752.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2768.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2784.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2800.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2816.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2832.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2848.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2864.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2880.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2896.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2912.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2928.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2944.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2960.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(3632.f, 2976.f), right, false));

    for (int j = 0; j < 3; j++)
        for (int i = 0; i < 20; i++)
            walls.push_back(generateWall(sf::Vector2f(2960.f + i * 32.f, 2496.f + j * 32.f), turnupright, true));
    for (int i = 0; i < 14; i++)
        walls.push_back(generateWall(sf::Vector2f(2960.f + i * 32.f, 2560.f), turnupright, true));
    for (int i = 0; i < 10; i++)
        walls.push_back(generateWall(sf::Vector2f(2960.f + i * 32.f, 2560.f), turnupright, true));
    for (int j = 0; j < 4; j++)
        for (int i = 0; i < 20; i++)
            walls.push_back(generateWall(sf::Vector2f(3600.f + i * 32.f, 2272.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 20; i++)
            walls.push_back(generateWall(sf::Vector2f(3696.f + i * 32.f, 2400.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 3; j++)
        walls.push_back(generateWall(sf::Vector2f(3856.f, 2480.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 7; j++)
            walls.push_back(generateWall(sf::Vector2f(3888.f, 2464.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 16; j++)
        for (int i = 0; i < 20; i++)
            walls.push_back(generateWall(sf::Vector2f(3920.f + i * 32.f, 2464.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 6; i++)
            walls.push_back(generateWall(sf::Vector2f(3728.f + i * 32.f, 2912.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 4; j++)
        for (int i = 0; i < 21; i++)
            walls.push_back(generateWall(sf::Vector2f(2960.f + i * 32.f, 2704.f + j * 32.f), turnupright, true));
    for (int i = 0; i < 14; i++)
        walls.push_back(generateWall(sf::Vector2f(2960.f + i * 32.f, 2592.f), turnupright, true));
    for (int j = 0; j < 4; j++)
        for (int i = 0; i < 5; i++)
            walls.push_back(generateWall(sf::Vector2f(3472.f + i * 32.f, 2832.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 2; i++)
            walls.push_back(generateWall(sf::Vector2f(3648.f + i * 32.f, 2672.f + j * 32.f), turnupright, true));
    for (int i = 0; i < 4; i++)
        walls.push_back(generateWall(sf::Vector2f(3600.f + i * 32.f, 2528.f), turnupright, true));

    std::vector<Wall*> decorations;
    
    decorations = generateBigTree(sf::Vector2f(3777.f, 2712.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateTallTree(sf::Vector2f(3684.f, 2505.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3810.f, 2669.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3833.f, 2734.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3795.f, 2764.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3743.f, 2768.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3875.f, 2779.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3613.f, 2655.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(3555.f, 2628.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateSmallTree(sf::Vector2f(3642.f, 2432.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3347.f, 2646.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3655.f, 2607.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3686.f, 2777.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3775.f, 2540.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3822.f, 2506.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3775.f, 2834.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(3826.f, 2850.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateVerySmallTree(sf::Vector2f(3772.f, 2632.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3244.f, 2680.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3003.f, 2679.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3877.f, 2832.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3840.f, 2800.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3738.f, 2874.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3709.f, 2834.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3673.f, 2886.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3690.f, 2953.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(3829.f,2607.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateWeirdPlant(sf::Vector2f(3114.f,2669.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3271.f,2664.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3211.f,2669.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3166.f,2658.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3312.f,2645.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3350.f,2664.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3321.f,2676.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3391.f,2656.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3421.f,2674.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3465.f,2661.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3498.f,2661.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3526.f,2632.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3535.f,2651.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3522.f,2667.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3566.f,2653.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3602.f,2663.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(3645.f,2622.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateBigFern(sf::Vector2f(2974.f,2658.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3585.f,2625.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3628.f,2639.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3436.f,2652.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3695.f,2605.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3745.f,2583.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3795.f,2563.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3470.f,2634.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3028.f,2658.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3667.f,2477.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3796.f,2607.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3841.f,2625.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3829.f,2678.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3851.f,2699.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3726.f,2778.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3673.f,2795.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3848.f,2822.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(3665.f,2931.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateSmallFern(sf::Vector2f(3820.f,2526.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3848.f,2868.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3695.f,2884.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3686.f,2835.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3755.f,2853.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3808.f,2743.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3771.f,2757.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3789.f,2503.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3758.f,2519.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3734.f,2487.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3655.f,2488.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3648.f,2449.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(3613.f,2431.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    return walls;
}