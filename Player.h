/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PLAYER_H
#define PLAYER_H

#include "Entity.h"

enum Effect { normal, machete, scorpion, liquid, mask };
enum PlayerState { idle, moving, action, LAST };
enum Script { none, transitionWait, sleep, shortWait, sink, swim, drip, drop, splash, wakeup, sitting, sliding,
    getoff, geton, invisibleWait };
enum MovementMode { normalMovement, stairMovement };

class RoomGraph;
class Bench;

class Player: public Entity
{
    private:
        EntityDirection _animationDirection;
        Effect _effect;
        PlayerState _state;
        MovementMode _movementMode;
        sf::Vector2f _stairVector = sf::Vector2f();

        Script _currentScript;
        std::vector<std::vector<Animation*>> _animations;
        std::vector<Animation*> _extraAnimations;

        std::vector<bool> _obtainedEffects;

        bool _zPressed = true;
        bool _xPressed = true;

        Bench* _bench;
        bool _schoolgirlKilled;
        bool _waterfallActivated;

        unsigned int _waitTimer = 0;

        float _direction;
        static constexpr float _interactDistance = 4.f;

        RoomGraph* _graph;

        void handleInput();
        void calculateSpeed();

        void requestInteraction();

    public:
        Player(sf::Vector2f position, sf::Vector2f origin,
               std::vector<std::vector<Animation*>> _animations, std::vector<Animation*> extraAnimations,
               sf::FloatRect hitbox);
        virtual ~Player();

        float getDirection() const;
        EntityDirection getAnimationDirection() const;
        
        Effect getEffect() const;
        PlayerState getState() const;
        Script getScript() const;
        std::vector<bool> getObtainedEffects() const;
        
        void setObtainedEffects(std::vector<bool> effects);
        void setGraph(RoomGraph* graph);
        void setStairs(sf::Vector2f rightVec);
        void setEffect(Effect effect);
        void setScale(float scalex, float scaley);

        void changeEffect(Effect effect);

        void startScript(Script script);
        void endScript(Script script);
        void endScript();

        void sit(Bench* bench);
        void setSchoolgirlKilled(bool killed);
        bool isSchoolgirlKilled() const;
        void setWaterfallActivated(bool active);
        bool areWaterfallActivated() const;

        void resetInputs();

        virtual void behave();
        virtual void move();
        virtual void changeAnimation();
};

#endif
