/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "WastelandEntrance.h"
#include "Room.h"
#include "Player.h"
#include "Animation.h"

WastelandEntrance::WastelandEntrance(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox,
    std::vector<Animation*> animations, unsigned int destination, sf::Vector2f transitionVec, bool absolute,
    sf::FloatRect transitionHitbox, Player* player)
    : CollidableObject(position, origin, hitbox), _destination(destination), _transitionVector(transitionVec),
    _transitionAbsolute(absolute), _transitionHitbox(transitionHitbox), _animations(animations),
    _player(player), _open(true)
{
    _currentAnimation = _animations[0];
    for (Animation* anim: _animations)
        anim->setOrigin(_origin);
}

WastelandEntrance::~WastelandEntrance()
{
    for (Animation* anim: _animations)
        delete anim;
}

void WastelandEntrance::behave()
{
    if (_open)
    {
        if (!_room->isInView(this))
        {
            _open = false;
            _currentAnimation = _animations[1];
        }
        sf::FloatRect adjHitbox = sf::FloatRect(_transitionHitbox.left + _position.x, _transitionHitbox.top + _position.y,
            _transitionHitbox.width, _transitionHitbox.height);
        if (_room->checkCollision(adjHitbox, _player) && _player->getPosition().y > adjHitbox.top + adjHitbox.height)
            _room->changeRoom(_destination, _transitionVector, _transitionAbsolute, 63);
    }
    changeAnimation();
}
