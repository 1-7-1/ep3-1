/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GAME_OBJ_GEN_H
#define GAME_OBJ_GEN_H

#include "Database.h"
#include "SFML/Graphics.hpp"
#include <vector>

class Animation;

enum Direction { genleft, genright };

class GameObjectGenerator
{
    protected:
        Database _db;

        bool _isLoaded = false;

        virtual std::vector<std::vector<Animation*>> generateAnimationVector(sf::Texture* texture,
            std::vector<std::vector<std::vector<sf::Vector2u>>> offsets, std::vector<std::vector<unsigned int>> times,
            sf::Vector2u frameSize) const;

        virtual void load() = 0;

    public:
        GameObjectGenerator();
        virtual ~GameObjectGenerator();
        
        virtual void unload();
};

#endif
