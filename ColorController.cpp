/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ColorController.h"
#include "Animation.h"

ColorController::ColorController(Animation* anim, std::vector<GameObject*> objects)
    : GameObject(sf::Vector2f(), sf::Vector2f()), _objects(objects)
{
    _currentAnimation = anim;
    _currentAnimation->setOrigin(_origin);
    unsigned int color = rand() % 6;

    switch (color)
    {
    case 0:
        _currentColor = sf::Color(255, 0, 0, 255);
        break;

    case 1:
        _currentColor = sf::Color(0, 255, 0, 255);
        break;

    case 2:
        _currentColor = sf::Color(0, 0, 255, 255);
        break;

    case 3:
        _currentColor = sf::Color(0, 255, 255, 255);
        break;

    case 4:
        _currentColor = sf::Color(255, 0, 255, 255);
        break;

    default:
        _currentColor = sf::Color(255, 255, 0, 255);
        break;
    }
}

ColorController::~ColorController()
{
    delete _currentAnimation;
}

void ColorController::behave()
{
    if (_currentColor.r < 255 && _currentColor.g == 0 && _currentColor.b == 255)
        _currentColor.r++;
    else if (_currentColor.r > 0 && _currentColor.g == 255 && _currentColor.b == 0)
        _currentColor.r--;
    else if (_currentColor.r == 255 && _currentColor.g < 255 && _currentColor.b == 0)
        _currentColor.g++;
    else if (_currentColor.r == 0 && _currentColor.g > 0 && _currentColor.b == 255)
        _currentColor.g--;
    else if (_currentColor.r == 0 && _currentColor.g == 255 && _currentColor.b < 255)
        _currentColor.b++;
    else if (_currentColor.r ==255 && _currentColor.g == 0 && _currentColor.b > 0)
        _currentColor.b--;

    for (GameObject* obj: _objects)
        obj->getAnimation()->setColor(_currentColor);
}

void ColorController::remove(GameObject* obj)
{
    for (unsigned int i = 0; i < _objects.size(); i++)
        if (_objects[i] == obj)
            _objects.erase(_objects.begin() + i);
}

void ColorController::add(std::vector<GameObject*> objects)
{
    _objects.insert(_objects.end(), objects.begin(), objects.end());
}
