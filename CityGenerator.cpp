/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "CityGenerator.h"
#include "TileCalculator.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "AutoTransition.h"
#include "Door.h"
#include "Decoration.h"
#include "Room.h"
#include "ColorController.h"
#include "CityClock.h"
#include "Wall.h"
#include "CityNpc1.h"
#include "CityNpc2.h"
#include "CityNpc3.h"
#include "CityNpc4.h"
#include "CityNpc5.h"
#include "Player.h"
#include <math.h>

CityGenerator::CityGenerator()
{

}

CityGenerator::~CityGenerator()
{

}

void CityGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/tilesets/2.png", sf::Color(128, 0, 128, 255));
    _db.loadTexture("graphics/bg/2.png", sf::Color(0, 255, 255, 255));
    _db.loadTexture("graphics/char/3.png", sf::Color(128, 0, 128, 255));
    _db.loadTexture("graphics/char/4.png", sf::Color(128, 0, 128, 255));
    _db.loadTexture("graphics/char/5.png", sf::Color(128, 0, 128, 255));
    _db.loadTexture("graphics/char/6.png", sf::Color(128, 0, 128, 255));
    _db.loadTexture("graphics/char/7.png", sf::Color(128, 0, 128, 255));
    _db.loadTexture("graphics/char/1.png", sf::Color(0, 255, 255, 255));
    _db.loadTexture("graphics/tilesets/14.png", sf::Color(0, 255, 255, 255));
    _isLoaded = true;
}

AutoTransition* CityGenerator::generateSpikeTile(const sf::Vector2f position, const unsigned int destination,
    const sf::Vector2f transitionVec, const bool absolute, Player* _player) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(0, 144), sf::Vector2u(128, 128));
    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}), std::vector<unsigned int>({0})),
        _db.getTexture(tileset));

    return new AutoTransition(position, sf::Vector2f(64.f, 64.f), sf::FloatRect(-8.f, -8.f, 16.f, 16.f),
        anim, destination, transitionVec, absolute, _player);
}

std::vector<std::vector<std::vector<sf::Vector2u>>> CityGenerator::generateDoorOffsetVectors() const
{
    std::vector<std::vector<sf::Vector2u>> closedVec;
    closedVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(2, 8) }));

    std::vector<std::vector<sf::Vector2u>> openVec;
    openVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(2, 8) }));

    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ closedVec, openVec });
}

std::vector<std::vector<unsigned int>> CityGenerator::generateDoorTimeVectors() const
{
    std::vector<unsigned int> closedTimes = { 0 };
    std::vector<unsigned int> openTimes = { 0 };

    return std::vector<std::vector<unsigned int>>({ closedTimes, openTimes });
}

Door* CityGenerator::generateDoor(const sf::Vector2f position, const unsigned int destination,
    const sf::Vector2f transitionVec, const bool absolute) const
{
    std::vector<std::vector<Animation*>> animationVector = generateAnimationVector(_db.getTexture(maze),
    generateDoorOffsetVectors(), generateDoorTimeVectors(), sf::Vector2u(32, 32));

    return new Door(position + sf::Vector2f(0.f, 0.02f), sf::Vector2f(16.f, 16.f), sf::FloatRect(-16.f, 14.f, 32.f, 3.f),
        animationVector, nullptr, destination, transitionVec, absolute, 1);
}

std::vector<Wall*> CityGenerator::generateBuilding1(const sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(0, 288), sf::Vector2u(128, 128));
    TileCalculator invisibleCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    Animation* wallAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}), std::vector<unsigned int>({0})),
        _db.getTexture(tileset));

    Animation* glowAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 0), sf::Vector2u(2, 0),
        sf::Vector2u(3, 0)}), std::vector<unsigned int>({10, 10, 10})), _db.getTexture(tileset));

    std::vector<Wall*> walls;

    walls.push_back(new Wall(position, sf::Vector2f(48.f, 112.f),
        wallAnim, sf::FloatRect(-16.f, 15.f, 33.f, 1.f)));

    for (int i = 0; i < 16; i++)
    {
        Animation* anim = new Animation(invisibleCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));

        walls.push_back(new Wall(position, sf::Vector2f(),
            anim, sf::FloatRect(-32.f, 15.f - i, 49.f + i, 1.f)));
    }

    walls.push_back(new Wall(position + sf::Vector2f(0.f, 0.01f), sf::Vector2f(48.f, 112.f),
        glowAnim, sf::FloatRect(51.f, -1.f, 6.f, 11.f)));

    return walls;
}

std::vector<Wall*> CityGenerator::generateBuilding2(const sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(0, 432), sf::Vector2u(128, 128));
    TileCalculator invisibleCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    Animation* wallAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}), std::vector<unsigned int>({0})),
        _db.getTexture(tileset));

    Animation* glowAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 0), sf::Vector2u(2, 0),
        sf::Vector2u(3, 0)}), std::vector<unsigned int>({10, 10, 10})), _db.getTexture(tileset));

    std::vector<Wall*> walls;

    walls.push_back(new Wall(position, sf::Vector2f(48.f, 112.f),
        wallAnim, sf::FloatRect(-32.f, 15.f, 17.f, 1.f)));

    for (int i = 0; i < 16; i++)
    {
        Animation* anim = new Animation(invisibleCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));

        walls.push_back(new Wall(position, sf::Vector2f(),
            anim, sf::FloatRect(-32.f, 15.f - i, 17.f + i, 1.f)));
    }

    for (int i = 0; i < 16; i++)
    {
        Animation* anim = new Animation(invisibleCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));

        walls.push_back(new Wall(position, sf::Vector2f(),
            anim, sf::FloatRect(32.f, 15.f - i, 17.f + i, 1.f)));
    }

    walls.push_back(new Wall(position + sf::Vector2f(0.f, 0.01f), sf::Vector2f(48.f, 112.f),
        glowAnim, sf::FloatRect(-32.f, 15.f, 17.f, 1.f)));

    return walls;
}

std::vector<Wall*> CityGenerator::generateBuilding3(const sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(0, 576), sf::Vector2u(128, 128));
    TileCalculator invisibleCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    Animation* wallAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}), std::vector<unsigned int>({0})),
        _db.getTexture(tileset));

    Animation* glowAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 0), sf::Vector2u(2, 0),
        sf::Vector2u(3, 0)}), std::vector<unsigned int>({10, 10, 10})), _db.getTexture(tileset));

    std::vector<Wall*> walls;

    walls.push_back(new Wall(position, sf::Vector2f(56.f, 112.f),
        wallAnim, sf::FloatRect(-48.f, 15.f, 97.f, 1.f)));

    for (int i = 0; i < 16; i++)
    {
        Animation* anim = new Animation(invisibleCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));

        walls.push_back(new Wall(position, sf::Vector2f(),
            anim, sf::FloatRect(-48.f, 15.f - i, 97.f + i, 1.f)));
    }

    walls.push_back(new Wall(position + sf::Vector2f(0.f, 0.01f), sf::Vector2f(56.f, 112.f),
        glowAnim, sf::FloatRect(-48.f, 15.f, 97.f, 1.f)));

    return walls;
}

std::vector<Wall*> CityGenerator::generateBuilding(const sf::Vector2f position, const unsigned int type) const
{
    switch (type % 3)
    {
    case 0:
        return generateBuilding1(position);
        break;

    case 1:
        return generateBuilding2(position);
        break;

    default:
        return generateBuilding3(position);
        break;
    }
}

std::vector<Wall*> CityGenerator::generatePost(const sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(512, 288), sf::Vector2u(128, 256));
    TileCalculator invisibleCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    Animation* wallAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}), std::vector<unsigned int>({0})),
        _db.getTexture(tileset));

    Animation* glowAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 0), sf::Vector2u(2, 0),
        sf::Vector2u(3, 0)}), std::vector<unsigned int>({10, 10, 10})), _db.getTexture(tileset));

    std::vector<Wall*> walls;

    walls.push_back(new Wall(position, sf::Vector2f(64.f, 240.f),
        wallAnim, sf::FloatRect(-4.f, 0.f, 8.f, 16.f)));

    walls.push_back(new Wall(position + sf::Vector2f(0.f, 0.01f), sf::Vector2f(64.f, 240.f),
        glowAnim, sf::FloatRect(-4.f, 0.f, 8.f, 16.f)));

    return walls;
}

std::vector<Wall*> CityGenerator::generateBarrier(const sf::Vector2f position, const unsigned int length) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(512, 576), sf::Vector2u(128, 144));
    TileCalculator invisibleCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    std::vector<Wall*> walls;

    for (unsigned int i = 0; i < length; i++)
    {
        Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0), sf::Vector2u(0, 1),
        sf::Vector2u(0, 2)}), std::vector<unsigned int>({10, 10, 10})),
        _db.getTexture(tileset));

        walls.push_back(new Wall(position + sf::Vector2f(128.f * i, 0.f), sf::Vector2f(0.f, 124.f),
        anim, sf::FloatRect(0.f, 0.f, 128.f, 4.f)));
    }

    Animation* borderAnim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 0), sf::Vector2u(1, 1),
        sf::Vector2u(1, 2)}), std::vector<unsigned int>({10, 10, 10})), _db.getTexture(tileset));

    walls.push_back(new Wall(position + sf::Vector2f(128.f * length, 0.f), sf::Vector2f(0.f, 124.f),
        borderAnim, sf::FloatRect(0.f, 0.f, 16.f, 4.f)));

    return walls;
}

std::vector<Decoration*> CityGenerator::generateFloor() const
{
    TileCalculator floorCalc1 = TileCalculator(sf::Vector2u(128, 144), sf::Vector2u(128, 128));
    TileCalculator floorCalc2 = TileCalculator(sf::Vector2u(0, 144), sf::Vector2u(128, 128));
    std::vector<Decoration*> floors;

    for (int i = 0; i < _roomWidth / 128; i++)
    {
        Animation* anim;

        if (i > 8)
        {
            anim = new Animation(floorCalc1(std::vector<sf::Vector2u>({sf::Vector2u(4, 0)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));
                anim->setRotation(90.f * (i % 4));
        }
        else
        {
            anim = new Animation(floorCalc2(std::vector<sf::Vector2u>({sf::Vector2u(4, 0)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));
        }

        floors.push_back(new Decoration(sf::Vector2f(64.f + i * 128, 64.f), sf::Vector2f(64.f, 64.f),
            sf::Vector2f(), anim));
    }

    for (int i = 0; i < _roomWidth / 128; i++)
    {
        if (i != 2)
        {
            Animation* anim;

            if (i > 8)
            {
                anim = new Animation(floorCalc1(std::vector<sf::Vector2u>({sf::Vector2u(4, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                    anim->setRotation(90.f * (i % 4));
            }
            else
            {
                anim = new Animation(floorCalc2(std::vector<sf::Vector2u>({sf::Vector2u(4, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
            }

            floors.push_back(new Decoration(sf::Vector2f(64.f + i * 128, 192.f), sf::Vector2f(64.f, 64.f),
                sf::Vector2f(), anim));
        }
    }

    for (int i = 0; i < _roomWidth / 128; i++)
        for (int j = 2; j < 7; j++)
        {
            Animation* anim;

            if (i > 8)
            {
                anim = new Animation(floorCalc1(std::vector<sf::Vector2u>({sf::Vector2u(4, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                    anim->setRotation(90.f * (i % 4));
            }
            else
            {
                anim = new Animation(floorCalc2(std::vector<sf::Vector2u>({sf::Vector2u(4, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
            }

            floors.push_back(new Decoration(sf::Vector2f(64.f + i * 128, 64.f + j * 128), sf::Vector2f(64.f, 64.f),
                sf::Vector2f(), anim));
        }

    for (int i = 0; i < _roomWidth / 128; i++)
    {
        if (i != 9)
        {
            Animation* anim;

            if (i > 8)
            {
                anim = new Animation(floorCalc1(std::vector<sf::Vector2u>({sf::Vector2u(4, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                    anim->setRotation(90.f * (i % 4));
            }
            else
            {
                anim = new Animation(floorCalc2(std::vector<sf::Vector2u>({sf::Vector2u(4, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
            }

            floors.push_back(new Decoration(sf::Vector2f(64.f + i * 128, 960.f), sf::Vector2f(64.f, 64.f),
                sf::Vector2f(), anim));
        }
    }

    for (int i = 0; i < _roomWidth / 128; i++)
        for (int j = 8; j < _roomHeight / 128; j++)
        {
            Animation* anim;

            if (i > 8)
            {
                anim = new Animation(floorCalc1(std::vector<sf::Vector2u>({sf::Vector2u(4, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                    anim->setRotation(90.f * (i % 4));
            }
            else
            {
                anim = new Animation(floorCalc2(std::vector<sf::Vector2u>({sf::Vector2u(4, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
            }

            floors.push_back(new Decoration(sf::Vector2f(64.f + i * 128, 64.f + j * 128), sf::Vector2f(64.f, 64.f),
                sf::Vector2f(), anim));
        }

    return floors;
}

std::vector<Decoration*> CityGenerator::generateBackground() const
{
    TileCalculator backCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(320, 240));
    std::vector<Decoration*> backgrounds;

    for (int i = 0; i < _roomWidth / 320; i++)
        for (int j = 0; j < _roomHeight / 240; j++)
        {
            Animation* anim = new Animation(backCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
            std::vector<unsigned int>({0})), _db.getTexture(bg));

            backgrounds.push_back(new Decoration(sf::Vector2f(i * 320, j * 240), sf::Vector2f(), sf::Vector2f(-5.f, -3.f),
                anim));
        }

    return backgrounds;
}

ColorController* CityGenerator::generateColorController(std::vector<GameObject *> objects) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}), std::vector<unsigned int>({0})),
        _db.getTexture(invisible));

    return new ColorController(anim, objects);
}

Decoration* CityGenerator::generateClockFrame(sf::Vector2f position) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(128, 128));
    Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 0)}), std::vector<unsigned int>({0})),
        _db.getTexture(tileset));

    return new Decoration(position, sf::Vector2f(64.f, 64.f), sf::Vector2f(), anim);
}

CityClock* CityGenerator::generateCityClock(sf::Vector2f position) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(128, 128));
    Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 0)}), std::vector<unsigned int>({0})),
        _db.getTexture(tileset));

    return new CityClock(position, sf::Vector2f(64.f, 64.f), anim);
}

std::vector<std::vector<std::vector<sf::Vector2u>>> CityGenerator::generateFakerOffsetVectors() const
{
    std::vector<std::vector<sf::Vector2u>> idleVec;
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 1) }));
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 0) }));
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 3) }));
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 2) }));

    std::vector<std::vector<sf::Vector2u>> walkVec;
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 1), sf::Vector2u(1, 1), sf::Vector2u(2, 1),
        sf::Vector2u(1, 1) }));
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 0), sf::Vector2u(1, 0), sf::Vector2u(2, 0),
        sf::Vector2u(1, 0) }));
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 3), sf::Vector2u(1, 3), sf::Vector2u(2, 3),
        sf::Vector2u(1, 3) }));
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 2), sf::Vector2u(1, 2), sf::Vector2u(2, 2),
        sf::Vector2u(1, 2) }));

    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ idleVec, walkVec });
}

std::vector<std::vector<unsigned int>> CityGenerator::generateFakerTimeVectors() const
{
    std::vector<unsigned int> idleTimes = { 0 };
    std::vector<unsigned int> walkTimes = { 10, 10, 10, 10 };

    return std::vector<std::vector<unsigned int>>({ idleTimes, walkTimes });
}

std::vector<std::vector<std::vector<sf::Vector2u>>> CityGenerator::generateCityNpc1OffsetVectors() const
{
    std::vector<std::vector<sf::Vector2u>> vec;
    vec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 0), sf::Vector2u(2, 0), sf::Vector2u(4, 0),
        sf::Vector2u(6, 0), sf::Vector2u(8, 0), sf::Vector2u(6, 0), sf::Vector2u(4, 0), sf::Vector2u(2, 0) }));

    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ vec });
}

std::vector<std::vector<unsigned int>> CityGenerator::generateCityNpc1TimeVectors() const
{
    std::vector<unsigned int> times = { 10, 10, 10, 10, 10, 10, 10, 10 };

    return std::vector<std::vector<unsigned int>>({ times });
}

CityNpc1* CityGenerator::generateCityNpc1(sf::Vector2f position, ColorController* controller) const
{
    std::vector<std::vector<Animation*>> animationVector = generateAnimationVector(_db.getTexture(npc1),
        generateCityNpc1OffsetVectors(), generateCityNpc1TimeVectors(), sf::Vector2u(24, 32));

    std::vector<std::vector<Animation*>> fakerAnimationVector = generateAnimationVector(_db.getTexture(faker),
        generateFakerOffsetVectors(), generateFakerTimeVectors(), sf::Vector2u(24, 32));

    return new CityNpc1(position, sf::Vector2f(12.f, 29.f), sf::FloatRect(-6.f, -2.f, 12.f, 4.f), animationVector[0][0],
        controller, fakerAnimationVector);
}

std::vector<std::vector<std::vector<sf::Vector2u>>> CityGenerator::generateCityNpc2OffsetVectors() const
{
    std::vector<std::vector<sf::Vector2u>> horVec;
    horVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 0), sf::Vector2u(1, 0), sf::Vector2u(2, 0),
        sf::Vector2u(3, 0) }));

    std::vector<std::vector<sf::Vector2u>> vertVec;
    vertVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 1), sf::Vector2u(1, 1), sf::Vector2u(2, 1),
        sf::Vector2u(3, 1) }));

    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ horVec, vertVec });
}

std::vector<std::vector<unsigned int>> CityGenerator::generateCityNpc2TimeVectors() const
{
    std::vector<unsigned int> horTimes = { 12, 12, 12, 12};
    std::vector<unsigned int> vertTimes = { 12, 12, 12, 12};

    return std::vector<std::vector<unsigned int>>({ horTimes, vertTimes });
}

CityNpc2* CityGenerator::generateCityNpc2(sf::Vector2f position, ColorController* controller) const
{
    std::vector<std::vector<Animation*>> animationVector = generateAnimationVector(_db.getTexture(npc2),
        generateCityNpc2OffsetVectors(), generateCityNpc2TimeVectors(), sf::Vector2u(24, 32));

    std::vector<std::vector<Animation*>> fakerAnimationVector = generateAnimationVector(_db.getTexture(faker),
        generateFakerOffsetVectors(), generateFakerTimeVectors(), sf::Vector2u(24, 32));

    std::vector<Animation*> anims = {animationVector[0][0], animationVector[1][0]};

    return new CityNpc2(position, sf::Vector2f(12.f, 29.f), sf::FloatRect(-6.f, -2.f, 12.f, 4.f), anims,
        controller, fakerAnimationVector);
}

std::vector<std::vector<std::vector<sf::Vector2u>>> CityGenerator::generateCityNpc3OffsetVectors() const
{
    std::vector<std::vector<sf::Vector2u>> vec;
    vec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 0), sf::Vector2u(2, 0), sf::Vector2u(4, 0) }));

    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ vec });
}

std::vector<std::vector<unsigned int>> CityGenerator::generateCityNpc3TimeVectors() const
{
    std::vector<unsigned int> times = { 10, 10, 10 };

    return std::vector<std::vector<unsigned int>>({ times });
}

CityNpc3* CityGenerator::generateCityNpc3(sf::Vector2f position, ColorController* controller) const
{
    std::vector<std::vector<Animation*>> animationVector = generateAnimationVector(_db.getTexture(npc3),
        generateCityNpc3OffsetVectors(), generateCityNpc3TimeVectors(), sf::Vector2u(48, 32));

    std::vector<std::vector<Animation*>> fakerAnimationVector = generateAnimationVector(_db.getTexture(faker),
        generateFakerOffsetVectors(), generateFakerTimeVectors(), sf::Vector2u(24, 32));

    return new CityNpc3(position, sf::Vector2f(24.f, 29.f), sf::FloatRect(-24.f, -2.f, 48.f, 4.f), animationVector[0][0],
        controller, fakerAnimationVector);
}

std::vector<std::vector<std::vector<sf::Vector2u>>> CityGenerator::generateCityNpc4OffsetVectors() const
{
    std::vector<std::vector<sf::Vector2u>> vec;
    vec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 0), sf::Vector2u(2, 0), sf::Vector2u(4, 0),
        sf::Vector2u(6, 0) }));

    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ vec });
}

std::vector<std::vector<unsigned int>> CityGenerator::generateCityNpc4TimeVectors() const
{
    std::vector<unsigned int> times = { 15, 15, 15, 15 };

    return std::vector<std::vector<unsigned int>>({ times });
}

CityNpc4* CityGenerator::generateCityNpc4(sf::Vector2f position, ColorController* controller) const
{
    std::vector<std::vector<Animation*>> animationVector = generateAnimationVector(_db.getTexture(npc4),
        generateCityNpc4OffsetVectors(), generateCityNpc4TimeVectors(), sf::Vector2u(24, 32));

    std::vector<std::vector<Animation*>> fakerAnimationVector = generateAnimationVector(_db.getTexture(faker),
        generateFakerOffsetVectors(), generateFakerTimeVectors(), sf::Vector2u(24, 32));

    return new CityNpc4(position, sf::Vector2f(12.f, 29.f), sf::FloatRect(-6.f, -2.f, 12.f, 4.f), animationVector[0][0],
        controller, fakerAnimationVector);
}

std::vector<std::vector<std::vector<sf::Vector2u>>> CityGenerator::generateCityNpc5OffsetVectors() const
{
    std::vector<std::vector<sf::Vector2u>> vec;
    vec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 0), sf::Vector2u(2, 0), sf::Vector2u(4, 0),
        sf::Vector2u(6, 0), sf::Vector2u(8, 0), sf::Vector2u(10, 0) }));

    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ vec });
}

std::vector<std::vector<unsigned int>> CityGenerator::generateCityNpc5TimeVectors() const
{
    std::vector<unsigned int> times = { 4, 4, 4, 4, 4, 4 };

    return std::vector<std::vector<unsigned int>>({ times });
}

CityNpc5* CityGenerator::generateCityNpc5(sf::Vector2f position, ColorController* controller) const
{
    std::vector<std::vector<Animation*>> animationVector = generateAnimationVector(_db.getTexture(npc5),
        generateCityNpc5OffsetVectors(), generateCityNpc5TimeVectors(), sf::Vector2u(24, 32));

    std::vector<std::vector<Animation*>> fakerAnimationVector = generateAnimationVector(_db.getTexture(faker),
        generateFakerOffsetVectors(), generateFakerTimeVectors(), sf::Vector2u(24, 32));

    return new CityNpc5(position, sf::Vector2f(12.f, 29.f), sf::FloatRect(-12.f, -2.f, 24.f, 4.f), animationVector[0][0],
        controller, fakerAnimationVector);
}

Npc* CityGenerator::generateCityNpc(sf::Vector2f position, ColorController* controller, unsigned int type) const
{
    switch (type % 5)
    {
    case 0:
        return generateCityNpc5(position, controller);
        break;

    case 1:
        return generateCityNpc1(position, controller);
        break;

    case 2:
        return generateCityNpc2(position, controller);
        break;

    case 3:
        return generateCityNpc3(position, controller);
        break;

    default:
        return generateCityNpc4(position, controller);
        break;
    }
}

Room* CityGenerator::generateCity(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;

    CityNpc::_killCount = 0;

    std::vector<GameObject*> colorControlled;

    ColorController* colorController = generateColorController(colorControlled);

    std::vector<Decoration*> backs = generateBackground();
    backgrounds.insert(backgrounds.end(), backs.begin(), backs.end());

    std::vector<Decoration*> cityfloor = generateFloor();
    floors.insert(floors.end(), cityfloor.begin(), cityfloor.end());

    std::vector<sf::Vector2f> buildingPos = { sf::Vector2f(759.f, 960.f), sf::Vector2f(503.f, 866.f),
        sf::Vector2f(631.f, 1631.f), sf::Vector2f(592.f, 1850.f), sf::Vector2f(981.f, 1084.f), sf::Vector2f(529.f, 336.f),
        sf::Vector2f(379.f, 1756.f), sf::Vector2f(886.f, 1052.f), sf::Vector2f(922.f, 1864.f), sf::Vector2f(110.f, 881.f),
        sf::Vector2f(860.f, 664.f), sf::Vector2f(828.f, 1174.f), sf::Vector2f(582.f, 1712.f), sf::Vector2f(514.f, 398.f),
        sf::Vector2f(623.f, 1780.f), sf::Vector2f(255.f, 1221.f), sf::Vector2f(599.f, 1040.f), sf::Vector2f(303.f, 783.f),
        sf::Vector2f(166.f, 1059.f), sf::Vector2f(853.f, 1714.f), sf::Vector2f(441.f, 711.f), sf::Vector2f(987.f, 889.f),
        sf::Vector2f(449.f, 1825.f), sf::Vector2f(111.f, 1467.f), sf::Vector2f(181.f, 1673.f), sf::Vector2f(276.f, 1805.f),
        sf::Vector2f(277.f, 1739.f), sf::Vector2f(405.f, 261.f), sf::Vector2f(306.f, 26.f), sf::Vector2f(870.f, 417.f),
        sf::Vector2f(719.f, 496.f), sf::Vector2f(826.f, 1827.f), sf::Vector2f(741.f, 1876.f), sf::Vector2f(531.f, 308.f),
        sf::Vector2f(715.f, 315.f), sf::Vector2f(487.f, 1575.f), sf::Vector2f(626.f, 776.f), sf::Vector2f(981.f, 1466.f),
        sf::Vector2f(775.f, 1090.f), sf::Vector2f(275.f, 1706.f), sf::Vector2f(762.f, 1017.f)};

    for (unsigned int i = 0; i < buildingPos.size(); i++)
    {
        std::vector<Wall*> building = generateBuilding(buildingPos[i], i % 3);
        collidables.insert(collidables.end(), building.begin(), building.end());
        colorControlled.push_back(building[building.size() - 1]);
    }

    std::vector<sf::Vector2f> postPos = {sf::Vector2f(2235.f, 1096.f), sf::Vector2f(1277.f, 49.f),
        sf::Vector2f(2035.f, 1116.f), sf::Vector2f(2018.f, 381.f), sf::Vector2f(2214.f, 1404.f),
        sf::Vector2f(1978.f, 837.f), sf::Vector2f(2363.f, 863.f), sf::Vector2f(2162.f, 645.f),
        sf::Vector2f(1676.f, 1298.f), sf::Vector2f(1889.f, 1848.f), sf::Vector2f(1548.f, 781.f),
        sf::Vector2f(1633.f, 71.f), sf::Vector2f(2366.f, 256.f), sf::Vector2f(2131.f, 464.f),
        sf::Vector2f(1500.f, 937.f), sf::Vector2f(1636.f, 112.f), sf::Vector2f(2039.f, 16.f),
        sf::Vector2f(15.f, 220.f), sf::Vector2f(1569.f, 53.f), sf::Vector2f(1038.f, 999.f),
        sf::Vector2f(1240.f, 815.f), sf::Vector2f(2274.f, 705.f), sf::Vector2f(2025.f, 238.f),
        sf::Vector2f(1213.f, 1826.f), sf::Vector2f(2528.f, 1738.f), sf::Vector2f(2060.f, 889.f),
        sf::Vector2f(1880.f, 684.f), sf::Vector2f(1187.f, 1603.f), sf::Vector2f(1962.f, 1427.f),
        sf::Vector2f(1356.f, 218.f), sf::Vector2f(2353.f, 1084.f), sf::Vector2f(1625.f, 1138.f),
        sf::Vector2f(2394.f, 0.f), sf::Vector2f(1582.f, 709.f), sf::Vector2f(1092.f, 1323.f),
        sf::Vector2f(1520.f, 1614.f), sf::Vector2f(2387.f, 297.f), sf::Vector2f(1908.f, 1159.f)};

    for (unsigned int i = 0; i < postPos.size(); i++)
    {
        std::vector<Wall*> post = generatePost(postPos[i]);
        collidables.insert(collidables.end(), post.begin(), post.end());
        colorControlled.push_back(post[post.size() - 1]);
    }

    std::vector<sf::Vector2f> barrierPos = {sf::Vector2f(1610.f, 43.f), sf::Vector2f(1117.f, 573.f),
        sf::Vector2f(1987.f, 142.f), sf::Vector2f(1403.f, 381.f), sf::Vector2f(1843.f, 923.f),
        sf::Vector2f(1844.f, 97.f), sf::Vector2f(1695.f, 458.f), sf::Vector2f(1236.f, 605.f)};

    std::vector<unsigned int> barrierLength = { 2, 4, 1, 3, 2, 3, 6, 1};

    for (unsigned int i = 0; i < barrierLength.size(); i++)
    {
        std::vector<Wall*> barrier = generateBarrier(barrierPos[i], barrierLength[i]);
        collidables.insert(collidables.end(), barrier.begin(), barrier.end());
        colorControlled.insert(colorControlled.end(), barrier.begin(), barrier.end());
    }

    std::vector<sf::Vector2f> npc5Pos = {sf::Vector2f(28.f, 748.f), sf::Vector2f(623.f, 635.f),
        sf::Vector2f(287.f, 580.f), sf::Vector2f(584.f, 599.f), sf::Vector2f(536.f, 615.f),
        sf::Vector2f(1001.f, 560.f), sf::Vector2f(813.f, 534.f),
        sf::Vector2f(270.f, 287.f), sf::Vector2f(107.f, 362.f), sf::Vector2f(461.f, 444.f),
        sf::Vector2f(537.f, 164.f), sf::Vector2f(753.f, 210.f), sf::Vector2f(109.f, 197.f),
        sf::Vector2f(74.f, 133.f), sf::Vector2f(243.f, 1904.f), sf::Vector2f(545.f, 1651.f),
        sf::Vector2f(722.f, 1492.f), sf::Vector2f(102.f, 1526.f), sf::Vector2f(903.f, 1543.f),
        sf::Vector2f(185.f, 1455.f), sf::Vector2f(1165.f, 1354.f), sf::Vector2f(1083.f, 1374.f),
        sf::Vector2f(1528.f, 1278.f), sf::Vector2f(1230.f, 1253.f), sf::Vector2f(1153.f, 989.f),
        sf::Vector2f(2461.f, 805.f)};

    for (unsigned int i = 0; i < npc5Pos.size(); i++)
    {
        Npc* npc = generateCityNpc(npc5Pos[i], colorController, 5);
        entities.push_back(npc);
        interactibles.push_back(npc);
        colorControlled.push_back(npc);
    }

    std::vector<sf::Vector2f> npcPos = {sf::Vector2f(669.f, 636.f), sf::Vector2f(488.f, 495.f),
        sf::Vector2f(27.f, 486.f), sf::Vector2f(753.f, 1899.f), sf::Vector2f(262.f, 1187.f),
        sf::Vector2f(944.f, 360.f), sf::Vector2f(347.f, 2551.f), sf::Vector2f(450.f, 1143.f),
        sf::Vector2f(647.f, 90.f), sf::Vector2f(676.f, 1765.f), sf::Vector2f(164.f, 1437.f),
        sf::Vector2f(315.f, 314.f), sf::Vector2f(397.f, 2430.f), sf::Vector2f(614.f, 812.f),
        sf::Vector2f(430.f, 1139.f), sf::Vector2f(766.f, 2480.f), sf::Vector2f(309.f, 1275.f),
        sf::Vector2f(214.f, 2089.f), sf::Vector2f(697.f, 587.f), sf::Vector2f(566.f, 247.f),
        sf::Vector2f(428.f, 254.f), sf::Vector2f(890.f, 509.f), sf::Vector2f(338.f, 1005.f),
        sf::Vector2f(862.f, 970.f), sf::Vector2f(458.f, 325.f), sf::Vector2f(593.f, 428.f),
        sf::Vector2f(85.f, 674.f), sf::Vector2f(417.f, 2229.f), sf::Vector2f(730.f, 546.f),
        sf::Vector2f(796.f, 1724.f), sf::Vector2f(523.f, 237.f), sf::Vector2f(213.f, 2118.f),
        sf::Vector2f(179.f, 65.f), sf::Vector2f(990.f, 2470.f), sf::Vector2f(932.f, 410.f),
        sf::Vector2f(981.f, 2437.f), sf::Vector2f(357.f, 574.f), sf::Vector2f(35.f, 542.f),
        sf::Vector2f(991.f, 1727.f), sf::Vector2f(933.f, 1680.f), sf::Vector2f(354.f, 2223.f),
        sf::Vector2f(857.f, 41.f), sf::Vector2f(939.f, 2100.f), sf::Vector2f(80.f, 725.f),
        sf::Vector2f(35.f, 2519.f), sf::Vector2f(792.f, 1438.f), sf::Vector2f(98.f, 2370.f),
        sf::Vector2f(305.f, 1604.f), sf::Vector2f(78.f, 190.f), sf::Vector2f(122.f, 1222.f),
        sf::Vector2f(560.f, 1644.f), sf::Vector2f(23.f, 499.f), sf::Vector2f(344.f, 766.f),
        sf::Vector2f(820.f, 439.f), sf::Vector2f(819.f, 5.f), sf::Vector2f(685.f, 1659.f),
        sf::Vector2f(128.f, 136.f), sf::Vector2f(62.f, 507.f), sf::Vector2f(325.f, 1833.f),
        sf::Vector2f(65.f, 2157.f), sf::Vector2f(356.f, 2001.f), sf::Vector2f(136.f, 457.f),
        sf::Vector2f(387.f, 2220.f), sf::Vector2f(499.f, 454.f), sf::Vector2f(1010.f, 883.f),
        sf::Vector2f(744.f, 1613.f), sf::Vector2f(649.f, 568.f), sf::Vector2f(959.f, 1941.f),
        sf::Vector2f(327.f, 255.f), sf::Vector2f(562.f, 2525.f), sf::Vector2f(626.f, 1700.f),
        sf::Vector2f(2220.f, 1730.f), sf::Vector2f(1528.f, 29.f), sf::Vector2f(492.f, 1420.f),
        sf::Vector2f(362.f, 1750.f), sf::Vector2f(682.f, 2344.f), sf::Vector2f(992.f, 503.f),
        sf::Vector2f(326.f, 2045.f), sf::Vector2f(811.f, 460.f), sf::Vector2f(638.f, 155.f),
        sf::Vector2f(467.f, 492.f), sf::Vector2f(594.f, 1741.f), sf::Vector2f(802.f, 1629.f),
        sf::Vector2f(81.f, 901.f), sf::Vector2f(690.f, 1153.f), sf::Vector2f(672.f, 159.f),
        sf::Vector2f(890.f, 896.f), sf::Vector2f(481.f, 2382.f), sf::Vector2f(53.f, 1066.f),
        sf::Vector2f(863.f, 52.f), sf::Vector2f(259.f, 1424.f), sf::Vector2f(543.f, 85.f),
        sf::Vector2f(736.f, 2002.f), sf::Vector2f(303.f, 542.f), sf::Vector2f(215.f, 1463.f),
        sf::Vector2f(1016.f, 373.f), sf::Vector2f(652.f, 44.f), sf::Vector2f(838.f, 786.f),
        sf::Vector2f(783.f, 1149.f), sf::Vector2f(922.f, 1779.f), sf::Vector2f(1002.f, 523.f),
        sf::Vector2f(5.f, 2265.f), sf::Vector2f(513.f, 1247.f), sf::Vector2f(1018.f, 2182.f),
        sf::Vector2f(224.f, 1797.f),

        sf::Vector2f(984.f, 1191.f), sf::Vector2f(226.f, 1665.f),
        sf::Vector2f(289.f, 198.f), sf::Vector2f(426.f, 1291.f), sf::Vector2f(378.f, 1031.f),
        sf::Vector2f(830.f, 1320.f), sf::Vector2f(316.f, 1335.f), sf::Vector2f(615.f, 693.f),
        sf::Vector2f(50.f, 56.f), sf::Vector2f(91.f, 1504.f), sf::Vector2f(717.f, 666.f),
        sf::Vector2f(495.f, 1463.f), sf::Vector2f(388.f, 586.f), sf::Vector2f(349.f, 882.f),
        sf::Vector2f(499.f, 1490.f), sf::Vector2f(793.f, 553.f), sf::Vector2f(207.f, 1784.f),
        sf::Vector2f(428.f, 186.f), sf::Vector2f(280.f, 599.f), sf::Vector2f(555.f, 753.f),
        sf::Vector2f(1010.f, 224.f), sf::Vector2f(214.f, 1499.f), sf::Vector2f(190.f, 60.f),
        sf::Vector2f(329.f, 1560.f), sf::Vector2f(171.f, 465.f), sf::Vector2f(339.f, 681.f),
        sf::Vector2f(319.f, 1237.f), sf::Vector2f(681.f, 1391.f), sf::Vector2f(16.f, 1406.f),
        sf::Vector2f(199.f, 861.f), sf::Vector2f(550.f, 1824.f), sf::Vector2f(755.f, 624.f),
        sf::Vector2f(63.f, 296.f), sf::Vector2f(240.f, 1455.f), sf::Vector2f(146.f, 681.f),
        sf::Vector2f(541.f, 1434.f), sf::Vector2f(641.f, 1181.f), sf::Vector2f(676.f, 1477.f),
        sf::Vector2f(514.f, 1441.f), sf::Vector2f(883.f, 517.f), sf::Vector2f(288.f, 1031.f),
        sf::Vector2f(629.f, 423.f), sf::Vector2f(187.f, 390.f), sf::Vector2f(333.f, 347.f),
        sf::Vector2f(581.f, 1880.f), sf::Vector2f(500.f, 1891.f), sf::Vector2f(558.f, 904.f),
        sf::Vector2f(248.f, 124.f), sf::Vector2f(1020.f, 622.f), sf::Vector2f(405.f, 901.f),
        sf::Vector2f(968.f, 1708.f), sf::Vector2f(964.f, 1627.f), sf::Vector2f(150.f, 519.f),
        sf::Vector2f(170.f, 202.f), sf::Vector2f(723.f, 1821.f), sf::Vector2f(229.f, 979.f),
        sf::Vector2f(302.f, 807.f), sf::Vector2f(561.f, 784.f), sf::Vector2f(566.f, 843.f),
        sf::Vector2f(87.f, 1569.f), sf::Vector2f(699.f, 963.f), sf::Vector2f(274.f, 1310.f),
        sf::Vector2f(69.f, 1005.f), sf::Vector2f(960.f, 1507.f), sf::Vector2f(912.f, 323.f),
        sf::Vector2f(165.f, 496.f), sf::Vector2f(730.f, 1088.f), sf::Vector2f(904.f, 530.f),
        sf::Vector2f(852.f, 1677.f), sf::Vector2f(742.f, 1526.f), sf::Vector2f(294.f, 813.f),
        sf::Vector2f(300.f, 1241.f), sf::Vector2f(731.f, 1251.f), sf::Vector2f(960.f, 493.f),
        sf::Vector2f(895.f, 1529.f), sf::Vector2f(579.f, 991.f), sf::Vector2f(265.f, 903.f),
        sf::Vector2f(326.f, 1249.f), sf::Vector2f(757.f, 43.f), sf::Vector2f(121.f, 1444.f),
        sf::Vector2f(365.f, 1126.f), sf::Vector2f(398.f, 780.f), sf::Vector2f(889.f, 1695.f),
        sf::Vector2f(447.f, 657.f), sf::Vector2f(1024.f, 932.f), sf::Vector2f(518.f, 826.f),
        sf::Vector2f(676.f, 767.f), sf::Vector2f(805.f, 1334.f), sf::Vector2f(905.f, 858.f),
        sf::Vector2f(950.f, 998.f), sf::Vector2f(795.f, 472.f), sf::Vector2f(40.f, 1294.f),
        sf::Vector2f(78.f, 1239.f), sf::Vector2f(128.f, 862.f), sf::Vector2f(848.f, 1329.f),
        sf::Vector2f(825.f, 1774.f), sf::Vector2f(177.f, 1628.f), sf::Vector2f(329.f, 777.f),
        sf::Vector2f(633.f, 283.f), sf::Vector2f(948.f, 711.f), sf::Vector2f(221.f, 496.f),
        sf::Vector2f(193.f, 1597.f), sf::Vector2f(455.f, 869.f), sf::Vector2f(358.f, 1594.f),
        sf::Vector2f(892.f, 1858.f),

        sf::Vector2f(65.f, 338.f), sf::Vector2f(970.f, 522.f),
        sf::Vector2f(180.f, 51.f), sf::Vector2f(138.f, 1487.f), sf::Vector2f(670.f, 870.f),
        sf::Vector2f(968.f, 1606.f), sf::Vector2f(749.f, 1406.f), sf::Vector2f(1021.f, 1372.f),
        sf::Vector2f(1012.f, 453.f), sf::Vector2f(412.f, 1901.f), sf::Vector2f(160.f, 1284.f),
        sf::Vector2f(946.f, 863.f), sf::Vector2f(64.f, 1225.f), sf::Vector2f(422.f, 620.f),
        sf::Vector2f(150.f, 1025.f), sf::Vector2f(350.f, 1860.f), sf::Vector2f(494.f, 1071.f),
        sf::Vector2f(693.f, 1845.f), sf::Vector2f(341.f, 812.f), sf::Vector2f(544.f, 1327.f),
        sf::Vector2f(312.f, 1770.f), sf::Vector2f(737.f, 878.f), sf::Vector2f(569.f, 39.f),
        sf::Vector2f(108.f, 1560.f), sf::Vector2f(612.f, 465.f), sf::Vector2f(736.f, 1835.f),
        sf::Vector2f(817.f, 690.f), sf::Vector2f(708.f, 1645.f), sf::Vector2f(23.f, 1722.f),
        sf::Vector2f(693.f, 447.f), sf::Vector2f(224.f, 74.f), sf::Vector2f(783.f, 28.f),
        sf::Vector2f(36.f, 1626.f), sf::Vector2f(199.f, 424.f), sf::Vector2f(744.f, 950.f),
        sf::Vector2f(71.f, 1017.f), sf::Vector2f(781.f, 909.f), sf::Vector2f(563.f, 1106.f),
        sf::Vector2f(985.f, 193.f), sf::Vector2f(432.f, 576.f), sf::Vector2f(813.f, 18.f),
        sf::Vector2f(38.f, 835.f), sf::Vector2f(828.f, 982.f), sf::Vector2f(742.f, 1399.f),
        sf::Vector2f(272.f, 1278.f), sf::Vector2f(629.f, 1169.f), sf::Vector2f(965.f, 143.f),
        sf::Vector2f(185.f, 186.f), sf::Vector2f(643.f, 1802.f), sf::Vector2f(662.f, 1620.f),
        sf::Vector2f(91.f, 1259.f), sf::Vector2f(390.f, 607.f), sf::Vector2f(202.f, 1653.f),
        sf::Vector2f(857.f, 1281.f), sf::Vector2f(589.f, 1302.f), sf::Vector2f(698.f, 1679.f),
        sf::Vector2f(623.f, 1492.f), sf::Vector2f(815.f, 413.f), sf::Vector2f(804.f, 1052.f),
        sf::Vector2f(872.f, 733.f), sf::Vector2f(581.f, 255.f), sf::Vector2f(21.f, 351.f),
        sf::Vector2f(890.f, 295.f), sf::Vector2f(329.f, 1412.f), sf::Vector2f(439.f, 1107.f),
        sf::Vector2f(203.f, 1231.f), sf::Vector2f(42.f, 186.f), sf::Vector2f(247.f, 1524.f),
        sf::Vector2f(312.f, 261.f), sf::Vector2f(705.f, 1686.f), sf::Vector2f(824.f, 705.f),
        sf::Vector2f(631.f, 1203.f), sf::Vector2f(565.f, 1551.f), sf::Vector2f(61.f, 644.f),
        sf::Vector2f(641.f, 1893.f), sf::Vector2f(971.f, 186.f), sf::Vector2f(542.f, 1733.f),
        sf::Vector2f(310.f, 1052.f), sf::Vector2f(633.f, 1347.f), sf::Vector2f(331.f, 942.f),
        sf::Vector2f(318.f, 1844.f), sf::Vector2f(460.f, 19.f), sf::Vector2f(555.f, 1819.f),
        sf::Vector2f(12.f, 926.f), sf::Vector2f(163.f, 1236.f), sf::Vector2f(606.f, 1665.f),
        sf::Vector2f(205.f, 727.f), sf::Vector2f(239.f, 1592.f), sf::Vector2f(670.f, 491.f),
        sf::Vector2f(563.f, 490.f), sf::Vector2f(939.f, 1441.f), sf::Vector2f(193.f, 1601.f),
        sf::Vector2f(330.f, 394.f), sf::Vector2f(355.f, 398.f), sf::Vector2f(304.f, 1128.f),
        sf::Vector2f(705.f, 1857.f), sf::Vector2f(341.f, 76.f), sf::Vector2f(917.f, 762.f),
        sf::Vector2f(351.f, 750.f), sf::Vector2f(300.f, 558.f), sf::Vector2f(705.f, 677.f),
        sf::Vector2f(738.f, 138.f), sf::Vector2f(107.f, 77.f), sf::Vector2f(850.f, 312.f),
        sf::Vector2f(651.f, 1581.f),

        sf::Vector2f(511.f, 88.f), sf::Vector2f(901.f, 1374.f),
        sf::Vector2f(403.f, 1300.f), sf::Vector2f(480.f, 1031.f), sf::Vector2f(777.f, 1735.f),
        sf::Vector2f(535.f, 384.f), sf::Vector2f(713.f, 842.f), sf::Vector2f(247.f, 1852.f),
        sf::Vector2f(161.f, 1700.f), sf::Vector2f(212.f, 884.f), sf::Vector2f(888.f, 1518.f),
        sf::Vector2f(1020.f, 1019.f), sf::Vector2f(314.f, 826.f), sf::Vector2f(344.f, 1235.f),
        sf::Vector2f(156.f, 1728.f), sf::Vector2f(486.f, 56.f), sf::Vector2f(846.f, 461.f),
        sf::Vector2f(627.f, 1158.f), sf::Vector2f(945.f, 1242.f), sf::Vector2f(502.f, 1401.f),
        sf::Vector2f(161.f, 660.f), sf::Vector2f(292.f, 1338.f), sf::Vector2f(108.f, 1880.f),
        sf::Vector2f(598.f, 1825.f), sf::Vector2f(49.f, 1436.f), sf::Vector2f(570.f, 1370.f),
        sf::Vector2f(554.f, 910.f), sf::Vector2f(398.f, 1483.f), sf::Vector2f(165.f, 1169.f),
        sf::Vector2f(853.f, 450.f), sf::Vector2f(953.f, 1623.f), sf::Vector2f(469.f, 558.f),
        sf::Vector2f(102.f, 123.f), sf::Vector2f(246.f, 1480.f), sf::Vector2f(534.f, 1148.f),
        sf::Vector2f(779.f, 586.f), sf::Vector2f(988.f, 1041.f), sf::Vector2f(1021.f, 658.f),
        sf::Vector2f(221.f, 820.f), sf::Vector2f(985.f, 1890.f), sf::Vector2f(421.f, 1218.f),
        sf::Vector2f(512.f, 1695.f), sf::Vector2f(768.f, 808.f), sf::Vector2f(850.f, 1647.f),
        sf::Vector2f(351.f, 770.f), sf::Vector2f(894.f, 1364.f), sf::Vector2f(587.f, 975.f),
        sf::Vector2f(940.f, 1687.f), sf::Vector2f(609.f, 622.f), sf::Vector2f(819.f, 1161.f),
        sf::Vector2f(497.f, 1712.f), sf::Vector2f(331.f, 238.f), sf::Vector2f(843.f, 953.f),
        sf::Vector2f(78.f, 267.f), sf::Vector2f(312.f, 244.f), sf::Vector2f(115.f, 563.f),
        sf::Vector2f(119.f, 817.f), sf::Vector2f(191.f, 227.f), sf::Vector2f(295.f, 1816.f),
        sf::Vector2f(124.f, 1215.f), sf::Vector2f(593.f, 628.f), sf::Vector2f(548.f, 712.f),
        sf::Vector2f(970.f, 1335.f), sf::Vector2f(59.f, 1572.f), sf::Vector2f(793.f, 40.f),
        sf::Vector2f(480.f, 440.f), sf::Vector2f(144.f, 774.f), sf::Vector2f(942.f, 1756.f),
        sf::Vector2f(554.f, 459.f), sf::Vector2f(507.f, 1651.f), sf::Vector2f(810.f, 30.f),
        sf::Vector2f(543.f, 996.f), sf::Vector2f(819.f, 1598.f), sf::Vector2f(218.f, 489.f),
        sf::Vector2f(15.f, 1255.f), sf::Vector2f(967.f, 1771.f), sf::Vector2f(812.f, 509.f),
        sf::Vector2f(306.f, 43.f), sf::Vector2f(928.f, 1601.f), sf::Vector2f(29.f, 1535.f),
        sf::Vector2f(963.f, 1062.f), sf::Vector2f(716.f, 906.f), sf::Vector2f(325.f, 249.f),
        sf::Vector2f(492.f, 1021.f), sf::Vector2f(722.f, 1236.f), sf::Vector2f(299.f, 1665.f),
        sf::Vector2f(113.f, 46.f), sf::Vector2f(920.f, 1467.f), sf::Vector2f(468.f, 277.f),
        sf::Vector2f(50.f, 35.f), sf::Vector2f(790.f, 733.f), sf::Vector2f(498.f, 194.f),
        sf::Vector2f(633.f, 1317.f), sf::Vector2f(829.f, 716.f), sf::Vector2f(224.f, 887.f),
        sf::Vector2f(278.f, 48.f), sf::Vector2f(637.f, 594.f), sf::Vector2f(930.f, 1217.f),
        sf::Vector2f(296.f, 741.f), sf::Vector2f(424.f, 409.f), sf::Vector2f(955.f, 949.f),
        sf::Vector2f(785.f, 1789.f), sf::Vector2f(150.f, 1562.f), sf::Vector2f(780.f, 389.f),
        sf::Vector2f(312.f, 79.f),

        sf::Vector2f(614.f, 1102.f), sf::Vector2f(570.f, 615.f),
        sf::Vector2f(241.f, 1618.f), sf::Vector2f(281.f, 1844.f), sf::Vector2f(169.f, 740.f),
        sf::Vector2f(961.f, 759.f), sf::Vector2f(104.f, 356.f), sf::Vector2f(884.f, 1063.f),
        sf::Vector2f(621.f, 1682.f), sf::Vector2f(14.f, 808.f), sf::Vector2f(74.f, 490.f),
        sf::Vector2f(834.f, 1315.f), sf::Vector2f(133.f, 1120.f), sf::Vector2f(786.f, 1262.f),
        sf::Vector2f(595.f, 1325.f), sf::Vector2f(589.f, 752.f), sf::Vector2f(546.f, 1639.f),
        sf::Vector2f(155.f, 1381.f), sf::Vector2f(308.f, 1880.f), sf::Vector2f(611.f, 1157.f),
        sf::Vector2f(695.f, 1816.f), sf::Vector2f(604.f, 453.f), sf::Vector2f(145.f, 349.f),
        sf::Vector2f(610.f, 1592.f), sf::Vector2f(428.f, 957.f), sf::Vector2f(980.f, 223.f),
        sf::Vector2f(517.f, 953.f), sf::Vector2f(670.f, 106.f), sf::Vector2f(764.f, 937.f),
        sf::Vector2f(140.f, 450.f), sf::Vector2f(699.f, 1623.f), sf::Vector2f(344.f, 863.f),
        sf::Vector2f(853.f, 1607.f), sf::Vector2f(741.f, 1648.f), sf::Vector2f(823.f, 1654.f),
        sf::Vector2f(399.f, 1099.f), sf::Vector2f(674.f, 255.f), sf::Vector2f(458.f, 456.f),
        sf::Vector2f(258.f, 566.f), sf::Vector2f(584.f, 239.f), sf::Vector2f(125.f, 1618.f),
        sf::Vector2f(129.f, 534.f), sf::Vector2f(447.f, 878.f), sf::Vector2f(365.f, 1714.f),
        sf::Vector2f(561.f, 945.f), sf::Vector2f(622.f, 47.f), sf::Vector2f(688.f, 1165.f),
        sf::Vector2f(491.f, 284.f), sf::Vector2f(779.f, 927.f), sf::Vector2f(568.f, 1031.f),
        sf::Vector2f(359.f, 86.f), sf::Vector2f(387.f, 185.f), sf::Vector2f(418.f, 258.f),
        sf::Vector2f(980.f, 1737.f), sf::Vector2f(163.f, 553.f), sf::Vector2f(981.f, 1619.f),
        sf::Vector2f(205.f, 1143.f), sf::Vector2f(29.f, 739.f), sf::Vector2f(531.f, 1168.f),
        sf::Vector2f(783.f, 1248.f), sf::Vector2f(911.f, 609.f), sf::Vector2f(392.f, 1166.f),
        sf::Vector2f(726.f, 798.f), sf::Vector2f(239.f, 1273.f), sf::Vector2f(9.f, 117.f),
        sf::Vector2f(948.f, 26.f), sf::Vector2f(501.f, 1134.f), sf::Vector2f(865.f, 1706.f),
        sf::Vector2f(296.f, 912.f), sf::Vector2f(273.f, 1004.f), sf::Vector2f(706.f, 858.f),
        sf::Vector2f(0.f, 306.f), sf::Vector2f(568.f, 889.f), sf::Vector2f(229.f, 457.f),
        sf::Vector2f(409.f, 195.f), sf::Vector2f(458.f, 25.f), sf::Vector2f(412.f, 1934.f),
        sf::Vector2f(990.f, 322.f), sf::Vector2f(660.f, 251.f), sf::Vector2f(76.f, 737.f),
        sf::Vector2f(536.f, 1396.f), sf::Vector2f(123.f, 997.f), sf::Vector2f(946.f, 1637.f),
        sf::Vector2f(168.f, 929.f), sf::Vector2f(722.f, 190.f), sf::Vector2f(299.f, 1041.f),
        sf::Vector2f(614.f, 1436.f), sf::Vector2f(737.f, 1117.f), sf::Vector2f(137.f, 944.f),
        sf::Vector2f(955.f, 475.f), sf::Vector2f(415.f, 1449.f), sf::Vector2f(300.f, 1003.f),
        sf::Vector2f(144.f, 1172.f), sf::Vector2f(639.f, 1074.f), sf::Vector2f(551.f, 1300.f),
        sf::Vector2f(254.f, 835.f), sf::Vector2f(959.f, 107.f), sf::Vector2f(820.f, 1296.f),
        sf::Vector2f(322.f, 13.f), sf::Vector2f(908.f, 495.f), sf::Vector2f(725.f, 86.f),
        sf::Vector2f(386.f, 700.f), sf::Vector2f(293.f, 629.f), sf::Vector2f(916.f, 1293.f),
        sf::Vector2f(534.f, 281.f),

        sf::Vector2f(1273.f, 959.f), sf::Vector2f(2245.f, 1902.f),
        sf::Vector2f(1660.f, 263.f), sf::Vector2f(1627.f, 1892.f), sf::Vector2f(2503.f, 1085.f),
        sf::Vector2f(2288.f, 850.f), sf::Vector2f(2167.f, 948.f), sf::Vector2f(1459.f, 1048.f),
        sf::Vector2f(1955.f, 28.f), sf::Vector2f(1809.f, 1217.f), sf::Vector2f(1649.f, 1227.f),
        sf::Vector2f(2331.f, 1637.f), sf::Vector2f(2245.f, 785.f), sf::Vector2f(1362.f, 688.f),
        sf::Vector2f(1384.f, 1062.f), sf::Vector2f(1057.f, 936.f), sf::Vector2f(1243.f, 340.f),
        sf::Vector2f(2532.f, 279.f), sf::Vector2f(1677.f, 375.f), sf::Vector2f(1101.f, 373.f),
        sf::Vector2f(2111.f, 738.f), sf::Vector2f(1703.f, 739.f), sf::Vector2f(2018.f, 449.f),
        sf::Vector2f(2448.f, 1275.f), sf::Vector2f(2098.f, 1810.f), sf::Vector2f(2341.f, 851.f),
        sf::Vector2f(1891.f, 608.f), sf::Vector2f(2074.f, 878.f), sf::Vector2f(2368.f, 990.f),
        sf::Vector2f(2118.f, 1428.f), sf::Vector2f(1175.f, 304.f), sf::Vector2f(1996.f, 1119.f),
        sf::Vector2f(1550.f, 284.f), sf::Vector2f(1373.f, 315.f), sf::Vector2f(2169.f, 1297.f),
        sf::Vector2f(1562.f, 237.f), sf::Vector2f(2262.f, 1334.f), sf::Vector2f(2047.f, 1142.f),
        sf::Vector2f(1415.f, 1149.f), sf::Vector2f(1583.f, 209.f), sf::Vector2f(2174.f, 586.f),
        sf::Vector2f(1806.f, 1499.f), sf::Vector2f(2489.f, 445.f), sf::Vector2f(2333.f, 1505.f),
        sf::Vector2f(1030.f, 582.f), sf::Vector2f(2016.f, 541.f), sf::Vector2f(1862.f, 72.f),
        sf::Vector2f(1120.f, 53.f), sf::Vector2f(1722.f, 600.f), sf::Vector2f(1671.f, 1271.f),
        sf::Vector2f(1875.f, 514.f), sf::Vector2f(1361.f, 1036.f), sf::Vector2f(2296.f, 1031.f),
        sf::Vector2f(2471.f, 121.f), sf::Vector2f(1676.f, 1456.f), sf::Vector2f(1193.f, 758.f),
        sf::Vector2f(1491.f, 1311.f), sf::Vector2f(2134.f, 1609.f), sf::Vector2f(1410.f, 398.f),
        sf::Vector2f(1051.f, 52.f), sf::Vector2f(1329.f, 1892.f), sf::Vector2f(2558.f, 1190.f),
        sf::Vector2f(1664.f, 332.f), sf::Vector2f(2459.f, 1855.f), sf::Vector2f(1551.f, 1428.f),
        sf::Vector2f(1058.f, 914.f), sf::Vector2f(1558.f, 1046.f), sf::Vector2f(2483.f, 801.f),
        sf::Vector2f(1171.f, 621.f), sf::Vector2f(1069.f, 163.f), sf::Vector2f(1049.f, 1124.f),
        sf::Vector2f(2153.f, 1855.f), sf::Vector2f(1719.f, 145.f), sf::Vector2f(1169.f, 333.f),
        sf::Vector2f(1269.f, 676.f), sf::Vector2f(1521.f, 876.f), sf::Vector2f(2259.f, 1173.f),
        sf::Vector2f(2020.f, 1857.f), sf::Vector2f(2555.f, 780.f), sf::Vector2f(2497.f, 21.f),
        sf::Vector2f(2084.f, 903.f), sf::Vector2f(1311.f, 474.f), sf::Vector2f(2527.f, 1177.f),
        sf::Vector2f(1738.f, 764.f), sf::Vector2f(1227.f, 63.f), sf::Vector2f(1246.f, 1045.f),
        sf::Vector2f(1376.f, 466.f), sf::Vector2f(1914.f, 1574.f), sf::Vector2f(2466.f, 1903.f),
        sf::Vector2f(1280.f, 1567.f), sf::Vector2f(2393.f, 1686.f), sf::Vector2f(1057.f, 1694.f),
        sf::Vector2f(1237.f, 1016.f), sf::Vector2f(1108.f, 613.f), sf::Vector2f(1556.f, 817.f),
        sf::Vector2f(2065.f, 1132.f), sf::Vector2f(1671.f, 1907.f), sf::Vector2f(1639.f, 1847.f),
        sf::Vector2f(1377.f, 1805.f), sf::Vector2f(1270.f, 1901.f), sf::Vector2f(2026.f, 1485.f),
        sf::Vector2f(1176.f, 840.f), sf::Vector2f(1712.f, 177.f), sf::Vector2f(1564.f, 1892.f),
        sf::Vector2f(2294.f, 1363.f)};

    for (unsigned int i = 0; i < npcPos.size(); i++)
    {
        Npc* npc = generateCityNpc(npcPos[i], colorController, (i % 4) + 1);
        entities.push_back(npc);
        interactibles.push_back(npc);
        colorControlled.push_back(npc);
    }

    Door* mazeDoor = generateDoor(sf::Vector2f(852.f, 1174.f), 14, sf::Vector2f(1280.f, 960.f), true);
    collidables.push_back(mazeDoor);
    interactibles.push_back(mazeDoor);

    floors.push_back(generateSpikeTile(sf::Vector2f(320.f, 192.f), 4, sf::Vector2f(700.f, 704.f), true, player));

    floors.push_back(generateClockFrame(sf::Vector2f(1216.f, 960.f)));
    floors.push_back(generateCityClock(sf::Vector2f(1216.f, 960.f)));

    colorControlled.insert(colorControlled.end(), backgrounds.begin(), backgrounds.end());

    colorController->add(colorControlled);
    backgrounds.push_back(colorController);

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, std::vector<GameObject*>(), std::vector<GameObject*>() };

    return new Room(player, graph, nullptr, objects, sf::Vector2f(_roomWidth, _roomHeight),
        sf::Vector2<bool>(true, true));
}
