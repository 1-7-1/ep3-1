/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "EffectSwitch.h"
#include "Animation.h"


EffectSwitch::EffectSwitch(GameObject* target, sf::Vector2f position, sf::Vector2f origin,
    Animation* animation, sf::Sound* sound, Room* room)
    : SeekingSpecialEffect(target, position, origin, animation, sound, room)
{

}

EffectSwitch::~EffectSwitch()
{

}

void EffectSwitch::behave()
{
    _currentAnimation->rotate(3.f);

    SeekingSpecialEffect::behave();
}
