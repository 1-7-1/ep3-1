/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "MenuGenerator.h"
#include "PauseMenu.h"
#include "MainMenu.h"
#include "EffectMenu.h"
#include "MenuCursor.h"
#include "Animation.h"
#include "TileCalculator.h"
#include "AnimationFrames.h"
#include "Decoration.h"

MenuGenerator::MenuGenerator()
{
    
}

MenuGenerator::~MenuGenerator()
{
    
}

void MenuGenerator::load()
{
    _db.loadTexture("graphics/menu/6.png", sf::Color(0, 255, 255, 255));
    _db.loadTexture("graphics/menu/1.png", sf::Color(0, 255, 255, 255));
    _db.loadTexture("graphics/menu/7.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/menu/2.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/menu/3.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/menu/4.png", sf::Color(0, 255, 255, 255));
    _db.loadTexture("graphics/menu/5.png", sf::Color(0, 0, 0, 255));

    _db.loadSound("sounds/sfx/1.wav");
    _db.loadSound("sounds/sfx/2.wav");

    _isLoaded = true;
}

std::vector<std::vector<std::vector<sf::Vector2u>>> MenuGenerator::generateMainCursorOffsetVectors() const
{
    std::vector<std::vector<std::vector<sf::Vector2u>>> offsets;

    std::vector<sf::Vector2u> animVec;
    animVec.push_back(sf::Vector2u(0, 0));
    animVec.push_back(sf::Vector2u(1, 0));
    animVec.push_back(sf::Vector2u(2, 0));

    offsets.push_back(std::vector<std::vector<sf::Vector2u>>({ animVec }));
    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ offsets });
}

std::vector<std::vector<unsigned int>> MenuGenerator::generateMainCursorTimeVectors() const
{
    std::vector<unsigned int> animTimes = { 5, 5, 5 };
    
    return std::vector<std::vector<unsigned int>>({ animTimes });
}

std::vector<std::vector<std::vector<sf::Vector2u>>> MenuGenerator::generateMainMenuOffsetVectors() const
{
    std::vector<std::vector<std::vector<sf::Vector2u>>> offsets;

    std::vector<sf::Vector2u> animVec;
    animVec.push_back(sf::Vector2u(0, 0));
    animVec.push_back(sf::Vector2u(1, 0));
    animVec.push_back(sf::Vector2u(2, 0));
    animVec.push_back(sf::Vector2u(3, 0));
    animVec.push_back(sf::Vector2u(4, 0));
    animVec.push_back(sf::Vector2u(5, 0));
    animVec.push_back(sf::Vector2u(6, 0));
    animVec.push_back(sf::Vector2u(7, 0));

    offsets.push_back(std::vector<std::vector<sf::Vector2u>>({ animVec }));
    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ offsets });
}

std::vector<std::vector<unsigned int>> MenuGenerator::generateMainMenuTimeVectors() const
{
    std::vector<unsigned int> animTimes = { 60, 6, 6, 6, 30, 12, 12, 12 };
    
    return std::vector<std::vector<unsigned int>>({ animTimes });
}

std::vector<std::vector<std::vector<sf::Vector2u>>> MenuGenerator::generatePauseCursorOffsetVectors() const
{
    std::vector<std::vector<std::vector<sf::Vector2u>>> offsets;

    std::vector<sf::Vector2u> animVec;
    animVec.push_back(sf::Vector2u(0, 0));
    animVec.push_back(sf::Vector2u(0, 1));
    animVec.push_back(sf::Vector2u(0, 2));
    animVec.push_back(sf::Vector2u(0, 1));

    offsets.push_back(std::vector<std::vector<sf::Vector2u>>({ animVec }));
    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ offsets });
}

std::vector<std::vector<unsigned int>> MenuGenerator::generatePauseCursorTimeVectors() const
{
    std::vector<unsigned int> animTimes = { 5, 5, 5, 5 };
    
    return std::vector<std::vector<unsigned int>>({ animTimes });
}

std::vector<std::vector<std::vector<sf::Vector2u>>> MenuGenerator::generatePauseMenuOffsetVectors() const
{
    std::vector<std::vector<std::vector<sf::Vector2u>>> offsets;

    std::vector<sf::Vector2u> animVec;
    animVec.push_back(sf::Vector2u(0, 0));
    animVec.push_back(sf::Vector2u(1, 0));
    animVec.push_back(sf::Vector2u(2, 0));
    animVec.push_back(sf::Vector2u(1, 0));
    animVec.push_back(sf::Vector2u(0, 0));
    animVec.push_back(sf::Vector2u(1, 0));
    animVec.push_back(sf::Vector2u(2, 0));
    animVec.push_back(sf::Vector2u(1, 0));
    animVec.push_back(sf::Vector2u(0, 0));
    animVec.push_back(sf::Vector2u(1, 0));
    animVec.push_back(sf::Vector2u(2, 0));
    animVec.push_back(sf::Vector2u(1, 0));

    offsets.push_back(std::vector<std::vector<sf::Vector2u>>({ animVec }));
    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ offsets });
}

std::vector<std::vector<unsigned int>> MenuGenerator::generatePauseMenuTimeVectors() const
{
    std::vector<unsigned int> animTimes = { 3600, 5, 60, 5, 5, 5, 10, 5, 5, 5, 120, 5 };
    
    return std::vector<std::vector<unsigned int>>({ animTimes });
}

std::vector<std::vector<std::vector<sf::Vector2u>>> MenuGenerator::generateEffectCursorOffsetVectors() const
{
    std::vector<std::vector<std::vector<sf::Vector2u>>> offsets;

    std::vector<sf::Vector2u> animVec;
    animVec.push_back(sf::Vector2u(0, 0));
    animVec.push_back(sf::Vector2u(0, 1));
    animVec.push_back(sf::Vector2u(0, 2));
    animVec.push_back(sf::Vector2u(0, 1));

    offsets.push_back(std::vector<std::vector<sf::Vector2u>>({ animVec }));
    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ offsets });
}

std::vector<std::vector<unsigned int>> MenuGenerator::generateEffectCursorTimeVectors() const
{
    std::vector<unsigned int> animTimes = { 5, 5, 5, 5 };
    
    return std::vector<std::vector<unsigned int>>({ animTimes });
}

std::vector<std::vector<std::vector<sf::Vector2u>>> MenuGenerator::generateEffectMenuOffsetVectors() const
{
    std::vector<std::vector<std::vector<sf::Vector2u>>> offsets;

    std::vector<sf::Vector2u> animVec;
    animVec.push_back(sf::Vector2u(0, 0));

    offsets.push_back(std::vector<std::vector<sf::Vector2u>>({ animVec }));
    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ offsets });
}

std::vector<std::vector<unsigned int>> MenuGenerator::generateEffectMenuTimeVectors() const
{
    std::vector<unsigned int> animTimes = { 0 };
    
    return std::vector<std::vector<unsigned int>>({ animTimes });
}

std::vector<Decoration*> MenuGenerator::generateEffectLabels() const
{
    TileCalculator labelCalc = TileCalculator(sf::Vector2u(0, 0), sf::Vector2u(80, 32));

    std::vector<Decoration*> labelVec;
    for (unsigned int i = 0; i < 4; i++)
    {
        Animation* anim = new Animation(labelCalc(std::vector<sf::Vector2u>( { sf::Vector2u(i, 0) } ),
            std::vector<unsigned int>( { 0 } )), _db.getTexture(effectLabel));
        anim->setPosition(sf::Vector2f(i * 80.f, 208.f));
        labelVec.push_back(new Decoration(sf::Vector2f(i * 80.f, 208.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    return labelVec;
}

MainMenu* MenuGenerator::generateMainMenu(RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<std::vector<Animation*>> cursorAnimationVector = generateAnimationVector(_db.getTexture(mainMenuCursor),
        generateMainCursorOffsetVectors(), generateMainCursorTimeVectors(), sf::Vector2u(66, 18));

    std::vector<std::vector<Animation*>> menuAnimationVector = generateAnimationVector(_db.getTexture(mainMenu),
        generateMainMenuOffsetVectors(), generateMainMenuTimeVectors(), sf::Vector2u(320, 240));

    MenuCursor* cursor = new MenuCursor(cursorAnimationVector[0]);
    cursor->setOrigin(sf::Vector2f(1.f, 1.f));
    menuAnimationVector[0][0]->setLooping(false);

    MainMenu* newMenu = new MainMenu(menuAnimationVector[0], cursor, graph);
    newMenu->setActive(true);

    return newMenu;
}

PauseMenu* MenuGenerator::generatePauseMenu(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();

    std::vector<std::vector<Animation*>> effectCursorAnimationVector = generateAnimationVector(_db.getTexture(effectCursor),
        generateEffectCursorOffsetVectors(), generateEffectCursorTimeVectors(), sf::Vector2u(80, 32));

    std::vector<std::vector<Animation*>> pauseCursorAnimationVector = generateAnimationVector(_db.getTexture(pauseCursor),
        generatePauseCursorOffsetVectors(), generatePauseCursorTimeVectors(), sf::Vector2u(160, 32));

    std::vector<std::vector<Animation*>> menuAnimationVector = generateAnimationVector(_db.getTexture(pauseMenu),
        generatePauseMenuOffsetVectors(), generatePauseMenuTimeVectors(), sf::Vector2u(320, 240));

    std::vector<std::vector<Animation*>> effectMenuAnimationVector = generateAnimationVector(_db.getTexture(effectMenu),
        generateEffectMenuOffsetVectors(), generateEffectMenuTimeVectors(), sf::Vector2u(320, 240));

    std::vector<sf::Sound*> sounds;
    sounds.push_back(_db.getSound(accept));
    sounds.push_back(_db.getSound(cancel));

    MenuCursor* effectCursor = new MenuCursor(effectCursorAnimationVector[0]);
    MenuCursor* pauseCursor = new MenuCursor(pauseCursorAnimationVector[0]);

    EffectMenu* effect = new EffectMenu(player, effectMenuAnimationVector[0], effectCursor, generateEffectLabels(),
        nullptr, sounds);

    PauseMenu* pause = new PauseMenu(menuAnimationVector[0], pauseCursor, graph, std::vector<Menu*>({effect, nullptr}),
        sounds);
    pause->setActive(true);

    return pause;
}
