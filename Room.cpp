/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Room.h"
#include "SpecialEffect.h"
#include "RoomGraph.h"
#include "Player.h"
#include "InteractibleObject.h"
#include "FullScreenEvent.h"
#include "Animation.h"
#include <math.h>

Room::Room(Player* player, RoomGraph* graph, FullScreenEvent* fse, RoomObjects objects,
    sf::Vector2f size, sf::Vector2<bool> looping)
    : _size(size), _looping(looping), _player(player), _graph(graph), _fullScreenEvent(fse), _objects(objects),
    _specialEffectGenerator(), _currentState(entering), _transitionTimer(63),
    _grid(this, 64), _view(_player, this), _fadeLength(63)
{
    _player->endScript(transitionWait);
    _player->endScript(sleep);
    
    _player->setScale(1.f, 1.f);
    _player->setSpeedLength(1.f);

    if (_fullScreenEvent != nullptr)
        _fullScreenEvent->setRoom(this);
        
    setRooms(_objects.walls);
    setRooms(_objects.entities);
    setRooms(_objects.floorTiles);
    setRooms(_objects.backgroundObjects);
    setRooms(_objects.foregroundObjects);
    setRooms(_objects.specialEffects);
}

Room::~Room()
{
    destroy();

    if (_fullScreenEvent != nullptr)
        delete _fullScreenEvent;

    for (Entity* obj: _objects.entities)
    {
        if (obj != _player)
        delete obj;
    }

    deleteObjects(_objects.walls);
    deleteObjects(_objects.floorTiles);
    deleteObjects(_objects.backgroundObjects);
    deleteObjects(_objects.foregroundObjects);
    deleteObjects(_objects.specialEffects);
}

sf::Vector2f Room::getSize() const
{
    return _size;
}

std::vector<InteractibleObject*> Room::getInteractibles() const
{
    return _objects.interactibles;
}

std::vector<GameObject*> Room::getGraphics()
{
    std::vector<GameObject*> graphics = _view.getBackgrounds();
    std::vector<GameObject*> floors = _view.getFloorTiles();
    std::vector<GameObject*> objects = _view.getGraphics();
    std::vector<GameObject*> foregrounds = _view.getForegrounds();
    graphics.insert(graphics.end(), floors.begin(), floors.end());
    graphics.insert(graphics.end(), objects.begin(), objects.end());
    graphics.insert(graphics.end(), foregrounds.begin(), foregrounds.end());
    return graphics;
}

sf::Vector2f Room::getViewPosition() const
{
    return _view.getPosition();
}

bool Room::getFseActive() const
{
    return _fseActive;
}

void Room::setFseActive(bool active)
{
    if (_fullScreenEvent != nullptr)
    {
        _fseActive = active;
        if (!_fseActive)
            _player->startScript(shortWait);
        else
        {
            _fullScreenEvent->setPosition(_view.getPosition());
            _fullScreenEvent->activate();
        }
    }
}

bool Room::isInView(GameObject* obj) const
{
    return _view.isInView(obj);
}

unsigned int Room::getTransitionTime() const
{
    return _transitionTimer;
}

sf::Vector2<bool> Room::getLooping() const
{
    return _looping;
}

RoomState Room::getState() const
{
    return _currentState;
}

template <class T>
void Room::setRooms(std::vector<T*> vec)
{
    for (T* obj: vec)
        obj->setRoom(this);
}

template <class T>
void Room::deleteObjects(std::vector<T*> vec)
{
    for (T* obj: vec)
        delete obj;
}

template <class T1, class T2>
int Room::findObject(std::vector<T1*> vec, T2* object)
{
    for (int i = 0; i < int(vec.size()); i++)
    {
        if (vec[i] == object)
            return i;
    }
    return -1;
}

template <class T>
void Room::vectorTick(std::vector<T*> vec)
{
    for (T* obj: vec)
    {
        recenter(obj);
        obj->behave();
        _view.addEntity(obj);
    }
}

template <class T>
void Room::backgroundTick(std::vector<T*> vec)
{
    for (T* obj: vec)
    {
        recenter(obj);
        obj->behave();
        _view.addBackgroundObject(obj);
    }
}

template <class T>
void Room::foregroundTick(std::vector<T*> vec)
{
    for (T* obj: vec)
    {
        recenter(obj);
        obj->behave();
        _view.addForegroundObject(obj);
    }
}

template <class T>
void Room::floorTick(std::vector<T*> vec)
{
    for (T* obj: vec)
    {
        recenter(obj);
        obj->behave();
        _view.addFloorTile(obj);
    }
}

void Room::recenter(GameObject* object)
{
    if (_looping.x)
    {
        if (object->getPosition().x >= _size.x)
            object->setPosition(sf::Vector2f(object->getPosition().x - _size.x, object->getPosition().y));
        else if (object->getPosition().x < 0)
            object->setPosition(sf::Vector2f(object->getPosition().x + _size.x, object->getPosition().y));
    }
    if (_looping.y)
    {
        if (object->getPosition().y >= _size.y)
            object->setPosition(sf::Vector2f(object->getPosition().x, object->getPosition().y - _size.y));
        else if (object->getPosition().y < 0)
            object->setPosition(sf::Vector2f(object->getPosition().x, object->getPosition().y + _size.y));
    }
}

void Room::createSpecialEffect(sf::Vector2f position, sf::Vector2f origin, EffectType type, Layer layer,
    GameObject* target)
{
    SpecialEffect* eff;
    if (target)
        eff = _specialEffectGenerator.generateSeekingSpecialEffect(target, position, origin, type, this);
    else
        eff = _specialEffectGenerator.generateSpecialEffect(position, origin, type, this);
    if (layer == specialLayer)
        _objects.specialEffects.push_back(eff);
    else if (layer == floorLayer)
        _objects.floorTiles.push_back(eff);
}

void Room::setToDestroy(GameObject* obj)
{
    _toBeDestroyed.push_back(obj);
}

void Room::changeRoom(unsigned int destination, sf::Vector2f transitionVec, bool absolute, unsigned int fadeLength)
{
    if (_currentState != exiting)
    {
        _destination = destination;
        _transitionVector = transitionVec;
        _transitionAbsolute = absolute;
        _currentState = exiting;
        if (_fadeLength > fadeLength)
            _transitionTimer = 0;
        _fadeLength = fadeLength;
        _graph->prepareRoomChange(destination);
    }
}

void Room::destroy()
{
    for (unsigned int i = 0; i < _toBeDestroyed.size(); i++)
    {
        int interactibleId = findObject(_objects.interactibles, _toBeDestroyed[i]);
        int wallId = findObject(_objects.walls, _toBeDestroyed[i]);
        int entityId = findObject(_objects.entities, _toBeDestroyed[i]);
        int floorId = findObject(_objects.floorTiles, _toBeDestroyed[i]);
        int backgroundId = findObject(_objects.backgroundObjects, _toBeDestroyed[i]);
        int foregroundId = findObject(_objects.foregroundObjects, _toBeDestroyed[i]);
        int specialEffId = findObject(_objects.specialEffects, _toBeDestroyed[i]);

        if (wallId != -1 || entityId != -1)
            _grid.removeObject(_toBeDestroyed[i]);

        delete _toBeDestroyed[i];
        if (interactibleId != -1)
            _objects.interactibles.erase(_objects.interactibles.begin() + interactibleId);
        if (wallId != -1)
            _objects.walls.erase(_objects.walls.begin() + wallId);
        if (entityId != -1)
            _objects.entities.erase(_objects.entities.begin() + entityId);
        if (floorId != -1)
            _objects.floorTiles.erase(_objects.floorTiles.begin() + floorId);
        if (backgroundId != -1)
            _objects.backgroundObjects.erase(_objects.backgroundObjects.begin() + backgroundId);
        if (foregroundId != -1)
            _objects.foregroundObjects.erase(_objects.foregroundObjects.begin() + foregroundId);
        if (specialEffId != -1)
            _objects.specialEffects.erase(_objects.specialEffects.begin() + specialEffId);
    }
    _toBeDestroyed.clear();
}

void Room::tick()
{
    if (_fseActive)
    {
        _view.clearGraphics();
        _fullScreenEvent->behave();
        vectorTick(_fullScreenEvent->getGraphics());
        _view.tick();
        _view.sort();
    }
    else
    {
        if (_currentState != exiting)
        {
            destroy();
            _view.clearGraphics();
            vectorTick(_objects.walls);
            vectorTick(_objects.entities);
            floorTick(_objects.floorTiles);
            backgroundTick(_objects.backgroundObjects);
            foregroundTick(_objects.foregroundObjects);
            vectorTick(_objects.specialEffects);
            
            _view.tick();
            _view.sort();
        }
        else
        {
            _transitionTimer++;
            if (_transitionTimer == _fadeLength)
            {
                if (!_transitionAbsolute)
                {
                    sf::Vector2f pos = _player->getPosition();
                    pos += _transitionVector;
                    _player->setPosition(pos);
                }
                else
                    _player->setPosition(_transitionVector);

                _graph->changeRoom(_destination);
            }
        }
        
        if (_currentState == entering)
        {
            _transitionTimer--;
            if (_transitionTimer == 0)
                _currentState = active;
        }
    }
}

bool Room::checkCollision(sf::FloatRect hitbox, CollidableObject* other)
{
    sf::Vector2f otherPos = other->getPosition();
    sf::FloatRect otherHitbox = other->getHitbox();

    sf::FloatRect adjOtherHitbox = sf::FloatRect(otherPos.x + otherHitbox.left,
        otherPos.y + otherHitbox.top, otherHitbox.width, otherHitbox.height);

    if (_looping.x)
    {
        if (hitbox.left + hitbox.width - (adjOtherHitbox.left) >= _size.x)
            hitbox.left -= _size.x;
        else if (hitbox.left - (adjOtherHitbox.left + adjOtherHitbox.width) <= -_size.x)
            hitbox.left += _size.x;
    }
    if (_looping.y)
    {
        if (hitbox.top + hitbox.height - (adjOtherHitbox.top) >= _size.y)
            hitbox.top -= _size.y;
        else if (hitbox.top - (adjOtherHitbox.top + adjOtherHitbox.height) <= -_size.y)
            hitbox.top += _size.y;
    }

    return hitbox.intersects(adjOtherHitbox);
}

void Room::updateGrid(CollidableObject* obj)
{
    _grid.updateGrid(obj);
}
