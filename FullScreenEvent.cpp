/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "FullScreenEvent.h"
#include "Animation.h"
#include "Decoration.h"
#include "Room.h"
#include "Player.h"

FullScreenEvent::FullScreenEvent(std::vector<Decoration*> elems, Animation* anim)
    : GameObject(sf::Vector2f(), sf::Vector2f()), _elements(elems)
{
    _currentAnimation = anim;
    _currentAnimation->setOrigin(_origin);
}

FullScreenEvent::~FullScreenEvent()
{
    for (Decoration* dec: _elements)
        delete dec;

    delete _currentAnimation;
}

void FullScreenEvent::behave()
{
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::X))
    {
        _room->setFseActive(false);
    }

    for (Decoration* dec: _elements)
    {
        sf::Vector2f decPos = dec->getPosition();
        sf::Vector2f decSize = dec->getSize();
        if (dec->getCornerPosition().x + dec->getSize().x < getCornerPosition().x)
            dec->setPosition(sf::Vector2f(decPos.x + decSize.x + getSize().x, decPos.y));
        else if (dec->getCornerPosition().x > getCornerPosition().x + getSize().x)
            dec->setPosition(sf::Vector2f(decPos.x - decSize.x - getSize().x, decPos.y));

        if (dec->getCornerPosition().y + dec->getSize().y < getCornerPosition().y)
            dec->setPosition(sf::Vector2f(decPos.x, decPos.y + decSize.y + getSize().y));
        else if (dec->getCornerPosition().y > getCornerPosition().y + getSize().y)
            dec->setPosition(sf::Vector2f(decPos.x, decPos.y - decSize.y - getSize().y));

        dec->behave();
    }

    changeAnimation();
}

std::vector<GameObject*> FullScreenEvent::getGraphics()
{
    std::vector<GameObject*> objs;
    objs.push_back(this);
    for (Decoration* dec: _elements)
        objs.push_back(dec);

    return objs;
}
