/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "EndingCutscene.h"
#include "Animation.h"
#include "Room.h"
#include "SFML/Audio.hpp"

EndingCutscene::EndingCutscene(sf::Vector2f position, sf::Vector2f origin,
    std::vector<Animation*> anims, std::vector<sf::Sound*> sounds)
    : GameObject(position, origin), _animations(anims), _timer(0), _state(invisible), _sounds(sounds)
{
    _currentAnimation = _animations[0];
    _currentAnimation->setTransparency(0);
    
    for (Animation* anim: _animations)
        anim->setOrigin(_origin);
}

EndingCutscene::~EndingCutscene()
{
    for (Animation* anim: _animations)
        delete anim;
}

void EndingCutscene::behave()
{
    switch (_state)
    {
        case invisible:
            if (_timer > 180)
            {
                _state = playing;
                _timer = 0;
                _currentAnimation->setTransparency(255);
            }
            else
                _timer++;
            break;

        case playing:
            if (_currentAnimation->isFinished())
            {
                if (_currentAnimation == _animations[0])
                {
                    _currentAnimation = _animations[1];
                    _currentAnimation->setLooping(false);
                }
                else
                {
                    _state = ending;
                    _timer = 0;
                    if (_sounds[1])
                        _sounds[1]->play();
                }
            }
            else
                changeAnimation();
            
            if (_timer == 60)
            {
                if (_sounds[0])
                    _sounds[0]->play();
            }
            _timer++;
            break;

        case ending:
            if (_timer > 300)
                _room->changeRoom(2, sf::Vector2f(), false, 63);
            else
                _timer++;
            break;
        
        default:
            break;
    }
}