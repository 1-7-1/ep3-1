/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Ending2Generator.h"
#include "TileCalculator.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "EndingCutscene.h"
#include "Player.h"
#include "FinalRoom.h"

Ending2Generator::Ending2Generator()
{

}

Ending2Generator::~Ending2Generator()
{

}

void Ending2Generator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/end/2.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/end/3.png", sf::Color(255, 0, 255, 255));

    _db.loadSound("sounds/sfx/5.wav");
    _db.loadSound("sounds/sfx/6.wav");

    _isLoaded = true;
}

EndingCutscene* Ending2Generator::generateScene() const
{
    TileCalculator bgCalc = TileCalculator(sf::Vector2u(0, 0), sf::Vector2u(320, 240));

    std::vector<Animation*> anims;
    std::vector<sf::Sound*> sounds;

    anims.push_back(new Animation(bgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0), sf::Vector2u(1, 0),
        sf::Vector2u(0, 1)}),
        std::vector<unsigned int>({60, 6, 6})), _db.getTexture(bg1)));

    anims.push_back(new Animation(bgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0), sf::Vector2u(1, 0),
        sf::Vector2u(0, 1), sf::Vector2u(1, 1)}),
        std::vector<unsigned int>({12, 12, 120, 1})), _db.getTexture(bg2)));

    sounds.push_back(_db.getSound(open));
    sounds.push_back(_db.getSound(close));

    return new EndingCutscene(sf::Vector2f(), sf::Vector2f(), anims, sounds);
}

Room* Ending2Generator::generateEnd2(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;
    std::vector<GameObject*> foregrounds;

    backgrounds.push_back(generateScene());

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, foregrounds, std::vector<GameObject*>() };

    return new FinalRoom(player, graph, objects);
}
