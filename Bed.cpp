/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Bed.h"
#include "Animation.h"
#include "Player.h"
#include "Room.h"
#include "MathConstants.h"

Bed::Bed(sf::Vector2f position, sf::Vector2f origin, std::vector<Animation*> animations,
    sf::FloatRect hitbox, unsigned int destination, sf::Vector2f transitionVec, bool absolute)
    : CollidableObject(position, origin, hitbox), InteractibleObject(position, origin, hitbox), _state(free),
    _animations(animations), _destination(destination), _transitionVector(transitionVec), _transitionAbsolute(absolute)
{
    for (Animation* anim : _animations)
        anim->setOrigin(_origin);

    _currentAnimation = _animations[_state];
}

Bed::~Bed()
{
    for (Animation* anim : _animations)
        delete anim;
}

void Bed::interact(Player* player)
{
    float playerDir = player->getDirection() * 180.f / math::PI;
    if (player->getState() == idle && playerDir > 20.f && playerDir < 160.f && _state == free)
    {
        _state = occupied;
        player->startScript(sleep);
        player->setPosition(_position + sf::Vector2f(20, 15));
        _currentAnimation = _animations[_state];
        _currentAnimation->setFrameIndex(0);
        _currentAnimation->setLooping(false);
        _currentAnimation->setElapsedTicks(1);
    }
}

void Bed::behave()
{
    if (_state == occupied && _currentAnimation->isFinished())
        _room->changeRoom(_destination, _transitionVector, _transitionAbsolute, 63);
    changeAnimation();
}
