/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "SkyGenerator.h"
#include "SkyRoom.h"
#include "TileCalculator.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "Decoration.h"
#include "Wall.h"
#include "Door.h"
#include "AutoTransition.h"
#include "Player.h"

SkyGenerator::SkyGenerator()
{

}

SkyGenerator::~SkyGenerator()
{

}

void SkyGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/tilesets/17.png", sf::Color(63, 139, 255, 255));
    _db.loadTexture("graphics/bg/5.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/object/5.png", sf::Color(63, 139, 255, 255));

    _db.loadSound("sounds/sfx/5.wav");

    _isLoaded = true;
}

std::vector<Decoration*> SkyGenerator::generateBackground() const
{
    TileCalculator bgCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(322, 240));
    
    std::vector<Decoration*> backs;

    for (int i = 0; i < 4; i++)
    {
        Animation* anim = new Animation(bgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})),
            _db.getTexture(bg));
        backs.push_back(new Decoration(sf::Vector2f(-160.f + i * 320.f, 0.f), sf::Vector2f(1.f, 0.f),
            sf::Vector2f(-0.1f, 0.f), anim));
    }

    return backs;
}

Decoration* SkyGenerator::generateFloor() const
{
    TileCalculator bgCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(640, 240));

    Animation* anim = new Animation(bgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(floor));

    return new Decoration(sf::Vector2f(320.f, 0.f), sf::Vector2f(), sf::Vector2f(), anim);
}

Wall* SkyGenerator::generateWall(sf::Vector2f position, sf::FloatRect hitbox) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(invisible));

    return new Wall(position, sf::Vector2f(), anim, hitbox);
}

std::vector<Wall*> SkyGenerator::generateWalls(sf::Vector2f position) const
{
    std::vector<Wall*> walls;

    //bottom/top
    walls.push_back(generateWall(position + sf::Vector2f(240.f, 208.f),
        sf::FloatRect(0.f, 0.f, 160.f, 16.f)));
    walls.push_back(generateWall(position + sf::Vector2f(192.f, 96.f),
        sf::FloatRect(0.f, 0.f, 256.f, 16.f)));

    //left side
    for (int i = 0; i < 8; i ++)
        walls.push_back(generateWall(position + sf::Vector2f(176.f + i, 112.f + i * 2.f),
        sf::FloatRect(0.f, 0.f, 16.f, 2.f)));
    for (int i = 0; i < 16; i ++)
        walls.push_back(generateWall(position + sf::Vector2f(192.f + i, 144.f + i * 2.f),
        sf::FloatRect(0.f, 0.f, 16.f, 2.f)));
    for (int i = 0; i < 8; i ++)
        walls.push_back(generateWall(position + sf::Vector2f(216.f + i, 192.f + i * 2.f),
        sf::FloatRect(0.f, 0.f, 16.f, 2.f)));

    //left girders
    for (int i = 0; i < 8; i ++)
        walls.push_back(generateWall(position + sf::Vector2f(64.f + i, 128.f + i * 2.f),
        sf::FloatRect(0.f, 0.f, 16.f, 2.f)));
    walls.push_back(generateWall(position + sf::Vector2f(80.f, 144.f),
        sf::FloatRect(0.f, 0.f, 128.f, 2.f)));
    walls.push_back(generateWall(position + sf::Vector2f(80.f, 126.f),
        sf::FloatRect(0.f, 0.f, 119.f, 2.f)));

    for (int i = 0; i < 8; i ++)
        walls.push_back(generateWall(position + sf::Vector2f(88.f + i, 176.f + i * 2.f),
        sf::FloatRect(0.f, 0.f, 16.f, 2.f)));
    walls.push_back(generateWall(position + sf::Vector2f(104.f, 192.f),
        sf::FloatRect(0.f, 0.f, 128.f, 2.f)));
    walls.push_back(generateWall(position + sf::Vector2f(104.f, 174.f),
        sf::FloatRect(0.f, 0.f, 119.f, 2.f)));

    //right side
    for (int i = 0; i < 8; i ++)
        walls.push_back(generateWall(position + sf::Vector2f(448.f - i, 112.f + i * 2.f),
        sf::FloatRect(0.f, 0.f, 16.f, 2.f)));
    for (int i = 0; i < 16; i ++)
        walls.push_back(generateWall(position + sf::Vector2f(432.f - i, 144.f + i * 2.f),
        sf::FloatRect(0.f, 0.f, 16.f, 2.f)));
    for (int i = 0; i < 8; i ++)
        walls.push_back(generateWall(position + sf::Vector2f(408.f - i, 192.f + i * 2.f),
        sf::FloatRect(0.f, 0.f, 16.f, 2.f)));

    //right girders
    for (int i = 0; i < 8; i ++)
        walls.push_back(generateWall(position + sf::Vector2f(560.f - i, 128.f + i * 2.f),
        sf::FloatRect(0.f, 0.f, 16.f, 2.f)));
    walls.push_back(generateWall(position + sf::Vector2f(432.f, 144.f),
        sf::FloatRect(0.f, 0.f, 128.f, 2.f)));
    walls.push_back(generateWall(position + sf::Vector2f(441.f, 126.f),
        sf::FloatRect(0.f, 0.f, 119.f, 2.f)));

    for (int i = 0; i < 8; i ++)
        walls.push_back(generateWall(position + sf::Vector2f(536.f - i, 176.f + i * 2.f),
        sf::FloatRect(0.f, 0.f, 16.f, 2.f)));
    walls.push_back(generateWall(position + sf::Vector2f(408.f, 192.f),
        sf::FloatRect(0.f, 0.f, 128.f, 2.f)));
    walls.push_back(generateWall(position + sf::Vector2f(417.f, 174.f),
        sf::FloatRect(0.f, 0.f, 119.f, 2.f)));

    return walls;
}

std::vector<Wall*> SkyGenerator::generateHouse(sf::Vector2f position) const
{
    std::vector<Wall*> walls;

    walls.push_back(generateWall(position + sf::Vector2f(-128.f, -16.f),
        sf::FloatRect(0.f, 0.f, 256.f, 15.f)));

    //sides
    for (int i = 0; i < 32; i ++)
        walls.push_back(generateWall(position + sf::Vector2f(-96.f + i, 0.f + i * 1.5f),
        sf::FloatRect(0.f, 0.f, 192.f - i * 2.f, 1.f)));

    return walls;
}

std::vector<std::vector<std::vector<sf::Vector2u>>> SkyGenerator::generateDoorOffsetVectors() const
{
    std::vector<std::vector<sf::Vector2u>> closedVec;
    closedVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 0) }));

    std::vector<std::vector<sf::Vector2u>> openVec;
    openVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 0), sf::Vector2u(2, 0), sf::Vector2u(3, 0),
        sf::Vector2u(4, 0), sf::Vector2u(5, 0), sf::Vector2u(6, 0), sf::Vector2u(7, 0),sf::Vector2u(8, 0) }));

    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ closedVec, openVec });
}

std::vector<std::vector<unsigned int>> SkyGenerator::generateDoorTimeVectors() const
{
    std::vector<unsigned int> closedTimes = { 0 };
    std::vector<unsigned int> openTimes = { 2, 2, 2, 2, 2, 2, 2, 2 };
    
    return std::vector<std::vector<unsigned int>>({ closedTimes, openTimes });
}

Door* SkyGenerator::generateDoor(sf::Vector2f position) const
{
    std::vector<std::vector<Animation*>> animVec = generateAnimationVector(_db.getTexture(door),
        generateDoorOffsetVectors(), generateDoorTimeVectors(), sf::Vector2u(16, 32));

    return new Door(position, sf::Vector2f(8.f, 30.f), sf::FloatRect(-8.f, -2.f, 16.f, 3.f), animVec, _db.getSound(doorSfx),
        18, sf::Vector2f(168.f, 152.f), true, 1);
}

std::vector<AutoTransition*> SkyGenerator::generateTiles(sf::Vector2f position, Player* player) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    std::vector<AutoTransition*> tiles;

    {
        Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(invisible));

        tiles.push_back(new AutoTransition(position + sf::Vector2f(200.f, 112.f), sf::Vector2f(),
            sf::FloatRect(0.f, 0.f, 24.f, 16.f), anim, 19, sf::Vector2f(288.f, 176.f), true, player));
    }

    {
        Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(invisible));

        tiles.push_back(new AutoTransition(position + sf::Vector2f(416.f, 112.f), sf::Vector2f(),
            sf::FloatRect(0.f, 0.f, 24.f, 16.f), anim, 19, sf::Vector2f(32.f, 176.f), true, player));
    }

    return tiles;
}

Room* SkyGenerator::generateSky(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;
    std::vector<GameObject*> foregrounds;

    std::vector<Decoration*> back = generateBackground();
    backgrounds.insert(backgrounds.end(), back.begin(), back.end());

    std::vector<Wall*> walls = generateWalls(sf::Vector2f(320.f, 0.f));
    collidables.insert(collidables.end(), walls.begin(), walls.end());
    
    std::vector<Wall*> house = generateHouse(sf::Vector2f(640.f, 128.f));
    collidables.insert(collidables.end(), house.begin(), house.end());
    
    std::vector<AutoTransition*> tiles = generateTiles(sf::Vector2f(320.f, 0.f), player);
    floors.insert(floors.end(), tiles.begin(), tiles.end());

    Door* newDoor = generateDoor(sf::Vector2f(648.f, 174.f));
    collidables.push_back(newDoor);
    interactibles.push_back(newDoor);

    floors.push_back(generateFloor());

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, foregrounds, std::vector<GameObject*>() };

    return new SkyRoom(player, graph, objects);
}
