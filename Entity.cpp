/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Entity.h"
#include "Room.h"

Entity::Entity(sf::Vector2f position, sf::Vector2f origin, float speedLength, sf::FloatRect hitbox):
               CollidableObject(position, origin, hitbox), _speedLength(speedLength)
{
    _speed = sf::Vector2f(0.f, 0.f);
    _room = nullptr;
}

Entity::~Entity()
{

}

void Entity::setSpeed(sf::Vector2f speed)
{
    _speed = speed;
}

void Entity::setSpeedLength(float speed)
{
    _speedLength = speed;
}

sf::Vector2f Entity::getSpeed() const
{
    return _speed;
}

float Entity::getSpeedLength() const
{
    return _speedLength;
}

void Entity::move()
{
    if (_speed.x != 0 || _speed.y != 0)
        _room->updateGrid(this);
    _position += _speed;
}

bool Entity::checkCollision(sf::Vector2f speed, std::vector<CollidableObject*>* list) const
{
    sf::Vector2f selfPos = _position + speed;
    
    sf::FloatRect adjHitbox = sf::FloatRect(selfPos.x + _hitbox.left,
                                                selfPos.y + _hitbox.top,
                                                _hitbox.width, _hitbox.height);

    for(unsigned int i = 0; i < list->size(); i++)
    {
        if (this != (*list)[i])
        {
            if (_room->checkCollision(adjHitbox, (*list)[i]))
                return true;
        }
    }

    return false;
}
