/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Chaser.h"
#include "MathConstants.h"
#include "Animation.h"
#include "Room.h"
#include "Player.h"
#include <cmath>

Chaser::Chaser(sf::Vector2f position, float chaseSpeedLength, sf::Vector2f origin,
    std::vector<std::vector<Animation*>> animations, std::vector<std::vector<Animation*>> chasingAnimations,
    sf::FloatRect hitbox, Entity* target, bool chasing, unsigned int waitTime, unsigned int destination,
    sf::Vector2f transitionVec, bool absolute)
    : CollidableObject(position, origin, hitbox), InteractibleObject(position, origin, hitbox),
    Entity(position, origin, 1.f, hitbox),
    _animations(animations), _chasingAnimations(chasingAnimations), _chasing(chasing), _waitTimer(waitTime),
    _chaseSpeedLength(chaseSpeedLength), _target(target), _relocating(sf::Vector2<bool>(false, false)),
    _destination(destination), _transitionVector(transitionVec), _transitionAbsolute(absolute)
{
    _currentDirection = right;
    _currentState = idle;

    for (std::vector<Animation*> animList : _animations)
    {
        for (Animation* anim : animList)
            anim->setOrigin(_origin);
    }
    for (std::vector<Animation*> animList : _chasingAnimations)
    {
        for (Animation* anim : animList)
            anim->setOrigin(_origin);
    }

    if (_chasing)
    {
        _currentAnimation = _chasingAnimations[_currentState][_currentDirection];
        _speedLength = _chaseSpeedLength;
    }
    else
    {
        _currentAnimation = _animations[_currentState][_currentDirection];
        _waitTimer = 0;
    }
}

Chaser::~Chaser()
{
    for (std::vector<Animation*> animList : _animations)
        for (Animation* anim : animList)
            delete anim;
    for (std::vector<Animation*> animList : _chasingAnimations)
        for (Animation* anim : animList)
            delete anim;
}

void Chaser::interact(Player* player)
{
    if (!_chasing && _currentState != dead && player->getEffect() == machete && player->getState() == action)
    {
        _room->createSpecialEffect(_position, sf::Vector2f(0.f, 16.f), blood);
        _currentState = dead;
        _currentAnimation = _animations[_currentState][_currentDirection];
        _currentAnimation->setFrameIndex(0);
        _currentAnimation->setLooping(false);
    }
}

void Chaser::behave()
{
    if (!_chasing)
    {
        switch (_currentState)
        {
        case idle:
            if (rand() % 60 == 0)
            {
                int degrees = rand() % 360;
                if (degrees < 45 || degrees >= 315)
                    _currentDirection = right;
                else if (degrees < 135 && degrees >= 45)
                    _currentDirection = up;
                else if (degrees < 225 && degrees >= 135)
                    _currentDirection = left;
                else
                    _currentDirection = down;
                _direction = float(degrees) * math::PI / 180.f;
                _speed.x = _speedLength * cos(_direction);
                _speed.y = -_speedLength * sin(_direction);
                _currentState = moving;
            }
            break;

        case moving:
            if (rand() % 60 == 0)
            {
                _speed = sf::Vector2f(0.f, 0.f);
                _currentState = idle;
            }
            if (_currentState == moving)
            {
                _speed.x = _speedLength * cos(_direction);
                _speed.y = -_speedLength * sin(_direction);
            }
            break;

        case dead:
            if (_currentAnimation->isFinished())
            {
                _chasing = true;
                _speedLength = _chaseSpeedLength;
                _currentState = idle;
                _currentAnimation = _chasingAnimations[_currentState][_currentDirection];
            }
            break;

        default:
            break;
        }
    }
    else if (_waitTimer == 0)
    {
        sf::Vector2f roomSize = _room->getSize();
        sf::Vector2f tempPos = _position;

        sf::Vector2f targetPos = _target->getPosition();
        sf::FloatRect targetHitbox = _target->getHitbox();
        sf::FloatRect adjTargetHitbox = sf::FloatRect(targetHitbox.left + targetPos.x, targetHitbox.top + targetPos.y,
            targetHitbox.width, targetHitbox.height);

        sf::FloatRect adjHitbox = sf::FloatRect(_hitbox.left + _position.x, _hitbox.top + _position.y,
            _hitbox.width, _hitbox.height);

        if (!_relocating.x)
        {
            float xOffset = 0.f;
            if (adjTargetHitbox.left + adjTargetHitbox.width < adjHitbox.left + adjHitbox.width &&
                adjTargetHitbox.left + adjTargetHitbox.width > adjHitbox.left)
                xOffset = -adjHitbox.width / 2.f;
            else if (adjTargetHitbox.left > adjHitbox.left &&
                adjTargetHitbox.left < adjHitbox.left + adjHitbox.width)
                xOffset = adjHitbox.width / 2.f;

            if (xOffset)
            {
                _relocating.x = true;
                _chasingOffset.x = xOffset;
            }
        }
        else if ((_chasingOffset.x > 0.f && _position.x >= _target->getPosition().x + _chasingOffset.x) ||
            (_chasingOffset.x < 0.f && _position.x <= _target->getPosition().x + _chasingOffset.x))
            _relocating.x = false;

        if (!_relocating.y)
        {
            float yOffset = 0.f;
            if (adjTargetHitbox.top + adjTargetHitbox.height < adjHitbox.top + adjHitbox.height &&
                adjTargetHitbox.top + adjTargetHitbox.height > adjHitbox.top)
                yOffset = -adjHitbox.height / 2.f;
            else if (adjTargetHitbox.top > adjHitbox.top &&
                adjTargetHitbox.top < adjHitbox.top + adjHitbox.height)
                yOffset = adjHitbox.height / 2.f;

            if (yOffset)
            {
                _relocating.y = true;
                _chasingOffset.y = yOffset;
            }
        }
        else if ((_chasingOffset.y > 0.f && _position.y >= _target->getPosition().y + _chasingOffset.y) ||
            (_chasingOffset.y < 0.f && _position.y <= _target->getPosition().y + _chasingOffset.y))
            _relocating.y = false;

        targetPos += _chasingOffset;

        if (_position.x - (targetPos.x - roomSize.x) < targetPos.x - _position.x)
            tempPos.x += roomSize.x;
        else if (targetPos.x - (_position.x - roomSize.x) < _position.x - targetPos.x)
            tempPos.x -= roomSize.x;

        if (_position.y - (targetPos.y - roomSize.y) < targetPos.y - _position.y)
            tempPos.y += roomSize.y;
        else if (targetPos.y - (_position.y - roomSize.y) < _position.y - targetPos.y)
            tempPos.y -= roomSize.y;

        sf::Vector2f positionDiff = targetPos - tempPos;
        float diffLength = sqrt(positionDiff.x * positionDiff.x + positionDiff.y * positionDiff.y);
        _speed = _speedLength * positionDiff / diffLength;

        adjHitbox.left += _speed.x;
        adjHitbox.top += _speed.y;

        if (_room->checkCollision(adjHitbox, _target))
            _room->changeRoom(_destination, _transitionVector, _transitionAbsolute, 63);
    }
    else
        _waitTimer--;

    if (_speed.x != 0.f || _speed.y != 0.f)
    {
        bool freeNext = true;
        bool freeX = (_speed.x != 0.f);
        bool freeY = (_speed.y != 0.f);

        for (unsigned int j = 0; j < _lists.size(); j++)
        {
            if (freeNext)
                freeNext = !checkCollision(_speed, _lists[j]);
            if (freeX)
                freeX = !checkCollision(sf::Vector2f(copysign(_speedLength, _speed.x), 0.f), _lists[j]);
            if (freeY)
                freeY = !checkCollision(sf::Vector2f(0.f, copysign(_speedLength, _speed.y)), _lists[j]);
        }

        if (!freeNext)
        {
            if (freeX/* && fabs(_speed.x) > sin(math::PI / 12.0) * _speedLength*/)
                _speed = sf::Vector2f(copysign(_speedLength, _speed.x), 0.f);
            else if (freeY/* && fabs(_speed.y) > sin(math::PI / 12.0) * _speedLength*/)
                _speed = sf::Vector2f(0.f, copysign(_speedLength, _speed.y));
            else
                _speed = sf::Vector2f(0.f, 0.f);
        }
    }

    move();
    changeAnimation();
}

void Chaser::move()
{
    Entity::move();
}

void Chaser::changeAnimation()
{
    if (_currentState != dead)
    {
        if (_speed == sf::Vector2f(0.f, 0.f))
            _currentState = idle;
        else
        {
            _currentState = moving;
            float direction = atan2(-_speed.y, _speed.x);
            float degrees = direction * 180.f / math::PI;

            if (degrees < 45.f && degrees >= -45.f)
                _currentDirection = right;
            else if (degrees < 135.f && degrees >= 45.f)
                _currentDirection = up;
            else if (degrees < -135.f || degrees >= 135.f)
                _currentDirection = left;
            else
                _currentDirection = down;
        }
    }

    if (_chasing)
        _currentAnimation = _chasingAnimations[_currentState][_currentDirection];
    else
        _currentAnimation = _animations[_currentState][_currentDirection];

    GameObject::changeAnimation();
}
