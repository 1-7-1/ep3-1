/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "SpecialEffect.h"
#include "Animation.h"
#include "Room.h"

SpecialEffect::SpecialEffect(sf::Vector2f position, sf::Vector2f origin, Animation* animation, sf::Sound* sound, Room* room)
: GameObject(position, origin), _sound(sound)
{
    _currentAnimation = animation;
    _currentAnimation->setOrigin(_origin);
    _currentAnimation->setFrameIndex(0);
    _currentAnimation->setLooping(false);

    _room = room;

    if (_sound && _room->isInView(this))
    {
        _sound->stop();
        _sound->play();
    }
}

SpecialEffect::~SpecialEffect()
{
    delete _currentAnimation;
}

void SpecialEffect::behave()
{
    if (_currentAnimation->isFinished())
        _room->setToDestroy(this);

    changeAnimation();
}
