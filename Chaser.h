/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CHASER_H
#define CHASER_H

#include "Entity.h"
#include "InteractibleObject.h"

class Chaser: public InteractibleObject, public Entity
{
    private:
        enum ChaserState { idle, moving, dead };

        EntityDirection _currentDirection;
        ChaserState _currentState;
        std::vector<std::vector<Animation*>> _animations;
        std::vector<std::vector<Animation*>> _chasingAnimations;

        bool _chasing;
        unsigned int _waitTimer;

        float _chaseSpeedLength;
        float _direction;

        Entity* _target;

        sf::Vector2<bool> _relocating;
        sf::Vector2f _chasingOffset;

        unsigned int _destination;
        sf::Vector2f _transitionVector;
        bool _transitionAbsolute;

    public:
        Chaser();
        Chaser(sf::Vector2f position, float chaseSpeedLength, sf::Vector2f origin,
            std::vector<std::vector<Animation*>> animations, std::vector<std::vector<Animation*>> chasingAnimations,
            sf::FloatRect hitbox, Entity* target, bool chasing, unsigned int waitTime, unsigned int destination,
            sf::Vector2f transitionVec, bool absolute);
        virtual ~Chaser();

        virtual void interact(Player* player);

        virtual void behave();
        virtual void move();
        virtual void changeAnimation();
};

#endif
