/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "MazeBackground.h"
#include "Animation.h"
#include "Room.h"

MazeBackground::MazeBackground(sf::Vector2f position, sf::Vector2f origin, Animation* animation, Player* player)
    : Decoration(position, origin, sf::Vector2f(), animation), _player(player)
{

}

MazeBackground::~MazeBackground()
{
    
}

void MazeBackground::behave()
{
    _position = _room->getViewPosition() + sf::Vector2f(160.f, 120.f);
    if (_currentAnimation->getScale().x > 1.f)
        _currentAnimation->setScale(0.000990352f, 0.000990352f);
    else
        _currentAnimation->setScale(_currentAnimation->getScale() / -0.95f);

    _currentAnimation->rotate(3.f);
    
    changeAnimation();
}
