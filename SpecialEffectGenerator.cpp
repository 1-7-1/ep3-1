/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "SpecialEffectGenerator.h"
#include "EffectSwitch.h"
#include "TileCalculator.h"
#include "AnimationFrames.h"
#include "Animation.h"

SpecialEffectGenerator::SpecialEffectGenerator(): GameObjectGenerator()
{
    
}

SpecialEffectGenerator::~SpecialEffectGenerator()
{
    
}

void SpecialEffectGenerator::load()
{
    _db.loadTexture("graphics/effect/1.png", sf::Color(0, 255, 255, 255));
    _db.loadTexture("graphics/effect/3.png", sf::Color(0, 255, 255, 255));
    _db.loadTexture("graphics/effect/5.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/effect/2.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/tilesets/22.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/char/1.png", sf::Color(0, 255, 255, 255));

    _db.loadSound("sounds/sfx/8.wav");
    _db.loadSound("sounds/sfx/4.wav");
    _db.loadSound("sounds/sfx/7.wav");
    _isLoaded = true;
}

std::vector<std::vector<std::vector<sf::Vector2u>>> SpecialEffectGenerator::generateBloodOffsetVectors() const
{
    std::vector<std::vector<std::vector<sf::Vector2u>>> offsets;

    std::vector<sf::Vector2u> bloodVec;
    for (unsigned int j = 0; j < 2; j++)
        for (unsigned int i = 0; i < 4; i++)
            bloodVec.push_back(sf::Vector2u(i, j));

    offsets.push_back(std::vector<std::vector<sf::Vector2u>>({ bloodVec }));
    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ offsets });
}

std::vector<std::vector<unsigned int>> SpecialEffectGenerator::generateBloodTimeVectors() const
{
    std::vector<unsigned int> bloodTimes = { 3, 3, 3, 3, 3, 3, 3, 3 };
    
    return std::vector<std::vector<unsigned int>>({ bloodTimes });
}

std::vector<std::vector<std::vector<sf::Vector2u>>> SpecialEffectGenerator::generateSwitchOffsetVectors() const
{
    std::vector<std::vector<std::vector<sf::Vector2u>>> offsets;

    std::vector<sf::Vector2u> swVec;
    swVec.push_back(sf::Vector2u(0, 0));
    swVec.push_back(sf::Vector2u(1, 0));
    swVec.push_back(sf::Vector2u(2, 0));
    swVec.push_back(sf::Vector2u(3, 0));
    swVec.push_back(sf::Vector2u(0, 1));
    swVec.push_back(sf::Vector2u(1, 1));
    swVec.push_back(sf::Vector2u(2, 1));
    swVec.push_back(sf::Vector2u(3, 1));
    swVec.push_back(sf::Vector2u(0, 2));

    offsets.push_back(std::vector<std::vector<sf::Vector2u>>({ swVec }));
    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ offsets });
}

std::vector<std::vector<unsigned int>> SpecialEffectGenerator::generateSwitchTimeVectors() const
{
    std::vector<unsigned int> swTimes;
    for (unsigned int i = 0; i < 9; i++)
        swTimes.push_back(3);
    
    return std::vector<std::vector<unsigned int>>({ swTimes });
}

std::vector<std::vector<std::vector<sf::Vector2u>>> SpecialEffectGenerator::generateSavedOffsetVectors() const
{
    std::vector<std::vector<std::vector<sf::Vector2u>>> offsets;

    std::vector<sf::Vector2u> savedVec;
    savedVec.push_back(sf::Vector2u(0, 0));

    offsets.push_back(std::vector<std::vector<sf::Vector2u>>({ savedVec }));
    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ offsets });
}

std::vector<std::vector<unsigned int>> SpecialEffectGenerator::generateSavedTimeVectors() const
{
    std::vector<unsigned int> savedTimes = { 30 };
    
    return std::vector<std::vector<unsigned int>>({ savedTimes });
}

Animation* SpecialEffectGenerator::generateRippleAnimation() const
{
    TileCalculator floorCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(32, 32));

    std::vector<sf::Vector2u> offsets = { sf::Vector2u(5, 0), sf::Vector2u(6, 0), sf::Vector2u(7, 0),
        sf::Vector2u(8, 0), sf::Vector2u(9, 0), sf::Vector2u(5, 1), sf::Vector2u(6, 1), sf::Vector2u(7, 1) };

    return new Animation(floorCalc(offsets, std::vector<unsigned int>( { 5, 5, 5, 5, 5, 5, 5, 60 } )),
            _db.getTexture(ripple));
}

std::vector<std::vector<std::vector<sf::Vector2u>>> SpecialEffectGenerator::generateSwimOffsetVectors(EffectType type) const
{
    std::vector<std::vector<std::vector<sf::Vector2u>>> offsets;

    std::vector<sf::Vector2u> swimVec;
    swimVec.push_back(sf::Vector2u(type - 1, 2));
    swimVec.push_back(sf::Vector2u(type - 1, 3));
    swimVec.push_back(sf::Vector2u(type - 1, 4));
    swimVec.push_back(sf::Vector2u(type - 1, 5));

    offsets.push_back(std::vector<std::vector<sf::Vector2u>>({ swimVec }));
    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ offsets });
}

std::vector<std::vector<unsigned int>> SpecialEffectGenerator::generateSwimTimeVectors() const
{
    std::vector<unsigned int> swimTimes = { 12, 12, 12, 12 };
    
    return std::vector<std::vector<unsigned int>>({ swimTimes });
}

SpecialEffect* SpecialEffectGenerator::generateSpecialEffect(const sf::Vector2f position, const sf::Vector2f origin,
    const EffectType type, Room* room)
{
    if (!_isLoaded)
        load();
    SpecialEffect* eff;
    std::vector<std::vector<Animation*>> animationVector;
    switch (type)
    {
        case blood:
            animationVector = generateAnimationVector(_db.getTexture(blood),
                generateBloodOffsetVectors(), generateBloodTimeVectors(), sf::Vector2u(24, 24));
            eff = new SpecialEffect(position, sf::Vector2f(12.f, 12.f) + origin, animationVector[0][0],
                _db.getSound(bloodSfx), room);
            break;

        case effectSwitch:
            animationVector = generateAnimationVector(_db.getTexture(effectSwitch),
                generateSwitchOffsetVectors(), generateSwitchTimeVectors(), sf::Vector2u(48, 48));
            eff = new SpecialEffect(position, sf::Vector2f(24.f, 24.f) + origin, animationVector[0][0], nullptr, room);
            break;

        case gameSaved:
            animationVector = generateAnimationVector(_db.getTexture(gameSaved),
                generateSavedOffsetVectors(), generateSavedTimeVectors(), sf::Vector2u(32, 16));
            eff = new SpecialEffect(position, sf::Vector2f(16.f, 8.f) + origin, animationVector[0][0], nullptr, room);
            break;

        case effectGet:
            animationVector = generateAnimationVector(_db.getTexture(effectGet),
                generateSavedOffsetVectors(), generateSavedTimeVectors(), sf::Vector2u(32, 16));
            eff = new SpecialEffect(position, sf::Vector2f(16.f, 8.f) + origin, animationVector[0][0],
                _db.getSound(effectGetSfx), room);
            break;

        case ripple:
            eff = new SpecialEffect(position, sf::Vector2f(16.f, 16.f) + origin, generateRippleAnimation(),
                _db.getSound(rippleSfx), room);
            break;

        case swim1:
        case swim2:
        case swim3:
        case swim4:
            animationVector = generateAnimationVector(_db.getTexture(swim1),
                generateSwimOffsetVectors(type), generateSwimTimeVectors(), sf::Vector2u(24, 32));
            eff = new SpecialEffect(position, sf::Vector2f(12.f, 29.f) + origin, animationVector[0][0], nullptr, room);
            break;
        
        default:
            break;
    }

    return eff;
}

SpecialEffect* SpecialEffectGenerator::generateSeekingSpecialEffect(GameObject* target, const sf::Vector2f position,
    const sf::Vector2f origin, const EffectType type, Room* room)
{
    if (!_isLoaded)
        load();
    SeekingSpecialEffect* eff;
    std::vector<std::vector<Animation*>> animationVector;
    switch (type)
    {
        case blood:
            animationVector = generateAnimationVector(_db.getTexture(blood),
                generateBloodOffsetVectors(), generateBloodTimeVectors(), sf::Vector2u(24, 24));
            eff = new SeekingSpecialEffect(target, position, sf::Vector2f(12.f, 12.f) + origin, animationVector[0][0],
                nullptr, room);
            break;

        case effectSwitch:
            animationVector = generateAnimationVector(_db.getTexture(effectSwitch),
                generateSwitchOffsetVectors(), generateSwitchTimeVectors(), sf::Vector2u(48, 48));
            eff = new EffectSwitch(target, position, sf::Vector2f(24.f, 24.f) + origin, animationVector[0][0], nullptr, room);
            break;

        case gameSaved:
            animationVector = generateAnimationVector(_db.getTexture(gameSaved),
                generateSavedOffsetVectors(), generateSavedTimeVectors(), sf::Vector2u(32, 16));
            eff = new SeekingSpecialEffect(target, position, sf::Vector2f(16.f, 8.f) + origin, animationVector[0][0], nullptr, room);
            break;

        case effectGet:
            animationVector = generateAnimationVector(_db.getTexture(effectGet),
                generateSavedOffsetVectors(), generateSavedTimeVectors(), sf::Vector2u(32, 16));
            eff = new SeekingSpecialEffect(target, position, sf::Vector2f(16.f, 8.f) + origin, animationVector[0][0], nullptr, room);
            break;

        case swim1:
        case swim2:
        case swim3:
        case swim4:
            animationVector = generateAnimationVector(_db.getTexture(4),
                generateSwimOffsetVectors(type), generateSwimTimeVectors(), sf::Vector2u(24, 32));
            eff = new SeekingSpecialEffect(target, position, sf::Vector2f(12.f, 29.f) + origin, animationVector[0][0],
                nullptr, room);
            break;
        
        default:
            break;
    }

    return eff;
}
