/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Ending1Generator.h"
#include "EndingRoom.h"
#include "TileCalculator.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "Decoration.h"
#include "Player.h"
#include "EndingText.h"

Ending1Generator::Ending1Generator()
{

}

Ending1Generator::~Ending1Generator()
{

}

void Ending1Generator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/end/1.png", sf::Color(255, 0, 255, 255));
    _isLoaded = true;
}

Decoration* Ending1Generator::generateBackground() const
{
    TileCalculator bgCalc = TileCalculator(sf::Vector2u(0, 0), sf::Vector2u(320, 240));

    Animation* anim = new Animation(bgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0), sf::Vector2u(1, 0),
        sf::Vector2u(0, 1), sf::Vector2u(1, 0)}),
        std::vector<unsigned int>({12, 12, 12, 12})), _db.getTexture(bg));

    return new Decoration(sf::Vector2f(), sf::Vector2f(), sf::Vector2f(), anim);
}

EndingText* Ending1Generator::generateEndText() const
{
    TileCalculator bgCalc = TileCalculator(sf::Vector2u(0, 0), sf::Vector2u(320, 240));

    Animation* anim = new Animation(bgCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 1)}),
        std::vector<unsigned int>({0})), _db.getTexture(bg));

    return new EndingText(sf::Vector2f(132.f, 15.f), sf::Vector2f(), sf::Vector2f(), anim);
}

Room* Ending1Generator::generateEnd1(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;
    std::vector<GameObject*> foregrounds;

    backgrounds.push_back(generateBackground());
    backgrounds.push_back(generateEndText());

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, foregrounds, std::vector<GameObject*>() };

    return new EndingRoom(player, graph, objects);
}
