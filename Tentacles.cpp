/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Tentacles.h"
#include "Animation.h"
#include "Player.h"
#include "Room.h"

Tentacles::Tentacles(sf::Vector2f position, sf::Vector2f origin, std::vector<Animation*> animations, Player* player)
    : GameObject(position, origin), _player(player), _animations(animations)
{
    _currentAnimation = _animations[following];
    for (Animation* anim: _animations)
        anim->setOrigin(_origin);
}

Tentacles::~Tentacles()
{
    for (Animation* anim: _animations)
        delete anim;
}

void Tentacles::behave()
{
    _position = _player->getPosition() + sf::Vector2f(0.f, -0.01f);
    if ((_player->getState() != idle || _player->getScript() != none) && _currentAnimation->getFrameIndex() < 10)
    {
        if (_currentAnimation == _animations[sinkhorizontal] || _currentAnimation == _animations[sinkvertical])
        {
            _currentAnimation = _animations[breaking];
            _currentAnimation->setFrameIndex(0);
            _currentAnimation->setLooping(false);
        }
        if (_currentAnimation == _animations[breaking] && _currentAnimation->isFinished())
        {
            _currentAnimation = _animations[following];
        }
    }
    else
    {
        if (_currentAnimation->getFrameIndex() > 0 &&
            (_currentAnimation == _animations[sinkhorizontal] || _currentAnimation == _animations[sinkvertical]))
            _position.y += 0.02f;
        if (_currentAnimation == _animations[following] ||
            (_currentAnimation == _animations[breaking] && _currentAnimation->isFinished()))
        {
            if (_player->getAnimationDirection() ==  right || _player->getAnimationDirection() == left)
                _currentAnimation = _animations[sinkhorizontal];
            else
                _currentAnimation = _animations[sinkvertical];

            _currentAnimation->setFrameIndex(0);
            _currentAnimation->setLooping(false);
        }
    }

    if ((_currentAnimation == _animations[sinkhorizontal] && _currentAnimation->getFrameIndex() == 16) ||
        (_currentAnimation == _animations[sinkvertical] && _currentAnimation->getFrameIndex() == 13))
        _player->startScript(invisibleWait);
    else if (_currentAnimation->getFrameIndex() == 10)
        _player->startScript(transitionWait);

    if (_currentAnimation->isFinished() &&
        (_currentAnimation == _animations[sinkvertical] || _currentAnimation == _animations[sinkhorizontal]))
    {
        _room->changeRoom(24, sf::Vector2f(160.f, 80.f), true, 63);
    }

    changeAnimation();
}
