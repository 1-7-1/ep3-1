/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "WhiteThing.h"
#include "Animation.h"

WhiteThing::WhiteThing(sf::Vector2f position, sf::Vector2f origin, sf::Vector2f speed, Animation* animation)
    : Decoration(position, origin, speed, animation)
{

}

WhiteThing::~WhiteThing()
{

}

void WhiteThing::behave()
{
    Decoration::behave();

    if (_position.y <= 64.f  || _position.y >=1168.f)
    {
        _speed.y *= -1.f;
        if (_currentAnimation->getRotation() == 0.f || _currentAnimation->getRotation() == 180.f)
            _currentAnimation->scale(1.f, -1.f);
        else
            _currentAnimation->scale(-1.f, 1.f);
    }
}
