/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Grid.h"
#include "Entity.h"
#include "Room.h"
#include "GridNode.h"
#include <math.h>

Grid::Grid(Room* room, const int gridSize): _room(room), _gridSize(gridSize),
    _gridDimensions(_room->getSize() / float(gridSize))
{
    _grid.resize(_gridSize);
    for (unsigned int i = 0; i < _grid.size(); i++)
        _grid[i].resize(_gridSize);
    for (CollidableObject* obj: room->_objects.walls)
    {
        obj->clearList();
        updateGrid(obj);
    }
    for (Entity* ent: room->_objects.entities)
    {
        ent->clearList();
        updateGrid(ent);
    }
}

Grid::~Grid()
{
    for (std::vector<std::vector<CollidableObject*>> vectorVec: _grid)
    {
        for (std::vector<CollidableObject*> vec: vectorVec)
            vec.clear();
        vectorVec.clear();
    }
}

std::vector<std::vector<std::vector<CollidableObject*>>> Grid::getGrid() const
{
    return _grid;
}

void Grid::updateGrid(CollidableObject* obj)
{
    std::vector<sf::Vector2i> gridPos = obj->getGridPositions();
    std::vector<sf::Vector2i> newGridPos;

    sf::Vector2f pos = obj->getPosition();
    sf::FloatRect hitbox = obj->getHitbox();
    sf::FloatRect adjHitbox = sf::FloatRect(pos.x + hitbox.left, pos.y + hitbox.top, hitbox.width, hitbox.height);

    int left = floor(adjHitbox.left / _gridDimensions.x);
    int right = ceil((adjHitbox.left + adjHitbox.width) / _gridDimensions.x);
    while (left <= right)
    {
        int top = floor(adjHitbox.top / _gridDimensions.y);
        int bottom = ceil((adjHitbox.top + adjHitbox.height) / _gridDimensions.y);
        while (top <= bottom)
        {
            newGridPos.push_back(sf::Vector2i((left + _gridSize) % _gridSize, (top + _gridSize) % _gridSize));
            top++;
        }
        left++;
    }

    if (gridPos != newGridPos)
    {
        for (sf::Vector2i position: gridPos)
            for (unsigned int i = 0; i < _grid[position.x][position.y].size(); i++)
                if (_grid[position.x][position.y][i] == obj)
                    _grid[position.x][position.y].erase(_grid[position.x][position.y].begin() + i);

        obj->clearList();
        
        for (sf::Vector2i position: newGridPos)
        {
            _grid[position.x][position.y].push_back(obj);
            obj->addList(&_grid[position.x][position.y]);
        }

        obj->setGridPositions(newGridPos);
    }
}

void Grid::removeObject(GameObject* obj)
{
    for (unsigned int i = 0; i < _grid.size(); i++)
        for (unsigned int j = 0; j < _grid[i].size(); j++)
            for (unsigned int k = 0; k < _grid[i][j].size(); k++)
                if (_grid[i][j][k] == obj)
                    _grid[i][j].erase(_grid[i][j].begin() + k);
}
