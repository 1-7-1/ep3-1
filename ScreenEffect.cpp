/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ScreenEffect.h"
#include "Room.h"

ScreenEffect::ScreenEffect(sf::Vector2f position, sf::Vector2f origin, sf::Vector2f speed, Animation* animation)
    : Decoration(position, origin, speed, animation), _relativePosition(position)
{

}

ScreenEffect::~ScreenEffect()
{
    
}

void ScreenEffect::behave()
{
    _relativePosition += _speed;
    _position = _room->getViewPosition() + _relativePosition;
    sf::Vector2f cornerPos = _relativePosition - _origin;

    if (cornerPos.x + getSize().x <= 0.f)
        _relativePosition.x += getSize().x + 320.f;
    else if (cornerPos.x >= 320.f)
        _relativePosition.x -= getSize().x + 320.f;

    if (cornerPos.y + getSize().y <= 0.f)
        _relativePosition.y += getSize().y + 240.f;
    else if (cornerPos.y >= 240.f)
        _relativePosition.y -= getSize().y + 240.f;

    changeAnimation();
}
