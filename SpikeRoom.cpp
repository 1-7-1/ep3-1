/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "SpikeRoom.h"
#include "Player.h"
#include "Waterfall.h"

SpikeRoom::SpikeRoom(Player* player, RoomGraph* graph, RoomObjects objects, std::vector<Waterfall*> waterfalls)
    : Room(player, graph, nullptr, objects, sf::Vector2f(1280.f, 1280.f), sf::Vector2<bool>(true, true)),
    _waterfalls(waterfalls), _waterfallCounter(0)
{
    
}

SpikeRoom::~SpikeRoom()
{

}

int SpikeRoom::findWaterfall(std::vector<Waterfall*> vec, GameObject* object)
{
    for (int i = 0; i < int(vec.size()); i++)
    {
        if (vec[i] == object)
            return i;
    }
    return -1;
}

/*void SpikeRoom::destroy()
{
    for (unsigned int i = 0; i < _toBeDestroyed.size(); i++)
    {
        int interactibleId = findObject(_objects.interactibles, _toBeDestroyed[i]);
        int wallId = findObject(_objects.walls, _toBeDestroyed[i]);
        int entityId = findObject(_objects.entities, _toBeDestroyed[i]);
        int floorId = findObject(_objects.floorTiles, _toBeDestroyed[i]);
        int backgroundId = findObject(_objects.backgroundObjects, _toBeDestroyed[i]);
        int foregroundId = findObject(_objects.foregroundObjects, _toBeDestroyed[i]);
        int specialEffId = findObject(_objects.specialEffects, _toBeDestroyed[i]);
        int waterfallId = findWaterfall(_waterfalls, _toBeDestroyed[i]);

        if (wallId != -1 || entityId != -1)
            _grid.removeObject(_toBeDestroyed[i]);

        delete _toBeDestroyed[i];
        if (interactibleId != -1)
            _objects.interactibles.erase(_objects.interactibles.begin() + interactibleId);
        if (wallId != -1)
            _objects.walls.erase(_objects.walls.begin() + wallId);
        if (entityId != -1)
            _objects.entities.erase(_objects.entities.begin() + entityId);
        if (floorId != -1)
            _objects.floorTiles.erase(_objects.floorTiles.begin() + floorId);
        if (backgroundId != -1)
            _objects.backgroundObjects.erase(_objects.backgroundObjects.begin() + backgroundId);
        if (foregroundId != -1)
            _objects.foregroundObjects.erase(_objects.foregroundObjects.begin() + foregroundId);
        if (specialEffId != -1)
            _objects.specialEffects.erase(_objects.specialEffects.begin() + specialEffId);
        if (waterfallId != -1)
            _waterfalls.erase(_waterfalls.begin() + waterfallId);
    }
    _toBeDestroyed.clear();
}*/

void SpikeRoom::tick()
{
    if (_player->getPosition().y < 0.f)
        _waterfallCounter++;
    else if (_player->getPosition().y >= 1280.f)
        _waterfallCounter--;
    if (_waterfallCounter == 3)
    {
        _waterfallCounter = 0;
        for (Waterfall* wfall: _waterfalls)
            wfall->uncover();
    }
    if (_currentState != exiting)
        if (_player->getEffect() == liquid)
            _player->startScript(sink);
    Room::tick();
}
