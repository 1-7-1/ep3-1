/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef RIPPLE_H
#define RIPPLE_H

#include "GameObject.h"

class SpikeRipples: public GameObject
{
    private:
        sf::Vector2f _speed;
        unsigned int _maxTime = 10;
        unsigned int _timer = 10;
        unsigned int _startTimer = 4254;

    public:
        SpikeRipples(sf::Vector2f position, sf::Vector2f origin, sf::Vector2f speed, Animation* animation);
        ~SpikeRipples();

        virtual void behave();
};

#endif
