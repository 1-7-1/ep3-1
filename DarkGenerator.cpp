/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "DarkGenerator.h"
#include "Room.h"
#include "TileCalculator.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "Decoration.h"
#include "Player.h"
#include "WhiteThing.h"
#include "Waterfall.h"
#include "Tentacles.h"
#include "Post.h"
#include "MathConstants.h"
#include <math.h>

DarkGenerator::DarkGenerator()
{

}

DarkGenerator::~DarkGenerator()
{

}

void DarkGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/tilesets/3.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/char/8.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/tilesets/22.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/char/15.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/tilesets/21.png", sf::Color(255, 0, 255, 255));

    _isLoaded = true;
}

std::vector<Decoration*> DarkGenerator::generateFloor() const
{
    std::vector<Decoration*> floors;

    TileCalculator floorCalc1 = TileCalculator(sf::Vector2u(0, 0), sf::Vector2u(32, 32));
    TileCalculator floorCalc2 = TileCalculator(sf::Vector2u(0, 32), sf::Vector2u(32, 16));
    TileCalculator floorCalc3 = TileCalculator(sf::Vector2u(32, 0), sf::Vector2u(48, 48));
    TileCalculator floorCalc4 = TileCalculator(sf::Vector2u(80, 0), sf::Vector2u(32, 48));
    TileCalculator floorCalc5 = TileCalculator(sf::Vector2u(112, 0), sf::Vector2u(48, 48));
    TileCalculator floorCalc6 = TileCalculator(sf::Vector2u(160, 0), sf::Vector2u(64, 80));
    {
        Animation* anim = new Animation(floorCalc1(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(30.f, 200.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc2(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(100.f, 320.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc3(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(450.f, 200.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc4(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(700.f, 300.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc3(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(1000.f, 290.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc2(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(460.f, 560.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc1(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(900.f, 470.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc4(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(260.f, 720.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc5(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(490.f, 940.f), sf::Vector2f(), sf::Vector2f(), anim));
    }

    {
        Animation* anim = new Animation(floorCalc1(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(770.f, 810.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc3(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(920.f, 780.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc4(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(990.f, 820.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc5(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(1120.f, 1020.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc6(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(670.f, 970.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc3(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(790.f, 1000.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc2(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(840.f, 960.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc4(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(1030.f, 1070.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc1(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(870.f, 950.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc5(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(880.f, 1060.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc2(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(980.f, 980.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc6(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(1040.f, 990.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc3(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(1100.f, 1100.f), sf::Vector2f(), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(floorCalc2(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        floors.push_back(new Decoration(sf::Vector2f(890.f, 1060.f), sf::Vector2f(), sf::Vector2f(), anim));
    }

    return floors;
}

std::vector<WhiteThing*> DarkGenerator::generateWhiteThings() const
{
    TileCalculator npcCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(96, 96));

    std::vector<sf::Vector2u> offsets = {sf::Vector2u(0, 0), sf::Vector2u(2, 0), sf::Vector2u(4, 0),
        sf::Vector2u(0, 2), sf::Vector2u(2, 2), sf::Vector2u(4, 2),
        sf::Vector2u(0, 4), sf::Vector2u(2, 4)};

    std::vector<unsigned int> times = { 3, 3, 3, 3, 3, 3, 3, 3 };

    std::vector<sf::Vector2f> positions = {sf::Vector2f(160.f, 160.f), sf::Vector2f(300.f, 150.f),
        sf::Vector2f(640, 70.f), sf::Vector2f(750.f, 225.f), sf::Vector2f(970.f, 280.f), sf::Vector2f(810.f, 215.f),
        sf::Vector2f(120.f, 320.f), sf::Vector2f(350.f, 360.f), sf::Vector2f(480.f, 350.f), sf::Vector2f(600.f, 170.f),
        sf::Vector2f(390.f, 445.f), sf::Vector2f(350.f, 400.f), sf::Vector2f(725.f, 930.f), sf::Vector2f(900.f, 600.f),
        sf::Vector2f(120.f, 630.f), sf::Vector2f(280.f, 785.f), sf::Vector2f(720.f, 720.f), sf::Vector2f(950.f, 900.f),
        sf::Vector2f(975.f, 945.f), sf::Vector2f(230.f, 975.f), sf::Vector2f(340.f, 1100.f), sf::Vector2f(650.f, 1160.f),
        sf::Vector2f(980.f, 1105.f), sf::Vector2f(920.f, 1060.f), sf::Vector2f(880.f, 1025.f), sf::Vector2f(165.f, 140.f)};

    std::vector<float> angles = { 135.f, -45.f, -45.f, -135.f, 45.f, -45.f, 45.f, -45.f, -45.f, 135.f, 45.f, -45.f, -135.f,
        45.f, 45.f, -135.f, 45.f, 135.f, 135.f, -45.f, 135.f, -135.f, 45.f, -45.f, -45.f, 135.f, 45.f, -135.f  };
    
    float speed = 8.f;

    std::vector<WhiteThing*> npcs;

    for (unsigned int i = 0; i < positions.size(); i++)
    {
        Animation* anim = new Animation(npcCalc(offsets, times), _db.getTexture(npc));

        anim->setRotation(-(angles[i] - 45.f));

        npcs.push_back(new WhiteThing(positions[i], sf::Vector2f(48.f, 48.f),
            sf::Vector2f(speed * cos(angles[i] * math::PI / 180.f), -speed * sin(angles[i] * math::PI / 180.f)), anim));
    }

    return npcs;
}

std::vector<Waterfall*> DarkGenerator::generateWaterfalls(Player* player) const
{
    TileCalculator watCalc = TileCalculator(sf::Vector2u(0, 0), sf::Vector2u(32, 80));
    std::vector<Waterfall*> waterfalls;

    for (int i = 0; i < 1280; i += 32)
    {
        Animation* anim = new Animation(watCalc(std::vector<sf::Vector2u>({sf::Vector2u(11, 0), sf::Vector2u(12, 0), sf::Vector2u(13, 0),
            sf::Vector2u(14, 0), sf::Vector2u(15, 0), sf::Vector2u(16, 0)}),
            std::vector<unsigned int>({6, 6, 6, 6, 6, 6})), _db.getTexture(waterfall));

        waterfalls.push_back(new Waterfall(sf::Vector2f(i, 0.f), sf::Vector2f(16.f, 16.f),
            sf::FloatRect(-16.f, -16.f, 32.f, 32.f), anim, 4, player));
    }

    for (int i = 0; i < 1280; i += 32)
    {
        Animation* anim = new Animation(watCalc(std::vector<sf::Vector2u>({sf::Vector2u(11, 2), sf::Vector2u(12, 2), sf::Vector2u(13, 2),
            sf::Vector2u(14, 2), sf::Vector2u(15, 2), sf::Vector2u(16, 2)}),
            std::vector<unsigned int>({6, 6, 6, 6, 6, 6})), _db.getTexture(waterfall));

        waterfalls.push_back(new Waterfall(sf::Vector2f(i, 1216.f), sf::Vector2f(16.f, 64.f),
            sf::FloatRect(-16.f, -16.f, 32.f, 32.f), anim, 4, player));
    }

    for (Waterfall* wfall: waterfalls)
        wfall->activate();

    return waterfalls;
}

Tentacles* DarkGenerator::generateTentacles(Player* player) const
{
    TileCalculator tentCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(32, 48));

    std::vector<sf::Vector2u> followOff = { sf::Vector2u() };
    std::vector<unsigned int> followTimes = { 0 };

    std::vector<sf::Vector2u> vertOff = { sf::Vector2u(0, 0), sf::Vector2u(1, 0), sf::Vector2u(2, 0), sf::Vector2u(3, 0),
    sf::Vector2u(4, 0), sf::Vector2u(5, 0), sf::Vector2u(6, 0), sf::Vector2u(7, 0), sf::Vector2u(8, 0),
    sf::Vector2u(9, 0), sf::Vector2u(0, 1), sf::Vector2u(1, 1), sf::Vector2u(2, 1), sf::Vector2u(3, 1),
    sf::Vector2u(4, 1), sf::Vector2u(5, 1), sf::Vector2u(6, 1), sf::Vector2u(7, 1), sf::Vector2u(8, 1), sf::Vector2u(0, 0) };
    
    std::vector<unsigned int> vertTimes;
    for (unsigned int i = 0; i < vertOff.size(); i++)
        vertTimes.push_back(6);
    vertTimes[0] = 30;
    vertTimes[vertTimes.size() - 1] = 60;

    std::vector<sf::Vector2u> horiOff = { sf::Vector2u(0, 0), sf::Vector2u(0, 2), sf::Vector2u(1, 2), sf::Vector2u(2, 2),
    sf::Vector2u(3, 2), sf::Vector2u(4, 2), sf::Vector2u(5, 2), sf::Vector2u(6, 2), sf::Vector2u(7, 2),
    sf::Vector2u(8, 2), sf::Vector2u(9, 2), sf::Vector2u(0, 3), sf::Vector2u(1, 3), sf::Vector2u(2, 3),
    sf::Vector2u(3, 3), sf::Vector2u(4, 3), sf::Vector2u(5, 3), sf::Vector2u(6, 3), sf::Vector2u(7, 3),
    sf::Vector2u(8, 3), sf::Vector2u(9, 3), sf::Vector2u(0, 0) };

    std::vector<unsigned int> horiTimes;
    for (unsigned int i = 0; i < horiOff.size(); i++)
        horiTimes.push_back(6);
    horiTimes[0] = 30;
    horiTimes[horiTimes.size() - 1] = 60;

    std::vector<sf::Vector2u> breakOff = { sf::Vector2u(0, 4), sf::Vector2u(0, 0), sf::Vector2u(1, 4), sf::Vector2u(0, 0),
    sf::Vector2u(2, 4), sf::Vector2u(0, 0), sf::Vector2u(3, 4) };
    
    std::vector<unsigned int> breakTimes;
    for (unsigned int i = 0; i < breakOff.size(); i++)
        breakTimes.push_back(6);

    std::vector<Animation*> anims;

    anims.push_back(new Animation(tentCalc(followOff, followTimes), _db.getTexture(tentacles)));
    anims.push_back(new Animation(tentCalc(vertOff, vertTimes), _db.getTexture(tentacles)));
    anims.push_back(new Animation(tentCalc(horiOff, horiTimes), _db.getTexture(tentacles)));
    anims.push_back(new Animation(tentCalc(breakOff, breakTimes), _db.getTexture(tentacles)));

    return new Tentacles(sf::Vector2f(), sf::Vector2f(16.f, 30.f), anims, player);
}

Post* DarkGenerator::generatePost(sf::Vector2f position) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 48));

    std::vector<sf::Vector2u> offsets = {sf::Vector2u(0, 0), sf::Vector2u(0, 1), sf::Vector2u(0, 4), sf::Vector2u(0, 5),
        sf::Vector2u(0, 6), sf::Vector2u(0, 7)};

    std::vector<unsigned int> times = {12, 12, 12, 12, 12, 12};

    Animation* anim = new Animation(tileCalc(offsets, times), _db.getTexture(space));

    return new Post(position, sf::Vector2f(9.f, 39.f), sf::FloatRect(-4.f, -2.f, 9.f, 5.f), anim,
        25, sf::Vector2f(336.f, 264.f), true);
}

Room* DarkGenerator::generateDarkWorld(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;
    std::vector<GameObject*> specialEffects;

    std::vector<Decoration*> decs = generateFloor();
    backgrounds.insert(backgrounds.end(), decs.begin(), decs.end());

    std::vector<WhiteThing*> npcs = generateWhiteThings();
    floors.insert(floors.end(), npcs.begin(), npcs.end());

    std::vector<Waterfall*> water = generateWaterfalls(player);
    floors.insert(floors.end(), water.begin(), water.end());

    Tentacles* tent = generateTentacles(player);
    specialEffects.push_back(tent);

    Post* post = generatePost(sf::Vector2f(960.f, 960.f));
    interactibles.push_back(post);
    collidables.push_back(post);

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, std::vector<GameObject*>(), specialEffects };

    return new Room(player, graph, nullptr, objects, sf::Vector2f(_roomWidth, _roomHeight),
        sf::Vector2<bool>(true, true));
}