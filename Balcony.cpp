/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Balcony.h"
#include "Player.h"

Balcony::Balcony(Player* player, RoomGraph* graph,
            std::vector<CollidableObject*> walls, std::vector<GameObject*> autoTiles, GameObject* background)
    : Room(player, graph, nullptr,
    {std::vector<InteractibleObject*>(), walls, std::vector<Entity*>({player}), autoTiles, std::vector<GameObject*>({background}),
    std::vector<GameObject*>(), std::vector<GameObject*>()}, sf::Vector2f(320.f, 240.f), sf::Vector2<bool>(false, false))
{

}

Balcony::~Balcony()
{

}
