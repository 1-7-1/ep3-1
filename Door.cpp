/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Door.h"
#include "Animation.h"
#include "Room.h"
#include "Player.h"
#include "MathConstants.h"

Door::Door(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox, std::vector<std::vector<Animation*>> animations,
    sf::Sound* sound, unsigned int destination, sf::Vector2f transitionVec, bool absolute, unsigned int direction)
    : CollidableObject(position, origin, hitbox), InteractibleObject(position, origin, hitbox),
    _destination(destination), _transitionVector(transitionVec), _transitionAbsolute(absolute), _sound(sound), _direction(direction)
{
    _animations.push_back(animations[0][0]);
    _animations.push_back(animations[1][0]);
    _currentState = closed;
    for (Animation* anim : _animations)
            anim->setOrigin(_origin);

    _currentAnimation = _animations[_currentState];
}

Door::~Door()
{
    for (Animation* anim : _animations)
        delete anim;
}

void Door::open()
{
    if (_sound)
        _sound->play();
    _currentState = opened;
    _currentAnimation = _animations[_currentState];
    _currentAnimation->setFrameIndex(0);
    _currentAnimation->setLooping(false);
}

void Door::interact(Player* player)
{
    float playerDir = player->getDirection() * 180.f / math::PI;
    bool goodDirection;
    if (player->getState() == idle && _currentState == closed)
    {
        switch (_direction)
        {
            case 0:
                goodDirection = (playerDir > -70.f && playerDir < 70.f);
                break;

            case 1:
                goodDirection = (playerDir > 20.f && playerDir < 160.f);
                break;
            
            case 2:
                goodDirection = (playerDir > 110.f || playerDir < -110.f);
                break;

            default:
                goodDirection = (playerDir > -160.f && playerDir < -20.f);
                break;
        }
        
        if (goodDirection)
        {
            player->startScript(transitionWait);
            open();
        }
    }
}

void Door::behave()
{
    if (_currentState != waiting)
    {
        if (_currentState == opened && _currentAnimation->isFinished())
        {
            _room->changeRoom(_destination, _transitionVector, _transitionAbsolute, 63);
            _currentState = waiting;
        }
        changeAnimation();
    }
}
