/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CITY_GEN_H
#define CITY_GEN_H

#include "GameObjectGenerator.h"

class Room;
class Player;
class RoomGraph;
class AutoTransition;
class Decoration;
class ColorController;
class GameObject;
class CityClock;
class Wall;
class CityNpc1;
class CityNpc2;
class CityNpc3;
class CityNpc4;
class CityNpc5;
class Npc;
class Door;

class CityGenerator: public GameObjectGenerator
{
    private:
        enum Grahpics{ invisible, tileset, bg, npc1, npc2, npc3, npc4, npc5, faker, maze };
        virtual void load();

        const float _roomWidth = 2560.f;
        const float _roomHeight = 1920.f;

        AutoTransition* generateSpikeTile(sf::Vector2f position, unsigned int destination,
        sf::Vector2f transitionVec, bool absolute, Player* _player) const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateDoorOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateDoorTimeVectors() const;

        Door* generateDoor(sf::Vector2f position, unsigned int destination,
        sf::Vector2f transitionVec, bool absolute) const;

        ColorController* generateColorController(std::vector<GameObject *> objects) const;

        Decoration* generateClockFrame(sf::Vector2f position) const;
        CityClock* generateCityClock(sf::Vector2f position) const;

        std::vector<Wall*> generateBuilding1(sf::Vector2f position) const;
        std::vector<Wall*> generateBuilding2(sf::Vector2f position) const;
        std::vector<Wall*> generateBuilding3(sf::Vector2f position) const;
        std::vector<Wall*> generateBuilding(sf::Vector2f position, unsigned int type) const;

        std::vector<Wall*> generatePost(sf::Vector2f position) const;
        std::vector<Wall*> generateBarrier(sf::Vector2f position, unsigned int length) const;

        std::vector<Decoration*> generateFloor() const;

        std::vector<Decoration*> generateBackground() const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateFakerOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateFakerTimeVectors() const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateCityNpc1OffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateCityNpc1TimeVectors() const;

        CityNpc1* generateCityNpc1(sf::Vector2f position, ColorController* controller) const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateCityNpc2OffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateCityNpc2TimeVectors() const;

        CityNpc2* generateCityNpc2(sf::Vector2f position, ColorController* controller) const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateCityNpc3OffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateCityNpc3TimeVectors() const;

        CityNpc3* generateCityNpc3(sf::Vector2f position, ColorController* controller) const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateCityNpc4OffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateCityNpc4TimeVectors() const;

        CityNpc4* generateCityNpc4(sf::Vector2f position, ColorController* controller) const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateCityNpc5OffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateCityNpc5TimeVectors() const;

        CityNpc5* generateCityNpc5(sf::Vector2f position, ColorController* controller) const;

        Npc* generateCityNpc(sf::Vector2f position, ColorController* controller, unsigned int type) const;

    public:
        CityGenerator();
        ~CityGenerator();

        Room* generateCity(Player* player, RoomGraph* graph);
};

#endif
