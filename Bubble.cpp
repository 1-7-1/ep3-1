/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Bubble.h"
#include "Animation.h"
#include "Player.h"
#include "Room.h"
#include "MathConstants.h"

Bubble::Bubble(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox,
    Animation* animation)
    : CollidableObject(position, origin, hitbox), InteractibleObject(position, origin, hitbox)
{
    _currentAnimation = animation;
    _currentAnimation->setOrigin(_origin);
}

Bubble::~Bubble()
{
    delete _currentAnimation;
}

void Bubble::interact(Player* player)
{
    if (player->getState() != action && !player->getObtainedEffects()[liquid])
    {
        std::vector<bool> obtEffects = player->getObtainedEffects();
        obtEffects[liquid] = true;
        player->setObtainedEffects(obtEffects);
        _room->createSpecialEffect(player->getPosition() + sf::Vector2f(0.f, 0.5f), sf::Vector2f(0.f, 45.f), effectGet);
    }
}

void Bubble::behave()
{
    changeAnimation();
}
