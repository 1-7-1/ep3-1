/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "SpaceViewGenerator.h"
#include "Room.h"
#include "TileCalculator.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "Decoration.h"
#include "Player.h"
#include "AutoTransition.h"
#include "Wall.h"
#include "Spaceship.h"

SpaceViewGenerator::SpaceViewGenerator()
{

}

SpaceViewGenerator::~SpaceViewGenerator()
{

}

void SpaceViewGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/bg/9.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/bg/10.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/events/2.png", sf::Color(255, 0, 255, 255));
    _isLoaded = true;
}

Decoration* SpaceViewGenerator::generateBackground() const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(320, 240));

    Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(bg));
    return new Decoration(sf::Vector2f(), sf::Vector2f(), sf::Vector2f(), anim);
}

std::vector<Decoration*> SpaceViewGenerator::generateLights() const
{
    std::vector<Decoration*> decs;

    std::vector<sf::Vector2u> bigOffsets = {sf::Vector2u(5, 0), sf::Vector2u(0, 0), sf::Vector2u(1, 0), sf::Vector2u(2, 0),
        sf::Vector2u(3, 0), sf::Vector2u(4, 0), sf::Vector2u(3, 0), sf::Vector2u(2, 0), sf::Vector2u(1, 0),
        sf::Vector2u(0, 0)};

    std::vector<sf::Vector2u> smallOffsets = {sf::Vector2u(4, 0), sf::Vector2u(3, 0), sf::Vector2u(2, 0), sf::Vector2u(1, 0),
        sf::Vector2u(0, 0), sf::Vector2u(5, 0), sf::Vector2u(0, 0), sf::Vector2u(1, 0), sf::Vector2u(2, 0),
        sf::Vector2u(3, 0)};

    std::vector<unsigned int> times = {30, 12, 12, 12, 12, 30, 12, 12, 12, 12};

    TileCalculator bigRedCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(48, 32));
    {
        Animation* anim = new Animation(bigRedCalc(bigOffsets, times), _db.getTexture(lights));
        decs.push_back(new Decoration(sf::Vector2f(84.f, 79.f), sf::Vector2f(), sf::Vector2f(), anim));
    }

    TileCalculator bigYellowCalc = TileCalculator(sf::Vector2u(0, 32), sf::Vector2u(32, 96));
    {
        Animation* anim = new Animation(bigYellowCalc(bigOffsets, times), _db.getTexture(lights));
        decs.push_back(new Decoration(sf::Vector2f(90.f, 120.f), sf::Vector2f(), sf::Vector2f(), anim));
    }

    TileCalculator smallCalc = TileCalculator(sf::Vector2u(0, 128), sf::Vector2u(16, 16));
    {
        Animation* anim = new Animation(smallCalc(smallOffsets, times), _db.getTexture(lights));
        decs.push_back(new Decoration(sf::Vector2f(77.f, 41.f), sf::Vector2f(1.f, 1.f), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(smallCalc(smallOffsets, times), _db.getTexture(lights));
        decs.push_back(new Decoration(sf::Vector2f(267.f, 118.f), sf::Vector2f(1.f, 1.f), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(smallCalc(smallOffsets, times), _db.getTexture(lights));
        decs.push_back(new Decoration(sf::Vector2f(264.f, 139.f), sf::Vector2f(1.f, 1.f), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(smallCalc(smallOffsets, times), _db.getTexture(lights));
        decs.push_back(new Decoration(sf::Vector2f(259.f, 144.f), sf::Vector2f(1.f, 1.f), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(smallCalc(smallOffsets, times), _db.getTexture(lights));
        decs.push_back(new Decoration(sf::Vector2f(274.f, 145.f), sf::Vector2f(1.f, 1.f), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(smallCalc(smallOffsets, times), _db.getTexture(lights));
        decs.push_back(new Decoration(sf::Vector2f(277.f, 148.f), sf::Vector2f(1.f, 1.f), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(smallCalc(smallOffsets, times), _db.getTexture(lights));
        decs.push_back(new Decoration(sf::Vector2f(287.f, 151.f), sf::Vector2f(1.f, 1.f), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(smallCalc(smallOffsets, times), _db.getTexture(lights));
        decs.push_back(new Decoration(sf::Vector2f(230.f, 149.f), sf::Vector2f(1.f, 1.f), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(smallCalc(smallOffsets, times), _db.getTexture(lights));
        decs.push_back(new Decoration(sf::Vector2f(238.f, 149.f), sf::Vector2f(1.f, 1.f), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(smallCalc(smallOffsets, times), _db.getTexture(lights));
        decs.push_back(new Decoration(sf::Vector2f(25.f, 139.f), sf::Vector2f(1.f, 1.f), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(smallCalc(smallOffsets, times), _db.getTexture(lights));
        decs.push_back(new Decoration(sf::Vector2f(20.f, 189.f), sf::Vector2f(1.f, 1.f), sf::Vector2f(), anim));
    }
    {
        Animation* anim = new Animation(smallCalc(smallOffsets, times), _db.getTexture(lights));
        decs.push_back(new Decoration(sf::Vector2f(20.f, 198.f), sf::Vector2f(1.f, 1.f), sf::Vector2f(), anim));
    }

    return decs;
}

std::vector<Wall*> SpaceViewGenerator::generateWalls() const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    std::vector<Wall*> walls;

    for (int i = 0; i < 32; i++)
    {
        Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));
        walls.push_back(new Wall(sf::Vector2f(124.f - i, 204.f + i), sf::Vector2f(),
            anim, sf::FloatRect(0.f, 0.f, 4.f, 4.f)));
    }

    for (int i = 0; i < 32; i++)
    {
        Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));
        walls.push_back(new Wall(sf::Vector2f(192.f + i, 204.f + i), sf::Vector2f(),
            anim, sf::FloatRect(0.f, 0.f, 4.f, 4.f)));
    }

    {
        Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));
        walls.push_back(new Wall(sf::Vector2f(128.f, 204.f), sf::Vector2f(),
            anim, sf::FloatRect(0.f, 0.f, 64.f, 4.f)));
    }

    return walls;
}

AutoTransition* SpaceViewGenerator::generateTile(Player* player) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}),
        std::vector<unsigned int>({0})), _db.getTexture(invisible));

    return new AutoTransition(sf::Vector2f(96.f, 239.f), sf::Vector2f(), sf::FloatRect(0.f, 0.f, 128.f, 16.f),
        anim, 26, sf::Vector2f(296.f, 208.f), true, player);
}

Spaceship* SpaceViewGenerator::generateSpaceship() const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(112, 112));

    std::vector<Animation*> anims;

    anims.push_back(new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0), sf::Vector2u(1, 0)}),
        std::vector<unsigned int>({2, 2})), _db.getTexture(ship)));
    anims.push_back(new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 1), sf::Vector2u(1, 1)}),
        std::vector<unsigned int>({2, 2})), _db.getTexture(ship)));
    anims.push_back(new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 2), sf::Vector2u(1, 2)}),
        std::vector<unsigned int>({2, 2})), _db.getTexture(ship)));
    anims.push_back(new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 3), sf::Vector2u(1, 3)}),
        std::vector<unsigned int>({2, 2})), _db.getTexture(ship)));
    
    return new Spaceship(sf::Vector2f(200.f, 92.f), sf::Vector2f(61.f, 61.f), anims);
}

Room* SpaceViewGenerator::generateView(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;
    std::vector<GameObject*> foregrounds;
    
    floors.push_back(generateTile(player));
    
    std::vector<Decoration*> decs = generateLights();
    floors.insert(floors.end(), decs.begin(), decs.end());

    std::vector<Wall*> walls = generateWalls();
    collidables.insert(collidables.end(), walls.begin(), walls.end());

    backgrounds.push_back(generateBackground());

    foregrounds.push_back(generateSpaceship());

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, foregrounds, std::vector<GameObject*>() };

    return new Room(player, graph, nullptr, objects, sf::Vector2f(_roomWidth, _roomHeight),
        sf::Vector2<bool>(false, false));
}
