/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Desk.h"
#include "Player.h"
#include "MathConstants.h"
#include "Room.h"
#include "Animation.h"
#include <fstream>

Desk::Desk(sf::Vector2f position, sf::Vector2f origin, Animation* animation, sf::FloatRect hitbox)
    : CollidableObject(position, origin, hitbox), InteractibleObject(position, origin, hitbox)
{
    _currentAnimation = animation;
    _currentAnimation->setOrigin(_origin);
}

Desk::~Desk()
{
    delete _currentAnimation;
}

void Desk::save(std::vector<bool> effects)
{
    unsigned char c = unsigned(rand()) % 256;
    c &= 0b10100101;
    if (effects[machete])
        c |= 0b00000010;
    if (effects[scorpion])
        c |= 0b00001000;
    if (effects[liquid])
        c |= 0b00010000;
    if (effects[mask])
        c |= 0b01000000;
    std::ofstream stream;
    stream.open("save.bin", std::ofstream::out | std::ofstream::binary);
    stream << c;
    stream.close();
}

void Desk::interact(Player* player)
{
    float playerDir = player->getDirection() * 180.f / math::PI;
    if (player->getState() == idle && playerDir > 20.f && playerDir < 160.f)
    {
        if (_saveTimer == 0)
        {
            std::vector<bool> effs = player->getObtainedEffects();
            save(effs);
            _room->createSpecialEffect(player->getPosition() + sf::Vector2f(0.f, 0.5f), sf::Vector2f(0.f, 45.f), gameSaved);
            _saveTimer = _saveDelay;
        }
    }
}

void Desk::behave()
{
    if (_saveTimer > 0)
        _saveTimer--;
}