/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TILE_CALC_H
#define TILE_CALC_H

#include "SFML/Graphics.hpp"
#include <vector>

struct AnimationFrame;

class TileCalculator
{
private:
    sf::Vector2u _initialOffset;
    sf::Vector2u _frameSize;
public:
    TileCalculator(sf::Vector2u initOff, sf::Vector2u frameSize);
    ~TileCalculator();

    std::vector<AnimationFrame> operator()(std::vector<sf::Vector2u> coordList, std::vector<unsigned int> times);
};

#endif
