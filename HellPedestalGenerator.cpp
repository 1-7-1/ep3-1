/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "HellPedestalGenerator.h"
#include "Room.h"
#include "TileCalculator.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "Decoration.h"
#include "Wall.h"
#include "Player.h"
#include "Room.h"
#include "AutoTransition.h"
#include "MaskPedestal.h"


HellPedestalGenerator::HellPedestalGenerator()
{

}

HellPedestalGenerator::~HellPedestalGenerator()
{

}

void HellPedestalGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/tilesets/7.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/tilesets/8.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/object/3.png", sf::Color(0, 255, 255, 255));
    _isLoaded = true;
}

Decoration* HellPedestalGenerator::generateBackground() const
{
    TileCalculator bgCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(320, 240));

    Animation* anim = new Animation(bgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(bg));

    return new Decoration(sf::Vector2f(), sf::Vector2f(), sf::Vector2f(), anim);
}

Decoration* HellPedestalGenerator::generateForeground() const
{
    TileCalculator fgCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(320, 240));

    Animation* anim = new Animation(fgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(fg));

    return new Decoration(sf::Vector2f(), sf::Vector2f(), sf::Vector2f(), anim);
}

MaskPedestal* HellPedestalGenerator::generatePedestal(Player* player) const
{
    TileCalculator fgCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 32));

    std::vector<Animation*> anims;

    anims.push_back(new Animation(fgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(pedestal)));

    anims.push_back(new Animation(fgCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(pedestal)));

    return new MaskPedestal(sf::Vector2f(160.f, 168.f), sf::Vector2f(9.f, 30.f), sf::FloatRect(-7.f, -2.f, 15.f, 4.f), anims, player);
}

std::vector<Wall*> HellPedestalGenerator::generateWalls() const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    std::vector<Wall*> walls;

    std::vector<std::pair<sf::Vector2f, sf::FloatRect>> wallParams = {
        //left side
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(134.f, 220.f), sf::FloatRect(0.f, 0.f, 11.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(122.f, 214.f), sf::FloatRect(0.f, 0.f, 17.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(127.f, 215.f), sf::FloatRect(0.f, 0.f, 14.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(139.f, 216.f), sf::FloatRect(0.f, 0.f, 3.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(140.f, 217.f), sf::FloatRect(0.f, 0.f, 3.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(141.f, 218.f), sf::FloatRect(0.f, 0.f, 3.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(119.f, 214.f), sf::FloatRect(0.f, 0.f, 12.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(113.f, 213.f), sf::FloatRect(0.f, 0.f, 6.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(108.f, 212.f), sf::FloatRect(0.f, 0.f, 6.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(104.f, 211.f), sf::FloatRect(0.f, 0.f, 4.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(100.f, 210.f), sf::FloatRect(0.f, 0.f, 4.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(96.f, 210.f), sf::FloatRect(0.f, 0.f, 4.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(93.f, 209.f), sf::FloatRect(0.f, 0.f, 4.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(90.f, 208.f), sf::FloatRect(0.f, 0.f, 4.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(87.f, 208.f), sf::FloatRect(0.f, 0.f, 3.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(85.f, 207.f), sf::FloatRect(0.f, 0.f, 3.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(83.f, 206.f), sf::FloatRect(0.f, 0.f, 3.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(81.f, 205.f), sf::FloatRect(0.f, 0.f, 2.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(79.f, 204.f), sf::FloatRect(0.f, 0.f, 2.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(77.f, 203.f), sf::FloatRect(0.f, 0.f, 2.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(75.f, 202.f), sf::FloatRect(0.f, 0.f, 2.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(72.f, 200.f), sf::FloatRect(0.f, 0.f, 3.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(70.f, 199.f), sf::FloatRect(0.f, 0.f, 3.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(68.f, 197.f), sf::FloatRect(0.f, 0.f, 3.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(69.f, 198.f), sf::FloatRect(0.f, 0.f, 3.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(67.f, 195.f), sf::FloatRect(0.f, 0.f, 3.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(66.f, 193.f), sf::FloatRect(0.f, 0.f, 3.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(65.f, 190.f), sf::FloatRect(0.f, 0.f, 3.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(64.f, 174.f), sf::FloatRect(0.f, 0.f, 5.f, 27.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(68.f, 173.f), sf::FloatRect(0.f, 0.f, 2.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(68.f, 173.f), sf::FloatRect(0.f, 0.f, 3.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(68.f, 170.f), sf::FloatRect(0.f, 0.f, 4.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(70.f, 169.f), sf::FloatRect(0.f, 0.f, 4.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(69.f, 169.f), sf::FloatRect(0.f, 0.f, 6.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(73.f, 167.f), sf::FloatRect(0.f, 0.f, 4.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(71.f, 165.f), sf::FloatRect(0.f, 0.f, 8.f, 10.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(76.f, 169.f), sf::FloatRect(0.f, 0.f, 5.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(78.f, 169.f), sf::FloatRect(0.f, 0.f, 5.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(80.f, 168.f), sf::FloatRect(0.f, 0.f, 5.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(80.f, 167.f), sf::FloatRect(0.f, 0.f, 7.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(86.f, 167.f), sf::FloatRect(0.f, 0.f, 4.f, 3.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(87.f, 162.f), sf::FloatRect(0.f, 0.f, 6.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(88.f, 159.f), sf::FloatRect(0.f, 0.f, 8.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(94.f, 161.f), sf::FloatRect(0.f, 0.f, 6.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(96.f, 159.f), sf::FloatRect(0.f, 0.f, 8.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(99.f, 159.f), sf::FloatRect(0.f, 0.f, 9.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(106.f, 159.f), sf::FloatRect(0.f, 0.f, 7.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(109.f, 158.f), sf::FloatRect(0.f, 0.f, 10.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(117.f, 156.f), sf::FloatRect(0.f, 0.f, 10.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(125.f, 156.f), sf::FloatRect(0.f, 0.f, 11.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(135.f, 155.f), sf::FloatRect(0.f, 0.f, 49.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(138.f, 220.f), sf::FloatRect(0.f, 0.f, 7.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(138.f, 225.f), sf::FloatRect(0.f, 0.f, 6.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(138.f, 230.f), sf::FloatRect(0.f, 0.f, 5.f, 10.f)),

        //right side
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(177.f, 229.f), sf::FloatRect(0.f, 0.f, 9.f, 11.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(176.f, 220.f), sf::FloatRect(0.f, 0.f, 9.f, 12.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(175.f, 220.f), sf::FloatRect(0.f, 0.f, 7.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(177.f, 219.f), sf::FloatRect(0.f, 0.f, 6.f, 3.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(181.f, 223.f), sf::FloatRect(0.f, 0.f, 12.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(193.f, 222.f), sf::FloatRect(0.f, 0.f, 14.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(201.f, 221.f), sf::FloatRect(0.f, 0.f, 10.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(207.f, 220.f), sf::FloatRect(0.f, 0.f, 8.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(212.f, 219.f), sf::FloatRect(0.f, 0.f, 11.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(216.f, 218.f), sf::FloatRect(0.f, 0.f, 11.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(220.f, 217.f), sf::FloatRect(0.f, 0.f, 7.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(224.f, 216.f), sf::FloatRect(0.f, 0.f, 8.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(227.f, 215.f), sf::FloatRect(0.f, 0.f, 11.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(230.f, 214.f), sf::FloatRect(0.f, 0.f, 8.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(233.f, 213.f), sf::FloatRect(0.f, 0.f, 9.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(235.f, 212.f), sf::FloatRect(0.f, 0.f, 3.f, 3.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(237.f, 211.f), sf::FloatRect(0.f, 0.f, 5.f, 2.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(239.f, 210.f), sf::FloatRect(0.f, 0.f, 6.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(241.f, 209.f), sf::FloatRect(0.f, 0.f, 5.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(243.f, 208.f), sf::FloatRect(0.f, 0.f, 5.f, 3.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(245.f, 207.f), sf::FloatRect(0.f, 0.f, 7.f, 3.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(246.f, 206.f), sf::FloatRect(0.f, 0.f, 5.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(248.f, 205.f), sf::FloatRect(0.f, 0.f, 6.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(249.f, 204.f), sf::FloatRect(0.f, 0.f, 8.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(250.f, 203.f), sf::FloatRect(0.f, 0.f, 2.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(251.f, 202.f), sf::FloatRect(0.f, 0.f, 3.f, 3.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(252.f, 201.f), sf::FloatRect(0.f, 0.f, 6.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(253.f, 200.f), sf::FloatRect(0.f, 0.f, 4.f, 3.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(254.f, 199.f), sf::FloatRect(0.f, 0.f, 4.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(255.f, 197.f), sf::FloatRect(0.f, 0.f, 3.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(256.f, 186.f), sf::FloatRect(0.f, 0.f, 6.f, 18.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(255.f, 179.f), sf::FloatRect(0.f, 0.f, 4.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(254.f, 181.f), sf::FloatRect(0.f, 0.f, 3.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(253.f, 180.f), sf::FloatRect(0.f, 0.f, 3.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(252.f, 178.f), sf::FloatRect(0.f, 0.f, 3.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(251.f, 178.f), sf::FloatRect(0.f, 0.f, 3.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(250.f, 177.f), sf::FloatRect(0.f, 0.f, 3.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(249.f, 177.f), sf::FloatRect(0.f, 0.f, 3.f, 3.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(248.f, 170.f), sf::FloatRect(0.f, 0.f, 7.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(246.f, 172.f), sf::FloatRect(0.f, 0.f, 9.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(245.f, 172.f), sf::FloatRect(0.f, 0.f, 6.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(243.f, 173.f), sf::FloatRect(0.f, 0.f, 5.f, 3.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(241.f, 167.f), sf::FloatRect(0.f, 0.f, 14.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(239.f, 170.f), sf::FloatRect(0.f, 0.f, 3.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(237.f, 167.f), sf::FloatRect(0.f, 0.f, 6.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(235.f, 166.f), sf::FloatRect(0.f, 0.f, 12.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(233.f, 168.f), sf::FloatRect(0.f, 0.f, 4.f, 3.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(230.f, 165.f), sf::FloatRect(0.f, 0.f, 7.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(227.f, 164.f), sf::FloatRect(0.f, 0.f, 7.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(224.f, 163.f), sf::FloatRect(0.f, 0.f, 4.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(220.f, 161.f), sf::FloatRect(0.f, 0.f, 7.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(216.f, 162.f), sf::FloatRect(0.f, 0.f, 7.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(212.f, 160.f), sf::FloatRect(0.f, 0.f, 7.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(207.f, 157.f), sf::FloatRect(0.f, 0.f, 10.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(201.f, 158.f), sf::FloatRect(0.f, 0.f, 18.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(193.f, 157.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(184.f, 156.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)) };

    for (unsigned int i = 0; i < wallParams.size(); i++)
    {
        Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));

        walls.push_back(new Wall(wallParams[i].first, sf::Vector2f(), anim, wallParams[i].second));
    }

    return walls;
}

std::vector<AutoTransition*> HellPedestalGenerator::generateTiles(Player* player) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    std::vector<AutoTransition*> tiles;

    {
        Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));

        tiles.push_back(new AutoTransition(sf::Vector2f(142.f, 239.f), sf::Vector2f(), sf::FloatRect(0.f, 0.f, 36.f, 8.f),
            anim, 22, sf::Vector2f(160.f, 232.f), true, player));
    }

    return tiles;
}

Room* HellPedestalGenerator::generateHell(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;
    std::vector<GameObject*> foregrounds;

    backgrounds.push_back(generateBackground());
    foregrounds.push_back(generateForeground());

    std::vector<Wall*> walls = generateWalls();
    collidables.insert(collidables.end(), walls.begin(), walls.end());
    
    std::vector<AutoTransition*> tiles = generateTiles(player);
    floors.insert(floors.end(), tiles.begin(), tiles.end());

    MaskPedestal* mPed = generatePedestal(player);
    collidables.push_back(mPed);
    interactibles.push_back(mPed);

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, foregrounds, std::vector<GameObject*>() };

    return new Room(player, graph, nullptr, objects,
        sf::Vector2f(_roomWidth, _roomHeight), sf::Vector2<bool>(false, false));
}
