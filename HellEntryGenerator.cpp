/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "HellEntryGenerator.h"
#include "Room.h"
#include "TileCalculator.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "Decoration.h"
#include "Wall.h"
#include "Player.h"
#include "AutoTransition.h"

HellEntryGenerator::HellEntryGenerator()
{

}

HellEntryGenerator::~HellEntryGenerator()
{

}

void HellEntryGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/tilesets/9.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/tilesets/10.png", sf::Color(255, 0, 255, 255));
    _isLoaded = true;
}

Decoration* HellEntryGenerator::generateBackground() const
{
    TileCalculator bgCalc = TileCalculator(sf::Vector2u(88, 0), sf::Vector2u(168, 240));

    Animation* anim = new Animation(bgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(tileset));

    return new Decoration(sf::Vector2f(88.f, 0.f), sf::Vector2f(), sf::Vector2f(), anim);
}

Decoration* HellEntryGenerator::generateForeground() const
{
    TileCalculator fgCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(320, 240));

    Animation* anim = new Animation(fgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(fg));

    return new Decoration(sf::Vector2f(), sf::Vector2f(), sf::Vector2f(), anim);
}

std::vector<Wall*> HellEntryGenerator::generateWalls() const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    std::vector<Wall*> walls;

    std::vector<std::pair<sf::Vector2f, sf::FloatRect>> wallParams = {
        //top wall
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(96.f, 127.f), sf::FloatRect(0.f, 0.f, 5.f, 46.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(99.f, 118.f), sf::FloatRect(0.f, 0.f, 3.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(97.f, 111.f), sf::FloatRect(0.f, 0.f, 6.f, 11.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(97.f, 104.f), sf::FloatRect(0.f, 0.f, 7.f, 12.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(102.f, 99.f), sf::FloatRect(0.f, 0.f, 3.f, 10.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(102.f, 95.f), sf::FloatRect(0.f, 0.f, 4.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(102.f, 92.f), sf::FloatRect(0.f, 0.f, 5.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(104.f, 91.f), sf::FloatRect(0.f, 0.f, 4.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(107.f, 87.f), sf::FloatRect(0.f, 0.f, 2.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(107.f, 85.f), sf::FloatRect(0.f, 0.f, 3.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(106.f, 85.f), sf::FloatRect(0.f, 0.f, 5.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(106.f, 83.f), sf::FloatRect(0.f, 0.f, 6.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(109.f, 83.f), sf::FloatRect(0.f, 0.f, 4.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(108.f, 80.f), sf::FloatRect(0.f, 0.f, 6.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(110.f, 77.f), sf::FloatRect(0.f, 0.f, 5.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(111.f, 75.f), sf::FloatRect(0.f, 0.f, 5.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(113.f, 72.f), sf::FloatRect(0.f, 0.f, 4.f, 10.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(111.f, 71.f), sf::FloatRect(0.f, 0.f, 7.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(113.f, 72.f), sf::FloatRect(0.f, 0.f, 6.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(113.f, 70.f), sf::FloatRect(0.f, 0.f, 7.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(115.f, 71.f), sf::FloatRect(0.f, 0.f, 6.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(113.f, 68.f), sf::FloatRect(0.f, 0.f, 9.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(117.f, 68.f), sf::FloatRect(0.f, 0.f, 6.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(119.f, 66.f), sf::FloatRect(0.f, 0.f, 5.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(120.f, 66.f), sf::FloatRect(0.f, 0.f, 6.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(119.f, 65.f), sf::FloatRect(0.f, 0.f, 9.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(117.f, 62.f), sf::FloatRect(0.f, 0.f, 14.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(123.f, 62.f), sf::FloatRect(0.f, 0.f, 11.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(126.f, 63.f), sf::FloatRect(0.f, 0.f, 10.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(132.f, 61.f), sf::FloatRect(0.f, 0.f, 6.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(132.f, 58.f), sf::FloatRect(0.f, 0.f, 8.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(133.f, 58.f), sf::FloatRect(0.f, 0.f, 10.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(135.f, 54.f), sf::FloatRect(0.f, 0.f, 24.f, 10.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(154.f, 53.f), sf::FloatRect(0.f, 0.f, 19.f, 10.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(170.f, 56.f), sf::FloatRect(0.f, 0.f, 15.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(182.f, 59.f), sf::FloatRect(0.f, 0.f, 6.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(185.f, 59.f), sf::FloatRect(0.f, 0.f, 9.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(188.f, 55.f), sf::FloatRect(0.f, 0.f, 17.f, 10.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(190.f, 62.f), sf::FloatRect(0.f, 0.f, 3.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(191.f, 63.f), sf::FloatRect(0.f, 0.f, 3.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(192.f, 63.f), sf::FloatRect(0.f, 0.f, 4.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(193.f, 64.f), sf::FloatRect(0.f, 0.f, 4.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(194.f, 65.f), sf::FloatRect(0.f, 0.f, 4.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(195.f, 66.f), sf::FloatRect(0.f, 0.f, 13.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(197.f, 68.f), sf::FloatRect(0.f, 0.f, 5.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(198.f, 68.f), sf::FloatRect(0.f, 0.f, 6.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(199.f, 70.f), sf::FloatRect(0.f, 0.f, 5.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(200.f, 73.f), sf::FloatRect(0.f, 0.f, 4.f, 3.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(201.f, 73.f), sf::FloatRect(0.f, 0.f, 3.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(202.f, 74.f), sf::FloatRect(0.f, 0.f, 8.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(203.f, 74.f), sf::FloatRect(0.f, 0.f, 5.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(204.f, 76.f), sf::FloatRect(0.f, 0.f, 8.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(205.f, 76.f), sf::FloatRect(0.f, 0.f, 5.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(206.f, 76.f), sf::FloatRect(0.f, 0.f, 5.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(207.f, 78.f), sf::FloatRect(0.f, 0.f, 5.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(208.f, 80.f), sf::FloatRect(0.f, 0.f, 4.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(209.f, 80.f), sf::FloatRect(0.f, 0.f, 5.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(210.f, 82.f), sf::FloatRect(0.f, 0.f, 5.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(211.f, 79.f), sf::FloatRect(0.f, 0.f, 8.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(212.f, 84.f), sf::FloatRect(0.f, 0.f, 8.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(214.f, 84.f), sf::FloatRect(0.f, 0.f, 7.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(215.f, 87.f), sf::FloatRect(0.f, 0.f, 8.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(217.f, 86.f), sf::FloatRect(0.f, 0.f, 6.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(220.f, 86.f), sf::FloatRect(0.f, 0.f, 15.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(224.f, 91.f), sf::FloatRect(0.f, 0.f, 7.f, 3.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(227.f, 89.f), sf::FloatRect(0.f, 0.f, 8.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(230.f, 90.f), sf::FloatRect(0.f, 0.f, 11.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(232.f, 93.f), sf::FloatRect(0.f, 0.f, 4.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(233.f, 93.f), sf::FloatRect(0.f, 0.f, 3.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(234.f, 94.f), sf::FloatRect(0.f, 0.f, 6.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(235.f, 95.f), sf::FloatRect(0.f, 0.f, 6.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(236.f, 95.f), sf::FloatRect(0.f, 0.f, 4.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(237.f, 97.f), sf::FloatRect(0.f, 0.f, 5.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(238.f, 100.f), sf::FloatRect(0.f, 0.f, 6.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(239.f, 99.f), sf::FloatRect(0.f, 0.f, 7.f, 18.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(240.f, 117.f), sf::FloatRect(0.f, 0.f, 7.f, 33.f)),

        //bottom wall
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(100.f, 161.f), sf::FloatRect(0.f, 0.f, 2.f, 10.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(99.f, 163.f), sf::FloatRect(0.f, 0.f, 4.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(100.f, 166.f), sf::FloatRect(0.f, 0.f, 4.f, 11.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(96.f, 169.f), sf::FloatRect(0.f, 0.f, 9.f, 12.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(98.f, 171.f), sf::FloatRect(0.f, 0.f, 8.f, 11.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(101.f, 172.f), sf::FloatRect(0.f, 0.f, 6.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(103.f, 173.f), sf::FloatRect(0.f, 0.f, 5.f, 15.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(100.f, 175.f), sf::FloatRect(0.f, 0.f, 9.f, 15.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(103.f, 177.f), sf::FloatRect(0.f, 0.f, 7.f, 14.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(106.f, 179.f), sf::FloatRect(0.f, 0.f, 5.f, 12.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(108.f, 182.f), sf::FloatRect(0.f, 0.f, 4.f, 13.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(108.f, 184.f), sf::FloatRect(0.f, 0.f, 5.f, 12.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(104.f, 186.f), sf::FloatRect(0.f, 0.f, 10.f, 17.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(106.f, 188.f), sf::FloatRect(0.f, 0.f, 9.f, 15.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(110.f, 190.f), sf::FloatRect(0.f, 0.f, 6.f, 13.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(112.f, 192.f), sf::FloatRect(0.f, 0.f, 5.f, 14.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(113.f, 195.f), sf::FloatRect(0.f, 0.f, 5.f, 12.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(114.f, 198.f), sf::FloatRect(0.f, 0.f, 5.f, 12.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(116.f, 202.f), sf::FloatRect(0.f, 0.f, 4.f, 12.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(116.f, 205.f), sf::FloatRect(0.f, 0.f, 5.f, 10.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(117.f, 207.f), sf::FloatRect(0.f, 0.f, 5.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(118.f, 209.f), sf::FloatRect(0.f, 0.f, 5.f, 10.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(119.f, 210.f), sf::FloatRect(0.f, 0.f, 6.f, 10.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(122.f, 211.f), sf::FloatRect(0.f, 0.f, 4.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(123.f, 212.f), sf::FloatRect(0.f, 0.f, 4.f, 10.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(121.f, 213.f), sf::FloatRect(0.f, 0.f, 8.f, 11.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(123.f, 214.f), sf::FloatRect(0.f, 0.f, 7.f, 11.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(128.f, 215.f), sf::FloatRect(0.f, 0.f, 5.f, 9.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(130.f, 216.f), sf::FloatRect(0.f, 0.f, 6.f, 10.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(130.f, 217.f), sf::FloatRect(0.f, 0.f, 25.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(159.f, 217.f), sf::FloatRect(0.f, 0.f, 9.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(166.f, 216.f), sf::FloatRect(0.f, 0.f, 5.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(170.f, 215.f), sf::FloatRect(0.f, 0.f, 6.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(173.f, 214.f), sf::FloatRect(0.f, 0.f, 8.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(175.f, 213.f), sf::FloatRect(0.f, 0.f, 7.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(176.f, 212.f), sf::FloatRect(0.f, 0.f, 7.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(177.f, 211.f), sf::FloatRect(0.f, 0.f, 5.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(178.f, 210.f), sf::FloatRect(0.f, 0.f, 10.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(181.f, 209.f), sf::FloatRect(0.f, 0.f, 13.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(185.f, 208.f), sf::FloatRect(0.f, 0.f, 14.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(193.f, 207.f), sf::FloatRect(0.f, 0.f, 10.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(202.f, 206.f), sf::FloatRect(0.f, 0.f, 10.f, 3.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(207.f, 205.f), sf::FloatRect(0.f, 0.f, 14.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(211.f, 204.f), sf::FloatRect(0.f, 0.f, 6.f, 6.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(212.f, 203.f), sf::FloatRect(0.f, 0.f, 11.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(213.f, 202.f), sf::FloatRect(0.f, 0.f, 6.f, 4.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(214.f, 201.f), sf::FloatRect(0.f, 0.f, 5.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(215.f, 200.f), sf::FloatRect(0.f, 0.f, 5.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(216.f, 199.f), sf::FloatRect(0.f, 0.f, 5.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(217.f, 198.f), sf::FloatRect(0.f, 0.f, 5.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(218.f, 197.f), sf::FloatRect(0.f, 0.f, 5.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(219.f, 196.f), sf::FloatRect(0.f, 0.f, 5.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(220.f, 194.f), sf::FloatRect(0.f, 0.f, 6.f, 7.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(221.f, 193.f), sf::FloatRect(0.f, 0.f, 6.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(222.f, 191.f), sf::FloatRect(0.f, 0.f, 6.f, 8.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(223.f, 189.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(224.f, 187.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(225.f, 187.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(226.f, 185.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(227.f, 183.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(228.f, 179.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(229.f, 173.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(230.f, 169.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(231.f, 165.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(232.f, 160.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(233.f, 153.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(234.f, 155.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(235.f, 150.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(236.f, 146.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(237.f, 144.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(238.f, 142.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(239.f, 141.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)),
        std::pair<sf::Vector2f, sf::FloatRect>(sf::Vector2f(240.f, 137.f), sf::FloatRect(0.f, 0.f, 14.f, 5.f)) };

    for (unsigned int i = 0; i < wallParams.size(); i++)
    {
        Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));

        walls.push_back(new Wall(wallParams[i].first, sf::Vector2f(), anim, wallParams[i].second));
    }

    TileCalculator stairsCalc = TileCalculator(sf::Vector2u(16, 144), sf::Vector2u(16, 48));

    {
        Animation* anim = new Animation(stairsCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        walls.push_back(new Wall(sf::Vector2f(144.f, 176.f), sf::Vector2f(8.f, 32.f),
            anim, sf::FloatRect(-8.f, -10.f, 16.f, 26.f)));
    }

    {
        Animation* anim = new Animation(stairsCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        walls.push_back(new Wall(sf::Vector2f(160.f, 128.f), sf::Vector2f(8.f, -16.f),
            anim, sf::FloatRect(-8.f, 48.f, 16.f, 16.f)));
    }

    {
        Animation* anim = new Animation(stairsCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        walls.push_back(new Wall(sf::Vector2f(176.f, 128.f), sf::Vector2f(8.f, -16.f),
            anim, sf::FloatRect(-8.f, 48.f, 16.f, 16.f)));
    }

    {
        Animation* anim = new Animation(stairsCalc(std::vector<sf::Vector2u>({sf::Vector2u(3, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        walls.push_back(new Wall(sf::Vector2f(192.f, 176.f), sf::Vector2f(8.f, 32.f),
            anim, sf::FloatRect(-8.f, -10.f, 16.f, 26.f)));
    }

    {
        TileCalculator spikeCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 32));

        Animation* anim = new Animation(spikeCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        walls.push_back(new Wall(sf::Vector2f(215.f, 120.f), sf::Vector2f(8.f, 30.f),
            anim, sf::FloatRect(-8.f, -2.f, 16.f, 4.f)));
    }

    {
        TileCalculator spikeCalc = TileCalculator(sf::Vector2u(32, 0), sf::Vector2u(32, 48));

        Animation* anim = new Animation(spikeCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        walls.push_back(new Wall(sf::Vector2f(133.f, 95.f), sf::Vector2f(16.f, 45.f),
            anim, sf::FloatRect(-16.f, -3.f, 32.f, 6.f)));
    }

    {
        TileCalculator spikeCalc = TileCalculator(sf::Vector2u(16, 64), sf::Vector2u(16, 16));

        Animation* anim = new Animation(spikeCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        walls.push_back(new Wall(sf::Vector2f(184.f, 91.f), sf::Vector2f(8.f, 15.f),
            anim, sf::FloatRect(-8.f, -1.f, 16.f, 2.f)));
    }

    {
        TileCalculator spikeCalc = TileCalculator(sf::Vector2u(48, 64), sf::Vector2u(32, 48));

        Animation* anim = new Animation(spikeCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        walls.push_back(new Wall(sf::Vector2f(124.f, 141.f), sf::Vector2f(16.f, 42.f),
            anim, sf::FloatRect(-15.f, -2.f, 26.f, 4.f)));
    }

    {
        TileCalculator spikeCalc = TileCalculator(sf::Vector2u(0, 96), sf::Vector2u(32, 32));

        Animation* anim = new Animation(spikeCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(tileset));

        walls.push_back(new Wall(sf::Vector2f(186.f, 142.f), sf::Vector2f(16.f, 30.f),
            anim, sf::FloatRect(-7.f, -2.f, 22.f, 4.f)));
    }

    return walls;
}

std::vector<AutoTransition*> HellEntryGenerator::generateTiles(Player* player) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    std::vector<AutoTransition*> tiles;

    {
        Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(invisible));

        tiles.push_back(new AutoTransition(sf::Vector2f(152.f, 168.f), sf::Vector2f(), sf::FloatRect(0.f, 0.f, 32.f, 8.f),
            anim, 22, sf::Vector2f(160.f, 8.f), true, player));
    }

    return tiles;
}

Room* HellEntryGenerator::generateHell(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;
    std::vector<GameObject*> foregrounds;

    backgrounds.push_back(generateBackground());
    foregrounds.push_back(generateForeground());

    std::vector<Wall*> walls = generateWalls();
    collidables.insert(collidables.end(), walls.begin(), walls.end());
    
    std::vector<AutoTransition*> tiles = generateTiles(player);
    floors.insert(floors.end(), tiles.begin(), tiles.end());

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, foregrounds, std::vector<GameObject*>() };

    return new Room(player, graph, nullptr, objects,
        sf::Vector2f(_roomWidth, _roomHeight), sf::Vector2<bool>(false, false));
}
