/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SPIKE_GEN_H
#define SPIKE_GEN_H

#include "GameObjectGenerator.h"

class Room;
class Player;
class RoomGraph;
class Wall;
class Decoration;
class AutoTransition;
class SpikeRipples;
class SpikeNpc;
class Waterfall;

class SpikeGenerator: public GameObjectGenerator
{
    private:
        enum SpikeGraphics { spikeInvisible, spikeTileset, bigSpike, spikeNpc, spikeBg, city };
        virtual void load();

        Animation* generateSpikeAnimation() const;
        Wall* generateSpike(sf::Vector2f position, Direction dir) const;

        Animation* generateSpikeFloorAnimation() const;
        Decoration* generateSpikeShadow(sf::Vector2f position, Direction dir) const;

        std::vector<Animation*> generateBigSpikeAnimations() const;
        std::vector<Wall*> generateBigSpike(sf::Vector2f position) const;
        Decoration* generateBigSpikeShadow(sf::Vector2f position) const;

        AutoTransition* generateTile(const sf::Vector2f position, sf::FloatRect hitbox, const unsigned int destination,
            const sf::Vector2f transitionVec, const bool absolute, Player* player) const;
        AutoTransition* generateCityTile(const sf::Vector2f position, const unsigned int destination,
            const sf::Vector2f transitionVec, const bool absolute, Player* player) const;
        std::vector<Waterfall*> generateWaterfalls(Player* player) const;

        Animation* generateRippleAnimation() const;
        SpikeRipples* generateRipple(sf::Vector2f position, sf::Vector2f speed) const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateSpikeNpcOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateSpikeNpcTimeVectors() const;

        SpikeNpc* generateSpikeNpc(sf::Vector2f position, unsigned int initialD) const;

        Decoration* generateBackground(const sf::Vector2f position, const sf::Vector2f speed) const;
        
    public:
        SpikeGenerator();
        ~SpikeGenerator();

        Room* generateSpikeArea(Player* player, RoomGraph* graph);
};

#endif
