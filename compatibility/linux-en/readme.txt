Thank you for downloading Ep3 version 1.0.1! This file contains helpful information for whoever may need it.

Table of contents:
1. Information
	a) Licensing information
	b) General and contact information
	c) Miscellaneous information
2. Instructions
	a) Running the game
	b) Game flow
3. Controls
	a) In game
	b) In menus
	c) Other controls

1. Information:

a) Licensing information:

	This software uses OpenAl Soft (https://openal-soft.org/). OpenAL Soft is licensed under the GNU Library General Public License, version 2 (LGPL v2). The full license text is available online here: https://repo.or.cz/w/openal-soft.git/blob/HEAD:/COPYING .

b) General and contact information:

	This is version 1.0.1 of Ep3, a Yume Nikki fangame made by 171 (that's me!). It was originally for the "Dream Diary Jam 3" game jam hosted on https://itch.io/jam/dream-diary-jam-3.

	If you have questions or comments about the game, or if you'd like to report a bug, you can comment on the itch.io page. You can also e-mail me directly at 171@tutanota.de . In particular, I would love to hear whether you like the game or not and what you like/dislike about it. This is very useful information for me!

c) Miscellaneous information:

	This game is programmed in C++ on top of the SFML multimedia library, which you can find here: http://sfml-dev.org/. Although I had 6 weeks to make the game, the game's development lasted for about 8 to 9 weeks in total. This is because the first 2-3 weeks were used to make the game engine on which the game would be built, which is legal (see here: https://dreamdiaryjam.tumblr.com/post/184729888754/hello-ive-wanted-to-participate-in-the-current).

	Because it is a Yume Nikki fangame, Ep3 is very much inspired by, you guessed it, Yume Nikki. If you don't know what Yume Nikki is (in which case I'm seriously wondering how you got to reading this), it's an exploration/horror game made in 2004 by pseudonymous developper Kikiyama. People familiar with Yume Nikki should have no problems understanding how Ep3 works, but I will lay down some additional instructions for you just in case.

2. Instructions:

a) Running the game:

	Run the ep3 program from a terminal by entering:

	./ep3

	If this is your first time running the game, you will probably need certain packages, which you can find and install using the information printed out by the console. At any time, you may press Escape to quit the game or use the function keys to set up the game window the way you like (see Controls below).

b) Game flow:

	As previously stated, the game works much like Yume Nikki. You start in a room with a bed and a computer. Interacting with the computer will save the game, much like the diary would in Yume Nikki, although only one save file is available. Interacting with the bed makes you go to sleep, where you can explore your dreams.

	Within your dreams you can find effects, which are then unlocked and can be equipped via the pause menu while dreaming. Effects are typically found by interacting with objects or creatures in the dream world. There are four effects in total. Complete the game by finding three or more effects and going to the balcony in the real world.

	In addition to effects, there are many things to do and secrets to find in the game. Try experimenting to discover new locations and events!

3. Controls:

a) In game:

	Up arrow:	Move up
	Down arrow:	Move down
	Left arrow:	Move left
	Right arrow:	Move right
	Z:		Interact
	X:		Special action (changes with equipped effect)
	W or 9:		Pinch your cheek and wake up from your dream
	Enter:		Pause/unpause the game

b) In menus:

	Up arrow:	Move cursor up
	Down arrow:	Move cursor down
	Left arrow:	Move cursor left
	Right arrow:	Move cursor right
	Z:		Enter your selection
	X:		Cancel your selection and go back to the previous menu

c) Other controls (available anytime):

	F1:		Move window to the center of the screen
	F2:		Toggle window borders
	F3:		Resize window, select between 1x, 2x and 3x the game's resolution
	F4/Alt + Enter:	Fullscreen (looks awful and buggy, that's why the other keys are there)
	Escape:		Quit the game
