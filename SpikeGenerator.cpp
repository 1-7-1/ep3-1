/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "SpikeGenerator.h"
#include "SpikeRoom.h"
#include "Wall.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "TileCalculator.h"
#include "Decoration.h"
#include "AutoTransition.h"
#include "SpikeRipples.h"
#include "SpikeNpc.h"
#include "Player.h"
#include "Waterfall.h"
#include "MathConstants.h"
#include <math.h>

SpikeGenerator::SpikeGenerator()
{
    
}

SpikeGenerator::~SpikeGenerator()
{

}

void SpikeGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/tilesets/22.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/tilesets/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/char/14.png", sf::Color(0, 255, 255, 255));
    _db.loadTexture("graphics/bg/11.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/tilesets/2.png", sf::Color(0, 0, 0, 255));
    _isLoaded = true;
}

Animation* SpikeGenerator::generateSpikeAnimation() const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(80, 80));
    return new Animation(wallCalc(std::vector<sf::Vector2u>( { sf::Vector2u(0, 0) } ),
            std::vector<unsigned int>( { 0 } )),
            _db.getTexture(spikeTileset));
}

Wall* SpikeGenerator::generateSpike(sf::Vector2f position, Direction dir) const
{
    Wall* spike = new Wall(position, sf::Vector2f(40.f, 64.f), generateSpikeAnimation(), sf::FloatRect(-24.f, 0.f, 48.f, 4.f));
    if (dir == genright)
        spike->getAnimation()->setScale(-1.f, 1.f);
    return spike;
}

Animation* SpikeGenerator::generateSpikeFloorAnimation() const
{
    TileCalculator floorCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(80, 80));

    std::vector<sf::Vector2u> offsets = { sf::Vector2u(0, 1), sf::Vector2u(1, 1), sf::Vector2u(2, 1), sf::Vector2u(3, 1),
    sf::Vector2u(0, 2), sf::Vector2u(1, 2), sf::Vector2u(2, 2), sf::Vector2u(3, 2) };

    Animation* anim = new Animation(floorCalc(offsets, std::vector<unsigned int>( { 5, 5, 5, 5, 5, 5, 5, 30 } )),
        _db.getTexture(spikeTileset));

    anim->setFrameIndex(rand() % offsets.size());

    return anim;
}

Decoration* SpikeGenerator::generateSpikeShadow(sf::Vector2f position, Direction dir) const
{
    Decoration* shad = new Decoration(position, sf::Vector2f(40.f, 64.f), sf::Vector2f(), generateSpikeFloorAnimation());
    if (dir == genright)
        shad->getAnimation()->setScale(-1.f, 1.f);
    return shad;
}

std::vector<Animation*> SpikeGenerator::generateBigSpikeAnimations() const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(576, 688));
    TileCalculator transCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    std::vector<Animation*> anims;
    anims.push_back(new Animation(wallCalc(std::vector<sf::Vector2u>( { sf::Vector2u(0, 0) } ),
        std::vector<unsigned int>( { 0 } )),
        _db.getTexture(bigSpike)));

    for (unsigned int i = 0; i < 74; i++)
        anims.push_back(new Animation(transCalc(std::vector<sf::Vector2u>( { sf::Vector2u(0, 0) } ),
            std::vector<unsigned int>( { 0 } )),
            _db.getTexture(spikeInvisible)));
    return anims;
}

std::vector<Wall*> SpikeGenerator::generateBigSpike(sf::Vector2f position) const
{
    std::vector<Animation*> anims = generateBigSpikeAnimations();
    std::vector<Wall*> walls;

    walls.push_back(new Wall(position, sf::Vector2f(272.f, 544.f), anims[0], sf::FloatRect(-16.f, -1.f, 17.f, 2.f)));
    walls.push_back(new Wall(position + sf::Vector2f(273.f, 541.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[1], sf::FloatRect(0.f, 0.f, 1.f, 6.f)));
    walls.push_back(new Wall(position + sf::Vector2f(274.f, 540.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[2], sf::FloatRect(0.f, 0.f, 1.f, 8.f)));
    walls.push_back(new Wall(position + sf::Vector2f(275.f, 538.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[3], sf::FloatRect(0.f, 0.f, 1.f, 12.f)));
    walls.push_back(new Wall(position + sf::Vector2f(276.f, 537.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[4], sf::FloatRect(0.f, 0.f, 1.f, 14.f)));
    walls.push_back(new Wall(position + sf::Vector2f(277.f, 536.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[5], sf::FloatRect(0.f, 0.f, 1.f, 16.f)));
    walls.push_back(new Wall(position + sf::Vector2f(278.f, 535.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[6], sf::FloatRect(0.f, 0.f, 2.f, 18.f)));
    walls.push_back(new Wall(position + sf::Vector2f(280.f, 534.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[7], sf::FloatRect(0.f, 0.f, 1.f, 20.f)));
    walls.push_back(new Wall(position + sf::Vector2f(281.f, 533.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[8], sf::FloatRect(0.f, 0.f, 2.f, 22.f)));
    walls.push_back(new Wall(position + sf::Vector2f(283.f, 532.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[9], sf::FloatRect(0.f, 0.f, 1.f, 24.f)));
    walls.push_back(new Wall(position + sf::Vector2f(284.f, 531.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[10], sf::FloatRect(0.f, 0.f, 2.f, 26.f)));
    walls.push_back(new Wall(position + sf::Vector2f(286.f, 530.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[11], sf::FloatRect(0.f, 0.f, 1.f, 28.f)));
    walls.push_back(new Wall(position + sf::Vector2f(287.f, 529.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[12], sf::FloatRect(0.f, 0.f, 2.f, 30.f)));
    walls.push_back(new Wall(position + sf::Vector2f(289.f, 528.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[13], sf::FloatRect(0.f, 0.f, 2.f, 32.f)));
    walls.push_back(new Wall(position + sf::Vector2f(291.f, 527.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[14], sf::FloatRect(0.f, 0.f, 2.f, 34.f)));
    walls.push_back(new Wall(position + sf::Vector2f(293.f, 526.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[15], sf::FloatRect(0.f, 0.f, 3.f, 36.f)));
    walls.push_back(new Wall(position + sf::Vector2f(296.f, 526.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[16], sf::FloatRect(0.f, 0.f, 3.f, 37.f)));
    walls.push_back(new Wall(position + sf::Vector2f(299.f, 525.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[17], sf::FloatRect(0.f, 0.f, 3.f, 39.f)));
    walls.push_back(new Wall(position + sf::Vector2f(302.f, 523.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[18], sf::FloatRect(0.f, 0.f, 4.f, 42.f)));
    walls.push_back(new Wall(position + sf::Vector2f(306.f, 522.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[19], sf::FloatRect(0.f, 0.f, 4.f, 44.f)));
    walls.push_back(new Wall(position + sf::Vector2f(310.f, 521.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[20], sf::FloatRect(0.f, 0.f, 5.f, 46.f)));
    walls.push_back(new Wall(position + sf::Vector2f(315.f, 520.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[21], sf::FloatRect(0.f, 0.f, 5.f, 48.f)));
    walls.push_back(new Wall(position + sf::Vector2f(320.f, 519.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[22], sf::FloatRect(0.f, 0.f, 5.f, 50.f)));
    walls.push_back(new Wall(position + sf::Vector2f(325.f, 518.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[23], sf::FloatRect(0.f, 0.f, 6.f, 52.f)));
    walls.push_back(new Wall(position + sf::Vector2f(331.f, 517.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[24], sf::FloatRect(0.f, 0.f, 6.f, 54.f)));
    walls.push_back(new Wall(position + sf::Vector2f(337.f, 516.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[25], sf::FloatRect(0.f, 0.f, 7.f, 40.f)));
    walls.push_back(new Wall(position + sf::Vector2f(344.f, 515.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[26], sf::FloatRect(0.f, 0.f, 8.f, 42.f)));
    walls.push_back(new Wall(position + sf::Vector2f(352.f, 514.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[27], sf::FloatRect(0.f, 0.f, 8.f, 44.f)));
    walls.push_back(new Wall(position + sf::Vector2f(360.f, 513.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[28], sf::FloatRect(0.f, 0.f, 8.f, 46.f)));
    walls.push_back(new Wall(position + sf::Vector2f(368.f, 512.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[29], sf::FloatRect(0.f, 0.f, 32.f, 64.f)));
    walls.push_back(new Wall(position + sf::Vector2f(400.f, 512.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[30], sf::FloatRect(0.f, 0.f, 8.f, 63.f)));
    walls.push_back(new Wall(position + sf::Vector2f(408.f, 512.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[31], sf::FloatRect(0.f, 0.f, 8.f, 62.f)));
    walls.push_back(new Wall(position + sf::Vector2f(416.f, 512.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[32], sf::FloatRect(0.f, 0.f, 8.f, 61.f)));
    walls.push_back(new Wall(position + sf::Vector2f(424.f, 512.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[33], sf::FloatRect(0.f, 0.f, 7.f, 60.f)));
    walls.push_back(new Wall(position + sf::Vector2f(431.f, 512.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[34], sf::FloatRect(0.f, 0.f, 6.f, 59.f)));
    walls.push_back(new Wall(position + sf::Vector2f(437.f, 513.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[35], sf::FloatRect(0.f, 0.f, 6.f, 57.f)));
    walls.push_back(new Wall(position + sf::Vector2f(443.f, 513.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[36], sf::FloatRect(0.f, 0.f, 5.f, 56.f)));
    walls.push_back(new Wall(position + sf::Vector2f(448.f, 513.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[37], sf::FloatRect(0.f, 0.f, 5.f, 55.f)));
    walls.push_back(new Wall(position + sf::Vector2f(453.f, 513.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[38], sf::FloatRect(0.f, 0.f, 5.f, 54.f)));
    walls.push_back(new Wall(position + sf::Vector2f(458.f, 513.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[39], sf::FloatRect(0.f, 0.f, 4.f, 53.f)));
    walls.push_back(new Wall(position + sf::Vector2f(462.f, 514.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[40], sf::FloatRect(0.f, 0.f, 4.f, 51.f)));
    walls.push_back(new Wall(position + sf::Vector2f(466.f, 514.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[41], sf::FloatRect(0.f, 0.f, 3.f, 50.f)));
    walls.push_back(new Wall(position + sf::Vector2f(469.f, 514.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[42], sf::FloatRect(0.f, 0.f, 3.f, 49.f)));
    walls.push_back(new Wall(position + sf::Vector2f(472.f, 514.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[43], sf::FloatRect(0.f, 0.f, 3.f, 48.f)));
    walls.push_back(new Wall(position + sf::Vector2f(475.f, 514.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[44], sf::FloatRect(0.f, 0.f, 2.f, 47.f)));
    walls.push_back(new Wall(position + sf::Vector2f(477.f, 514.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[45], sf::FloatRect(0.f, 0.f, 2.f, 46.f)));
    walls.push_back(new Wall(position + sf::Vector2f(479.f, 514.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[46], sf::FloatRect(0.f, 0.f, 2.f, 45.f)));
    walls.push_back(new Wall(position + sf::Vector2f(481.f, 514.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[47], sf::FloatRect(0.f, 0.f, 1.f, 44.f)));
    walls.push_back(new Wall(position + sf::Vector2f(482.f, 515.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[48], sf::FloatRect(0.f, 0.f, 2.f, 42.f)));
    walls.push_back(new Wall(position + sf::Vector2f(484.f, 515.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[49], sf::FloatRect(0.f, 0.f, 1.f, 41.f)));
    walls.push_back(new Wall(position + sf::Vector2f(485.f, 515.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[50], sf::FloatRect(0.f, 0.f, 2.f, 40.f)));
    walls.push_back(new Wall(position + sf::Vector2f(487.f, 515.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[51], sf::FloatRect(0.f, 0.f, 1.f, 39.f)));
    walls.push_back(new Wall(position + sf::Vector2f(488.f, 515.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[52], sf::FloatRect(0.f, 0.f, 2.f, 38.f)));
    walls.push_back(new Wall(position + sf::Vector2f(490.f, 515.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[53], sf::FloatRect(0.f, 0.f, 1.f, 37.f)));
    walls.push_back(new Wall(position + sf::Vector2f(491.f, 515.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[54], sf::FloatRect(0.f, 0.f, 1.f, 36.f)));
    walls.push_back(new Wall(position + sf::Vector2f(492.f, 515.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[55], sf::FloatRect(0.f, 0.f, 1.f, 35.f)));
    walls.push_back(new Wall(position + sf::Vector2f(493.f, 515.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[56], sf::FloatRect(0.f, 0.f, 1.f, 34.f)));
    walls.push_back(new Wall(position + sf::Vector2f(494.f, 515.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[57], sf::FloatRect(0.f, 0.f, 1.f, 33.f)));
    walls.push_back(new Wall(position + sf::Vector2f(495.f, 515.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[58], sf::FloatRect(0.f, 0.f, 1.f, 32.f)));
    walls.push_back(new Wall(position + sf::Vector2f(496.f, 515.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[59], sf::FloatRect(0.f, 0.f, 1.f, 32.f)));
    walls.push_back(new Wall(position + sf::Vector2f(497.f, 515.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[60], sf::FloatRect(0.f, 0.f, 1.f, 30.f)));
    walls.push_back(new Wall(position + sf::Vector2f(498.f, 515.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[61], sf::FloatRect(0.f, 0.f, 1.f, 29.f)));
    walls.push_back(new Wall(position + sf::Vector2f(499.f, 515.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[62], sf::FloatRect(0.f, 0.f, 1.f, 28.f)));
    walls.push_back(new Wall(position + sf::Vector2f(500.f, 515.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[63], sf::FloatRect(0.f, 0.f, 1.f, 27.f)));
    walls.push_back(new Wall(position + sf::Vector2f(501.f, 516.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[64], sf::FloatRect(0.f, 0.f, 1.f, 25.f)));
    walls.push_back(new Wall(position + sf::Vector2f(502.f, 516.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[65], sf::FloatRect(0.f, 0.f, 1.f, 24.f)));
    walls.push_back(new Wall(position + sf::Vector2f(503.f, 516.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[66], sf::FloatRect(0.f, 0.f, 1.f, 23.f)));
    walls.push_back(new Wall(position + sf::Vector2f(504.f, 516.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[67], sf::FloatRect(0.f, 0.f, 1.f, 22.f)));
    walls.push_back(new Wall(position + sf::Vector2f(505.f, 516.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[68], sf::FloatRect(0.f, 0.f, 1.f, 21.f)));
    walls.push_back(new Wall(position + sf::Vector2f(506.f, 516.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[69], sf::FloatRect(0.f, 0.f, 1.f, 20.f)));
    walls.push_back(new Wall(position + sf::Vector2f(507.f, 516.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[70], sf::FloatRect(0.f, 0.f, 1.f, 18.f)));
    walls.push_back(new Wall(position + sf::Vector2f(508.f, 517.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[71], sf::FloatRect(0.f, 0.f, 1.f, 15.f)));
    walls.push_back(new Wall(position + sf::Vector2f(509.f, 517.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[72], sf::FloatRect(0.f, 0.f, 1.f, 13.f)));
    walls.push_back(new Wall(position + sf::Vector2f(510.f, 517.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[73], sf::FloatRect(0.f, 0.f, 1.f, 10.f)));
    walls.push_back(new Wall(position + sf::Vector2f(511.f, 517.f) - sf::Vector2f(272.f, 544.f), sf::Vector2f(), anims[74], sf::FloatRect(0.f, 0.f, 1.f, 7.f)));

    return walls;
}

Decoration* SpikeGenerator::generateBigSpikeShadow(sf::Vector2f position) const
{
    TileCalculator shadCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(576, 688));

    Animation* anim = new Animation(shadCalc(std::vector<sf::Vector2u>( { sf::Vector2u(1, 0) } ),
        std::vector<unsigned int>( { 0 } )),
        _db.getTexture(bigSpike));

    Decoration* shad = new Decoration(position, sf::Vector2f(272.f, 544.f), sf::Vector2f(), anim);
    return shad;
}

AutoTransition* SpikeGenerator::generateTile(const sf::Vector2f position, sf::FloatRect hitbox, const unsigned int destination,
    const sf::Vector2f transitionVec, const bool absolute, Player* player) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));
    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}), std::vector<unsigned int>({0})),
        _db.getTexture(spikeInvisible));

    return new AutoTransition(position, sf::Vector2f(0.f, 0.f), hitbox,
        anim, destination, transitionVec, absolute, player);
}

AutoTransition* SpikeGenerator::generateCityTile(const sf::Vector2f position, const unsigned int destination,
    const sf::Vector2f transitionVec, const bool absolute, Player* player) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(48, 48));
    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u()}), std::vector<unsigned int>({0})),
        _db.getTexture(city));

    return new AutoTransition(position, sf::Vector2f(24.f, 24.f), sf::FloatRect(-8.f, -8.f, 16.f, 16.f),
        anim, destination, transitionVec, absolute, player);
}

std::vector<Waterfall*> SpikeGenerator::generateWaterfalls(Player* player) const
{
    TileCalculator watCalc = TileCalculator(sf::Vector2u(0, 0), sf::Vector2u(32, 80));
    std::vector<Waterfall*> waterfalls;

    for (int i = 0; i < 1280; i += 32)
    {
        Animation* anim = new Animation(watCalc(std::vector<sf::Vector2u>({sf::Vector2u(11, 0), sf::Vector2u(12, 0), sf::Vector2u(13, 0),
            sf::Vector2u(14, 0), sf::Vector2u(15, 0), sf::Vector2u(16, 0)}),
            std::vector<unsigned int>({6, 6, 6, 6, 6, 6})), _db.getTexture(spikeTileset));

        waterfalls.push_back(new Waterfall(sf::Vector2f(i, 0.f), sf::Vector2f(16.f, 16.f),
            sf::FloatRect(-16.f, -16.f, 32.f, 32.f), anim, 21, player));
    }

    for (int i = 0; i < 1280; i += 32)
    {
        Animation* anim = new Animation(watCalc(std::vector<sf::Vector2u>({sf::Vector2u(11, 2), sf::Vector2u(12, 2), sf::Vector2u(13, 2),
            sf::Vector2u(14, 2), sf::Vector2u(15, 2), sf::Vector2u(16, 2)}),
            std::vector<unsigned int>({6, 6, 6, 6, 6, 6})), _db.getTexture(spikeTileset));

        waterfalls.push_back(new Waterfall(sf::Vector2f(i, 1216.f), sf::Vector2f(16.f, 64.f),
            sf::FloatRect(-16.f, -16.f, 32.f, 32.f), anim, 21, player));
    }

    return waterfalls;
}

Animation* SpikeGenerator::generateRippleAnimation() const
{
    TileCalculator floorCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    std::vector<sf::Vector2u> offsets = { sf::Vector2u(0, 0) };

    return new Animation(floorCalc(offsets, std::vector<unsigned int>( { 0 } )),
        _db.getTexture(spikeInvisible));
}

SpikeRipples* SpikeGenerator::generateRipple(sf::Vector2f position, sf::Vector2f speed) const
{
    return new SpikeRipples(position, sf::Vector2f(16.f, 16.f), speed, generateRippleAnimation());
}

std::vector<std::vector<std::vector<sf::Vector2u>>> SpikeGenerator::generateSpikeNpcOffsetVectors() const
{
    std::vector<std::vector<sf::Vector2u>> idleVec;
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 1) }));
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 0) }));
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 3) }));
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 2) }));

    std::vector<std::vector<sf::Vector2u>> walkVec;
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 1), sf::Vector2u(1, 1), sf::Vector2u(2, 1),
        sf::Vector2u(1, 1) }));
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 0), sf::Vector2u(1, 0), sf::Vector2u(2, 0),
        sf::Vector2u(1, 0) }));
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 3), sf::Vector2u(1, 3), sf::Vector2u(2, 3),
        sf::Vector2u(1, 3) }));
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 2), sf::Vector2u(1, 2), sf::Vector2u(2, 2),
        sf::Vector2u(1, 2) }));

    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ idleVec, walkVec });
}

std::vector<std::vector<unsigned int>> SpikeGenerator::generateSpikeNpcTimeVectors() const
{
    std::vector<unsigned int> idleTimes = { 0 };
    std::vector<unsigned int> walkTimes = { 10, 10, 10, 10 };
    
    return std::vector<std::vector<unsigned int>>({ idleTimes, walkTimes });
}

SpikeNpc* SpikeGenerator::generateSpikeNpc(sf::Vector2f position, unsigned int initialD) const
{
    std::vector<std::vector<Animation*>> animationVector = generateAnimationVector(_db.getTexture(spikeNpc),
        generateSpikeNpcOffsetVectors(), generateSpikeNpcTimeVectors(), sf::Vector2u(24, 32));

    return new SpikeNpc(position, sf::Vector2f(12.f, 29.f), sf::FloatRect(-6.f, -2.f, 12.f, 4.f), animationVector,
        initialD);
}

Decoration* SpikeGenerator::generateBackground(const sf::Vector2f position, const sf::Vector2f speed) const
{
    TileCalculator tileCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(320, 240));

    Animation* anim = new Animation(tileCalc(std::vector<sf::Vector2u>({sf::Vector2u()}), std::vector<unsigned int>({0})),
        _db.getTexture(spikeBg));

    return new Decoration(position, sf::Vector2f(0.f, 0.f), speed, anim);
}

Room* SpikeGenerator::generateSpikeArea(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;

    std::vector<sf::Vector2f> spikePositions = { sf::Vector2f(200.f, 200.f), sf::Vector2f(280.f, 440.f),
        sf::Vector2f(330.f, 400.f), sf::Vector2f(760.f, 560.f), sf::Vector2f(1200.f, 120.f), sf::Vector2f(450.f, 450.f),
        sf::Vector2f(160.f, 350.f), sf::Vector2f(800.f, 350.f), sf::Vector2f(1210.f, 600.f), sf::Vector2f(920.f, 920.f),
        sf::Vector2f(860.f, 870.f), sf::Vector2f(120.f, 700.f), sf::Vector2f(260.f, 690.f), sf::Vector2f(420.f, 240.f),
        sf::Vector2f(1150.f, 740.f), sf::Vector2f(200.f, 960.f), sf::Vector2f(380.f, 970.f), sf::Vector2f(610.f, 940.f) };

    for (unsigned int i = 0; i < spikePositions.size(); i++)
    {
        Direction dir;
        if (((i / 2) * 81) % 2)
            dir = genleft;
        else
            dir = genright;
        collidables.push_back(generateSpike(spikePositions[i], dir));
        floors.push_back(generateSpikeShadow(spikePositions[i], dir));
    }

    std::vector<Wall*> bigSpikeWalls = generateBigSpike(sf::Vector2f(980.f, 880.f));
    collidables.insert(collidables.end(), bigSpikeWalls.begin(), bigSpikeWalls.end());

    floors.push_back(generateBigSpikeShadow(sf::Vector2f(980.f, 880.f)));

    for (unsigned int i = 0; i < 32; i++)
        floors.push_back(generateTile(sf::Vector2f(1188.f + i, 896.f - i),
            sf::FloatRect(0.f, 0.f, 2.f, 2.f), 5, sf::Vector2f(608.f, 520.f), true, player));

    floors.push_back(generateTile(sf::Vector2f(1044.f, 890.f), sf::FloatRect(0.f, 0.f, 16.f, 16.f), 6,
        sf::Vector2f(176.f, 152.f), true, player));
    floors.push_back(generateTile(sf::Vector2f(1060.f, 890.f), sf::FloatRect(0.f, 0.f, 16.f, 16.f), 6,
        sf::Vector2f(176.f, 152.f), true, player));
    floors.push_back(generateCityTile(sf::Vector2f(700.f, 680.f), 12,
        sf::Vector2f(320.f, 208.f), true, player));

    /*for (unsigned int i = 0; i < spikePositions.size(); i++)
    floors.push_back(generateRipple(spikePositions[i] - sf::Vector2f(i * 57.f, i * 23.f),
        sf::Vector2f((i + 1) * cos(i * (360.f / spikePositions.size()) * math::PI / 180.f),
            (i + 1) * sin(i * (360.f / spikePositions.size()) * math::PI / 180.f))));*/

    std::vector<sf::Vector2f> npcPositions = { sf::Vector2f(180.f, 770.f), sf::Vector2f(0.f, 250.f),
        sf::Vector2f(1000.f, 570.f), sf::Vector2f(780.f, 720.f), sf::Vector2f(640.f, 790.f) };

    for (unsigned int i = 0; i < npcPositions.size(); i++)
    {
        SpikeNpc* newSpikeNpc = generateSpikeNpc(npcPositions[i], i % 4);
        entities.push_back(newSpikeNpc);
        interactibles.push_back(newSpikeNpc);
    }

    backgrounds.push_back(generateBackground(sf::Vector2f(200.f, 40.f), sf::Vector2f(-0.1f, 0.07f)));
    backgrounds.push_back(generateBackground(sf::Vector2f(840.f, 680.f), sf::Vector2f(-0.1f, 0.07f)));
    //backgrounds.push_back(generateBackground(sf::Vector2f(1520.f, 1320.f), sf::Vector2f(-0.1f, 0.07f)));
    //backgrounds.push_back(generateBackground(sf::Vector2f(2160.f, 1960.f), sf::Vector2f(-0.1f, 0.07f)));

    std::vector<Waterfall*> wfalls = generateWaterfalls(player);
    floors.insert(floors.end(), wfalls.begin(), wfalls.end());

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, std::vector<GameObject*>(), std::vector<GameObject*>() };

    return new SpikeRoom(player, graph, objects, wfalls);
}
