/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "SpikeRipples.h"
#include "Animation.h"
#include "Room.h"

SpikeRipples::SpikeRipples(sf::Vector2f position, sf::Vector2f origin, sf::Vector2f speed, Animation* animation)
    : GameObject(position, origin), _speed(speed)
{
    _currentAnimation = animation;
    _currentAnimation->setOrigin(_origin);
}

SpikeRipples::~SpikeRipples()
{
    delete _currentAnimation;
}

void SpikeRipples::behave()
{
    _position += _speed;
    if (_startTimer == 0)
    {
        _timer--;
        if (_timer == 0)
        {
            _timer = _maxTime;
            _room->createSpecialEffect(_position, sf::Vector2f(), ripple, floorLayer);
        }
    }
    else
        _startTimer--;
}