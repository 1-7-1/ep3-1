/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "SeekingFloor.h"
#include "Room.h"
#include "Animation.h"
#include <math.h>

SeekingFloor::SeekingFloor(sf::Vector2f origin, Animation* animation,
    sf::Vector2f offset, sf::Vector2f spacing, Player* player)
    : Decoration(sf::Vector2f(), origin, sf::Vector2f(), animation), _positionOffset(offset), _spacing(spacing), _player(player)
{

}

SeekingFloor::~SeekingFloor()
{
    
}

void SeekingFloor::behave()
{
    _position = _room->getViewPosition() + _positionOffset;
    _position.x -= fmod(_position.x, _spacing.x);
    _position.y -= fmod(_position.y, _spacing.y);
    changeAnimation();
}
