/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BALC_GEN_H
#define BALC_GEN_H

#include "GameObjectGenerator.h"

class Room;
class Player;
class RoomGraph;
class Wall;
class Decoration;
class AutoTransition;

class BalconyGenerator: public GameObjectGenerator
{
    private:
        enum BalconyGraphics { balcInvisible, balcony };
        virtual void load();

        std::vector<Animation*> generateBalconyWallAnimations() const;
        std::vector<Wall*> generateBalconyWall(sf::Vector2f position) const;

        Decoration* generateBackground(const sf::Vector2f position, const sf::Vector2f speed) const;

        AutoTransition* generateTile(const sf::Vector2f position, const unsigned int destination,
            const sf::Vector2f transitionVec, const bool absolute, Player* _player) const;

    public:
        BalconyGenerator();
        ~BalconyGenerator();

        Room* generateBalcony(Player* player, RoomGraph* graph);
};

#endif
