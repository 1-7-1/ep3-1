/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "GameObject.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "MathConstants.h"
#include <math.h>

GameObject::GameObject(const sf::Vector2f position, const sf::Vector2f origin): _position(position), _origin(origin)
{

}

GameObject::~GameObject()
{
    
}

void GameObject::setPosition(const sf::Vector2f position)
{
    _position = position;
}

void GameObject::setOrigin(const sf::Vector2f origin)
{
    _origin = origin;
    _currentAnimation->setOrigin(origin);
}

void GameObject::setRoom(Room* room)
{
    _room = room;
}

sf::Vector2f GameObject::getPosition() const
{
    return _position;
}

sf::Vector2f GameObject::getOrigin() const
{
    return _origin;
}

Animation* GameObject::getAnimation() const
{
    return _currentAnimation;
}

sf::Vector2f GameObject::getCornerPosition() const
{
    /*float width = _currentAnimation->getFrames()[0].offset.width;
    float height = _currentAnimation->getFrames()[0].offset.height;

    sf::Vector2f topleft = -_origin;

    float dist = sqrt(pow((topleft.x), 2.f) + pow((topleft.y), 2.f));

    float angle = atan2(-topleft.y, topleft.x);
    angle += _currentAnimation->getRotation() * math::PI / 180.0;

    topleft = sf::Vector2f(dist * cos(angle), dist * sin(angle));*/

    return _position - _origin;
}

sf::Vector2f GameObject::getSize() const
{
    return sf::Vector2f(_currentAnimation->getSize());
}

void GameObject::changeAnimation() const
{
    _currentAnimation->animate();
}
