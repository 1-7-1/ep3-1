/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "HellRoom.h"
#include "Player.h"
#include "Animation.h"
#include <math.h>

HellRoom::HellRoom(Player* player, RoomGraph* graph, RoomObjects objects)
    : Room(player, graph, nullptr, objects, sf::Vector2f(320.f, 240.f), sf::Vector2<bool>(false, false))
{
    
}

HellRoom::~HellRoom()
{
    
}

void HellRoom::tick()
{
    float ratio = pow(_player->getPosition().y, 2.f) / 90000.f + 0.35f;
    _player->setSpeedLength(ratio);
    Room::tick();
    if (_currentState != exiting)
        _player->getAnimation()->setScale(ratio, ratio);
}
