/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "JungleGenerator.h"

std::vector<Wall*> JungleGenerator::generateHubArea() const
{
    std::vector<Wall*> walls;
    walls.push_back(generateWall(sf::Vector2f(2272.f, 2512.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2272.f, 2496.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2272.f, 2480.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2272.f, 2464.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2272.f, 2448.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2288.f, 2448.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2304.f, 2448.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2320.f, 2448.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2320.f, 2432.f), upleft, true));
    walls.push_back(generateWall(sf::Vector2f(2336.f, 2432.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2352.f, 2432.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2352.f, 2416.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2368.f, 2416.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2384.f, 2416.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2400.f, 2416.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2416.f, 2416.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2432.f, 2416.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2448.f, 2416.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2464.f, 2416.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2464.f, 2400.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 2400.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 2400.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 2400.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 2400.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2544.f, 2400.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2560.f, 2400.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 2400.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 2400.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2608.f, 2400.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 2400.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 2400.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 2400.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 2400.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 2400.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 2400.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 2416.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2720.f, 2416.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2720.f, 2432.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2720.f, 2448.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2720.f, 2464.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2720.f, 2480.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2720.f, 2496.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2736.f, 2496.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(2736.f, 2512.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2736.f, 2528.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2752.f, 2528.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2752.f, 2544.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2768.f, 2544.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2768.f, 2560.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2784.f, 2560.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2560.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2816.f, 2560.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2816.f, 2576.f), right, false));

    walls.push_back(generateWall(sf::Vector2f(2688.f, 2336.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 2336.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2720.f, 2336.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2736.f, 2336.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2752.f, 2336.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2768.f, 2336.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2784.f, 2336.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(2784.f, 2320.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2784.f, 2304.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2784.f, 2288.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2784.f, 2272.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2784.f, 2256.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2784.f, 2240.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2240.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2224.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2208.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2192.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2176.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2160.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2144.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2784.f, 2144.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2768.f, 2144.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2752.f, 2144.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2738.f, 2144.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2722.f, 2144.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 2144.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 2144.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 2144.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 2144.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 2144.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 2144.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2608.f, 2144.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 2144.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 2128.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 2112.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 2096.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2608.f, 2096.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 2096.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 2096.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 2096.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 2096.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 2080.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 2064.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 2048.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 2048.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 2032.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 2016.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 2016.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 2016.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 2000.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2608.f, 2000.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 2000.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 2000.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2560.f, 2000.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2560.f, 2016.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2544.f, 2016.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 2016.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 2016.f), upleft, true));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 2032.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 2048.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 2064.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 2064.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 2064.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 2080.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 2096.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 2112.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 2128.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 2144.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2464.f, 2144.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2448.f, 2144.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2432.f, 2144.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2416.f, 2144.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2400.f, 2144.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2384.f, 2144.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2384.f, 2160.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2384.f, 2176.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2384.f, 2192.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2384.f, 2208.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2384.f, 2224.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2384.f, 2240.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2384.f, 2256.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2384.f, 2272.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2384.f, 2288.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2400.f, 2288.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2400.f, 2304.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(2416.f, 2304.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2432.f, 2304.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2448.f, 2304.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2448.f, 2320.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2448.f, 2336.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(2464.f, 2336.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 2336.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 2336.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 2336.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 2336.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2544.f, 2336.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2560.f, 2336.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 2336.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 2336.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2608.f, 2336.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 2336.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 2336.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 2336.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 2336.f), down, false));

    walls.push_back(generateWall(sf::Vector2f(2864.f, 2576.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 2560.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 2544.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 2528.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 2512.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 2496.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 2480.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 2464.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 2448.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2848.f, 2448.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2848.f, 2432.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2848.f, 2416.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2848.f, 2400.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2848.f, 2384.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2848.f, 2368.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2848.f, 2352.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2832.f, 2352.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2832.f, 2336.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2832.f, 2320.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2832.f, 2304.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2832.f, 2288.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2832.f, 2272.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2832.f, 2256.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2848.f, 2256.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2848.f, 2240.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2848.f, 2224.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2848.f, 2208.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2848.f, 2192.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 2192.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 2208.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 2224.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 2240.f), turnupright, false));

    walls.push_back(generateWall(sf::Vector2f(2224.f, 2512.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2224.f, 2496.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2224.f, 2480.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2224.f, 2464.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2224.f, 2448.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 2448.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 2432.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 2416.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 2400.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2192.f, 2400.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2192.f, 2384.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2176.f, 2384.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2176.f, 2368.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(2160.f, 2368.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2160.f, 2352.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2144.f, 2352.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2144.f, 2336.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 2336.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 2320.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 2304.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 2288.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 2272.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2112.f, 2272.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2096.f, 2272.f), up, true));

    walls.push_back(generateWall(sf::Vector2f(2096.f, 2240.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2112.f, 2240.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 2240.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 2224.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 2208.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 2192.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 2176.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 2160.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2112.f, 2160.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2112.f, 2144.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2096.f, 2144.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2080.f, 2144.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2080.f, 2128.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2080.f, 2112.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2080.f, 2096.f), turndownright, false));

    walls.push_back(generateWall(sf::Vector2f(2096.f, 2096.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2112.f, 2096.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 2096.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2144.f, 2096.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2160.f, 2096.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2176.f, 2096.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2192.f, 2096.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 2096.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2224.f, 2096.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2224.f, 2080.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2224.f, 2064.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2240.f, 2064.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2256.f, 2064.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2272.f, 2064.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2288.f, 2064.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2304.f, 2064.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2320.f, 2064.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2336.f, 2064.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2336.f, 2080.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(2352.f, 2080.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2368.f, 2080.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2384.f, 2080.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2400.f, 2080.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2416.f, 2080.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2432.f, 2080.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2432.f, 2064.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2432.f, 2048.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2432.f, 2032.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2432.f, 2016.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2448.f, 2016.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2464.f, 2016.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 2016.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 2000.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 1984.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 1984.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 1968.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 1952.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 1952.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 1952.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2544.f, 1952.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2560.f, 1952.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 1952.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 1952.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2608.f, 1952.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 1952.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 1952.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 1952.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 1952.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 1952.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 1952.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 1968.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 1984.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 2000.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 2016.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 2032.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 2048.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 2064.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 2080.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 2096.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2720.f, 2096.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2736.f, 2096.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2752.f, 2096.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2768.f, 2096.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2784.f, 2096.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2096.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2816.f, 2096.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2816.f, 2080.f), right, false));

    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 4; i++)
            walls.push_back(generateWall(sf::Vector2f(2528.f + i * 32.f, 2032.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 3; j++)
        for (int i = 0; i < 3; i++)
                walls.push_back(generateWall(sf::Vector2f(2496.f + i * 32.f, 2064.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 4; j++)
        for (int i = 0; i < 12; i++)
            walls.push_back(generateWall(sf::Vector2f(2400.f + i * 32.f, 2160.f + j * 32.f), turnupright, true));
    for (int i = 0; i < 10; i++)
        walls.push_back(generateWall(sf::Vector2f(2464.f + i * 32.f, 2304.f), turnupright, true));
    for (int i = 0; i < 7; i++)
        walls.push_back(generateWall(sf::Vector2f(2480.f + i * 32.f, 2416.f), turnupright, true));
    for (int i = 0; i < 11; i++)
        walls.push_back(generateWall(sf::Vector2f(2368.f + i * 32.f, 2448.f), turnupright, true));
    for (int i = 0; i < 6; i++)
        walls.push_back(generateWall(sf::Vector2f(2304.f + i * 32.f, 2480.f), turnupright, true));
    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 3; i++)
            walls.push_back(generateWall(sf::Vector2f(2144.f + j * 32.f, 2412.f + i * 32.f), turnupright, true));
    for (int i = 0; i < 17; i++)
        walls.push_back(generateWall(sf::Vector2f(2096.f, 2288.f + i * 32.f), turnupright, true));
    for (int i = 0; i < 2; i++)
        walls.push_back(generateWall(sf::Vector2f(2096.f, 2176.f + i * 32.f), turnupright, true));
    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 4; i++)
            walls.push_back(generateWall(sf::Vector2f(2096.f + i * 32.f, 2032.f + j * 32.f), turnupright, true));
    for (int i = 0; i < 10; i++)
        walls.push_back(generateWall(sf::Vector2f(2096.f + i * 32.f, 2016.f), turnupright, true));
    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 12; i++)
            walls.push_back(generateWall(sf::Vector2f(2096.f + i * 32.f, 1952.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 4; j++)
        for (int i = 0; i < 3; i++)
            walls.push_back(generateWall(sf::Vector2f(2720.f + i * 32.f, 1952.f + j * 32.f), turnupright, true));
    for (int i = 0; i < 2; i++)
        walls.push_back(generateWall(sf::Vector2f(2736.f + i * 32.f, 2578.f), turnupright, true));
        
    for (int j = 0; j < 5; j++)
        for (int i = 0; i < 6; i++)
            walls.push_back(generateWall(sf::Vector2f(1904.f + i * 32.f, 1936.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 5; i++)
            walls.push_back(generateWall(sf::Vector2f(1920.f + i * 32.f, 2096.f + j * 32.f), turnupright, true));

    std::vector<Wall*> decorations;

    decorations = generateBigTree(sf::Vector2f(2180.f, 2196.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigTree(sf::Vector2f(2194.f, 2172.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigTree(sf::Vector2f(2233.f, 2199.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigTree(sf::Vector2f(2317.f, 2196.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigTree(sf::Vector2f(2345.f, 2171.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigTree(sf::Vector2f(2358.f, 2205.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    decorations = generateSmallTree(sf::Vector2f(2256.f, 2182.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(2272.f, 2230.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(2167.f, 2228.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(2621.f, 2397.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(2749.f, 2381.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(2856.f, 2155.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    decorations = generateVerySmallTree(sf::Vector2f(2189.f, 2267.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2284.f, 2312.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2810.f, 2485.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2812.f, 2549.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    decorations = generateTallTree(sf::Vector2f(2256.f, 2155.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(2193.f, 2321.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(2337.f, 2378.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(2314.f, 2261.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(2825.f, 2371.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateBigRock(sf::Vector2f(2286.f, 2183.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(2241.f, 2339.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(2303.f, 2378.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(2804.f, 2396.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(2765.f, 2490.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(2833.f, 2198.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    decorations = generateSmallRock(sf::Vector2f(2290.f, 2203.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(2332.f, 2399.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(2298.f, 2418.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(2793.f, 2114.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateBigFern(sf::Vector2f(2295.f, 2222.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2360.f, 2271.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2389.f, 2382.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2153.f, 2133.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2363.f, 2128.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2823.f, 2432.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2769.f, 2469.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2841.f, 2253.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateSmallFern(sf::Vector2f(2333.f, 2237.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2277.f, 2101.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2451.f, 2379.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2481.f, 2354.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2454.f, 2076.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2478.f, 2034.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateWeirdPlant(sf::Vector2f(2230.f, 2248.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(2417.f, 2338.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(2553.f, 1973.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(2246.f, 2493.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(2806.f, 2258.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    return walls;
}
