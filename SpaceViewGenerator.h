/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SPACE_VIEW_GEN_H
#define SPACE_VIEW_GEN_H

#include "GameObjectGenerator.h"

class Room;
class Player;
class RoomGraph;
class Decoration;
class AutoTransition;
class Wall;
class Spaceship;

class SpaceViewGenerator: public GameObjectGenerator
{
    private:
        enum Graphics { invisible, bg, lights, ship };
        virtual void load();
        
        const float _roomWidth = 320.f;
        const float _roomHeight = 240.f;

        Decoration* generateBackground() const;
        std::vector<Decoration*> generateLights() const;

        std::vector<Wall*> generateWalls() const;
        AutoTransition* generateTile(Player* player) const;

        Spaceship* generateSpaceship() const;

    public:
        SpaceViewGenerator();
        ~SpaceViewGenerator();

        Room* generateView(Player* player, RoomGraph* graph);
};

#endif
