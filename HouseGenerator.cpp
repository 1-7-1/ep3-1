/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "HouseGenerator.h"
#include "Room.h"
#include "Door.h"
#include "Decoration.h"
#include "TileCalculator.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "Wall.h"
#include "Player.h"

HouseGenerator::HouseGenerator()
{

}

HouseGenerator::~HouseGenerator()
{

}

void HouseGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/tilesets/20.png", sf::Color(0, 0, 0, 255));
    _db.loadTexture("graphics/object/5.png", sf::Color(63, 139, 255, 255));
    _db.loadTexture("graphics/tilesets/11.png", sf::Color(0, 0, 0, 255));

    _db.loadSound("sounds/sfx/5.wav");
    
    _isLoaded = true;
}

Decoration* HouseGenerator::generateFloor() const
{
    TileCalculator bgCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(320, 240));

    Animation* anim = new Animation(bgCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(floor));

    return new Decoration(sf::Vector2f(), sf::Vector2f(), sf::Vector2f(), anim);
}

Wall* HouseGenerator::generateWall(sf::Vector2f position, sf::FloatRect hitbox) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(16, 16));

    Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(invisible));

    return new Wall(position, sf::Vector2f(), anim, hitbox);
}

std::vector<Wall*> HouseGenerator::generateWalls(sf::Vector2f position) const
{
    std::vector<Wall*> walls;

    //bottom/top
    walls.push_back(generateWall(position + sf::Vector2f(64.f, 159.f),
        sf::FloatRect(0.f, 0.f, 192.f, 16.f)));
    walls.push_back(generateWall(position + sf::Vector2f(112.f, 96.f),
        sf::FloatRect(0.f, 0.f, 96.f, 16.f)));

    //left side
    for (int i = 0; i < 48; i ++)
        walls.push_back(generateWall(position + sf::Vector2f(96.f - i, 96.f + i),
        sf::FloatRect(0.f, 0.f, 16.f, 16.f)));

    //right side
    for (int i = 0; i < 48; i ++)
        walls.push_back(generateWall(position + sf::Vector2f(208.f + i, 96.f + i),
        sf::FloatRect(0.f, 0.f, 16.f, 16.f)));

    return walls;
}

std::vector<Wall*> HouseGenerator::generatePipes(sf::Vector2f position) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(), sf::Vector2u(32, 48));

    std::vector<Wall*> pipes;

    //pipe1
    {
        Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(pipe));

        pipes.push_back(new Wall(position + sf::Vector2f(99.f, 136.f), sf::Vector2f(10.f, 42.f),
            anim, sf::FloatRect(-6.f, -10.f, 26.f, 13.f)));
    }

    //pipe2
    {
        Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(1, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(pipe));

        pipes.push_back(new Wall(position + sf::Vector2f(209.f, 121.f), sf::Vector2f(15.f, 24.f),
            anim, sf::FloatRect(-8.f, 1.f, 16.f, 3.f)));
    }

    //pipe3
    {
        Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(2, 0)}),
            std::vector<unsigned int>({0})), _db.getTexture(pipe));

        pipes.push_back(new Wall(position + sf::Vector2f(218.f, 144.f), sf::Vector2f(9.f, 40.f),
            anim, sf::FloatRect(-6.f, -11.f, 24.f, 15.f)));
    }

    //pipe4
    {
        Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 1)}),
            std::vector<unsigned int>({0})), _db.getTexture(pipe));

        pipes.push_back(new Wall(position + sf::Vector2f(142.f, 64.f), sf::Vector2f(9.f, 10.f),
            anim, sf::FloatRect(-2.f, 0.f, 22.f, 2.f)));
    }

    //pipe5
    {
        Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 2)}),
            std::vector<unsigned int>({0})), _db.getTexture(pipe));

        pipes.push_back(new Wall(position + sf::Vector2f(153.f, 62.f), sf::Vector2f(6.f, 8.f),
            anim, sf::FloatRect(-2.f, 0.f, 26.f, 2.f)));
    }

    return pipes;
}

std::vector<std::vector<std::vector<sf::Vector2u>>> HouseGenerator::generateDoorOffsetVectors() const
{
    std::vector<std::vector<sf::Vector2u>> closedVec;
    closedVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 0) }));

    std::vector<std::vector<sf::Vector2u>> openVec;
    openVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 0) }));

    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ closedVec, openVec });
}

std::vector<std::vector<unsigned int>> HouseGenerator::generateDoorTimeVectors() const
{
    std::vector<unsigned int> closedTimes = { 0 };
    std::vector<unsigned int> openTimes = { 0 };
    
    return std::vector<std::vector<unsigned int>>({ closedTimes, openTimes });
}

Door* HouseGenerator::generateDoor(sf::Vector2f position) const
{
    std::vector<std::vector<Animation*>> animVec = generateAnimationVector(_db.getTexture(door),
        generateDoorOffsetVectors(), generateDoorTimeVectors(), sf::Vector2u(16, 32));

    return new Door(position, sf::Vector2f(8.f, 32.f), sf::FloatRect(-8.f, 0.f, 16.f, 4.f), animVec,
        _db.getSound(doorSfx), 17, sf::Vector2f(648.f, 184.f), true, 3);
}

Room* HouseGenerator::generateHouse(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;
    std::vector<GameObject*> foregrounds;

    floors.push_back(generateFloor());
    
    std::vector<Wall*> walls = generateWalls(sf::Vector2f());
    collidables.insert(collidables.end(), walls.begin(), walls.end());
    
    std::vector<Wall*> pipes = generatePipes(sf::Vector2f());
    collidables.insert(collidables.end(), pipes.begin(), pipes.end());

    Door* newDoor = generateDoor(sf::Vector2f(168.f, 160.f));
    collidables.push_back(newDoor);
    interactibles.push_back(newDoor);

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, foregrounds, std::vector<GameObject*>() };

    return new Room(player, graph, nullptr, objects, sf::Vector2f(_roomWidth, _roomHeight),
        sf::Vector2<bool>(false, false));
}
