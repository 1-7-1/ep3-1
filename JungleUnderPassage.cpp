/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "JungleGenerator.h"

std::vector<Wall*> JungleGenerator::generateUnderPassage() const
{
    std::vector<Wall*> walls;
    walls.push_back(generateWall(sf::Vector2f(2816.f, 2592.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2816.f, 2608.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2816.f, 2624.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2816.f, 2640.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2816.f, 2656.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2656.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2672.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2688.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2704.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2720.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2736.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2752.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2768.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2784.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2800.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2816.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2832.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2848.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2864.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(2784.f, 2864.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2768.f, 2864.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2768.f, 2880.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2752.f, 2880.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2736.f, 2880.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2720.f, 2880.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 2880.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 2896.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 2896.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 2912.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 2912.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 2928.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 2944.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 2944.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 2944.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 2944.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 2960.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2608.f, 2960.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 2960.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 2960.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2560.f, 2960.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2544.f, 2960.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 2960.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 2960.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 2960.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 2960.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2464.f, 2960.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2448.f, 2960.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2432.f, 2960.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2432.f, 2976.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(2416.f, 2976.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2400.f, 2976.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2384.f, 2976.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2368.f, 2976.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2352.f, 2976.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2336.f, 2976.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2320.f, 2976.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2304.f, 2976.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2288.f, 2976.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2272.f, 2976.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2256.f, 2976.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2240.f, 2976.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2224.f, 2976.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 2976.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2192.f, 2976.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2176.f, 2976.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2176.f, 2992.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2160.f, 2992.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2144.f, 2992.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 2992.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 2976.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 2960.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2112.f, 2960.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2096.f, 2960.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2080.f, 2960.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2080.f, 2976.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(2064.f, 2976.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2048.f, 2976.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2048.f, 2992.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(2032.f, 2992.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2032.f, 3008.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(2016.f, 3008.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(2016.f, 3024.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2000.f, 3024.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(1984.f, 3024.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(1984.f, 3040.f), downright, true));
    walls.push_back(generateWall(sf::Vector2f(1968.f, 3040.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(1952.f, 3040.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(1936.f, 3040.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(1920.f, 3040.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(1904.f, 3040.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(1888.f, 3040.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(1872.f, 3040.f), down, false));

    walls.push_back(generateWall(sf::Vector2f(2864.f, 2592.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 2608.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 2624.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 2624.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 2640.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2896.f, 2640.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2912.f, 2640.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2928.f, 2640.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2944.f, 2640.f), down, true));

    walls.push_back(generateWall(sf::Vector2f(2944.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2928.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2912.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2896.f, 2688.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 2688.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 2704.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 2720.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 2736.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 2752.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 2768.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 2784.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 2800.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 2816.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 2832.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 2848.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 2864.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 2880.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 2896.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2880.f, 2912.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 2912.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 2928.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2864.f, 2944.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2848.f, 2944.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2848.f, 2960.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2832.f, 2960.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2816.f, 2960.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2800.f, 2960.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2784.f, 2960.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2768.f, 2960.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2752.f, 2960.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2736.f, 2960.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2736.f, 2976.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2736.f, 2992.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2736.f, 3008.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2720.f, 3008.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2720.f, 3024.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2720.f, 3040.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2704.f, 3040.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2688.f, 3040.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2672.f, 3040.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2656.f, 3040.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2640.f, 3040.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2624.f, 3040.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2608.f, 3040.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 3040.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2592.f, 3056.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2576.f, 3056.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2560.f, 3056.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2544.f, 3056.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2528.f, 3056.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2512.f, 3056.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2496.f, 3056.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2480.f, 3056.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2464.f, 3056.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2448.f, 3056.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2432.f, 3056.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2416.f, 3056.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2400.f, 3056.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2384.f, 3056.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2368.f, 3056.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2352.f, 3056.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2336.f, 3056.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2320.f, 3056.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2304.f, 3056.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2288.f, 3056.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2272.f, 3056.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2256.f, 3056.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2256.f, 3040.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2240.f, 3040.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2224.f, 3040.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3040.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2192.f, 3040.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2176.f, 3040.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2160.f, 3040.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2144.f, 3040.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3040.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3056.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3072.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3088.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3104.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3120.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3136.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3152.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2144.f, 3152.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2144.f, 3168.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2160.f, 3168.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2160.f, 3184.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2176.f, 3184.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2176.f, 3200.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(2192.f, 3200.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2192.f, 3216.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2192.f, 3232.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2192.f, 3248.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2192.f, 3264.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3264.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3280.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3296.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3312.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3328.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3344.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3360.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3376.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3392.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3408.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3424.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3440.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3456.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3472.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3488.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3504.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3520.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3536.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3552.f), left, true));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3568.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3584.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3600.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3616.f), left, false));
    walls.push_back(generateWall(sf::Vector2f(2208.f, 3632.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2192.f, 3632.f), upleft, false));
    walls.push_back(generateWall(sf::Vector2f(2192.f, 3648.f), turnupleft, false));
    walls.push_back(generateWall(sf::Vector2f(2176.f, 3648.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2160.f, 3648.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2144.f, 3648.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3648.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2112.f, 3648.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2096.f, 3648.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2080.f, 3648.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2064.f, 3648.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2048.f, 3648.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2032.f, 3648.f), up, true));
    walls.push_back(generateWall(sf::Vector2f(2016.f, 3648.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2000.f, 3648.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(1984.f, 3648.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(1984.f, 3632.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(1984.f, 3616.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(1968.f, 3616.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(1968.f, 3600.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(1952.f, 3600.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(1952.f, 3584.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(1936.f, 3584.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(1936.f, 3568.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(1920.f, 3568.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(1920.f, 3552.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(1904.f, 3552.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(1888.f, 3552.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(1888.f, 3536.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(1888.f, 3520.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(1888.f, 3504.f), turndownright, false));
    walls.push_back(generateWall(sf::Vector2f(1904.f, 3504.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(1920.f, 3504.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(1936.f, 3504.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(1952.f, 3504.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(1968.f, 3504.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(1984.f, 3504.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(1984.f, 3520.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2000.f, 3520.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2016.f, 3520.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2016.f, 3536.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2032.f, 3536.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2032.f, 3552.f), downleft, true));
    walls.push_back(generateWall(sf::Vector2f(2048.f, 3552.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2048.f, 3568.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2064.f, 3568.f), turndownleft, false));
    walls.push_back(generateWall(sf::Vector2f(2064.f, 3584.f), downleft, false));
    walls.push_back(generateWall(sf::Vector2f(2080.f, 3584.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2096.f, 3584.f), down, true));
    walls.push_back(generateWall(sf::Vector2f(2112.f, 3584.f), down, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3584.f), downright, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3568.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3552.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3536.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3520.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3504.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3488.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3472.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3456.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3440.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3424.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3408.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3392.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3376.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3360.f), right, true));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3344.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3328.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3312.f), right, false));
    walls.push_back(generateWall(sf::Vector2f(2128.f, 3296.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2112.f, 3296.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(2096.f, 3296.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2096.f, 3280.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(2080.f, 3280.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2080.f, 3264.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2064.f, 3264.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2064.f, 3248.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2048.f, 3248.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2048.f, 3232.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2032.f, 3232.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2032.f, 3216.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2016.f, 3216.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2016.f, 3200.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(2000.f, 3200.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(2000.f, 3184.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(1984.f, 3184.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(1984.f, 3168.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(1968.f, 3168.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(1968.f, 3152.f), upright, true));
    walls.push_back(generateWall(sf::Vector2f(1952.f, 3152.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(1952.f, 3136.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(1936.f, 3136.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(1936.f, 3120.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(1920.f, 3120.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(1920.f, 3104.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(1904.f, 3104.f), turnupright, false));
    walls.push_back(generateWall(sf::Vector2f(1904.f, 3088.f), upright, false));
    walls.push_back(generateWall(sf::Vector2f(1888.f, 3088.f), up, false));
    walls.push_back(generateWall(sf::Vector2f(1872.f, 3088.f), up, true));

    for (int i = 0; i < 8; i++)
        walls.push_back(generateWall(sf::Vector2f(2768.f, 2608.f + i * 32.f), turnupright, true));
    for (int i = 0; i < 28; i++)
        walls.push_back(generateWall(sf::Vector2f(1872.f + i * 32.f, 2832.f), turnupright, true));
    for (int i = 0; i < 28; i++)
        walls.push_back(generateWall(sf::Vector2f(1872.f + i * 32.f, 2832.f), turnupright, true));
    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 25; i++)
            walls.push_back(generateWall(sf::Vector2f(1872.f + i * 32.f, 2864.f + j * 32.f), turnupright, true));
    for (int i = 0; i < 23; i++)
        walls.push_back(generateWall(sf::Vector2f(1872.f + i * 32.f, 2928.f), turnupright, true));
    for (int i = 0; i < 5; i++)
        walls.push_back(generateWall(sf::Vector2f(1872.f + i * 32.f, 2960.f), turnupright, true));
    for (int i = 0; i < 4; i++)
        walls.push_back(generateWall(sf::Vector2f(1872.f + i * 32.f, 2992.f), turnupright, true));
    
    for (int i = 0; i < 11; i++)
        walls.push_back(generateWall(sf::Vector2f(1872.f, 3136.f + i * 32.f), turnupright, true));
    for (int i = 0; i < 11; i++)
        walls.push_back(generateWall(sf::Vector2f(1904.f, 3136.f + i * 32.f), turnupright, true));
    for (int i = 0; i < 10; i++)
        walls.push_back(generateWall(sf::Vector2f(1936.f, 3168.f + i * 32.f), turnupright, true));
    for (int i = 0; i < 9; i++)
        walls.push_back(generateWall(sf::Vector2f(1968.f, 3200.f + i * 32.f), turnupright, true));
    for (int i = 0; i < 9; i++)
        walls.push_back(generateWall(sf::Vector2f(2000.f, 3232.f + i * 32.f), turnupright, true));
    for (int i = 0; i < 8; i++)
        walls.push_back(generateWall(sf::Vector2f(2032.f, 3264.f + i * 32.f), turnupright, true));
    for (int i = 0; i < 8; i++)
        walls.push_back(generateWall(sf::Vector2f(2064.f, 3296.f + i * 32.f), turnupright, true));
    for (int i = 0; i < 8; i++)
        walls.push_back(generateWall(sf::Vector2f(2096.f, 3328.f + i * 32.f), turnupright, true));
        
    for (int j = 0; j < 10; j++)
        for (int i = 0; i < 2; i++)
            walls.push_back(generateWall(sf::Vector2f(2896.f + i * 32.f, 2704.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 4; i++)
            walls.push_back(generateWall(sf::Vector2f(2752.f + i * 32.f, 2976.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 2; j++)
        for (int i = 0; i < 23; i++)
            walls.push_back(generateWall(sf::Vector2f(2144.f + i * 32.f, 3072.f + j * 32.f), turnupright, true));
    for (int i = 0; i < 22; i++)
        walls.push_back(generateWall(sf::Vector2f(2176.f + i * 32.f, 3104.f), turnupright, true));
    for (int j = 0; j < 13; j++)
        for (int i = 0; i < 20; i++)
            walls.push_back(generateWall(sf::Vector2f(2224.f + i * 32.f, 3136.f + j * 32.f), turnupright, true));
    for (int j = 0; j < 9; j++)
        for (int i = 0; i < 2; i++)
            walls.push_back(generateWall(sf::Vector2f(2896.f + i * 32.f, 2352.f + j * 32.f), turnupright, true));
    for (int i = 0; i < 3; i++)
        walls.push_back(generateWall(sf::Vector2f(2864.f, 2352.f + i * 32.f), turnupright, true));

    std::vector<Wall*> decorations;

    decorations = generateBigFern(sf::Vector2f(2848.f, 2626.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2838.f, 2608.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2857.f, 2601.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2707.f, 2959.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2287.f, 3008.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2302.f, 3029.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2332.f, 3017.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2366.f, 3024.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2448.f, 3004.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2621.f, 2998.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2159.f, 3343.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2193.f, 3365.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2172.f, 3383.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2200.f, 3400.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2186.f, 3436.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2153.f, 3448.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2165.f, 3463.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2190.f, 3472.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2621.f, 2998.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2177.f, 3499.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2167.f, 3532.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2189.f, 3556.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2163.f, 3582.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2179.f, 3602.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2186.f, 3629.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2120.f, 3633.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2165.f, 3231.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2117.f, 3199.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2048.f, 3179.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigFern(sf::Vector2f(2113.f, 3145.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateSmallFern(sf::Vector2f(2838.f, 2641.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2857.f, 2637.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2507.f, 3013.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2490.f, 2999.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2541.f, 2998.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2654.f, 3014.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2685.f, 3000.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2068.f, 3612.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2096.f, 3642.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2145.f, 3610.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2193.f, 3594.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2158.f, 3562.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2196.f, 3519.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2186.f, 3487.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2156.f, 3477.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2190.f, 3451.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2155.f, 3403.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2179.f, 3416.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2155.f, 3364.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2190.f, 3325.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2177.f, 3320.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2158.f, 3323.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2178.f, 3314.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2185.f, 3302.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2200.f, 3290.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2197.f, 3309.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2051.f, 3108.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2098.f, 3100.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(2117.f, 3112.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(1959.f, 3091.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(1893.f, 3070.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallFern(sf::Vector2f(1919.f, 3083.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateWeirdPlant(sf::Vector2f(2856.f, 2661.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(2734.f, 2938.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(2467.f, 3033.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(2398.f, 3005.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(2098.f, 3170.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(2037.f, 3126.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateWeirdPlant(sf::Vector2f(1961.f, 3070.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateBigTree(sf::Vector2f(2813.f, 2899.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigTree(sf::Vector2f(2069.f, 3144.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigTree(sf::Vector2f(2004.f, 3102.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigTree(sf::Vector2f(2139.f, 3268.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateTallTree(sf::Vector2f(2863.f, 2689.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(2225.f, 3012.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(2112.f, 2992.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateTallTree(sf::Vector2f(2070.f, 3194.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateSmallTree(sf::Vector2f(2845.f, 2735.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(2738.f, 2957.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(2523.f, 2994.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(2068.f, 3028.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(2009.f, 3160.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(2038.f, 3633.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallTree(sf::Vector2f(2011.f, 3564.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    decorations = generateVerySmallTree(sf::Vector2f(2830.f, 2800.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2857.f, 2933.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2721.f, 2995.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2654.f, 2980.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2344.f, 3038.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2315.f, 3006.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2303.f, 3028.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2130.f, 3220.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(2091.f, 3243.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(1961.f, 3541.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateVerySmallTree(sf::Vector2f(1910.f, 3549.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateBigRock(sf::Vector2f(2864.f, 2848.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(2710.f, 2939.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(2054.f, 3083.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateBigRock(sf::Vector2f(2019.f, 3612.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    
    decorations = generateSmallRock(sf::Vector2f(2857.f, 2761.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(2869.f, 2811.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(2591.f, 3020.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(2152.f, 3014.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(2090.f, 3071.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(2113.f, 3062.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());
    decorations = generateSmallRock(sf::Vector2f(2041.f, 3054.f));
    walls.insert(walls.end(), decorations.begin(), decorations.end());

    return walls;
}
