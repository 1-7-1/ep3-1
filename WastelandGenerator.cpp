/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "WastelandGenerator.h"
#include "Animation.h"
#include "AnimationFrames.h"
#include "TileCalculator.h"
#include "Room.h"
#include "Decoration.h"
#include "ScreenEffect.h"
#include "Wall.h"
#include "WastelandEntrance.h"
#include "Scorpion.h"
#include "Player.h"

WastelandGenerator::WastelandGenerator()
{

}

WastelandGenerator::~WastelandGenerator()
{

}

void WastelandGenerator::load()
{
    _db.loadTexture("graphics/floor/1.png", sf::Color(255, 0, 255, 255));
    _db.loadTexture("graphics/tilesets/23.png", sf::Color(255, 255, 255, 255));
    _db.loadTexture("graphics/char/12.png", sf::Color(255, 255, 255, 255));
    _isLoaded = true;
}

std::vector<ScreenEffect*> WastelandGenerator::generateSandEffect() const
{
    TileCalculator sandCalc = TileCalculator(sf::Vector2u(64, 64), sf::Vector2u(64, 16));
    std::vector<ScreenEffect*> sand;

    sf::Vector2f origin = sf::Vector2f(32.f, 8.f);
    sf::Vector2f speed = sf::Vector2f(-8.f, 1.f);

    for (int i = 0; i < 384; i += 64)
        for (int j = 0; j < 256; j += 16)
        {
            Animation* anim = new Animation(sandCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));

            sand.push_back(new ScreenEffect(sf::Vector2f(i, j), origin, speed, anim));
        }

    return sand;
}

std::vector<Decoration*> WastelandGenerator::generateFloor() const
{
    TileCalculator floorCalc = TileCalculator(sf::Vector2u(256, 176), sf::Vector2u(64, 64));
    std::vector<Decoration*> floors;

    for (int i = 0; i < _roomWidth / 64; i++)
        for (int j = 0; j < _roomHeight / 64; j++)
        {
            Animation* anim;
            
            anim = new Animation(floorCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                std::vector<unsigned int>({0})), _db.getTexture(tileset));

            floors.push_back(new Decoration(sf::Vector2f(i * 64.f, j * 64.f), sf::Vector2f(),
                sf::Vector2f(), anim));
        }

    return floors;
}

WastelandEntrance* WastelandGenerator::generateEntrance(sf::Vector2f position, Player* player) const
{
    TileCalculator wallCalc = TileCalculator(sf::Vector2u(0, 0), sf::Vector2u(64, 48));
    std::vector<Animation*> anims;
    anims.push_back(new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
        std::vector<unsigned int>({0})), _db.getTexture(tileset)));
    anims.push_back(new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 1)}),
        std::vector<unsigned int>({0})), _db.getTexture(tileset)));
    return new WastelandEntrance(position, sf::Vector2f(32.f, 42.f), sf::FloatRect(-16.f, -22.f, 40.f, 28.f), anims,
        14, sf::Vector2f(2112.f, 768.f), true, sf::FloatRect(-3.f, -30.f, 22.f, 37.f), player);
}

Wall* WastelandGenerator::generateWall(sf::Vector2f position, WallType type) const
{
    switch (type)
    {
        case small:
            {
                TileCalculator wallCalc = TileCalculator(sf::Vector2u(272, 0), sf::Vector2u(48, 32));
                Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                return new Wall(position, sf::Vector2f(24.f, 30.f), anim, sf::FloatRect(-24.f, -2.f, 47.f, 4.f));
            }
            break;

        case arm:
            {
                TileCalculator wallCalc = TileCalculator(sf::Vector2u(64, 0), sf::Vector2u(64, 64));
                Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                return new Wall(position, sf::Vector2f(32.f, 62.f), anim, sf::FloatRect(-31.f, -2.f, 57.f, 4.f));
            }
            break;

        case plus:
            {
                TileCalculator wallCalc = TileCalculator(sf::Vector2u(256, 48), sf::Vector2u(64, 48));
                Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                return new Wall(position, sf::Vector2f(32.f, 46.f), anim, sf::FloatRect(-28.f, -2.f, 56.f, 4.f));
            }
            break;

        case leg:
            {
                TileCalculator wallCalc = TileCalculator(sf::Vector2u(240, 96), sf::Vector2u(80, 64));
                Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                return new Wall(position, sf::Vector2f(40.f, 62.f), anim, sf::FloatRect(-39.f, -2.f, 76.f, 4.f));
            }
            break;

        case circle:
            {
                TileCalculator wallCalc = TileCalculator(sf::Vector2u(128, 0), sf::Vector2u(128, 96));
                Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                return new Wall(position, sf::Vector2f(64.f, 93.f), anim, sf::FloatRect(-60.f, -3.f, 119.f, 6.f));
            }
            break;

        case big:
            {
                TileCalculator wallCalc = TileCalculator(sf::Vector2u(0, 96), sf::Vector2u(240, 144));
                Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                return new Wall(position, sf::Vector2f(120.f, 138.f), anim, sf::FloatRect(-120.f, -6.f, 206.f, 12.f));
            }
            break;
        
        default:
            {
                TileCalculator wallCalc = TileCalculator(sf::Vector2u(272, 0), sf::Vector2u(48, 32));
                Animation* anim = new Animation(wallCalc(std::vector<sf::Vector2u>({sf::Vector2u(0, 0)}),
                    std::vector<unsigned int>({0})), _db.getTexture(tileset));
                return new Wall(position, sf::Vector2f(24.f, 30.f), anim, sf::FloatRect(-24.f, -2.f, 47.f, 4.f));
            }
            break;
    }
}

std::vector<std::vector<std::vector<sf::Vector2u>>> WastelandGenerator::generateScorpionOffsetVectors() const
{
    std::vector<std::vector<sf::Vector2u>> idleVec;
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 1) }));
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 0) }));
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 3) }));
    idleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 2) }));

    std::vector<std::vector<sf::Vector2u>> walkVec;
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 1), sf::Vector2u(1, 1), sf::Vector2u(2, 1),
        sf::Vector2u(1, 1) }));
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 0), sf::Vector2u(1, 0), sf::Vector2u(2, 0),
        sf::Vector2u(1, 0) }));
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 3), sf::Vector2u(1, 3), sf::Vector2u(2, 3),
        sf::Vector2u(1, 3) }));
    walkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 2), sf::Vector2u(1, 2), sf::Vector2u(2, 2),
        sf::Vector2u(1, 2) }));

    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ idleVec, walkVec });
}

std::vector<std::vector<unsigned int>> WastelandGenerator::generateScorpionTimeVectors() const
{
    std::vector<unsigned int> idleTimes = { 0 };
    std::vector<unsigned int> walkTimes = { 6, 6, 6, 6 };
    
    return std::vector<std::vector<unsigned int>>({ idleTimes, walkTimes });
}

Scorpion* WastelandGenerator::generateScorpion(sf::Vector2f position) const
{
    std::vector<std::vector<Animation*>> animationVector = generateAnimationVector(_db.getTexture(scorpion),
    generateScorpionOffsetVectors(), generateScorpionTimeVectors(), sf::Vector2u(16, 16));

    return new Scorpion(position, sf::Vector2f(8.f, 12.f), sf::FloatRect(-6.f, -6.f, 12.f, 7.f), animationVector);
}

Room* WastelandGenerator::generateWasteland(Player* player, RoomGraph* graph)
{
    if (!_isLoaded)
        load();
    std::vector<InteractibleObject*> interactibles;
    std::vector<CollidableObject*> collidables;
    std::vector<Entity*> entities;
    std::vector<GameObject*> floors;
    std::vector<GameObject*> backgrounds;
    std::vector<GameObject*> foregrounds;

    std::vector<Decoration*> wlFloor = generateFloor();
    floors.insert(floors.end(), wlFloor.begin(), wlFloor.end());
    std::vector<ScreenEffect*> sand = generateSandEffect();
    foregrounds.insert(foregrounds.end(), sand.begin(), sand.end());

    //group1
    collidables.push_back(generateEntrance(sf::Vector2f(960.f, 932.f), player));
    {
        std::vector<Wall*> walls = { generateWall(sf::Vector2f(1080.f, 1200.f), small),
            generateWall(sf::Vector2f(1030.f, 910.f), small) };
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }

    //group2
    {
        std::vector<Wall*> walls = { generateWall(sf::Vector2f(1000.f, 1630.f), arm),
            generateWall(sf::Vector2f(1440.f, 1720.f), leg), generateWall(sf::Vector2f(1510.f, 1700.f), small),
            generateWall(sf::Vector2f(1750.f, 1820.f), small) };
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }

    //group3
    {
        std::vector<Wall*> walls = { generateWall(sf::Vector2f(170.f, 980.f), plus),
            generateWall(sf::Vector2f(320.f, 1440.f), circle), generateWall(sf::Vector2f(170.f, 1710.f), arm),
            generateWall(sf::Vector2f(1900.f, 590.f), plus) };
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }

    //group4
    {
        std::vector<Wall*> walls = { generateWall(sf::Vector2f(90.f, 210.f), plus),
            generateWall(sf::Vector2f(650.f, 490.f), plus), generateWall(sf::Vector2f(340.f, 710.f), arm),
            generateWall(sf::Vector2f(410.f, 750.f), circle), generateWall(sf::Vector2f(690.f, 740.f), arm),
            generateWall(sf::Vector2f(350.f, 930.f), arm), generateWall(sf::Vector2f(470.f, 1010.f), arm) };
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }

    //group5
    {
        std::vector<Wall*> walls = { generateWall(sf::Vector2f(1300.f, 750.f), plus),
            generateWall(sf::Vector2f(1590.f, 900.f), small), generateWall(sf::Vector2f(1410.f, 980.f), small),
            generateWall(sf::Vector2f(1540.f, 1000.f), small), generateWall(sf::Vector2f(1650.f, 1010.f), small) };
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }

    //group6
    {
        std::vector<Wall*> walls = { generateWall(sf::Vector2f(1310.f, 530.f), big),
            generateWall(sf::Vector2f(1640.f, 320.f), arm) };
        collidables.insert(collidables.end(), walls.begin(), walls.end());
    }

    Scorpion* scorp = generateScorpion(sf::Vector2f(620.f, 800.f));
    entities.push_back(scorp);
    interactibles.push_back(scorp);

    entities.push_back(player);

    RoomObjects objects { interactibles, collidables, entities, floors, backgrounds, foregrounds, std::vector<GameObject*>() };

    return new Room(player, graph, nullptr, objects, sf::Vector2f(_roomWidth, _roomHeight),
        sf::Vector2<bool>(true, true));
}
