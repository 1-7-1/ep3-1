*English version below.*

# Ep3
«Fangame» de Yume Nikki, libre et «open-source». programmé par votre humble serviteur, [171](https://gitlab.com/1-7-1). Disponible en français et en anglais.

Ce dépôt Git contient le code source d'Ep3, y compris tous les hacks et raccourcis dégueulasses qui m'ont aidé à livrer la marchandise à temps, mais qui rende le programme presque impossible à soutenir dans le long terme. Je ne vous reccomande pas de lire le code, mais si vous le faites, attention à vos yeux!

Je n'ai mis que peu d'effort afin de faciliter le processus de compilation de ce programme avec mon Makefile «inspiré par une réponse sur Stack Overflow», mais je suis capable de le compiler et de l'exécuter sur mon ordinateur Linux avec les paquets SFML les plus récents (en date du 2021/03/08). Les gens sur Windows risquent d'avoir plus de problèmes. Si quelqu'un veut faire de ce programme un paquet pour une distribution Linux, vous pouvez probablement vous en tirez en le laissant comme ça, mais ça serait bien si vous pouviez améliorer le Makefile un peu, surtout si des choses brisent. Je vais peut-être (probablement pas du tout) mettre ce dépôt à jours dans le futur, mais n'y comptez pas trop.

Ce dépôt est conçu spécifiquement pour les versions 1 et 1.0.1 . C'est parce qu'en travaillant sur mon dépôt Git originel, je me suis mis à expérimenter avec toutes sortes de trucs, de telle sorte que ce qui s'y trouve ne ressemble pas beaucoup au Ep3 auquel vous êtes habitués. Par contre, cet ancien dépôt contient du code de qualité bien supérieure à ce que l'on trouve ici, avec moins de bogues et un meilleur moteur. Donc, si jamais je décide de publier une nouvelle version, elle sera probablement basée sur cet ancien dépot plutôt que celui-ci. Ne vous inquiétez pas, ce logiciel sera aussi libre et «open-source»!

## Dépendances
[SMFL-2.5.1](https://www.sfml-dev.org/).

Sur Linux, you devrez installer un paquet qui aura probablement un nom du genre «libSFML-dev» ou «SFML-devel». Si votre gestionnaire de paquet ne l'a pas ou si vous être sous Windows, vous devrez télécharger et installer/relier (dans le sens de «linking») SFML vous-mêmes à l'aide du lien ci-dessus.

Les seuls autres dépendances sont celles de SFML elle-même, qui devraient être installées par votre gestionnaire de paquets sur Linux. Sinon, vous trouverez la liste des dépendances sur le site de SFML. Si vous vivez dans le futur et que les serveurs de SFML ont été frappés par un météore, ne laissant de leur site web qu'un cratère fumant, alors les options -l dans le Makefile devraient vous donner une bonne idée des dépendances dont vous avez besoin.

## Compilation
Exécutez 'make'. Enlevez les commentaires dans Makefile pour compiler pour Windows. Si vous êtes sous Windows, vous aurez probablement à indiquez où se trouve votre installation de SFML avec une option -L à gcc. J'utilisait MinGW afin de compiler ce programme, alors utilisez ça si vous n'êtes pas capable autrement.

Le répertoire «compatibility» contient des fichiers qui sont supposés remplacer les fichiers correspondant à d'autres endroits. C'est pour changer la langue et le système d'exploitation de l'exécutable publié.

Si je me souviens bien, la seule différence entre les versions 1 et 1.0.1, c'est que le fichier «lisez-moi» et certaines images ont été changées, alors si vous voulez recréer la version 1, changez ces fichiers pour ceux qui ont été publiés sur itch.io .

## Bogues
Il y a plusieurs petits bogues encore dans le jeu; le plus important est une petite (moins de 1KB si je me souviens bien) fuite de mémoire qui à quelque chose à voir avec les premières salles du jeu... Je sais pas trop; si vous voulez vous en débarasser, sortez votre valgrind ou votre drmemory et faites vous plaisir!

Autres bogues:
- Lorsque l'on interagit avec le scorpion, on obtient l'effet scorpion même s'il est en train de mourir.
- Plusieurs problèmes avec l'ordre dans lequel les images sont dessinées. La plus flagrante est probablement celle du gros pic (big spike), là ou l'on peut y grimper. Une autre erreur, moins évidente, se trouve dans la salle où l'on obtient l'effest masque. Si vous vous coller à la rampe de gauche avec l'effet scorpion équippé, vous pouvez placer votre queue de telle sorte qu'elle «passe au travers» de la rampe.
- Le mode plein écran ne fonctionne pas très bien avec mon gestionnaire de fenêtre. En fait, il ne marche pas très bien tout court.
- Je ne me souviens plus si j'ai corrigé ce bogue avant la publication du jeu, mais parfois on a des problèmes graphiques causés par le manque de précision des nombres à virgules flottantes (ou quelque chose dans le genre) après la mise à l'échelle des images. On peut corriger ce bogue facilement en changeant le moteur de rendu pour faire le rendu à la résolution interne (320x240) et aggrandir ensuite. (non, je n'ai aucune excuse pour cette erreur grossière)
- Dans le labyrinthe, on peut facilement faire en sorte que les chasseurs (chasers) se rencontre à la première intersection et se bloquent mutuellement le chemin. Si c'est fait correctement, on dirait qu'ils s'embrassent! Saviez-vous que les chasseurs peuvent apparaître dans un état neutre et se fâcher contre vous comme dans Yume Nikki? Vous pouvez ramener cette fonctionnalité assez facilement si vous jouez un peu avec le code qui sert à générer les niveaux.
- Probablement plusieurs autres...

## Contributions
Il est peu probable que je revienne travailler de manière substancielle sur ce dépôt. Par contre, si vous vous y intéressez, contactez moi et je vous laisserez publier vos changements s'ils sont bons.

# Ep3
Free and open-source Yume Nikki fangame made by yours truly, [171](https://gitlab.com/1-7-1). Available in French and English.

This repository is here in order to make the source code of Ep3 available. This is Ep3 with all it's ugly hacks and other nonsense that would have made it a pain to maintain in the long term, but that allowed me to finish the game in time for the deadline. Get ready for some really bizarre and infuriating garbage if you actually go and read through it.

Very little care has been put into making the game easy to build using my "heavily inspired by a Stack Overflow post" Makefile, but it compile on my Linux system with the latest (as of this writing, 2021/03/08) SFML packages. Windows users will probably have more problems. If anyone wants to package this for a Linux distribution, you can probably get away with leaving it as-is, but you might have to change the Makefile a bit, or change the code itself in case of major breakage. I may or may not (more likely "may not") update this repository in the future, so don't count on it!

This is specifically the repository for versions 1 and 1.0.1 . This is because my original git repository went in a weird direction and ended up being more of a way for me to experiment with various things and the end result has little in common with Ep3 as it was distributed. However, that repository contains highly improved code that has much less bugs and a highly improved engine. Thus, if I ever end up making a new Ep3 version, it'll be based off of my private repository rather than this one. Don't worry, if that ever happens, it'll be free and open-source as well.

## Dependencies
[SMFL-2.5.1](https://www.sfml-dev.org/).

On Linux, you'll need a package that's probably called "libSFML-dev" or "SFML-devel" or something like that. If your package manager doesn't have it or if you're on Windows, you'll have to download it using the link above and install/link it yourself.

The only other dependencies are SFML's dependencies, which should be pulled automatically on Linux by your package manager. Otherwise, you'll find the list on SFML's website, linked above, so make sure you have them. If you live in the distant future when SFML's website has been purged from the face of the Earth, the -l flags in the Makefile should point you towards the right direction.

## How to build
Run 'make'. Uncomment stuff in Makefile for a Windows build. For Windows, you may have to specify where your SFML installation is using the -L gcc linking flag. The program was originally compiled using MinGW on Windows, so try using that if all else fails.

The files in the compatibility directory are meant to be swapped out with the corresponding files outside of it in order to change the language and operating system of the binary release.

If I remember correctly, the only difference between versions 1 and 1.0.1 is that the readme and some graphics files are different. So, to recreate version 1 instead of 1.0.1, swap out the readme and graphics files with the ones in the version 1 release.

## Known bugs
There's a bunch of small glitches in the game, but the most significant one is a small (if memory serves it was less than 1KB) memory leak that I think has to do with some of the first rooms in the game? I don't know. If you want to get rid of it, liberal use of valgrind or drmemory is advised.

Other bugs include:
- If you interact with the scorpion while it's dying, it'll still give you the scorpion effect.
- Various problems with graphics layering. Notably, the big spike around the point where you climb up it. One less obvious one is the left side of the banister in the mask effect room when using the scorpion effect and positioning the tail in a certain way.
- The full screen mode doesn't play very well with my window manager. Actually, it just doesn't play well in general!
- I don't remember if I got rid of this one before release, but I think sometimes the graphics glitch out due to floating point rounding errors (or something similar) when scaling them. This can be fixed by changing the rendering so that it renders the scene at internal resolution (320x240) and scaling that to the window size instead of scaling each individual sprite. (no, I don't have an excuse for this)
- It's possible to get the chasers to collide with each other in the maze in such a way that they both block each other's path at the very first intersection. If you do it right it looks like they're kissing! Fun fact: the chasers can actually start neutral and get angry when you attack them, just like the ones in Yume Nikki. This behavior is fully programmed in and you can restore it if you mess around in the level generation code.
- Probably many more...

## Contributing
I am very unlikely to work on this repository again in any significant manner, but if you want to, hit me up and I'll let you push your changes if they're good.
