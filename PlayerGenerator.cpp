/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "PlayerGenerator.h"
#include "Player.h"

PlayerGenerator::PlayerGenerator(): GameObjectGenerator()
{
    
}

PlayerGenerator::~PlayerGenerator()
{

}

void PlayerGenerator::load()
{
    _db.loadTexture("graphics/char/1.png", sf::Color(0, 255, 255, 255));
    _isLoaded = true;
}

std::vector<std::vector<std::vector<sf::Vector2u>>> PlayerGenerator::generatePlayerOffsetVectors() const
{
    std::vector<std::vector<sf::Vector2u>> noEffIdleVec;
    noEffIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 1) }));
    noEffIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 0) }));
    noEffIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 3) }));
    noEffIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 2) }));

    std::vector<std::vector<sf::Vector2u>> noEffWalkVec;
    noEffWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 1), sf::Vector2u(1, 1), sf::Vector2u(2, 1),
        sf::Vector2u(1, 1) }));
    noEffWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 0), sf::Vector2u(1, 0), sf::Vector2u(2, 0),
        sf::Vector2u(1, 0) }));
    noEffWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 3), sf::Vector2u(1, 3), sf::Vector2u(2, 3),
        sf::Vector2u(1, 3) }));
    noEffWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 2), sf::Vector2u(1, 2), sf::Vector2u(2, 2),
        sf::Vector2u(1, 2) }));

    std::vector<std::vector<sf::Vector2u>> noEffActionVec;
    noEffActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 1) }));
    noEffActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 0) }));
    noEffActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 3) }));
    noEffActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 2) }));

    std::vector<std::vector<sf::Vector2u>> swordIdleVec;
    swordIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 5) }));
    swordIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 4) }));
    swordIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 7) }));
    swordIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 6) }));

    std::vector<std::vector<sf::Vector2u>> swordWalkVec;
    swordWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 5), sf::Vector2u(1, 5), sf::Vector2u(2, 5),
        sf::Vector2u(1, 5) }));
    swordWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 4), sf::Vector2u(1, 4), sf::Vector2u(2, 4),
        sf::Vector2u(1, 4) }));
    swordWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 7), sf::Vector2u(1, 7), sf::Vector2u(2, 7),
        sf::Vector2u(1, 7) }));
    swordWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 6), sf::Vector2u(1, 6), sf::Vector2u(2, 6),
        sf::Vector2u(1, 6) }));

    std::vector<std::vector<sf::Vector2u>> swordActionVec;
    swordActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 9), sf::Vector2u(1, 9), sf::Vector2u(2, 9) }));
    swordActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 8), sf::Vector2u(1, 8), sf::Vector2u(2, 8) }));
    swordActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 11), sf::Vector2u(1, 11), sf::Vector2u(2, 11) }));
    swordActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 10), sf::Vector2u(1, 10), sf::Vector2u(2, 10) }));

    std::vector<std::vector<sf::Vector2u>> scorpIdleVec;
    scorpIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 13) }));
    scorpIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 12) }));
    scorpIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 15) }));
    scorpIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 14) }));

    std::vector<std::vector<sf::Vector2u>> scorpWalkVec;
    scorpWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 13), sf::Vector2u(1, 13), sf::Vector2u(2, 13),
        sf::Vector2u(1, 13) }));
    scorpWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 12), sf::Vector2u(1, 12), sf::Vector2u(2, 12),
        sf::Vector2u(1, 12) }));
    scorpWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 15), sf::Vector2u(1, 15), sf::Vector2u(2, 15),
        sf::Vector2u(1, 15) }));
    scorpWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 14), sf::Vector2u(1, 14), sf::Vector2u(2, 14),
        sf::Vector2u(1, 14) }));

    std::vector<std::vector<sf::Vector2u>> scorpActionVec;
    scorpActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 13) }));
    scorpActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 12) }));
    scorpActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 15) }));
    scorpActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 14) }));

    std::vector<std::vector<sf::Vector2u>> liquidIdleVec;
    liquidIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 17), sf::Vector2u(3, 17) }));
    liquidIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 16), sf::Vector2u(3, 16) }));
    liquidIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 19), sf::Vector2u(3, 19) }));
    liquidIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 18), sf::Vector2u(3, 18) }));

    std::vector<std::vector<sf::Vector2u>> liquidWalkVec;
    liquidWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 17), sf::Vector2u(1, 17), sf::Vector2u(2, 17),
        sf::Vector2u(1, 17) }));
    liquidWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 16), sf::Vector2u(1, 16), sf::Vector2u(2, 16),
        sf::Vector2u(1, 16) }));
    liquidWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 19), sf::Vector2u(1, 19), sf::Vector2u(2, 19),
        sf::Vector2u(1, 19) }));
    liquidWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 18), sf::Vector2u(1, 18), sf::Vector2u(2, 18),
        sf::Vector2u(1, 18) }));

    std::vector<std::vector<sf::Vector2u>> liquidActionVec;
    liquidActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 17) }));
    liquidActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 16) }));
    liquidActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 19) }));
    liquidActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 18) }));

    std::vector<std::vector<sf::Vector2u>> maskIdleVec;
    maskIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 21) }));
    maskIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 20) }));
    maskIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 23) }));
    maskIdleVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 22) }));

    std::vector<std::vector<sf::Vector2u>> maskWalkVec;
    maskWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 21), sf::Vector2u(1, 21), sf::Vector2u(2, 21),
        sf::Vector2u(1, 21) }));
    maskWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 20), sf::Vector2u(1, 20), sf::Vector2u(2, 20),
        sf::Vector2u(1, 20) }));
    maskWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 23), sf::Vector2u(1, 23), sf::Vector2u(2, 23),
        sf::Vector2u(1, 23) }));
    maskWalkVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(0, 22), sf::Vector2u(1, 22), sf::Vector2u(2, 22),
        sf::Vector2u(1, 22) }));

    std::vector<std::vector<sf::Vector2u>> maskActionVec;
    maskActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 21) }));
    maskActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 20) }));
    maskActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 23) }));
    maskActionVec.push_back(std::vector<sf::Vector2u> ({ sf::Vector2u(1, 22) }));

    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ noEffIdleVec, noEffWalkVec, noEffActionVec,
        swordIdleVec, swordWalkVec, swordActionVec, scorpIdleVec, scorpWalkVec, scorpActionVec, liquidIdleVec,
        liquidWalkVec, liquidActionVec, maskIdleVec, maskWalkVec, maskActionVec });
}

std::vector<std::vector<unsigned int>> PlayerGenerator::generatePlayerTimeVectors() const
{
    std::vector<unsigned int> noEffIdleTimes = { 0 };
    std::vector<unsigned int> noEffWalkTimes = { 10, 10, 10, 10 };
    std::vector<unsigned int> noEffActionTimes = { 0 };
    std::vector<unsigned int> swordIdleTimes = { 0 };
    std::vector<unsigned int> swordWalkTimes = { 10, 10, 10, 10 };
    std::vector<unsigned int> swordActionTimes = { 10, 5, 5 };
    std::vector<unsigned int> scorpIdleTimes = { 0 };
    std::vector<unsigned int> scorpWalkTimes = { 10, 10, 10, 10 };
    std::vector<unsigned int> scorpActionTimes = { 0 };
    std::vector<unsigned int> liquidIdleTimes = { 15, 15 };
    std::vector<unsigned int> liquidWalkTimes = { 10, 10, 10, 10 };
    std::vector<unsigned int> liquidActionTimes = { 0 };
    std::vector<unsigned int> maskIdleTimes = { 0 };
    std::vector<unsigned int> maskWalkTimes = { 10, 10, 10, 10 };
    std::vector<unsigned int> maskActionTimes = { 0 };
    
    return std::vector<std::vector<unsigned int>>({ noEffIdleTimes, noEffWalkTimes, noEffActionTimes,
        swordIdleTimes, swordWalkTimes, swordActionTimes, scorpIdleTimes, scorpWalkTimes, scorpActionTimes,
        liquidIdleTimes, liquidWalkTimes, liquidActionTimes, maskIdleTimes, maskWalkTimes, maskActionTimes });
}

std::vector<std::vector<std::vector<sf::Vector2u>>> PlayerGenerator::generateExtraOffsetVectors() const
{
    std::vector<std::vector<sf::Vector2u>> bedVec;
    std::vector<sf::Vector2u> bedOffsets;
    for (unsigned int i = 0; i < 8; i++)
        bedOffsets.push_back(sf::Vector2u(3, i));
    bedVec.push_back(bedOffsets);

    std::vector<std::vector<sf::Vector2u>> sinkVec;
    std::vector<sf::Vector2u> sinkOffsets;
    for (unsigned int i = 0; i < 8; i++)
        sinkOffsets.push_back(sf::Vector2u(3, i + 8));
    sinkOffsets.push_back(sf::Vector2u(4, 0));
    sinkVec.push_back(sinkOffsets);

    std::vector<std::vector<sf::Vector2u>> swimVec;
    std::vector<sf::Vector2u> swimOffsets;
    for (unsigned int i = 0; i < 4; i++)
        swimOffsets.push_back(sf::Vector2u(4 + i, 1));
    swimVec.push_back(swimOffsets);

    std::vector<std::vector<sf::Vector2u>> dripVec;
    std::vector<sf::Vector2u> dripOffsets;
    dripOffsets.push_back(sf::Vector2u(4, 6));
    dripOffsets.push_back(sf::Vector2u(5, 6));
    dripOffsets.push_back(sf::Vector2u(6, 6));
    dripOffsets.push_back(sf::Vector2u(7, 6));
    dripOffsets.push_back(sf::Vector2u(4, 7));
    dripOffsets.push_back(sf::Vector2u(5, 7));
    dripVec.push_back(dripOffsets);

    std::vector<std::vector<sf::Vector2u>> dropVec;
    std::vector<sf::Vector2u> dropOffsets;
    dropOffsets.push_back(sf::Vector2u(6, 7));
    dropOffsets.push_back(sf::Vector2u(7, 7));
    dropOffsets.push_back(sf::Vector2u(4, 8));
    dropVec.push_back(dropOffsets);

    std::vector<std::vector<sf::Vector2u>> splashVec;
    std::vector<sf::Vector2u> splashOffsets;
    splashOffsets.push_back(sf::Vector2u(5, 8));
    splashOffsets.push_back(sf::Vector2u(6, 8));
    splashOffsets.push_back(sf::Vector2u(7, 8));
    splashOffsets.push_back(sf::Vector2u(3, 14));
    splashOffsets.push_back(sf::Vector2u(3, 13));
    splashOffsets.push_back(sf::Vector2u(3, 12));
    splashOffsets.push_back(sf::Vector2u(3, 11));
    splashOffsets.push_back(sf::Vector2u(3, 10));
    splashOffsets.push_back(sf::Vector2u(3, 9));
    splashOffsets.push_back(sf::Vector2u(3, 8));
    splashVec.push_back(splashOffsets);

    std::vector<std::vector<sf::Vector2u>> wakeupVec;
    std::vector<sf::Vector2u> wakeupOffsets;
    wakeupOffsets.push_back(sf::Vector2u(3, 20));
    wakeupOffsets.push_back(sf::Vector2u(3, 21));
    wakeupOffsets.push_back(sf::Vector2u(3, 22));
    wakeupOffsets.push_back(sf::Vector2u(3, 23));
    wakeupVec.push_back(wakeupOffsets);

    std::vector<std::vector<sf::Vector2u>> sitVec;
    std::vector<sf::Vector2u> sitOffsets;
    sitOffsets.push_back(sf::Vector2u(4, 9));
    sitVec.push_back(sitOffsets);

    std::vector<std::vector<sf::Vector2u>> slideVec;
    std::vector<sf::Vector2u> slideOffsets;
    slideOffsets.push_back(sf::Vector2u(4, 9));
    slideOffsets.push_back(sf::Vector2u(5, 9));
    slideOffsets.push_back(sf::Vector2u(6, 9));
    slideOffsets.push_back(sf::Vector2u(7, 9));
    slideOffsets.push_back(sf::Vector2u(4, 10));
    slideOffsets.push_back(sf::Vector2u(5, 9));
    slideOffsets.push_back(sf::Vector2u(6, 9));
    slideOffsets.push_back(sf::Vector2u(7, 9));
    slideOffsets.push_back(sf::Vector2u(4, 10));
    slideOffsets.push_back(sf::Vector2u(5, 10));
    slideOffsets.push_back(sf::Vector2u(6, 10));
    slideOffsets.push_back(sf::Vector2u(7, 10));
    slideOffsets.push_back(sf::Vector2u(4, 11));
    slideOffsets.push_back(sf::Vector2u(5, 11));
    slideVec.push_back(slideOffsets);

    std::vector<std::vector<sf::Vector2u>> getOffVec;
    std::vector<sf::Vector2u> getOffOffsets;
    getOffOffsets.push_back(sf::Vector2u(6, 11));
    getOffOffsets.push_back(sf::Vector2u(7, 11));
    getOffVec.push_back(getOffOffsets);

    std::vector<std::vector<sf::Vector2u>> getOnVec;
    std::vector<sf::Vector2u> getOnOffsets;
    getOnOffsets.push_back(sf::Vector2u(4, 12));
    getOnOffsets.push_back(sf::Vector2u(5, 12));
    getOnOffsets.push_back(sf::Vector2u(6, 12));
    getOnOffsets.push_back(sf::Vector2u(7, 12));
    getOnVec.push_back(getOnOffsets);

    std::vector<std::vector<sf::Vector2u>> invisibleVec;
    std::vector<sf::Vector2u> invisibleOffsets;
    invisibleOffsets.push_back(sf::Vector2u(4, 0));
    invisibleVec.push_back(invisibleOffsets);

    return std::vector<std::vector<std::vector<sf::Vector2u>>>({ bedVec, sinkVec, swimVec, dripVec, dropVec, splashVec,
        wakeupVec, sitVec, slideVec, getOffVec, getOnVec, invisibleVec });
}

std::vector<std::vector<unsigned int>> PlayerGenerator::generateExtraTimeVectors() const
{
    std::vector<unsigned int> bedTimes = { 30, 6, 6, 60, 6, 6, 60, 6 };
    std::vector<unsigned int> sinkTimes = { 10, 5, 5, 5, 5, 5, 5, 5, 30 };
    std::vector<unsigned int> swimTimes = { 12, 12, 12, 12 };
    std::vector<unsigned int> dripTimes = { 12, 9, 6, 6, 6, 6 };
    std::vector<unsigned int> dropTimes = { 10, 10, 10 };
    std::vector<unsigned int> splashTimes = { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };
    std::vector<unsigned int> wakeupTimes = { 6, 6, 60, 40 };
    std::vector<unsigned int> sitTimes = { 0 };
    std::vector<unsigned int> slideTimes = { 48, 9, 9, 9, 36, 9, 9, 9, 36, 9, 9, 9, 24, 30 };
    std::vector<unsigned int> getOffTimes = { 8, 8 };
    std::vector<unsigned int> getOnTimes = { 40, 8, 8, 8 };
    std::vector<unsigned int> invisibleTimes = { 0 };
    
    return std::vector<std::vector<unsigned int>>({ bedTimes, sinkTimes, swimTimes, dripTimes, dropTimes, splashTimes,
        wakeupTimes, sitTimes, slideTimes, getOffTimes, getOnTimes, invisibleTimes });
}

Player* PlayerGenerator::generatePlayer(const sf::Vector2f position)
{
    if (!_isLoaded)
        load();
    std::vector<std::vector<Animation*>> animationVector = generateAnimationVector(_db.getTexture(0),
    generatePlayerOffsetVectors(), generatePlayerTimeVectors(), sf::Vector2u(24, 32));

    std::vector<std::vector<Animation*>> extraVector = generateAnimationVector(_db.getTexture(0),
    generateExtraOffsetVectors(), generateExtraTimeVectors(), sf::Vector2u(24, 32));

    std::vector<Animation*> extraAnims;

    for (std::vector<Animation*> animVec: extraVector)
        extraAnims.push_back(animVec[0]);

    return new Player(position, sf::Vector2f(12.f, 29.f), animationVector, extraAnims,
        sf::FloatRect(-6.f, -2.f, 12.f, 4.f));
}
