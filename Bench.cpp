/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Bench.h"
#include "Animation.h"
#include "Player.h"

Bench::Bench(sf::Vector2f position, sf::Vector2f origin, sf::FloatRect hitbox, Animation* animation,
    unsigned int length, unsigned int placeSize, unsigned int placeOffset)
    : CollidableObject(position, origin, hitbox), InteractibleObject(position, origin, hitbox),
    _placeOffset(placeOffset), _placeSize(placeSize)
{
    for (unsigned int i = 0; i < length; i++)
        _slots.push_back(nullptr);

    _currentAnimation = animation;
    _currentAnimation->setOrigin(_origin);
}

Bench::~Bench()
{
    delete _currentAnimation;
}

int Bench::occupy(GameObject* obj)
{
    unsigned int requested = (obj->getPosition().x - _placeOffset - getCornerPosition().x) / _placeSize;
    if (requested < _slots.size())
        if (!_slots[requested])
        {
            _slots[requested] = obj;
            return requested;
        }

    return -1;
}

void Bench::leave(GameObject* obj)
{
    for (unsigned int i = 0; i < _slots.size(); i++)
        if (_slots[i] == obj)
            _slots[i] = nullptr;
}

void Bench::interact(Player* player)
{
    if (player->getState() != action)
    {
        int place = occupy(player);
        if (place != -1)
        {
            player->sit(this);
            player->setPosition(sf::Vector2f(getCornerPosition().x + _placeOffset + place * _placeSize + player->getOrigin().x,
                _position.y + 12.f));
        }
    }
}

void Bench::behave()
{
    changeAnimation();
}
