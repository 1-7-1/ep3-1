/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DESO_CITY_GEN_H
#define DESO_CITY_GEN_H

#include "GameObjectGenerator.h"

class Room;
class Player;
class RoomGraph;
class Decoration;
class Wall;
class Faker;

class DesolateCityGenerator: public GameObjectGenerator
{
    private:
        enum Graphics { invisible, tileset, faker };
        enum Sounds { scream };
        virtual void load();

        const float _roomWidth = 2560.f;
        const float _roomHeight = 1920.f;

        std::vector<Wall*> generateBuilding1(sf::Vector2f position) const;
        std::vector<Wall*> generateBuilding2(sf::Vector2f position) const;
        std::vector<Wall*> generateBuilding3(sf::Vector2f position) const;
        std::vector<Wall*> generateBuilding(sf::Vector2f position, unsigned int type) const;

        std::vector<Wall*> generatePost(sf::Vector2f position) const;
        std::vector<Wall*> generateBarrier(sf::Vector2f position, unsigned int length) const;

        std::vector<Decoration*> generateFloor() const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateFakerOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateFakerTimeVectors() const;

        Faker* generateFaker(sf::Vector2f position, Player* player, RoomGraph* graph);

    public:
        DesolateCityGenerator();
        ~DesolateCityGenerator();

        Room* generateDesolateCity(Player* player, RoomGraph* graph);
};

#endif
