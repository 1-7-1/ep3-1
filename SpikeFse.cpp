/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "SpikeFse.h"
#include "Decoration.h"

SpikeFse::SpikeFse(std::vector<Decoration*> elems, Animation* anim)
    : FullScreenEvent(elems, anim), _relativePosition(sf::Vector2f(0.f, -240.f))
{
    
}

SpikeFse::~SpikeFse()
{

}

void SpikeFse::behave()
{
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        if (_relativePosition.y < 0.f)
        {
            _relativePosition.y += 0.25f;
            _position.y += 0.25f;
            for (Decoration* dec: _elements)
                dec->setPosition(dec->getPosition() + sf::Vector2f(0.f, 0.25f));
        }
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        if (_relativePosition.y >= -240.f)
        {
            _relativePosition.y -= 0.25f;
            _position.y -= 0.25f;
            for (Decoration* dec: _elements)
                dec->setPosition(dec->getPosition() + sf::Vector2f(0.f, -0.25f));
        }
    }
    FullScreenEvent::behave();
}

void SpikeFse::activate()
{
    _position += _relativePosition;
    _elements[0]->setPosition(sf::Vector2f(-160.f, 0.f) + _position);
    _elements[1]->setPosition(sf::Vector2f(160.f, 0.f) + _position);
}
