/*
	Copyright © 2021  171

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAZE_GEN_H
#define MAZE_GEN_H

#include "GameObjectGenerator.h"

class Wall;
class Room;
class Player;
class RoomGraph;
class Decoration;
class MazeBackground;
class Door;
class Chaser;

class MazeGenerator: public GameObjectGenerator
{
    private:
        enum Graphics { invisible, bg, tileset, chaser };
        enum FloorDirection { up, down, vmiddle, left, right, hmiddle, upleft, upright, downleft, downright, neutral };
        virtual void load();

        const float _roomWidth = 2560.f;
        const float _roomHeight = 2560.f;
        
        std::vector<MazeBackground*> generateBackground(Player* player) const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateDoorOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateDoorTimeVectors() const;

        Door* generateDoor(sf::Vector2f position, unsigned int destination,
        sf::Vector2f transitionVec, bool absolute) const;

        std::vector<std::vector<std::vector<sf::Vector2u>>> generateChaserOffsetVectors() const;
        std::vector<std::vector<unsigned int>> generateChaserTimeVectors() const;

        Chaser* generateChaser(sf::Vector2f position, Player* player) const;

        Decoration* generateFloor(sf::Vector2f position, FloorDirection dir) const;
        std::vector<Decoration*> generateGlow(sf::Vector2f position, FloorDirection dir, int length) const;

        std::vector<Wall*> generateWalls(sf::Vector2f position, FloorDirection dir, int length) const;

    public:
        MazeGenerator();
        ~MazeGenerator();

        Room* generateMaze(Player* player, RoomGraph* graph);
};

#endif
